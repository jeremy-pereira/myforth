#!/bin/sh

jazzy \
  --clean \
  --author "Jeremy Pereira" \
  --author_url https://sincereflattery.blog \
  --github_url https://bitbucket.org/jeremy-pereira/myforth \
  --module MyForth \
  --swift-build-tool spm \
  --build-tool-arguments -Xswiftc,-swift-version,-Xswiftc,5 \
  --min-acl internal
