// swift-tools-version:5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "MyForth",
    platforms: [
		.macOS(.v13),
    ],
    products: [
        // Products define the executables and libraries produced by a package, and make them visible to other packages.
        .library(
            name: "MyForth",
            targets: ["MyForth"]),
		.executable(name: "jforth", targets: ["jforth"]),
    ],
    dependencies: [
		.package(url: "https://jeremy-pereira@bitbucket.org/jeremy-pereira/Toolbox.git", from: "21.3.3"),
		.package(url: "https://github.com/apple/swift-argument-parser", from: "1.2.3"),
		.package(url: "https://github.com/apple/swift-system", from: "1.2.1"),
		.package(url: "https://jeremy-pereira@bitbucket.org/jeremy-pereira/linenoise-swift-utf8.git", from: "1.1.0"),
		.package(url: "https://jeremy-pereira@bitbucket.org/jeremy-pereira/cltest.git", from: "0.2.3"),
   ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
		//.target(name: "CForth"),
       .target(
            name: "MyForth",
            dependencies: ["Toolbox", "CForth", .product(name: "SystemPackage", package: "swift-system")],
			exclude: ["CForthWrapper.swift",
					  //"Documentation.docc"
					 ]),
        .executableTarget(
            name: "jforth",
			dependencies: ["MyForth",
						   .product(name: "LineNoise", package: "linenoise-swift-utf8"),
						   .product(name: "CLKit", package: "CLTest"),
						   .product(name: "Toolbox", package: "Toolbox"),
						   .product(name: "ArgumentParser", package: "swift-argument-parser")]),
		.testTarget(
            name: "MyForthTests",
            dependencies: ["MyForth", "Toolbox"]),
//		.testTarget(
//			name: "CForthTests",
//			dependencies: ["MyForth", "Toolbox"]),
    ]
)
