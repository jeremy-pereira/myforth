//
//  StackTests.swift
//  
//
//  Created by Jeremy Pereira on 14/07/2020.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
@testable import MyForth

class StackTests: XCTestCase
{

    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testSingleItem()
    {
		var stack = Forth.Stack<Int>(name: "test")
		stack.push(1)
        stack.push(2)
        XCTAssert(stack.count == 2, "Wrong stack count")
        XCTAssert(stack.top == 2, "Invalid peek")
        XCTAssert(stack.pop() == 2, "Pop failed")
        XCTAssert(stack.pop() == 1, "Seconf pop failed")
		XCTAssert(stack.isEmpty, "Stack should be empty")
        XCTAssert(stack.pop() == nil, "Managed to pop empty stack")
    }

    func testThrowingPop()
    {
		var stack = Forth.Stack<Int>(name: "test")
        stack.push(1)
        do
        {
            let x = try stack.throwingPop()
            XCTAssert(x == 1)
        }
        catch
        {
            XCTFail("\(error)")
        }
        do
        {
            _ = try stack.throwingPop()
            XCTFail("Throwing pop on an empty stack should throw")
        }
        catch
        {
        }
    }

	func testStackFrame()
	{
		var stack = Forth.Stack<Int>(name: "test")
		XCTAssert(stack.frameOffsetOfTop == 0)
		stack.push(1)
		XCTAssert(stack.frameOffsetOfTop == 1)
		stack.createFrame()
		XCTAssert(stack.frameOffsetOfTop == 0)
		stack.push(2)
		XCTAssert(stack.frameOffsetOfTop == 1)
		stack.push(3)
		do
		{
			XCTAssert(try stack.peek(frameOffset: -1) == 1)
			XCTAssert(try stack.peek(frameOffset: 1) == 2)
			XCTAssert(try stack.peek(frameOffset: 2) == 3)
			_ = stack.pop()
			_ = stack.pop()
			try stack.popFrame()
			XCTAssert(try stack.peek(frameOffset: 1) == 1)
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

    static var allTests = [
        ("testSingleItem", testSingleItem),
		("testThrowingPop", testThrowingPop),
    ]

}
