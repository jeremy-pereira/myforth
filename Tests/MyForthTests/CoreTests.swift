//
//  CoreTests.swift
//  
//
//  Created by Jeremy Pereira on 16/06/2020.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
// Some tests incorporate code from the Forth 2012 test suite by Gerry Jackson
//
// https://github.com/gerryjackson/forth2012-test-suite
//

import XCTest
import Toolbox
import MyForth

private let log = Logger.getLogger("MyForthTests.CoreTests")

/// Swift wrapped core tests
final class CoreTests: SuiteTest
{
	/// Basic assumptions
	func testBasicAssumptions()
	{
		t("", expected: "")              	// START WITH CLEAN SLATE
		// TEST IF ANY BITS ARE SET; ANSWER IN BASE 1
		t(": BITSSET? IF 0 0 ELSE 0 THEN ;", expected: "")
		t("0 BITSSET?", expected: "0")     // ZERO IS ALL BITS CLEAR
		t("1 BITSSET?", expected: "0 0")   // OTHER NUMBER HAVE AT LEAST ONE BIT
		t("-1 BITSSET?", expected: "0 0")
	}
//
//	\ ------------------------------------------------------------------------
//	TESTING BOOLEANS: INVERT AND OR XOR
//
	func testBooleans()
	{
		t("0 0 AND", expected: "0")
		t("0 1 AND", expected: "0")
		t("1 0 AND", expected: "0")
		t("1 1 AND", expected: "1")
		//
		t("0 INVERT 1 AND", expected: "1")
		t("1 INVERT 1 AND", expected: "0")
		//
		//	0    CONSTANT 0S
		//	0 INVERT CONSTANT 1S
		//
		t("0S INVERT", expected: "1S")
		t("1S INVERT", expected: "0S")
		//
		t("0S 0S AND", expected: "0S")
		t("0S 1S AND", expected: "0S")
		t("1S 0S AND", expected: "0S")
		t("1S 1S AND", expected: "1S")
		//
		t("0S 0S OR", expected: "0S")
		t("0S 1S OR", expected: "1S")
		t("1S 0S OR", expected: "1S")
		t("1S 1S OR", expected: "1S")
		//
		t("0S 0S XOR", expected: "0S")
		t("0S 1S XOR", expected: "1S")
		t("1S 0S XOR", expected: "1S")
		t("1S 1S XOR", expected: "0S")
	}
//
//	\ ------------------------------------------------------------------------
//	TESTING 2* 2/ LSHIFT RSHIFT
//
//	( WE TRUST 1S, INVERT, AND BITSSET?; WE WILL CONFIRM RSHIFT LATER )
//	1S 1 RSHIFT INVERT CONSTANT MSB

	func testLShiftRShift()
	{
		let predefined = """
		: BITSSET? IF 0 0 ELSE 0 THEN ;
		"""
		predefine(predefined)
		t("MSB BITSSET?", expected: "0 0")
		//
		t("0S 2*", expected: "0S")
		t("1 2*", expected: "2")
		t("4000 2*", expected: "8000")
		t("1S 2* 1 XOR", expected: "1S")
		t("MSB 2*", expected: "0S")
		//
		t("0S 2/", expected: "0S")
		t("1 2/", expected: "0")
		t("4000 2/", expected: "2000")
		t("1S 2/", expected: "1S")				// MSB PROPOGATED
		t("1S 1 XOR 2/", expected: "1S")
		t("MSB 2/ MSB AND", expected: "MSB")
		//
		t("1 0 LSHIFT", expected: "1")
		t("1 1 LSHIFT", expected: "2")
		t("1 2 LSHIFT", expected: "4")
		t("1 F LSHIFT", expected: "8000")		// BIGGEST GUARANTEED SHIFT
		t("1S 1 LSHIFT 1 XOR", expected: "1S")
		t("MSB 1 LSHIFT", expected: "0")
		//
		t("1 0 RSHIFT", expected: "1")
		t("1 1 RSHIFT", expected: "0")
		t("2 1 RSHIFT", expected: "1")
		t("4 2 RSHIFT", expected: "1")
		t("8000 F RSHIFT", expected: "1")		// BIGGEST
		t("MSB 1 RSHIFT MSB AND", expected: "0")// RSHIFT ZERO FILLS MSBS
		t("MSB 1 RSHIFT 2*", expected: "MSB")
	}
//
//	\ ------------------------------------------------------------------------
//	TESTING COMPARISONS: 0= = 0< < > U< MIN MAX

	func testComparisons()
	{
		let predefined = """
		0 INVERT         			CONSTANT MAX-UINT
		0 INVERT 1 RSHIFT      		CONSTANT MAX-INT
		0 INVERT 1 RSHIFT INVERT   	CONSTANT MIN-INT
		0 INVERT 1 RSHIFT      		CONSTANT MID-UINT
		0 INVERT 1 RSHIFT INVERT   	CONSTANT MID-UINT+1
		0S CONSTANT 				<FALSE>
		1S CONSTANT 				<TRUE>
		"""
		predefine(predefined)

		t("0 0=", expected: "<TRUE>")
		t("1 0=", expected: "<FALSE>")
		t("2 0=", expected: "<FALSE>")
		t("-1 0=", expected: "<FALSE>")
		t("MAX-UINT 0=", expected: "<FALSE>")
		t("MIN-INT 0=", expected: "<FALSE>")
		t("MAX-INT 0=", expected: "<FALSE>")
		//
		t("0 0 =", expected: "<TRUE>")
		t("1 1 =", expected: "<TRUE>")
		t("-1 -1 =", expected: "<TRUE>")
		t("1 0 =", expected: "<FALSE>")
		t("-1 0 =", expected: "<FALSE>")
		t("0 1 =", expected: "<FALSE>")
		t("0 -1 =", expected: "<FALSE>")
		//
		t("0 0<", expected: "<FALSE>")
		t("-1 0<", expected: "<TRUE>")
		t("MIN-INT 0<", expected: "<TRUE>")
		t("1 0<", expected: "<FALSE>")
		t("MAX-INT 0<", expected: "<FALSE>")
		//
		t("0 1 <", expected: "<TRUE>")
		t("1 2 <", expected: "<TRUE>")
		t("-1 0 <", expected: "<TRUE>")
		t("-1 1 <", expected: "<TRUE>")
		t("MIN-INT 0 <", expected: "<TRUE>")
		t("MIN-INT MAX-INT <", expected: "<TRUE>")
		t("0 MAX-INT <", expected: "<TRUE>")
		t("0 0 <", expected: "<FALSE>")
		t("1 1 <", expected: "<FALSE>")
		t("1 0 <", expected: "<FALSE>")
		t("2 1 <", expected: "<FALSE>")
		t("0 -1 <", expected: "<FALSE>")
		t("1 -1 <", expected: "<FALSE>")
		t("0 MIN-INT <", expected: "<FALSE>")
		t("MAX-INT MIN-INT <", expected: "<FALSE>")
		t("MAX-INT 0 <", expected: "<FALSE>")
		//
		t("0 1 >", expected: "<FALSE>")
		t("1 2 >", expected: "<FALSE>")
		t("-1 0 >", expected: "<FALSE>")
		t("-1 1 >", expected: "<FALSE>")
		t("MIN-INT 0 >", expected: "<FALSE>")
		t("MIN-INT MAX-INT >", expected: "<FALSE>")
		t("0 MAX-INT >", expected: "<FALSE>")
		t("0 0 >", expected: "<FALSE>")
		t("1 1 >", expected: "<FALSE>")
		t("1 0 >", expected: "<TRUE>")
		t("2 1 >", expected: "<TRUE>")
		t("0 -1 >", expected: "<TRUE>")
		t("1 -1 >", expected: "<TRUE>")
		t("0 MIN-INT >", expected: "<TRUE>")
		t("MAX-INT MIN-INT >", expected: "<TRUE>")
		t("MAX-INT 0 >", expected: "<TRUE>")
		//
		t("0 1 U<", expected: "<TRUE>")
		t("1 2 U<", expected: "<TRUE>")
		t("0 MID-UINT U<", expected: "<TRUE>")
		t("0 MAX-UINT U<", expected: "<TRUE>")
		t("MID-UINT MAX-UINT U<", expected: "<TRUE>")
		t("0 0 U<", expected: "<FALSE>")
		t("1 1 U<", expected: "<FALSE>")
		t("1 0 U<", expected: "<FALSE>")
		t("2 1 U<", expected: "<FALSE>")
		t("MID-UINT 0 U<", expected: "<FALSE>")
		t("MAX-UINT 0 U<", expected: "<FALSE>")
		t("MAX-UINT MID-UINT U<", expected: "<FALSE>")
		//
		t("0 1 MIN", expected: "0")
		t("1 2 MIN", expected: "1")
		t("-1 0 MIN", expected: "-1")
		t("-1 1 MIN", expected: "-1")
		t("MIN-INT 0 MIN", expected: "MIN-INT")
		t("MIN-INT MAX-INT MIN", expected: "MIN-INT")
		t("0 MAX-INT MIN", expected: "0")
		t("0 0 MIN", expected: "0")
		t("1 1 MIN", expected: "1")
		t("1 0 MIN", expected: "0")
		t("2 1 MIN", expected: "1")
		t("0 -1 MIN", expected: "-1")
		t("1 -1 MIN", expected: "-1")
		t("0 MIN-INT MIN", expected: "MIN-INT")
		t("MAX-INT MIN-INT MIN", expected: "MIN-INT")
		t("MAX-INT 0 MIN", expected: "0")
		//
		t("0 1 MAX", expected: "1")
		t("1 2 MAX", expected: "2")
		t("-1 0 MAX", expected: "0")
		t("-1 1 MAX", expected: "1")
		t("MIN-INT 0 MAX", expected: "0")
		t("MIN-INT MAX-INT MAX", expected: "MAX-INT")
		t("0 MAX-INT MAX", expected: "MAX-INT")
		t("0 0 MAX", expected: "0")
		t("1 1 MAX", expected: "1")
		t("1 0 MAX", expected: "1")
		t("2 1 MAX", expected: "2")
		t("0 -1 MAX", expected: "0")
		t("1 -1 MAX", expected: "1")
		t("0 MIN-INT MAX", expected: "0")
		t("MAX-INT MIN-INT MAX", expected: "MAX-INT")
		t("MAX-INT 0 MAX", expected: "MAX-INT")
//
	}
	/// Testing stack ops
	///
	/// `2DROP 2DUP 2OVER 2SWAP ?DUP DEPTH DROP DUP OVER ROT SWAP`
	func testStackOps()
	{
		t("1 2 2DROP", expected: "")
		t("1 2 2DUP", expected: "1 2 1 2")
		t("1 2 3 4 2OVER", expected: "1 2 3 4 1 2")
		t("1 2 3 4 2SWAP", expected: "3 4 1 2")
		t("0 ?DUP", expected: "0")
		t("1 ?DUP", expected: "1 1")
		t("-1 ?DUP", expected: "-1 -1")
		t("DEPTH", expected: "0")
		t("0 DEPTH", expected: "0 1")
		t("0 1 DEPTH", expected: "0 1 2")
		t("1 2 drop", expected: "1")
		t("0 DROP", expected: "")
		t("1 DUP", expected: "1 1")
		t("1 2 OVER", expected: "1 2 1")
		t("1 2 3 ROT", expected: "2 3 1")
		t("1 2 SWAP", expected: "2 1")
	}

	/// Return stack operations
	func testReturnStack()
	{
		t(": GR1 >R R> ;", expected: "")
		t(": GR2 >R R@ R> DROP ;", expected: "")
		t("123 GR1", expected: "123")
		t("123 GR2", expected: "123")
		t("1S GR1", expected: "1S")
	}

	///	TESTING ADD/SUBTRACT
	///
	///	 `+ - 1+ 1- ABS NEGATE`
	func testAddSubtract()
	{
		predefine("""
		0 INVERT 1 RSHIFT INVERT   	CONSTANT MIN-INT
		0 INVERT 1 RSHIFT      		CONSTANT MID-UINT
		0 INVERT 1 RSHIFT INVERT   	CONSTANT MID-UINT+1
		""")
		t("0 5 +", expected: "5")
		t("5 0 +", expected: "5")
		t("0 -5 +", expected: "-5")
		t("-5 0 +", expected: "-5")
		t("1 2 +", expected: "3")
		t("1 -2 +", expected: "-1")
		t("-1 2 +", expected: "1")
		t("-1 -2 +", expected: "-3")
		t("-1 1 +", expected: "0")
		t("MID-UINT 1 +", expected: "MID-UINT+1")
		//
		t("0 5 -", expected: "-5")
		t("5 0 -", expected: "5")
		t("0 -5 -", expected: "5")
		t("-5 0 -", expected: "-5")
		t("1 2 -", expected: "-1")
		t("1 -2 -", expected: "3")
		t("-1 2 -", expected: "-3")
		t("-1 -2 -", expected: "1")
		t("0 1 -", expected: "-1")
		t("MID-UINT+1 1 -", expected: "MID-UINT")
		//
		t("0 1+", expected: "1")
		t("-1 1+", expected: "0")
		t("1 1+", expected: "2")
		t("MID-UINT 1+", expected: "MID-UINT+1")
		//
		t("2 1-", expected: "1")
		t("1 1-", expected: "0")
		t("0 1-", expected: "-1")
		t("MID-UINT+1 1-", expected: "MID-UINT")
		//
		t("0 NEGATE", expected: "0")
		t("1 NEGATE", expected: "-1")
		t("-1 NEGATE", expected: "1")
		t("2 NEGATE", expected: "-2")
		t("-2 NEGATE", expected: "2")
		//
		t("0 ABS", expected: "0")
		t("1 ABS", expected: "1")
		t("-1 ABS", expected: "1")
		t("MIN-INT ABS", expected: "MID-UINT+1")
	}

	///	TESTING MULTIPLY
	///
	///	`S>D * M* UM*`
	func testMultiply()
	{
		predefine("""
		0 INVERT 1 RSHIFT INVERT   	CONSTANT MIN-INT
		0 INVERT 1 RSHIFT      		CONSTANT MAX-INT
		0 INVERT 1 RSHIFT INVERT   	CONSTANT MID-UINT+1
		0 INVERT         			CONSTANT MAX-UINT
		""")

		t("0 S>D", expected: "0 0")
		t("1 S>D", expected: "1 0")
		t("2 S>D", expected: "2 0")
		t("-1 S>D", expected: "-1 -1")
		t("-2 S>D", expected: "-2 -1")
		t("MIN-INT S>D", expected: "MIN-INT -1")
		t("MAX-INT S>D", expected: "MAX-INT 0")
		//
		t("0 0 M*", expected: "0 S>D")
		t("0 1 M*", expected: "0 S>D")
		t("1 0 M*", expected: "0 S>D")
		t("1 2 M*", expected: "2 S>D")
		t("2 1 M*", expected: "2 S>D")
		t("3 3 M*", expected: "9 S>D")
		t("-3 3 M*", expected: "-9 S>D")
		t("3 -3 M*", expected: "-9 S>D")
		t("-3 -3 M*", expected: "9 S>D")
		t("0 MIN-INT M*", expected: "0 S>D")
		t("1 MIN-INT M*", expected: "MIN-INT S>D")
		t("2 MIN-INT M*", expected: "0 1S")
		t("0 MAX-INT M*", expected: "0 S>D")
		t("1 MAX-INT M*", expected: "MAX-INT S>D")
		t("2 MAX-INT M*", expected: "MAX-INT 1 LSHIFT 0")
		t("MIN-INT MIN-INT M*", expected: "0 MSB 1 RSHIFT")
		t("MAX-INT MIN-INT M*", expected: "MSB MSB 2/")
		t("MAX-INT MAX-INT M*", expected: "1 MSB 2/ INVERT")
		//
		t("0 0 *", expected: "0")            // TEST IDENTITIES
		t("0 1 *", expected: "0")
		t("1 0 *", expected: "0")
		t("1 2 *", expected: "2")
		t("2 1 *", expected: "2")
		t("3 3 *", expected: "9")
		t("-3 3 *", expected: "-9")
		t("3 -3 *", expected: "-9")
		t("-3 -3 *", expected: "9")
		//
		t("MID-UINT+1 1 RSHIFT 2 *", expected: "MID-UINT+1")
		t("MID-UINT+1 2 RSHIFT 4 *", expected: "MID-UINT+1")
		t("MID-UINT+1 1 RSHIFT MID-UINT+1 OR 2 *", expected: "MID-UINT+1")
		//
		t("0 0 UM*", expected: "0 0")
		t("0 1 UM*", expected: "0 0")
		t("1 0 UM*", expected: "0 0")
		t("1 2 UM*", expected: "2 0")
		t("2 1 UM*", expected: "2 0")
		t("3 3 UM*", expected: "9 0")
		//
		t("MID-UINT+1 1 RSHIFT 2 UM*", expected: "MID-UINT+1 0")
		t("MID-UINT+1 2 UM*", expected: "0 1")
		t("MID-UINT+1 4 UM*", expected: "0 2")
		t("1S 2 UM*", expected: "1S 1 LSHIFT 1")
		t("MAX-UINT MAX-UINT UM*", expected: "1 1 INVERT")
	}

	///	TESTING DIVIDE:
	///	 `FM/MOD SM/REM UM/MOD */ */MOD / /MOD MOD`
	func testDivide()
	{
		predefine("""
		0 INVERT 1 RSHIFT INVERT   	CONSTANT MIN-INT
		0 INVERT 1 RSHIFT      		CONSTANT MAX-INT
		0 INVERT         			CONSTANT MAX-UINT
		""")

		t("0 S>D 1 FM/MOD", expected: "0 0")
		t("1 S>D 1 FM/MOD", expected: "0 1")
		t("2 S>D 1 FM/MOD", expected: "0 2")
		t("-1 S>D 1 FM/MOD", expected: "0 -1")
		t("-2 S>D 1 FM/MOD", expected: "0 -2")
		t("0 S>D -1 FM/MOD", expected: "0 0")
		t("1 S>D -1 FM/MOD", expected: "0 -1")
		t("2 S>D -1 FM/MOD", expected: "0 -2")
		t("-1 S>D -1 FM/MOD", expected: "0 1")
		t("-2 S>D -1 FM/MOD", expected: "0 2")
		t("2 S>D 2 FM/MOD", expected: "0 1")
		t("-1 S>D -1 FM/MOD", expected: "0 1")
		t("-2 S>D -2 FM/MOD", expected: "0 1")
		t(" 7 S>D  3 FM/MOD", expected: "1 2")
		t(" 7 S>D -3 FM/MOD", expected: "-2 -3")
		t("-7 S>D  3 FM/MOD", expected: "2 -3")
		t("-7 S>D -3 FM/MOD", expected: "-1 2")
		t("MAX-INT S>D 1 FM/MOD", expected: "0 MAX-INT")
		t("MIN-INT S>D 1 FM/MOD", expected: "0 MIN-INT")
		t("MAX-INT S>D MAX-INT FM/MOD", expected: "0 1")
		t("MIN-INT S>D MIN-INT FM/MOD", expected: "0 1")
		t("1S 1 4 FM/MOD", expected: "3 MAX-INT")
		t("1 MIN-INT M* 1 FM/MOD", expected: "0 MIN-INT")
		t("1 MIN-INT M* MIN-INT FM/MOD", expected: "0 1")
		t("2 MIN-INT M* 2 FM/MOD", expected: "0 MIN-INT")
		t("2 MIN-INT M* MIN-INT FM/MOD", expected: "0 2")
		t("1 MAX-INT M* 1 FM/MOD", expected: "0 MAX-INT")
		t("1 MAX-INT M* MAX-INT FM/MOD", expected: "0 1")
		t("2 MAX-INT M* 2 FM/MOD", expected: "0 MAX-INT")
		t("2 MAX-INT M* MAX-INT FM/MOD", expected: "0 2")
		t("MIN-INT MIN-INT M* MIN-INT FM/MOD", expected: "0 MIN-INT")
		t("MIN-INT MAX-INT M* MIN-INT FM/MOD", expected: "0 MAX-INT")
		t("MIN-INT MAX-INT M* MAX-INT FM/MOD", expected: "0 MIN-INT")
		t("MAX-INT MAX-INT M* MAX-INT FM/MOD", expected: "0 MAX-INT")
		//
		t("0 S>D 1 SM/REM", expected: "0 0")
		t("1 S>D 1 SM/REM", expected: "0 1")
		t("2 S>D 1 SM/REM", expected: "0 2")
		t("-1 S>D 1 SM/REM", expected: "0 -1")
		t("-2 S>D 1 SM/REM", expected: "0 -2")
		t("0 S>D -1 SM/REM", expected: "0 0")
		t("1 S>D -1 SM/REM", expected: "0 -1")
		t("2 S>D -1 SM/REM", expected: "0 -2")
		t("-1 S>D -1 SM/REM", expected: "0 1")
		t("-2 S>D -1 SM/REM", expected: "0 2")
		t("2 S>D 2 SM/REM", expected: "0 1")
		t("-1 S>D -1 SM/REM", expected: "0 1")
		t("-2 S>D -2 SM/REM", expected: "0 1")
		t(" 7 S>D  3 SM/REM", expected: "1 2")
		t(" 7 S>D -3 SM/REM", expected: "1 -2")
		t("-7 S>D  3 SM/REM", expected: "-1 -2")
		t("-7 S>D -3 SM/REM", expected: "-1 2")
		t("MAX-INT S>D 1 SM/REM", expected: "0 MAX-INT")
		t("MIN-INT S>D 1 SM/REM", expected: "0 MIN-INT")
		t("MAX-INT S>D MAX-INT SM/REM", expected: "0 1")
		t("MIN-INT S>D MIN-INT SM/REM", expected: "0 1")
		t("1S 1 4 SM/REM", expected: "3 MAX-INT")
		t("2 MIN-INT M* 2 SM/REM", expected: "0 MIN-INT")
		t("2 MIN-INT M* MIN-INT SM/REM", expected: "0 2")
		t("2 MAX-INT M* 2 SM/REM", expected: "0 MAX-INT")
		t("2 MAX-INT M* MAX-INT SM/REM", expected: "0 2")
		t("MIN-INT MIN-INT M* MIN-INT SM/REM", expected: "0 MIN-INT")
		t("MIN-INT MAX-INT M* MIN-INT SM/REM", expected: "0 MAX-INT")
		t("MIN-INT MAX-INT M* MAX-INT SM/REM", expected: "0 MIN-INT")
		t("MAX-INT MAX-INT M* MAX-INT SM/REM", expected: "0 MAX-INT")
		//
		t("0 0 1 UM/MOD", expected: "0 0")
		t("1 0 1 UM/MOD", expected: "0 1")
		t("1 0 2 UM/MOD", expected: "1 0")
		t("3 0 2 UM/MOD", expected: "1 1")
		t("MAX-UINT 2 UM* 2 UM/MOD", expected: "0 MAX-UINT")
		t("MAX-UINT 2 UM* MAX-UINT UM/MOD", expected: "0 2")
		t("MAX-UINT MAX-UINT UM* MAX-UINT UM/MOD", expected: "0 MAX-UINT")
		// This machine does symmetric division by default
		predefine("""
		: T/MOD  >R S>D R> SM/REM ;
		: T/     T/MOD SWAP DROP ;
		: TMOD   T/MOD DROP ;
		: T*/MOD >R M* R> SM/REM ;
		: T*/    T*/MOD SWAP DROP ;
		""")
		// If you want floored division, replace the above with
//		predefine("""
//		: T/MOD  >R S>D R> FM/MOD ;
//		: T/     T/MOD SWAP DROP ;
//		: TMOD   T/MOD DROP ;
//		: T*/MOD >R M* R> FM/MOD ;
//		: T*/    T*/MOD SWAP DROP ;
//		""")

		t("0 1 /MOD", expected: "0 1 T/MOD")
		t("1 1 /MOD", expected: "1 1 T/MOD")
		t("2 1 /MOD", expected: "2 1 T/MOD")
		t("-1 1 /MOD", expected: "-1 1 T/MOD")
		t("-2 1 /MOD", expected: "-2 1 T/MOD")
		t("0 -1 /MOD", expected: "0 -1 T/MOD")
		t("1 -1 /MOD", expected: "1 -1 T/MOD")
		t("2 -1 /MOD", expected: "2 -1 T/MOD")
		t("-1 -1 /MOD", expected: "-1 -1 T/MOD")
		t("-2 -1 /MOD", expected: "-2 -1 T/MOD")
		t("2 2 /MOD", expected: "2 2 T/MOD")
		t("-1 -1 /MOD", expected: "-1 -1 T/MOD")
		t("-2 -2 /MOD", expected: "-2 -2 T/MOD")
		t("7 3 /MOD", expected: "7 3 T/MOD")
		t("7 -3 /MOD", expected: "7 -3 T/MOD")
		t("-7 3 /MOD", expected: "-7 3 T/MOD")
		t("-7 -3 /MOD", expected: "-7 -3 T/MOD")
		t("MAX-INT 1 /MOD", expected: "MAX-INT 1 T/MOD")
		t("MIN-INT 1 /MOD", expected: "MIN-INT 1 T/MOD")
		t("MAX-INT MAX-INT /MOD", expected: "MAX-INT MAX-INT T/MOD")
		t("MIN-INT MIN-INT /MOD", expected: "MIN-INT MIN-INT T/MOD")
		//
		t("0 1 /", expected: "0 1 T/")
		t("1 1 /", expected: "1 1 T/")
		t("2 1 /", expected: "2 1 T/")
		t("-1 1 /", expected: "-1 1 T/")
		t("-2 1 /", expected: "-2 1 T/")
		t("0 -1 /", expected: "0 -1 T/")
		t("1 -1 /", expected: "1 -1 T/")
		t("2 -1 /", expected: "2 -1 T/")
		t("-1 -1 /", expected: "-1 -1 T/")
		t("-2 -1 /", expected: "-2 -1 T/")
		t("2 2 /", expected: "2 2 T/")
		t("-1 -1 /", expected: "-1 -1 T/")
		t("-2 -2 /", expected: "-2 -2 T/")
		t("7 3 /", expected: "7 3 T/")
		t("7 -3 /", expected: "7 -3 T/")
		t("-7 3 /", expected: "-7 3 T/")
		t("-7 -3 /", expected: "-7 -3 T/")
		t("MAX-INT 1 /", expected: "MAX-INT 1 T/")
		t("MIN-INT 1 /", expected: "MIN-INT 1 T/")
		t("MAX-INT MAX-INT /", expected: "MAX-INT MAX-INT T/")
		t("MIN-INT MIN-INT /", expected: "MIN-INT MIN-INT T/")
		//
		t("0 1 MOD", expected: "0 1 TMOD")
		t("1 1 MOD", expected: "1 1 TMOD")
		t("2 1 MOD", expected: "2 1 TMOD")
		t("-1 1 MOD", expected: "-1 1 TMOD")
		t("-2 1 MOD", expected: "-2 1 TMOD")
		t("0 -1 MOD", expected: "0 -1 TMOD")
		t("1 -1 MOD", expected: "1 -1 TMOD")
		t("2 -1 MOD", expected: "2 -1 TMOD")
		t("-1 -1 MOD", expected: "-1 -1 TMOD")
		t("-2 -1 MOD", expected: "-2 -1 TMOD")
		t("2 2 MOD", expected: "2 2 TMOD")
		t("-1 -1 MOD", expected: "-1 -1 TMOD")
		t("-2 -2 MOD", expected: "-2 -2 TMOD")
		t("7 3 MOD", expected: "7 3 TMOD")
		t("7 -3 MOD", expected: "7 -3 TMOD")
		t("-7 3 MOD", expected: "-7 3 TMOD")
		t("-7 -3 MOD", expected: "-7 -3 TMOD")
		t("MAX-INT 1 MOD", expected: "MAX-INT 1 TMOD")
		t("MIN-INT 1 MOD", expected: "MIN-INT 1 TMOD")
		t("MAX-INT MAX-INT MOD", expected: "MAX-INT MAX-INT TMOD")
		t("MIN-INT MIN-INT MOD", expected: "MIN-INT MIN-INT TMOD")
		//
		t("0 2 1 */", expected: "0 2 1 T*/")
		t("1 2 1 */", expected: "1 2 1 T*/")
		t("2 2 1 */", expected: "2 2 1 T*/")
		t("-1 2 1 */", expected: "-1 2 1 T*/")
		t("-2 2 1 */", expected: "-2 2 1 T*/")
		t("0 2 -1 */", expected: "0 2 -1 T*/")
		t("1 2 -1 */", expected: "1 2 -1 T*/")
		t("2 2 -1 */", expected: "2 2 -1 T*/")
		t("-1 2 -1 */", expected: "-1 2 -1 T*/")
		t("-2 2 -1 */", expected: "-2 2 -1 T*/")
		t("2 2 2 */", expected: "2 2 2 T*/")
		t("-1 2 -1 */", expected: "-1 2 -1 T*/")
		t("-2 2 -2 */", expected: "-2 2 -2 T*/")
		t("7 2 3 */", expected: "7 2 3 T*/")
		t("7 2 -3 */", expected: "7 2 -3 T*/")
		t("-7 2 3 */", expected: "-7 2 3 T*/")
		t("-7 2 -3 */", expected: "-7 2 -3 T*/")
		t("MAX-INT 2 MAX-INT */", expected: "MAX-INT 2 MAX-INT T*/")
		t("MIN-INT 2 MIN-INT */", expected: "MIN-INT 2 MIN-INT T*/")
		//
		t("0 2 1 */MOD", expected: "0 2 1 T*/MOD")
		t("1 2 1 */MOD", expected: "1 2 1 T*/MOD")
		t("2 2 1 */MOD", expected: "2 2 1 T*/MOD")
		t("-1 2 1 */MOD", expected: "-1 2 1 T*/MOD")
		t("-2 2 1 */MOD", expected: "-2 2 1 T*/MOD")
		t("0 2 -1 */MOD", expected: "0 2 -1 T*/MOD")
		t("1 2 -1 */MOD", expected: "1 2 -1 T*/MOD")
		t("2 2 -1 */MOD", expected: "2 2 -1 T*/MOD")
		t("-1 2 -1 */MOD", expected: "-1 2 -1 T*/MOD")
		t("-2 2 -1 */MOD", expected: "-2 2 -1 T*/MOD")
		t("2 2 2 */MOD", expected: "2 2 2 T*/MOD")
		t("-1 2 -1 */MOD", expected: "-1 2 -1 T*/MOD")
		t("-2 2 -2 */MOD", expected: "-2 2 -2 T*/MOD")
		t("7 2 3 */MOD", expected: "7 2 3 T*/MOD")
		t("7 2 -3 */MOD", expected: "7 2 -3 T*/MOD")
		t("-7 2 3 */MOD", expected: "-7 2 3 T*/MOD")
		t("-7 2 -3 */MOD", expected: "-7 2 -3 T*/MOD")
		t("MAX-INT 2 MAX-INT */MOD", expected: "MAX-INT 2 MAX-INT T*/MOD")
		t("MIN-INT 2 MIN-INT */MOD", expected: "MIN-INT 2 MIN-INT T*/MOD")

	}
//	\ ------------------------------------------------------------------------
//	TESTING HERE , @ ! CELL+ CELLS C, C@ C! CHARS 2@ 2! ALIGN ALIGNED +! ALLOT

	func testHere()
	{
		predefine("""
		0S CONSTANT <FALSE>
		1S CONSTANT <TRUE>
		HERE 1 ALLOT
		HERE
		CONSTANT 2NDA
		CONSTANT 1STA
		""")
		t("1STA 2NDA U<", expected: "<TRUE>")		// HERE MUST GROW WITH ALLOT
		// Modified so the absolute location doesn't matter
		t("2NDA 1STA -", expected: "1")   			// ... BY ONE ADDRESS UNIT
		//	( MISSING TEST: NEGATIVE ALLOT )
		//
		//	Added by GWJ so that ALIGN can be used before , (comma) is tested
		predefine(#"""
		1 ALIGNED CONSTANT ALMNT   \ -- 1|2|4|8 for 8|16|32|64 bit alignment
		ALIGN
		"""#)
		t("HERE 1 ALLOT ALIGN HERE SWAP - ALMNT =", expected: "<TRUE>")
		//	End of extra test
		predefine("""
		HERE 1 ,
		HERE 2 ,
		CONSTANT 2ND
		CONSTANT 1ST
		""")
		t("1ST 2ND U<", expected: "<TRUE>")     	// HERE MUST GROW WITH ALLOT
		t("1ST CELL+ 2nd -", expected: "0")         	// ... BY ONE CELL
		t("1ST 1 CELLS + 2nd -", expected: "0")
		t("1ST @ 2ND @", expected: "1 2")
		t("5 1ST !", expected: "")
		t("1ST @ 2ND @", expected: "5 2")
		t("6 2ND !", expected: "")
		t("1ST @ 2ND @", expected: "5 6")
		t("1ST 2@", expected: "6 5")
		t("2 1 1ST 2!", expected: "")
		t("1ST 2@", expected: "2 1")
		t("1S 1ST !  1ST @", expected: "1S")      	// CAN STORE CELL-WIDE VALUE
		predefine("""
			HERE 1 C,
			HERE 2 C,
			CONSTANT 2NDC
			CONSTANT 1STC
			2ndc 1stc - constant 2diff ( added because I can't rely on absolutes )
		""")
		t("1STC 2NDC U<", expected: "<TRUE>")     	// HERE MUST GROW WITH ALLOT
		t("1STC CHAR+ 2ndc -", expected: "0")       // ... BY ONE CHAR
		t("1STC 1 CHARS + 2ndc -", expected: "0")
		t("1STC C@ 2NDC C@", expected: "1 2")
		t("3 1STC C!", expected: "")
		t("1STC C@ 2NDC C@", expected: "3 2")
		t("4 2NDC C!", expected: "")
		t("1STC C@ 2NDC C@", expected: "3 4")
		predefine("""
			ALIGN 1 ALLOT HERE ALIGN HERE 3 CELLS ALLOT
			CONSTANT A-ADDR  CONSTANT UA-ADDR
		""")
		t("UA-ADDR ALIGNED a-addr -", expected: "0")
		t("   1 A-ADDR C!  A-ADDR C@", expected: "   1")
		t("1234 A-ADDR  !  A-ADDR  @", expected: "1234")
		t("123 456 A-ADDR 2!  A-ADDR 2@", expected: "123 456")
		t("2 A-ADDR CHAR+ C!  A-ADDR CHAR+ C@", expected: "2")
		t("3 A-ADDR CELL+ C!  A-ADDR CELL+ C@", expected: "3")
		t("1234 A-ADDR CELL+ !  A-ADDR CELL+ @", expected: "1234")
		t("123 456 A-ADDR CELL+ 2!  A-ADDR CELL+ 2@", expected: "123 456")
		predefine("""
			: BITS ( X -- U )
			   0 SWAP BEGIN DUP WHILE DUP MSB AND IF >R 1+ R> THEN 2* REPEAT DROP ;
			( CHARACTERS >= 1 AU, <= SIZE OF CELL, >= 8 BITS )
		""")
		t("1 CHARS 1 <", expected: "<FALSE>")
		t("1 CHARS 1 CELLS >", expected: "<FALSE>")
		//	( TBD: HOW TO FIND NUMBER OF BITS? )
		//
		//	( CELLS >= 1 AU, INTEGRAL MULTIPLE OF CHAR SIZE, >= 16 BITS )
		t("1 CELLS 1 <", expected: "<FALSE>")
		t("1 CELLS 1 CHARS MOD", expected: "0")
		t("1S BITS 10 <", expected: "<FALSE>")
		//
		t("0 1ST !", expected: "")
		t("1 1ST +!", expected: "")
		t("1ST @", expected: "1")
		t("-1 1ST +! 1ST @", expected: "0")
		//
	}
	///	TESTING CHAR
	///
	///	`[CHAR] [ ] BL S"`
	func testChar()
	{
		t("BL", expected: "20")
		t("CHAR X", expected: "58")
		t("CHAR G", expected: "47")
		t("CHAR HELLO", expected: "48")
		t(": GC1 [CHAR] X ;", expected: "")
		t(": GC2 [CHAR] HELLO ;", expected: "")
		t("GC1", expected: "58")
		t("GC2", expected: "48")
		t(": GC3 [ GC1 ] LITERAL ;", expected: "")
		t("GC3", expected: "58")
		t(#": GC4 S" XY" ;"#, expected: "")
		t("GC4 SWAP DROP", expected: "2")
		t("GC4 DROP DUP C@ SWAP CHAR+ C@", expected: "58 59")
	}

	/// TESTING '
	/// `['] FIND EXECUTE IMMEDIATE COUNT LITERAL POSTPONE STATE`
	func testQuote()
	{
		predefine("""
		: GT1 123 ;
		0S CONSTANT <FALSE>
		""")
		t("' GT1 EXECUTE", expected: "123 ")
		predefine(": GT2 ['] GT1 ; IMMEDIATE")
		t("GT2 EXECUTE", expected: "123 ")
		predefine("clearstack")
		predefine("""
				HERE 3 C, CHAR G C, CHAR T C, CHAR 1 C, CONSTANT GT1STRING
				HERE 3 C, CHAR G C, CHAR T C, CHAR 2 C, CONSTANT GT2STRING
		""")
		t("GT1STRING FIND", expected: "' GT1 -1 ")
		t("GT2STRING FIND", expected: "' GT2 1 ")
				//	( HOW TO SEARCH FOR NON-EXISTENT WORD? )
		t(": GT3 GT2 LITERAL ;", expected: "")
		t("GT3", expected: "' GT1 ")
		t("GT1STRING COUNT", expected: "GT1STRING CHAR+ 3 ")
				//
		t(": GT4 POSTPONE GT1 ; IMMEDIATE", expected: "")
		t(": GT5 GT4 ;", expected: "")
		t("GT5", expected: "123 ")
		t(": GT6 345 ; IMMEDIATE", expected: "")
		t(": GT7 POSTPONE GT6 ;", expected: "")
		t("GT7", expected: "345 ")
				//
		t(": GT8 STATE @ ; IMMEDIATE", expected: "")
		t("GT8", expected: "0 ")
		t(": GT9 GT8 LITERAL ;", expected: "")
		t("GT9 0=", expected: "<FALSE> ")
	}

	///	TESTING control words
	///	`IF ELSE THEN BEGIN WHILE REPEAT UNTIL RECURSE`
	func testControl()
	{
	t(" : GI1 IF 123 THEN ;", expected: "")
	t(" : GI2 IF 123 ELSE 234 THEN ;", expected: "")
	t(" 0 GI1", expected: "")
	t(" 1 GI1", expected: "123 ")
	t(" -1 GI1", expected: "123 ")
	t(" 0 GI2", expected: "234 ")
	t(" 1 GI2", expected: "123 ")
	t(" -1 GI1", expected: "123 ")

	t(" : GI3 BEGIN DUP 5 < WHILE DUP 1+ REPEAT ;", expected: "")
	t(" 0 GI3", expected: "0 1 2 3 4 5 ")
	t(" 4 GI3", expected: "4 5 ")
	t(" 5 GI3", expected: "5 ")
	t(" 6 GI3", expected: "6 ")

	t(" : GI4 BEGIN DUP 1+ DUP 5 > UNTIL ;", expected: "")
	t(" 3 GI4", expected: "3 4 5 6 ")
	t(" 5 GI4", expected: "5 6 ")
	t(" 6 GI4", expected: "6 7 ")

	t("""
		: GI5 BEGIN DUP 2 >
			WHILE DUP 5 < WHILE DUP 1+ REPEAT 123 ELSE 345 THEN ;
		""", expected: "")
	t(" 1 GI5", expected: "1 345 ")
	t(" 2 GI5", expected: "2 345 ")
	t(" 3 GI5", expected: "3 4 5 123 ")
	t(" 4 GI5", expected: "4 5 123 ")
	t(" 5 GI5", expected: "5 123 ")

	t(" : GI6 ( N -- 0,1,..N ) DUP IF DUP >R 1- RECURSE R> THEN ;", expected: "")
	t(" 0 GI6", expected: "0 ")
	t(" 1 GI6", expected: "0 1 ")
	t(" 2 GI6", expected: "0 1 2 ")
	t(" 3 GI6", expected: "0 1 2 3 ")
	t(" 4 GI6", expected: "0 1 2 3 4 ")
	}

	/// TESTING do loop
	///
	/// `DO LOOP +LOOP I J UNLOOP LEAVE EXIT`
	func testDoLoop()
	{
		predefine("""
		0 INVERT 1 RSHIFT INVERT   	CONSTANT MIN-INT
		0 INVERT 1 RSHIFT      		CONSTANT MID-UINT
		0 INVERT 1 RSHIFT INVERT   	CONSTANT MID-UINT+1
		""")

		t(": GD1 DO I LOOP ;", expected: "")
		if log.isDebug
		{
			for machine in testMachines
			{
				machine.dumpWordDefinition(word: Forth.Word("gd1"), to: &log.debugStream)
			}
		}

		t("4 1 GD1", expected: "1 2 3 ")
		t("2 -1 GD1", expected: "-1 0 1 ")
		t("MID-UINT+1 MID-UINT GD1", expected: "MID-UINT ")

		t(": GD2 DO I -1 +LOOP ;", expected: "")
		t("1 4 GD2", expected: "4 3 2 1 ")
		t("-1 2 GD2", expected: "2 1 0 -1 ")
		t("MID-UINT MID-UINT+1 GD2", expected: "MID-UINT+1 MID-UINT ")

		t(": GD3 DO 1 0 DO J LOOP LOOP ;", expected: "")
		t("4 1 GD3", expected: "1 2 3 ")
		t("2 -1 GD3", expected: "-1 0 1 ")
		t("MID-UINT+1 MID-UINT GD3", expected: "MID-UINT ")

		t(": GD4 DO 1 0 DO J LOOP -1 +LOOP ;", expected: "")
		t("1 4 GD4", expected: "4 3 2 1 ")
		t("-1 2 GD4", expected: "2 1 0 -1 ")
		t("MID-UINT MID-UINT+1 GD4", expected: "MID-UINT+1 MID-UINT ")

		t(": GD5 123 SWAP 0 DO I 4 > IF DROP 234 LEAVE THEN LOOP ;", expected: "")
		t("1 GD5", expected: "123 ")
		t("5 GD5", expected: "123 ")
		t("6 GD5", expected: "234 ")

		t("""
		  : GD6  ( PAT: T{0 0},{0 0}{1 0}{1 1},{0 0}{1 0}{1 1}{2 0}{2 1}{2 2} )
		   0 SWAP 0 DO
			  I 1+ 0 DO I J + 3 = IF I UNLOOP I UNLOOP EXIT THEN 1+ LOOP
			LOOP ;
		""", expected: "")
		t("1 GD6", expected: "1 ")
		t("2 GD6", expected: "3 ")
		t("3 GD6", expected: "4 1 2 ")
	}
	///	TESTING DEFINING WORDS:
	///
	///	`:` `;` `CONSTANT` `VARIABLE` `CREATE` `DOES>` `>BODY`
	func testDefiningWords()
	{
		predefine("""
		1S CONSTANT 				<TRUE>
		""")
		t("123 CONSTANT X123", expected: "")
		t("X123", expected: "123 ")
		t(": EQU CONSTANT ;", expected: "")
		t("X123 EQU Y123", expected: "")
		t("Y123", expected: "123 ")
		//
		t("VARIABLE V1", expected: "")
		t("123 V1 !", expected: "")
		t("V1 @", expected: "123 ")
		//
		t(": NOP : POSTPONE ; ;", expected: "")
		t("NOP NOP1 NOP NOP2", expected: "")
		t("NOP1", expected: "")
		t("NOP2", expected: "")
		//
		t(": DOES1 DOES> @ 1 + ;", expected: "")
		t(": DOES2 DOES> @ 2 + ;", expected: "")
		t("CREATE CR1", expected: "")
		// Modified because test and expected machines might have data space out of sync
		t("CR1 here =", expected: "<TRUE>")
		t("' CR1 >BODY here =", expected: "<TRUE>")
		t("1 ,", expected: "")
		t("CR1 @", expected: "1 ")
		t("DOES1", expected: "")
		t("CR1", expected: "2 ")
		t("DOES2", expected: "")
		t("CR1", expected: "3 ")
		//
		t(": WEIRD: CREATE DOES> 1 + DOES> 2 + ;", expected: "")
		t("WEIRD: W1", expected: "")
		t("' W1 >BODY here =", expected: "<TRUE>")
		t("W1 HERE 1 + =", expected: "<TRUE>")
		t("W1 HERE 2 + =", expected: "<TRUE>")

	}

	func testNOPProblem()
	{
		predefine("""
		: NOP : POSTPONE ; ;
		""")
		t("NOP NOP1 NOP NOP2", expected: "")
	}

	/// Test evaluate and executing stuff afterwards
	///
	/// Tests added by JP because I'm suspicious that things are not being
	/// evaluated in the right order
	func testEvaluateWithOtherStuff()
	{

		predefine("""
		: jp2 s" 1 1+" evaluate 99 ;
		""")
		t("jp2" , expected: "2 99")
	}

	/// Testing `evaluate`
	func testEvaluate()
	{
		predefine("""
		: GE1 S" 123" ; IMMEDIATE
		: GE2 S" 123 1+" ; IMMEDIATE
		: GE3 S" : GE4 345 ;" ;
		: GE5 EVALUATE ; IMMEDIATE
		""")

		t("GE1 EVALUATE", expected: "123 ")	// test evaluate in interp. state
		t("GE2 EVALUATE", expected: "124 ")
		t("GE3 EVALUATE", expected: "")
		t("GE4", expected: "345 ")
		//
		t(": GE6 GE1 GE5 ;", expected: "")	// test evaluate in compile state
		t("GE6", expected: "123 ")
		t(": GE7 GE2 GE5 ;", expected: "")
		t("GE7", expected: "124 ")
	}

	/// Testing `source` `>in` and `word`
	func testSourceInWord()
	{
		predefine("""
		1S CONSTANT 				<TRUE>
		: GS1 S" SOURCE" 2DUP EVALUATE
		   >R SWAP >R = R> R> = ;
		""")

		t("GS1", expected: "<TRUE> <TRUE>")

		predefine("""
		VARIABLE SCANS
		: RESCAN?  -1 SCANS +! SCANS @ IF 0 >IN ! THEN ;
		""")

		t("""
		  2 SCANS !
		  345 RESCAN?
		  """, expected: "345 345")

		predefine("""
		: GS2  5 SCANS ! S" 123 RESCAN?" EVALUATE ;
		""")
		t("GS2", expected: "123 123 123 123 123 ")

		predefine(": GS3 WORD COUNT SWAP C@ ;\n")
		t("BL GS3 HELLO", expected: "5 CHAR H")
		t(#"CHAR " GS3 GOODBYE""#, expected: "7 CHAR G")
		t("""
		  BL GS3
		  DROP
		  """, expected: "0")

		predefine("""
		: GS4 SOURCE >IN ! DROP ;
		""")
		t("GS4 123 456\n", expected: "")
	}

///	TESTING `<# # #S #>` HOLD SIGN BASE >NUMBER HEX DECIMAL
	func testVariousNumberFormatting()
	{
		predefine("""
		0 INVERT         			CONSTANT MAX-UINT
		1S CONSTANT 				<TRUE>
		0S CONSTANT 				<FALSE>
		""")
		predefine(#"""
		: S=  \ ( ADDR1 C1 ADDR2 C2 -- T/F ) COMPARE TWO STRINGS.
		   >R SWAP R@ = IF         \ MAKE SURE STRINGS HAVE SAME LENGTH
			  R> ?DUP IF         \ IF NON-EMPTY STRINGS
			0 DO
			   OVER C@ OVER C@ - IF 2DROP <FALSE> UNLOOP EXIT THEN
			   SWAP CHAR+ SWAP CHAR+
				 LOOP
			  THEN
			  2DROP <TRUE>         \ IF WE GET HERE, STRINGS MATCH
		   ELSE
			  R> DROP 2DROP <FALSE>      \ LENGTHS MISMATCH
		   THEN ;
		"""#)

		predefine(#"""
		: GP1  <# 41 HOLD 42 HOLD 0 0 #> S" BA" S= ;
		"""#)

		t("GP1", expected: "<TRUE>")

		predefine("""
		: GP2  <# -1 SIGN 0 SIGN -1 SIGN 0 0 #> S" --" S= ;
		""")
		t("GP2", expected: "<TRUE>")
		predefine("""
		: GP3  <# 1 0 # # #> S" 01" S= ;
		""")
		t("GP3", expected: "<TRUE>")
		predefine("""
		: GP4  <# 1 0 #S #> S" 1" S= ;
		""")
		t("GP4", expected: "<TRUE>")

		predefine(#"""
		24 CONSTANT MAX-BASE         \ BASE 2 .. 36
		: COUNT-BITS
		   0 0 INVERT BEGIN DUP WHILE >R 1+ R> 2* REPEAT DROP ;
		COUNT-BITS 2* CONSTANT #BITS-UD      \ NUMBER OF BITS IN UD

		: GP5
		   BASE @ <TRUE>
		   MAX-BASE 1+ 2 DO         \ FOR EACH POSSIBLE BASE
			  I BASE !            \ TBD: ASSUMES BASE WORKS
			  I 0 <# #S #> S" 10" S= AND
		   LOOP
		   SWAP BASE ! ;
		"""#)
		t("GP5", expected: "<TRUE>");


		predefine(#"""
		: GP6
		   BASE @ >R  2 BASE !
		   MAX-UINT MAX-UINT <# #S #>       \ MAXIMUM UD TO BINARY
		   R> BASE !            		\ S: C-ADDR U
		   DUP #BITS-UD = SWAP
		   0 DO               			\ S: C-ADDR FLAG
			  OVER C@ [CHAR] 1 = AND    \ ALL ONES
			  >R CHAR+ R>
		   LOOP SWAP DROP ;
		"""#)
		t("GP6", expected: "<TRUE>");

		predefine(#"""
		: GP7
		   BASE @ >R    MAX-BASE BASE !
		   <TRUE>
		   A 0 DO
			  I 0 <# #S #>
			  1 = SWAP C@ I 30 + = AND AND
		   LOOP
		   MAX-BASE A DO
			  I 0 <# #S #>
			  1 = SWAP C@ 41 I A - + = AND AND
		   LOOP
		   R> BASE ! ;
		"""#)

		t("GP7", expected: "<TRUE>");
	}

	/// >NUMBER TESTS
	func testToNumber()
	{
		predefine(#"""
		0 INVERT         			CONSTANT MAX-UINT
		24 CONSTANT MAX-BASE         \ BASE 2 .. 36
		CREATE GN-BUF 0 C,
		: GN-STRING   GN-BUF 1 ;
		: GN-CONSUMED   GN-BUF CHAR+ 0 ;
		: GN'      [CHAR] ' WORD CHAR+ C@ GN-BUF C!  GN-STRING ;
		"""#)

		t("0 0 GN' 0' >NUMBER", expected: "0 0 GN-CONSUMED")
		t("0 0 GN' 1' >NUMBER", expected: "1 0 GN-CONSUMED")
		t("1 0 GN' 1' >NUMBER", expected: "BASE @ 1+ 0 GN-CONSUMED")
		t("0 0 GN' -' >NUMBER", expected: "0 0 GN-STRING")   // SHOULD FAIL TO CONVERT THESE
		t("0 0 GN' +' >NUMBER", expected: "0 0 GN-STRING")
		t("0 0 GN' .' >NUMBER", expected: "0 0 GN-STRING")

		predefine(#"""
		: >NUMBER-BASED
			BASE @ >R BASE ! >NUMBER R> BASE ! ;
		"""#)

		t("0 0 GN' 2' 10 >NUMBER-BASED", expected: "2 0 GN-CONSUMED")
		t("0 0 GN' 2'  2 >NUMBER-BASED", expected: "0 0 GN-STRING")
		t("0 0 GN' F' 10 >NUMBER-BASED", expected: "F 0 GN-CONSUMED")
		t("0 0 GN' G' 10 >NUMBER-BASED", expected: "0 0 GN-STRING")
		t("0 0 GN' G' MAX-BASE >NUMBER-BASED", expected: "10 0 GN-CONSUMED")
		t("0 0 GN' Z' MAX-BASE >NUMBER-BASED", expected: "23 0 GN-CONSUMED")

		predefine(#"""
		: GN1   \ ( UD BASE -- UD' LEN ) UD SHOULD EQUAL UD' AND LEN SHOULD BE ZERO.
		   BASE @ >R BASE !
		   <# #S #>
		   0 0 2SWAP >NUMBER SWAP DROP      \ RETURN LENGTH ONLY
		   R> BASE ! ;
		"""#)
		t("0 0 2 GN1", expected: "0 0 0")
		t("MAX-UINT 0 2 GN1", expected: "MAX-UINT 0 0")
		t("MAX-UINT DUP 2 GN1", expected: "MAX-UINT DUP 0")
		t("0 0 MAX-BASE GN1", expected: "0 0 0")
		t("MAX-UINT 0 MAX-BASE GN1", expected: "MAX-UINT 0 0")
		t("MAX-UINT DUP MAX-BASE GN1", expected: "MAX-UINT DUP 0")

		predefine(#"""
		: GN2   \ ( -- 16 10 )
		   BASE @ >R  HEX BASE @  DECIMAL BASE @  R> BASE ! ;
		"""#)
		t("GN2", expected: "10 A")
	}
///	TESTING FILL MOVE
	func testFillMove()
	{
		predefine(#"""
		CREATE FBUF 00 C, 00 C, 00 C,
		CREATE SBUF 12 C, 34 C, 56 C,
		: SEEBUF FBUF C@  FBUF CHAR+ C@  FBUF CHAR+ CHAR+ C@ ;
		"""#)
		t("FBUF 0 20 FILL", expected: "")
		t("SEEBUF", expected: "00 00 00 ")
				//
		t("FBUF 1 20 FILL", expected: "")
		t("SEEBUF", expected: "20 00 00 ")
				//
		t("FBUF 3 20 FILL", expected: "")
		t("SEEBUF", expected: "20 20 20 ")
				//
		t("FBUF FBUF 3 CHARS MOVE", expected: "")      // BIZARRE SPECIAL CASE
		t("SEEBUF", expected: "20 20 20 ")
				//
		t("SBUF FBUF 0 CHARS MOVE", expected: "")
		t("SEEBUF", expected: "20 20 20 ")
				//
		t("SBUF FBUF 1 CHARS MOVE", expected: "")
		t("SEEBUF", expected: "12 20 20 ")
				//
		t("SBUF FBUF 3 CHARS MOVE", expected: "")
		t("SEEBUF", expected: "12 34 56 ")
				//
		t("FBUF FBUF CHAR+ 2 CHARS MOVE", expected: "")
		t("SEEBUF", expected: "12 12 34 ")
				//
		t("FBUF CHAR+ FBUF 2 CHARS MOVE", expected: "")
		t("SEEBUF", expected: "12 34 34 ")
	}
///	TESTING OUTPUT: . ." CR EMIT SPACE SPACES TYPE U.
	func testOutput()
	{
		predefine(#"""
		0 INVERT         			CONSTANT MAX-UINT
		0 INVERT 1 RSHIFT      		CONSTANT MAX-INT
		0 INVERT 1 RSHIFT INVERT   	CONSTANT MIN-INT
		
		: OUTPUT-TEST
		   ." YOU SHOULD SEE THE STANDARD GRAPHIC CHARACTERS:" CR
		   41 BL DO I EMIT LOOP CR
		   61 41 DO I EMIT LOOP CR
		   7F 61 DO I EMIT LOOP CR
		   ." YOU SHOULD SEE 0-9 SEPARATED BY A SPACE:" CR
		   9 1+ 0 DO I . LOOP CR
		   ." YOU SHOULD SEE 0-9 (WITH NO SPACES):" CR
		   [CHAR] 9 1+ [CHAR] 0 DO I 0 SPACES EMIT LOOP CR
		   ." YOU SHOULD SEE A-G SEPARATED BY A SPACE:" CR
		   [CHAR] G 1+ [CHAR] A DO I EMIT SPACE LOOP CR
		   ." YOU SHOULD SEE 0-5 SEPARATED BY TWO SPACES:" CR
		   5 1+ 0 DO I [CHAR] 0 + EMIT 2 SPACES LOOP CR
		   ." YOU SHOULD SEE TWO SEPARATE LINES:" CR
		   S" LINE 1" TYPE CR S" LINE 2" TYPE CR
		   ." YOU SHOULD SEE THE NUMBER RANGES OF SIGNED AND UNSIGNED NUMBERS:" CR
		   ."   SIGNED: " MIN-INT . MAX-INT . CR
		   ." UNSIGNED: " 0 U. MAX-UINT U. CR
		;
		"""#)

		// Bits of the example need trailing spaces, but the editor may be
		// configured to delete them. Trailing space strings are thus interpolated
		let separated0_9 = "0 1 2 3 4 5 6 7 8 9 "
		let separatedA_G = "A B C D E F G "
		let separatedBy2_0_5 = "0  1  2  3  4  5  "
		let signedRange = "\(String(Int.min, radix: 16)) \(String(Int.max, radix: 16)) "
		let unsignedRange = "\(String(UInt.min, radix: 16)) \(String(UInt.max, radix: 16)) "

		t("OUTPUT-TEST", expected: "", expectedOutput: #"""
		YOU SHOULD SEE THE STANDARD GRAPHIC CHARACTERS:
		 !"#$%&'()*+,-./0123456789:;<=>?@
		ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`
		abcdefghijklmnopqrstuvwxyz{|}~
		YOU SHOULD SEE 0-9 SEPARATED BY A SPACE:
		\#(separated0_9)
		YOU SHOULD SEE 0-9 (WITH NO SPACES):
		0123456789
		YOU SHOULD SEE A-G SEPARATED BY A SPACE:
		\#(separatedA_G)
		YOU SHOULD SEE 0-5 SEPARATED BY TWO SPACES:
		\#(separatedBy2_0_5)
		YOU SHOULD SEE TWO SEPARATE LINES:
		LINE 1
		LINE 2
		YOU SHOULD SEE THE NUMBER RANGES OF SIGNED AND UNSIGNED NUMBERS:
		  SIGNED: \#(signedRange)
		UNSIGNED: \#(unsignedRange)

		"""#)
	}
///	TESTING INPUT: ACCEPT
	func testInput()
	{
		predefine(#"""
		CREATE ABUF 50 CHARS ALLOT

		: ACCEPT-TEST
		   CR ." PLEASE TYPE UP TO 80 CHARACTERS:" CR
		   ABUF 50 ACCEPT
		   CR ." RECEIVED: " [CHAR] " EMIT
		   ABUF SWAP TYPE [CHAR] " EMIT CR
		;
	"""#)

		t("ACCEPT-TEST", stdIn: "gfdghdf\n", expected: "", expectedOutput: """

		PLEASE TYPE UP TO 80 CHARACTERS:

		RECEIVED: "gfdghdf"

		""")
	}
///	TESTING DICTIONARY SEARCH RULES
	func testDictionarySearch()
	{
		t(": GDX   123 ; : GDX   GDX 234 ;", expected: "")
		t("GDX", expected: "123 234")
	}

}
