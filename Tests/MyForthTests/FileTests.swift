//
//  FileTests.swift
//  
//
//  Created by Jeremy Pereira on 02/10/2023.
//
//  Copyright (c) Jeremy Pereira 2023
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
import Toolbox
import Foundation

private let log = Logger.getLogger("MyForthTests.FileTests")

final class FileTests: SuiteTest
{
	public override func setUp()
	{
		log.level = .info
		super.setUp()
		log.info("CWD = \(FileManager.default.currentDirectoryPath)")
		predefine("decimal")
		predefine(#"""
		1S CONSTANT 				<TRUE>
		0  CONSTANT 				<FALSE>
		: S=  \ ( ADDR1 C1 ADDR2 C2 -- T/F ) COMPARE TWO STRINGS.
			>R SWAP R@ = IF         \ MAKE SURE STRINGS HAVE SAME LENGTH
				R> ?DUP IF         \ IF NON-EMPTY STRINGS
			0 DO
				OVER C@ OVER C@ - IF 2DROP <FALSE> UNLOOP EXIT THEN
				SWAP CHAR+ SWAP CHAR+
					LOOP
				THEN
				2DROP <TRUE>         \ IF WE GET HERE, STRINGS MATCH
			ELSE
				R> DROP 2DROP <FALSE>      \ LENGTHS MISMATCH
			THEN ;
		: DEQ  ( d -- f )  ROT = >R = R> AND  ;  \ Same as D= in Double Number word set
		"""#)
		predefine(#"""
		VARIABLE (\?) 0 (\?) !     ( Flag: Word defined = 0 | word undefined = -1 )
		\ Equivalents of [IF] [ELSE] [THEN], these must not be nested
		: [?IF]  ( f -- )  (\?) ! ; IMMEDIATE
		: [?ELSE]  ( -- )  (\?) @ 0= (\?) ! ; IMMEDIATE
		: [?THEN]  ( -- )  0 (\?) ! ; IMMEDIATE
		( \? is a conditional comment )
		: \?  ( "..." -- )  (\?) @ IF EXIT THEN SOURCE >IN ! DROP ; IMMEDIATE

		\ Set the following flag to TRUE for more verbose output; this may
		\ allow you to tell which test caused your system to hang.
		VARIABLE VERBOSE
			TRUE VERBOSE !

		: TESTING	\ ( -- ) TALKING COMMENT.
			SOURCE VERBOSE @
			IF DUP >R TYPE CR R> >IN !
			ELSE >IN ! DROP
			THEN ;

		"""#)
	}
//	TESTING File Access word set
//
//	DECIMAL
//
//	\ ------------------------------------------------------------------------------
//	TESTING CREATE-FILE CLOSE-FILE
//
	func testCreateFileCloseFile()
	{
		predefine("""
		: FN1 S" fatest1.txt" ;
		VARIABLE FID1
		""")
		t("FN1 R/W CREATE-FILE SWAP FID1 !", expected: "0")
		t("FID1 @ CLOSE-FILE", expected: "0")

	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING OPEN-FILE W/O WRITE-LINE
//

	func testOpenFileEtc()
	{
		let fm = FileManager.default
		if fm.fileExists(atPath: "fatest1.txt")
		{
			try! fm.removeItem(atPath: "fatest1.txt")
		}
		fm.createFile(atPath: "fatest1.txt", contents: nil)

		predefine(#"""
		: FN1 S" fatest1.txt" ;
		VARIABLE FID1
		: LINE1 S" Line 1" ;
		"""#)

		t("FN1 W/O OPEN-FILE SWAP FID1 !", expected: "0")
		t("LINE1 FID1 @ WRITE-LINE", expected: "0")
		t("FID1 @ CLOSE-FILE", expected: "0")

		//	\ ------------------------------------------------------------------------------
		//	TESTING R/O FILE-POSITION (simple)  READ-LINE
		predefine("""
		200 CONSTANT BSIZE
		CREATE BUF BSIZE ALLOT
		VARIABLE #CHARS
		1S CONSTANT 				TRUE
		""")
		t("FN1 R/O OPEN-FILE SWAP FID1 !", expected: "0")
		t("FID1 @ FILE-POSITION", expected: "0 0 0")
		t("BUF 100 FID1 @ READ-LINE ROT DUP #CHARS !", expected: "TRUE 0 LINE1 SWAP DROP")
		t("BUF #CHARS @ LINE1 S=", expected: "TRUE")
		t("FID1 @ CLOSE-FILE", expected: "0")
//		Logger.pushLevel(.debug, forName: "MyForth.ThreadedForth")
//		defer { Logger.popLevel(forName: "MyForth.ThreadedForth") }
//
		//	Additional test contributed by Helmut Eller
		//	Test with buffer shorter than the line including zero length buffer.
		t("FN1 R/O OPEN-FILE SWAP FID1 !", expected: "0")
		t("FID1 @ FILE-POSITION", expected: "0 0 0")
		t("BUF 0 FID1 @ READ-LINE ROT DUP #CHARS !", expected: "TRUE 0 0")
		t("BUF 3 FID1 @ READ-LINE ROT DUP #CHARS !", expected: "TRUE 0 3")
		t("BUF #CHARS @ LINE1 DROP 3 S=", expected: "TRUE")
		t("BUF 100 FID1 @ READ-LINE ROT DUP #CHARS !", expected: "TRUE 0 LINE1 NIP 3 -")
		t("BUF #CHARS @ LINE1 3 /STRING S=", expected: "TRUE")
		t("FID1 @ CLOSE-FILE", expected: "0")

		// Additional test contributed by Helmut Eller
		// Test with buffer exactly as long as the line.

		t("FN1 R/O OPEN-FILE SWAP FID1 !", expected: "0")
		t("FID1 @ FILE-POSITION", expected: "0 0 0")
		t("BUF LINE1 NIP FID1 @ READ-LINE ROT DUP #CHARS !", expected: "TRUE 0 LINE1 NIP")
		t("BUF #CHARS @ LINE1 S=", expected: "TRUE")
		t("FID1 @ CLOSE-FILE", expected: "0")

		//	\ ------------------------------------------------------------------------------
		//	TESTING R/W WRITE-FILE REPOSITION-FILE READ-FILE FILE-POSITION S"

		predefine(#"""
		: LINE2 S" Line 2 blah blah blah" ;
		: RL1  BUF 100 FID1 @ READ-LINE  ;
		CREATE FP 0 , 0 ,    \ Don't use 2VARIABLE
		"""#)

		t("FN1 R/W OPEN-FILE SWAP FID1 !", expected: "0")
		t("FID1 @ FILE-SIZE DROP FID1 @ REPOSITION-FILE", expected: "0")
		// Modified because the second machine won't have opened the file
		t("FID1 @ FILE-SIZE drop FID1 @ FILE-POSITION drop rot = rot rot =", expected: "TRUE TRUE")
		t("LINE2 FID1 @ WRITE-FILE", expected: "0")
		t("10 0 FID1 @ REPOSITION-FILE", expected: "0")
		t("FID1 @ FILE-POSITION", expected: "10 0 0")
		t("0 0 FID1 @ REPOSITION-FILE", expected: "0")
		t("RL1", expected: "LINE1 SWAP DROP TRUE 0")
		t("RL1 ROT DUP #CHARS !", expected: "TRUE 0 LINE2 SWAP DROP")
		t("BUF #CHARS @ LINE2 S=", expected: "TRUE")
		t("RL1", expected: "0 FALSE 0")
		t("FID1 @ FILE-POSITION ROT ROT FP 2!", expected: "0")
		t("FP 2@ FID1 @ FILE-SIZE DROP DEQ", expected: "TRUE")
		t(#"S" " FID1 @ WRITE-LINE"#, expected: "0")
		t(#"S" " FID1 @ WRITE-LINE"#, expected: "0")
		t("FP 2@ FID1 @ REPOSITION-FILE", expected: "0")
		t("RL1", expected: "0 TRUE 0")
		t("RL1", expected: "0 TRUE 0")
		t("RL1", expected: "0 FALSE 0")
		t("FID1 @ CLOSE-FILE", expected: "0")

		//	\ ------------------------------------------------------------------------------
		//	TESTING RENAME-FILE FILE-STATUS FLUSH-FILE
		//
		predefine(#"""
		: FN3 S" fatest3.txt" ;
			: >END FID1 @ FILE-SIZE DROP FID1 @ REPOSITION-FILE ;
		"""#)

		t("FN3 DELETE-FILE DROP", expected: "")
		t("FN1 FN3 RENAME-FILE 0=", expected: "TRUE ")
		t("FN1 FILE-STATUS SWAP DROP 0=", expected: "FALSE ")
		t("FN3 FILE-STATUS SWAP DROP 0=", expected: "TRUE ")  // Return value is undefined
		t("FN3 R/W OPEN-FILE SWAP FID1 !", expected: "0 ")
		t(">END", expected: "0 ")
		t(#"S" Final line" fid1 @ WRITE-LINE"#, expected: "0 ")
		t("FID1 @ FLUSH-FILE", expected: "0 ")      // Can only test FLUSH-FILE doesn't fail
		t("FID1 @ CLOSE-FILE", expected: "0 ")

		// Tidy the test folder
		t("fn3 DELETE-FILE DROP", expected: "")
	}

	//	\ ------------------------------------------------------------------------------
	//	TESTING S" in interpretation mode (compile mode tested in Core tests)
	func testSQuoteInterpreting()
	{
		predefine(SuiteTest.utilitiesDefs)
		predefine(#"""
		1S CONSTANT 				TRUE
		0  CONSTANT 				FALSE
		: FN1 S" fatest1.txt" ;
		VARIABLE FID1
		: LINE1 S" Line 1" ;
		"""#)
		t(#"S" abcdef" $" abcdef" S="#, expected: "TRUE")
		t(#"S" " $" " S="#, expected: "TRUE")
		t(#"S" ghi"$" ghi" S= "#, expected: "TRUE")
	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING BIN READ-FILE FILE-SIZE

	func testBinReadFileEtc()
	{
		predefine(#"""
		200 CONSTANT BSIZE
		CREATE BUF BSIZE ALLOT
		: CBUF BUF BSIZE 0 FILL ;
		: FN2 S" FATEST2.TXT" ;
		VARIABLE FID2
		: SETPAD PAD 50 0 DO I OVER C! CHAR+ LOOP DROP ;

		SETPAD   \ If anything else is defined setpad must be called again
		   		 \ as pad may move
		"""#)

		t("FN2 R/W BIN CREATE-FILE SWAP FID2 !", expected: "0")
		t("PAD 50 FID2 @ WRITE-FILE FID2 @ FLUSH-FILE", expected: "0 0")
		t("FID2 @ FILE-SIZE", expected: "50 0 0")
		t("0 0 FID2 @ REPOSITION-FILE", expected: "0")
		t("CBUF BUF 29 FID2 @ READ-FILE", expected: "29 0")
		t("PAD 29 BUF 29 S=", expected: "TRUE")
		t("PAD 30 BUF 30 S=", expected: "FALSE")
		t("CBUF BUF 29 FID2 @ READ-FILE", expected: "21 0")
		t("PAD 29 + 21 BUF 21 S=", expected: "TRUE")
		t("FID2 @ FILE-SIZE DROP FID2 @ FILE-POSITION DROP DEQ", expected: "TRUE")
		t("BUF 10 FID2 @ READ-FILE", expected: "0 0")
		t("FID2 @ CLOSE-FILE", expected: "0")
		//
		//	\ ------------------------------------------------------------------------------
		//	TESTING RESIZE-FILE
		//
		t("FN2 R/W BIN OPEN-FILE SWAP FID2 !", expected: "0")
		t("37 0 FID2 @ RESIZE-FILE", expected: "0")
		t("FID2 @ FILE-SIZE", expected: "37 0 0")
		t("0 0 FID2 @ REPOSITION-FILE", expected: "0")
		t("CBUF BUF 100 FID2 @ READ-FILE", expected: "37 0")
		t("PAD 37 BUF 37 S=", expected: "TRUE")
		t("PAD 38 BUF 38 S=", expected: "FALSE")
		t("500 0 FID2 @ RESIZE-FILE", expected: "0")
		t("FID2 @ FILE-SIZE", expected: "500 0 0")
		t("0 0 FID2 @ REPOSITION-FILE", expected: "0")
		t("CBUF BUF 100 FID2 @ READ-FILE", expected: "100 0")
		t("PAD 37 BUF 37 S=", expected: "TRUE")
		t("FID2 @ CLOSE-FILE", expected: "0")
		//	\ ------------------------------------------------------------------------------
		//	TESTING DELETE-FILE
		//
		t("FN2 DELETE-FILE", expected: "0")
		t("FN2 R/W BIN OPEN-FILE SWAP DROP 0=", expected: "FALSE")
		t("FN2 DELETE-FILE 0=", expected: "FALSE")

	}
//
//
//	\ ------------------------------------------------------------------------------
//	TESTING multi-line ( comments
//
	func testMultilineComment()
	{
		t("""
		( 1 2 3
			4 5 6
			7 8 9 ) 11 22 33
		""", expected: "11 22 33")

	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING SOURCE-ID (can only test it does not return 0 or -1)
//
	func testSourceId()
	{
		t("SOURCE-ID DUP -1 = SWAP 0= OR", expected: "FALSE")
	}
//
//
//	\ ------------------------------------------------------------------------------
//	TESTING REQUIRED REQUIRE INCLUDED
//	\ Tests taken from Forth 2012 RfD

	func testRequiredRequireIncluded()
	{
		let rh1Name = "required-helper1.fth"
		let rh2Name = "required-helper2.fth"
		makeRequiredHelper(rh1Name)
		makeRequiredHelper(rh2Name)

		t(#"""
		0
		S" required-helper1.fth" REQUIRED
		REQUIRE required-helper1.fth
		INCLUDE required-helper1.fth
		"""#, expected: "2")
		t(#"""
		0
		INCLUDE required-helper2.fth
		S" required-helper2.fth" REQUIRED
		REQUIRE required-helper2.fth
		S" required-helper2.fth" INCLUDED
		"""#, expected: "2")
	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING S\" (Forth 2012 interpretation mode)

	func testSBackslashQuotes()
	{
		//	\ S\" in compilation mode already tested in Core Extension tests
		t(#": SSQ11 S\" \a\b\e\f\l\m\q\r\t\v\x0F0\x1Fa\xaBx\z\"\\" ;"#, expected: "")
		t(#"S\" \a\b\e\f\l\m\q\r\t\v\x0F0\x1Fa\xaBx\z\"\\" SSQ11  S="#, expected: "TRUE")
	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING two buffers available for S" and/or S\" (Forth 2012)
//
	func testSQuotesTwobuffers()
	{
		predefine(#"""
		: SSQ12 S" abcd" ;   : SSQ13 S" 1234" ;
		"""#)
		t(#"S" abcd"  S" 1234" SSQ13  S= ROT ROT SSQ12 S="#, expected: "TRUE TRUE")
		t(#"S\" abcd" S\" 1234" SSQ13 S= ROT ROT SSQ12 S="#, expected: "TRUE TRUE")
		t(#"S" abcd"  S\" 1234" SSQ13 S= ROT ROT SSQ12 S="#, expected: "TRUE TRUE")
		t(#"S\" abcd" S" 1234" SSQ13  S= ROT ROT SSQ12 S="#, expected: "TRUE TRUE")

	}
//
//
//	\ ------------------------------------------------------------------------------
//	TESTING SAVE-INPUT and RESTORE-INPUT with a file source

	func testSaveRestoreInput()
	{
		predefine(#"""
	VARIABLE SI_INC 0 SI_INC !
	VARIABLE SIV -1 SIV !

: SI1
   SI_INC @ >IN +!
   15 SI_INC !
;

: S$ S" SAVE-INPUT SI1 RESTORE-INPUT 12345" ;

	: NEVEREXECUTED
	  CR ." This should never be executed" CR
   ;
"""#)
		t(#"""

11111 SAVE-INPUT

		SIV @

			[?IF]
			\?   0 SIV !
			\?   RESTORE-INPUT
			\?   NEVEREXECUTED
			\?   33333
			[?ELSE]

			\? TESTING the -[ELSE]- part is executed
			\? 22222

			[?THEN]

"""#, expected: "11111 0 22222") // 0 comes from RESTORE-INPUT

			// TESTING nested SAVE-INPUT, RESTORE-INPUT and REFILL from a file
		predefine(#"""
			: READ_A_LINE
			   REFILL 0=
			   ABORT" REFILL FAILED"
			;
		
			0 SI_INC !
		
			CREATE 2RES -1 , -1 ,   \ Don't use 2VARIABLE from Double number word set
		
			: SI2
			   READ_A_LINE
			   READ_A_LINE
			   SAVE-INPUT
			   READ_A_LINE
			   READ_A_LINE
			   S$ EVALUATE 2RES 2!
			   RESTORE-INPUT
			;
		
			\ WARNING: do not delete or insert lines of text after si2 is called
			\ otherwise the next test will fail
"""#)
		t(#"""
			SI2
			33333               \ This line should be ignored
			2RES 2@ 44444      \ RESTORE-INPUT should return to this line
		
			55555
			\ TESTING the nested results
"""#, expected: "0 0 2345 44444 55555")
	}
	
	private func makeRequiredHelper(_ name: String)
	{
		do
		{

			try #"""
\ For testing REQUIRED etc

1+
"""#.write(toFile: name, atomically: false, encoding: .utf8)
		}
		catch
		{
			fatalError("\(error)")
		}
	}
}
