//
//  CorePlusTests.swift
//  
//
//  Created by Jeremy Pereira on 29/11/2023.
//
//  Copyright (c) Jeremy Pereira 2023
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest

/// Additional tests on the the ANS Forth Core word set
///
/// These tests are based on a test program written by Gerry Jackson in 2007,
///  with contributions from others where indicated. The orignial program
///  carried the following disclaimer.
///
///	> This program is distributed in the hope that it will be useful,
/// > but WITHOUT ANY WARRANTY; without even the implied warranty of
/// > MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
///
/// The tests are based on John Hayes test program for the core word set. This
/// file provides some more tests on Core words where the original Hayes tests
/// are thought to be incomplete
///
/// Words tested in this file are:
/// `DO` `I` `+LOOP` `RECURSE` `ELSE` `>IN` `IMMEDIATE` `FIND` `IF`...`BEGIN`...`REPEAT` `ALLOT` `DOES>`
///
/// and
///
/// - Parsing behaviour
/// - Number prefixes `#` `$` `%` and `'A'` character input
/// - Definition names
///
/// The tests are not claimed to be comprehensive or correct
final class CorePlusTests: SuiteTest
{
	override func setUp() 
	{
		super.setUp()
		let predefined = """
		0 INVERT         			CONSTANT MAX-UINT
		0 INVERT 1 RSHIFT      		CONSTANT MAX-INT
		0 INVERT 1 RSHIFT INVERT   	CONSTANT MIN-INT
		0 INVERT 1 RSHIFT      		CONSTANT MID-UINT
		0 INVERT 1 RSHIFT INVERT   	CONSTANT MID-UINT+1
		0S CONSTANT 				<FALSE>
		1S CONSTANT 				<TRUE>
		"""
		predefine(predefined)

		predefine("decimal")
	}

//	\ ------------------------------------------------------------------------------
//	\ The tests are based on John Hayes test program for the core word set
//	\
//	\ This file provides some more tests on Core words where the original Hayes
//	\ tests are thought to be incomplete
//	\
//	\ ------------------------------------------------------------------------------
//
//	TESTING DO +LOOP with run-time increment, negative increment, infinite loop
//	\ Contributed by Reinhold Straub
//
	func testDoPlusLoop()
	{
		predefine("""
		VARIABLE ITERATIONS
		VARIABLE INCREMENT
		: GD7 ( LIMIT START INCREMENT -- )
			INCREMENT !
			0 ITERATIONS !
			DO
				1 ITERATIONS +!
				I
				ITERATIONS @  6 = IF LEAVE THEN
				INCREMENT @
			+LOOP ITERATIONS @
		;
		""")

		t(" 4  4 -1 GD7", expected: "4 1 ")
		t(" 1  4 -1 GD7", expected: "4 3 2 1 4 ")
		t(" 4  1 -1 GD7", expected: "1 0 -1 -2 -3 -4 6 ")
		t(" 4  1  0 GD7", expected: "1 1 1 1 1 1 6 ")
		t(" 0  0  0 GD7", expected: "0 0 0 0 0 0 6 ")
		t(" 1  4  0 GD7", expected: "4 4 4 4 4 4 6 ")
		t(" 1  4  1 GD7", expected: "4 5 6 7 8 9 6 ")
		t(" 4  1  1 GD7", expected: "1 2 3 3 ")
		t(" 4  4  1 GD7", expected: "4 5 6 7 8 9 6 ")
		t(" 2 -1 -1 GD7", expected: "-1 -2 -3 -4 -5 -6 6 ")
		t("-1  2 -1 GD7", expected: "2 1 0 -1 4 ")
		t(" 2 -1  0 GD7", expected: "-1 -1 -1 -1 -1 -1 6 ")
		t("-1  2  0 GD7", expected: "2 2 2 2 2 2 6 ")
		t("-1  2  1 GD7", expected: "2 3 4 5 6 7 6 ")
		t(" 2 -1  1 GD7", expected: "-1 0 1 3 ")
		t("-20 30 -10 GD7", expected: "30 20 10 0 -10 -20 6 ")
		t("-20 31 -10 GD7", expected: "31 21 11 1 -9 -19 6 ")
		t("-20 29 -10 GD7", expected: "29 19 9 -1 -11 5 ")

	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING DO +LOOP with large and small increments
//
//	Contributed by Andrew Haley
//
	func testDoPlusLoop2()
	{
		predefine("""
			MAX-UINT 8 RSHIFT 1+ CONSTANT USTEP
			USTEP NEGATE CONSTANT -USTEP
			MAX-INT 7 RSHIFT 1+ CONSTANT STEP
			STEP NEGATE CONSTANT -STEP
		
			VARIABLE BUMP
		""")
		t(": GD8 BUMP ! DO 1+ BUMP @ +LOOP ;", expected: "")

		t("0 MAX-UINT 0 USTEP GD8", expected: "256 ")
		t("0 0 MAX-UINT -USTEP GD8", expected: "256 ")

		t("0 MAX-INT MIN-INT STEP GD8", expected: "256 ")
		t("0 MIN-INT MAX-INT -STEP GD8", expected: "256 ")

		// Two's complement arithmetic, wraps around modulo wordsize
		// Only tested if the Forth system does wrap around, use of conditional
		// compilation deliberately avoided
		predefine("""
			MAX-INT 1+ MIN-INT = CONSTANT +WRAP?
			MIN-INT 1- MAX-INT = CONSTANT -WRAP?
			MAX-UINT 1+ 0=       CONSTANT +UWRAP?
			0 1- MAX-UINT =      CONSTANT -UWRAP?
		
		""")
		// Substantially modified for Swift because we have two machines
		// so don't need to preserve the result n the return stack
		t("""
		: GD9  ( n limit start step f result -- )
		  drop IF GD8 ELSE 2DROP 2DROP R@ THEN
		;
		""", expected: "")

		t("0 0 0  USTEP +UWRAP? 256 GD9", expected: "256")
		t("0 0 0 -USTEP -UWRAP?   1 GD9", expected: "1")
		t("0 MIN-INT MAX-INT  STEP +WRAP? 1 GD9", expected: "1")
		t("0 MAX-INT MIN-INT -STEP -WRAP? 1 GD9", expected: "1")
	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING DO +LOOP with maximum and minimum increments
//
	func testDoPlusLoop3()
	{
		predefine("""
			: (-MI) MAX-INT DUP NEGATE + 0= IF MAX-INT NEGATE ELSE -32767 THEN ;
			(-MI) CONSTANT -MAX-INT
			VARIABLE BUMP
			: GD8 BUMP ! DO 1+ BUMP @ +LOOP ;
		""")

		t("0 1 0 MAX-INT GD8 ", expected: "1")
		t("0 -MAX-INT NEGATE -MAX-INT OVER GD8 ", expected: "2")

		t("0 MAX-INT  0 MAX-INT GD8 ", expected: "1")
		t("0 MAX-INT  1 MAX-INT GD8 ", expected: "1")
		t("0 MAX-INT -1 MAX-INT GD8 ", expected: "2")
		t("0 MAX-INT DUP 1- MAX-INT GD8 ", expected: "1")

		t("0 MIN-INT 1+   0 MIN-INT GD8 ", expected: "1")
		t("0 MIN-INT 1+  -1 MIN-INT GD8 ", expected: "1")
		t("0 MIN-INT 1+   1 MIN-INT GD8 ", expected: "2")
		t("0 MIN-INT 1+ DUP MIN-INT GD8 ", expected: "1")

	}
//
//	\ ------------------------------------------------------------------------------
//	\ TESTING +LOOP setting I to an arbitrary value
//
// The specification for +LOOP permits the loop index I to be set to any value
// including a value outside the range given to the corresponding  DO.

	func testArbitraryI()
	{
		//
		// SET-I is a helper to set I in a DO ... +LOOP to a given value
		// n2 is the value of I in a DO ... +LOOP
		// n3 is a test value
		// If n2=n3 then return n1-n2 else return 1
		predefine("""
			: SET-I  ( n1 n2 n3 -- n1-n2 | 1 )
			   OVER = IF - ELSE 2DROP 1 THEN
			;

			: -SET-I ( n1 n2 n3 -- n1-n2 | -1 )
			   SET-I DUP 1 = IF NEGATE THEN
			;
		""")

		predefine(": PL1 20 1 DO I 18 I 3 SET-I +LOOP ;")
		t("PL1", expected: "1 2 3 18 19")
		predefine(": PL2 20 1 DO I 20 I 2 SET-I +LOOP ;")
		t("PL2", expected: "1 2")
		predefine(": PL3 20 5 DO I 19 I 2 SET-I DUP 1 = IF DROP 0 I 6 SET-I THEN +LOOP ;")
		t("PL3", expected: "5 6 0 1 2 19")
		predefine(": PL4 20 1 DO I MAX-INT I 4 SET-I +LOOP ;")
		t("PL4", expected: "1 2 3 4")
		predefine(": PL5 -20 -1 DO I -19 I -3 -SET-I +LOOP ;")
		t("PL5", expected: "-1 -2 -3 -19 -20")
		predefine(": PL6 -20 -1 DO I -21 I -4 -SET-I +LOOP ;")
		t("PL6", expected: "-1 -2 -3 -4")
		predefine(": PL7 -20 -1 DO I MIN-INT I -5 -SET-I +LOOP ;")
		t("PL7", expected: "-1 -2 -3 -4 -5")
		predefine(": PL8 -20 -5 DO I -20 I -2 -SET-I DUP -1 = IF DROP 0 I -6 -SET-I THEN +LOOP ;")
		t("PL8", expected: "-5 -6 0 -1 -2 -20")
	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING multiple RECURSEs in one colon definition
//
	func testMultipleReurses()
	{
		predefine(#"""
		: ACK ( m n -- u )    \ Ackermann function, from Rosetta Code
			OVER 0= IF  NIP 1+ EXIT  THEN       \ ack(0, n) = n+1
			SWAP 1- SWAP                        ( -- m-1 n )
			DUP  0= IF  1+  RECURSE EXIT  THEN  \ ack(m, 0) = ack(m-1, 1)
			1- OVER 1+ SWAP RECURSE RECURSE     \ ack(m, n) = ack(m-1, ack(m,n-1))
		;
		"""#)

		t("0 0 ACK", expected: "1")
		t("3 0 ACK", expected: "5")
		t("2 4 ACK", expected: "11")
	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING multiple ELSE's in an IF statement
//	\ Discussed on comp.lang.forth and accepted as valid ANS Forth
//
	func testMutipleElse()
	{
		predefine(": MELSE IF 1 ELSE 2 ELSE 3 ELSE 4 ELSE 5 THEN ;")
		t("0 MELSE", expected: "2 4")
		t("-1 MELSE", expected: "1 3 5")
	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING manipulation of >IN in interpreter mode
//
	func testManipulationOfGtIn()
	{
		// Leading and training spaces in the input needed to make the offsets
		// correct. Originals had `T{` and ` -> 15 ` and ` }T` in them
		t("   12345 DEPTH OVER 9 < 34 AND + 3 + >IN !", expected: "12345 2345 345 45 5")
		t("   14145 8115 ?DUP 0= 34 AND >IN +! TUCK MOD 14 >IN ! GCD CALCULATION         ", expected: "15")
	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING IMMEDIATE with CONSTANT  VARIABLE and CREATE [ ... DOES> ]
//
	func testImediateConstantVariableCreate()
	{
		t("123 CONSTANT IW1 IMMEDIATE IW1", expected: "123")
		t(": IW2 IW1 LITERAL ; IW2", expected: "123")
		t("VARIABLE IW3 IMMEDIATE 234 IW3 ! IW3 @", expected: "234")
		t(": IW4 IW3 [ @ ] LITERAL ; IW4", expected: "234")
		t(":NONAME [ 345 ] IW3 [ ! ] ; DROP IW3 @", expected: "345")
		t("CREATE IW5 456 , IMMEDIATE", expected: "")
		t(":NONAME IW5 [ @ IW3 ! ] ; DROP IW3 @", expected: "456")
		t(": IW6 CREATE , IMMEDIATE DOES> @ 1+ ;", expected: "")
		t("111 IW6 IW7 IW7", expected: "112")
		t(": IW8 IW7 LITERAL 1+ ; IW8", expected: "113")
		t(": IW9 CREATE , DOES> @ 2 + IMMEDIATE ;", expected: "")
		predefine(": FIND-IW BL WORD FIND NIP ;  ( -- 0 | 1 | -1 )")
		t("222 IW9 IW10 FIND-IW IW10", expected: "-1")   // IW10 is not immediate
		t("IW10 FIND-IW IW10", expected: "224 1")        // IW10 becomes immediate
	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING that IMMEDIATE doesn't toggle a flag
//
	func testImmediateNoToggle()
	{
		predefine("VARIABLE IT1 0 IT1 !")
		predefine(": IT2 1234 IT1 ! ; IMMEDIATE IMMEDIATE")
		t(": IT3 IT2 ; IT1 @", expected: "1234")

	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING parsing behaviour of S" ." and (
//  which should parse to just beyond the terminating character no space needed
//
	func testParsingBehaviour()
	{
		t(#": GC5 S" A string"2DROP ; GC5"#, expected: "")
		t("( A comment)1234", expected: "1234 ")
		t(#": PB1 CR ." You should see 2345: "." 2345"( A comment) CR ; PB1"#, expected: "")
	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING number prefixes # $ % and 'c' character input
//	\ Adapted from the Forth 200X Draft 14.5 document
//
	func testNumberPrefixes()
	{
		predefine("""
		VARIABLE OLD-BASE
		DECIMAL BASE @ OLD-BASE !
		""")
		t("#1289", expected: "1289")
		t("#-1289", expected: "-1289")
		t("$12eF", expected: "4847")
		t("$-12eF", expected: "-4847")
		t("%10010110", expected: "150")
		t("%-10010110", expected: "-150")
		t("'z'", expected: "122")
		t("'Z'", expected: "90")
		// Check BASE is unchanged
		t("BASE @ OLD-BASE @ =", expected: "<TRUE>")

		// Repeat in Hex mode
		predefine("16 OLD-BASE ! 16 BASE !")
		t("#1289", expected: "509")
		t("#-1289", expected: "-509")
		t("$12eF", expected: "12EF")
		t("$-12eF", expected: "-12EF")
		t("%10010110", expected: "96")
		t("%-10010110", expected: "-96")
		t("'z'", expected: "7a")
		t("'Z'", expected: "5a")
		// Check BASE is unchanged
		t("BASE @ OLD-BASE @ =", expected: "<TRUE>")   // 2

		predefine("DECIMAL")
		//Check number prefixes in compile mode
		t(": nmp  #8327 $-2cbe %011010111 ''' ; nmp", expected: "8327 -11454 215 39")

	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING definition names
//	\ should support {1..31} graphical characters

	func testDefinitionNames()
	{
		predefine(##": !"#$%&'()*+,-./0123456789:;<=>? 1 ;"##)
		t(##"!"#$%&'()*+,-./0123456789:;<=>?"##, expected: "1")
		predefine(#": @ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^ 2 ;"#)
		t(#"@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^"#, expected: "2")
		predefine(#": _`abcdefghijklmnopqrstuvwxyz{|} 3 ;"#)
		t("_`abcdefghijklmnopqrstuvwxyz{|}", expected: "3")
		predefine(#": _`abcdefghijklmnopqrstuvwxyz{|~ 4 ;     \ Last character different"#)
		t("_`abcdefghijklmnopqrstuvwxyz{|~", expected: "4")
		t("_`abcdefghijklmnopqrstuvwxyz{|}", expected: "3")
	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING FIND with a zero length string and a non-existent word
//
	func testFindZeroLength()
	{
		predefine(#"""
			CREATE EMPTYSTRING 0 C,
			: EMPTYSTRING-FIND-CHECK ( c-addr 0 | xt 1 | xt -1 -- t|f )
				DUP IF ." FIND returns a TRUE value for an empty string!" CR THEN
				0= SWAP EMPTYSTRING = = ;
			"""#)
		t("EMPTYSTRING FIND EMPTYSTRING-FIND-CHECK", expected: "<TRUE>")
		predefine(#"""
			CREATE NON-EXISTENT-WORD   \ Same as in exceptiontest.fth
				   15 C, CHAR $ C, CHAR $ C, CHAR Q C, CHAR W C, CHAR E C, CHAR Q C,
			   CHAR W C, CHAR E C, CHAR Q C, CHAR W C, CHAR E C, CHAR R C, CHAR T C,
			   CHAR $ C, CHAR $ C,
		"""#)
		t("NON-EXISTENT-WORD FIND", expected: "NON-EXISTENT-WORD 0")
	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING IF ... BEGIN ... REPEAT (unstructured)
//
	func testIfBeginRepeatUnstructured()
	{
		t(": UNS1 DUP 0 > IF 9 SWAP BEGIN 1+ DUP 3 > IF EXIT THEN REPEAT ;", expected: "")
		t("-6 UNS1", expected: "-6")
		t("1 UNS1", expected: "9 4")
	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING DOES> doesn't cause a problem with a CREATEd address
//
	func testDoesCreate()
	{
		predefine(": MAKE-2CONST DOES> 2@ ;")
		t("""
		CREATE 2K 3 , 2K , MAKE-2CONST 2K ' 2K >BODY 3
			rot = rot rot =
		""", expected: "TRUE TRUE")
	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING ALLOT ( n -- ) where n <= 0
//
	func testAllotNNotPositive()
	{
		t("HERE 5 ALLOT -5 ALLOT HERE =", expected: "<TRUE>")
		t("HERE 0 ALLOT HERE =", expected: "<TRUE>")

	}
//
//	\ ------------------------------------------------------------------------------
//
//	CR .( End of additional Core tests) CR

}
