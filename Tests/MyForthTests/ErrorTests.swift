//
//  ErrorTests.swift
//  
//
//  Created by Jeremy Pereira on 25/08/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
import Toolbox
@testable import MyForth

private let log = Logger.getLogger("MyForthTests.ErrorTests")


/// Tests for mapping error numbers to errors
///
/// These tests provide the specification for the mapping from `Int` to
/// `MyForth.Error`
final class ErrorTests: XCTestCase
{
	func testErrors()
	{
		for errNo in [-256, -10]
		{
			var stub = ForthStub(stackItems: [errNo])
			do
			{
				try stub.throwError()
			}
			catch
			{
				switch (errNo, error)
				{
				// Simple cases
				case (-256, Forth.Error.integerOverflow)
				   , (-10, Forth.Error.divisionByZero):
					break
				default:
					XCTFail("Incorrect match for error \(errNo): \(error)")
				}
			}
		}
	}

	/// So we don't have to create a full Forth machine
	struct ForthStub: ForthProtocol
	{
		let parserSpaceIndex = 1

		let padSpaceIndex = 2

		var stdin: AnyIterator<UInt8> = AnyIterator<UInt8>({ return nil })

		func dumpDefinition(for word: String) -> String?
		{
			return nil
		}

		mutating func interpret<OUT: TextOutputStream>(input: inout Forth.InputSource, output: inout OUT) throws
		{
			notImplemented()
		}

		mutating func dataStackPop() throws -> Int
		{
			return try stack.throwingPop()
		}

		var trace: Bool = false

		var wordsExecuted: Int = 0

		var stack = Forth.Stack<Int>(name: "stub")

		init(stackItems: [Int])
		{
			stackItems.forEach
			{
				stack.push($0)
			}
		}
	}
}
