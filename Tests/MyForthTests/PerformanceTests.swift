//
//  PerformanceTests.swift
//  
//
//  Created by Jeremy Pereira on 07/06/2020.
//

import XCTest
import Toolbox
import MyForth

private let log = Logger.getLogger("MyForthTests.PerformanceTests")

final class PerformanceTests: XCTestCase
{
	let program =
	"""
	: fibo
		dup
		if ( non zero )
			dup 1 =
			if ( is 1 )
			else ( is greater than 1 )
				dup 1 - recurse swap
				2 - recurse
				+
			then
		then ;
	"""

	func testFibo()
	{
		var machine = ThreadedForth()
		do
		{
			var output = ""
			try machine.interpret(input: program, output: &output)
			output = ""
			try machine.interpret(input: "0 fibo .", output: &output)
			XCTAssert(output == "0 ", "Output: '\(output)'")
			output = ""
			try machine.interpret(input: "1 fibo .", output: &output)
			XCTAssert(output == "1 ", "Output: '\(output)'")
			output = ""
			try machine.interpret(input: "2 fibo .", output: &output)
			XCTAssert(output == "1 ", "Output: '\(output)'")
			output = ""
			try machine.interpret(input: "3 fibo .", output: &output)
			XCTAssert(output == "2 ", "Output: '\(output)'")
			output = ""
			try machine.interpret(input: "11 fibo .s .", output: &output)
			XCTAssert(output == "<1> 8989 ", "Output: '\(output)'")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

    static var allTests = [
        ("testFibo", testFibo),
    ]
}
