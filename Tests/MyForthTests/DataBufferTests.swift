//
//  DayaBufferTests.swift
//  
//
//  Created by Jeremy Pereira on 13/08/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
import Toolbox
@testable import MyForth

private let log = Logger.getLogger("MyForthTests.DataBufferTests")

final class DataBufferTests: XCTestCase
{
	func testStoreChars() throws
	{
		let chars1: [UInt8] = [1, 1, 1, 1, 1, 1, 1, 1]
		let chars2: [UInt8] =    [2, 2]
		var dataBuffer = Forth.DataSpace()
		try dataBuffer.allot(chars1.count + 1)
		do
		{
			try dataBuffer.store(characters: chars1, at: 1)
			try dataBuffer.store(characters: chars2, at: 2)
			try checkChar(buffer: dataBuffer, index: 0, expected: 0)
			try checkChar(buffer: dataBuffer, index: 1, expected: 1)
			try checkChar(buffer: dataBuffer, index: 2, expected: 2)
			try checkChar(buffer: dataBuffer, index: 4, expected: 1)
			try checkChar(buffer: dataBuffer, index: 9, expected: 0)
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	private func checkChar(buffer: Forth.DataSpace, index: Int, expected: Forth.Cell) throws
	{
		let actual = try buffer.loadCharacter(from: index)
		XCTAssert(actual == expected, "At index: \(index): got \(actual), expected \(expected)")
	}

}



