//
//  DoubleUIntTests.swift
//  
//
//  Created by Jeremy Pereira on 17/08/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
import Toolbox
@testable import MyForth

private let log = Logger.getLogger("MyForthTests.DoubleUIntTests")

final class DoubleUIntTests: XCTestCase
{
	func testIsNot0()
	{
		let divisor = DoublePrecision<UInt>(low: 1, high: 0)
		XCTAssert(divisor != 0, "Divisor should not be 0")
	}

	func testCompare()
	{
		let a = DoublePrecision<UInt>(low: 1, high: 0)
		let b = DoublePrecision<UInt>(low: 2, high: 0)
		let c = DoublePrecision<UInt>(low: 1, high: 1)
		let d = DoublePrecision<UInt>(low: 2, high: 1)

		XCTAssert(a <= a)
		XCTAssert(a <= b)
		XCTAssert(b <= c)
		XCTAssert(b <= d)
		XCTAssert(a >= a)
		XCTAssert(!(a >= b))
		XCTAssert(!(b >= c))
		XCTAssert(!(b >= d))
	}

	func testDivision()
	{
		let a = DoublePrecision<UInt>(low: 3, high: 0)
		let b = DoublePrecision<UInt>(low: 2, high: 0)
		divisionCheck(lhs: a, rhs: b, expectedQuotient: 1, expectedRemainder: 1, label: "3/2")
	}

	func testMaximalDivision()
	{
		divisionCheck(lhs: DoublePrecision<UInt>(tuple: (high: UInt.max - 1, low: 1)),
					  rhs: DoublePrecision<UInt>(tuple: (high: 0,            low: UInt.max)),
					  expectedQuotient: DoublePrecision<UInt>(tuple: (high: 0, low: UInt.max)),
					  expectedRemainder: 0, label: "FF...FE01/UInt.max")
		divisionCheck(lhs: DoublePrecision<UInt>(tuple: (high: UInt.max, low: UInt.max)),
					  rhs: DoublePrecision<UInt>(low: 2, high: 0),
					  expectedQuotient: DoublePrecision<UInt>(tuple: (high: UInt.max >> 1, low: UInt.max)),
					  expectedRemainder: 1, label: "FF...FFF/2")
	}

	private func divisionCheck(lhs: DoublePrecision<UInt>,
							   rhs: DoublePrecision<UInt>,
							   expectedQuotient: DoublePrecision<UInt>,
							   expectedRemainder: DoublePrecision<UInt>, label: String)
	{
		let (q, r) = lhs.quotientAndRemainder(dividingBy: rhs)
		XCTAssert(q == expectedQuotient, "Division: \(label): quotient wrong")
		XCTAssert(r == expectedRemainder, "Division: \(label): remainder wrong")
	}

	func testMultiplication()
	{
		let expectedResult = DoublePrecision<UInt>(tuple: (high: UInt.max - 1, low: 1))
		let a = DoublePrecision<UInt>(tuple: (high: 0, low: UInt.max))
		let (result, overflow) = a.multipliedReportingOverflow(by: a)
		XCTAssert(!overflow)
		XCTAssert(result == expectedResult)
	}

	func testRightShift()
	{
		let result = DoublePrecision<UInt>(tuple: (high: UInt.max - 1, low: 1)) >> 65
		XCTAssert(result == DoublePrecision<UInt>(tuple: (high: 0, low: (UInt.max - 1) >> 1)))
	}
}



