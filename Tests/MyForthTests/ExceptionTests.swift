//
//  ExceptionTests.swift
//  
//
//  Created by Jeremy Pereira on 28/12/2023.
//
//  Copyright (c) Jeremy Pereira 2023
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
@testable import MyForth


/// To test the ANS Forth Exception word set and extension words
///
/// This test case is based on tests written by Gerry Jackson in 2006, with
/// contributions from others where indicated, and is in the public domain - it
/// can be distributed and/or modified in any way but please retain this notice.
///
/// The tests are based on John Hayes test program for the core word set
///
/// Words tested in this test case are:
///     `CATCH`, `THROW`, `ABORT` and `ABORT"`
///
/// Assumptions and dependencies:
///
/// - the forth system under test throws an exception with throw code -13 for a
///   word not found by the text interpreter. The undefined word used is 
///   `$$qweqweqwert$$`,  if this happens to be a valid word in your system
///   change the definition of t7 below
///	- the Core word set available and tested
/// - CASE, OF, ENDOF and ENDCASE from the core extension wordset are present
///   and work correctly
final class ExceptionTests: SuiteTest
{
	override func setUp() 
	{
		super.setUp()
		predefine("DECIMAL")
	}
	//	\ ------------------------------------------------------------------------------
	//	TESTING CATCH THROW
	//
	func testCatchThrow()
	{
		return;
		predefine(#"""
			: T1 9 ;
			: C1 1 2 3 ['] T1 CATCH ;
		"""#)
		t("C1", expected: "1 2 3 9 0")         	// No THROW executed

		predefine(#"""
			: T2 8 0 THROW ;
			: C2 1 2 ['] T2 CATCH ;
		"""#)
		t("C2", expected: "1 2 8 0")            // 0 THROW does nothing

		predefine(#"""
			: T3 7 8 9 99 THROW ;
			: C3 1 2 ['] T3 CATCH ;
		"""#)
		t("C3", expected: "1 2 99")            	// Restores stack to CATCH depth

		predefine(#"""
			: T4 1- DUP 0> IF RECURSE ELSE 999 THROW -222 THEN ;
			: C4 3 4 5 10 ['] T4 CATCH -111 ;
		"""#)
		t("C4", expected: "3 4 5 0 999 -111")	// Test return stack unwinding

		predefine(#"""
			: T5 2DROP 2DROP 9999 THROW ;
			: C5 1 2 3 4 ['] T5 CATCH            \ Test depth restored correctly
			   DEPTH >R DROP 2DROP 2DROP R> ;   \ after stack has been emptied
		"""#)
			t("C5", expected: "5")

	}
	//
	//	\ ------------------------------------------------------------------------------
	//	TESTING ABORT ABORT"
	//
	//	-1  CONSTANT EXC_ABORT
	//	-2  CONSTANT EXC_ABORT"
	//	-13 CONSTANT EXC_UNDEF
	//	: T6 ABORT ;
	//
	//	\ The 77 in T10 is necessary for the second ABORT" test as the data stack
	//	\ is restored to a depth of 2 when THROW is executed. The 77 ensures the top
	//	\ of stack value is known for the results check
	//
	//	: T10 77 SWAP ABORT" This should not be displayed" ;
	//	: C6 CATCH
	//	   >R   R@ EXC_ABORT  = IF 11
	//	   ELSE R@ EXC_ABORT" = IF 12
	//	   ELSE R@ EXC_UNDEF  = IF 13
	//	   THEN THEN THEN R> DROP
	//	;
	//
	//	t("1 2 ' T6 C6 ", expected: "1 2 11")     \ Test that ABORT is caught
	//	t("3 0 ' T10 C6", expected: "3 77")       \ ABORT" does nothing
	//	t("4 5 ' T10 C6", expected: "4 77 12")    \ ABORT" caught, no message
	//
	//	\ ------------------------------------------------------------------------------
	//	TESTING a system generated exception
	//
	//	: T7 S" 333 $$QWEQWEQWERT$$ 334" EVALUATE 335 ;
	//	: T8 S" 222 T7 223" EVALUATE 224 ;
	//	: T9 S" 111 112 T8 113" EVALUATE 114 ;
	//
	//	t("6 7 ' T9 C6 3", expected: "6 7 13 3")         \ Test unlinking of sources
//
//	\ ------------------------------------------------------------------------------
//
//	EXCEPTION-ERRORS SET-ERROR-COUNT
//
//	CR .( End of Exception word tests) CR
//

}
