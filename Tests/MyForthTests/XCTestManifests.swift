import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(MyForthTests.allTests),
		testCase(WordParserTests.allTests),
    ]
}
#endif
