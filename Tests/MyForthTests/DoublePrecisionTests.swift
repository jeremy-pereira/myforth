//
//  DoublePrecisionTests.swift
//  
//
//  Created by Jeremy Pereira on 19/12/2023.
//
//  Copyright (c) Jeremy Pereira 2023
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest

///	To test the ANS Forth Double-Number word set and double number extensions
///
///	This test case is based o a program written by  Gerry Jackson in 2006, with
///	contributions from others where indicated. The original code bore the
///	following notice:
///
///	>This program is distributed in the hope that it will be useful,
/// >but WITHOUT ANY WARRANTY; without even the implied warranty of
/// >MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
///
/// The tests are not claimed to be comprehensive or correct
///
final class DoublePrecisionTests: SuiteTest
{
	override func setUp()
	{
		super.setUp()
		predefine(SuiteTest.utilitiesDefs)
		predefine(#"""
		\ Constant definitions

		DECIMAL
		0 INVERT        CONSTANT 1SD
		1SD 1 RSHIFT    CONSTANT MAX-INTD   \ 01...1
		MAX-INTD INVERT CONSTANT MIN-INTD   \ 10...0
		MAX-INTD 2/     CONSTANT HI-INT     \ 001...1
		MIN-INTD 2/     CONSTANT LO-INT     \ 110...1
		0S CONSTANT <FALSE>
		1S CONSTANT <TRUE>

		\ ------------------------------------------------------------------------------
		\ Some 2CONSTANTs for the following tests

		1SD MAX-INTD 2CONSTANT MAX-2INT  \ 01...1
		0   MIN-INTD 2CONSTANT MIN-2INT  \ 10...0
		MAX-2INT 2/  2CONSTANT HI-2INT   \ 001...1
		MIN-2INT 2/  2CONSTANT LO-2INT   \ 110...0

		"""#)
	}

//
//	\ ------------------------------------------------------------------------------
//	TESTING interpreter and compiler reading double numbers, with/without prefixes
//
	func testReadingDoubleNumbers()
	{
		t("1.", expected: "1 0")
		t("-2.", expected: "-2 -1")
		t(": RDL1 3. ; RDL1", expected: "3 0")
		t(": RDL2 -4. ; RDL2", expected: "-4 -1")
		predefine("""
		VARIABLE OLD-DBASE
		DECIMAL BASE @ OLD-DBASE !
		""")
		t("#12346789.", expected: "12346789.")
		t("#-12346789.", expected: "-12346789.")
		t("$12aBcDeF.", expected: "313249263.")
		t("$-12AbCdEf.", expected: "-313249263.")
		t("%10010110.", expected: "150.")
		t("%-10010110.", expected: "-150.")
		// Check BASE is unchanged
		t("BASE @ OLD-DBASE @ =", expected: "<TRUE>")

		// Repeat in Hex mode
		predefine("16 OLD-DBASE ! 16 BASE !")
		t("#12346789.", expected: "BC65A5.")
		t("#-12346789.", expected: "-BC65A5.")
		t("$12aBcDeF.", expected: "12AbCdeF.")
		t("$-12AbCdEf.", expected: "-12ABCDef.")
		t("%10010110.", expected: "96.")
		t("%-10010110.", expected: "-96.")
		// Check BASE is unchanged
		t("BASE @ OLD-DBASE @ =", expected: "<TRUE>")   // 2

		predefine("DECIMAL")
		// Check number prefixes in compile mode
		t(": dnmp  #8327. $-2cbe. %011010111. ; dnmp", expected: "8327. -11454. 215.")

	}

//	\ ------------------------------------------------------------------------------
//	TESTING 2CONSTANT
	func test2Constant()
	{
		t("1 2 2CONSTANT 2C1", expected: "")
		t("2C1", expected: " 1 2")
		t(": CD1 2C1 ;", expected: "")
		t("CD1", expected: " 1 2")
		t(": CD2 2CONSTANT ;", expected: "")
		t("-1 -2 CD2 2C2", expected: "")
		t("2C2", expected: " -1 -2")
		t("4 5 2CONSTANT 2C3 IMMEDIATE 2C3", expected: " 4 5")
		t(": CD6 2C3 2LITERAL ; CD6", expected: " 4 5")
	}

//	\ ------------------------------------------------------------------------------
//	TESTING DNEGATE
//
	func testDnegate()
	{
		t("0. DNEGATE", expected: "0.")
		t("1. DNEGATE", expected: "-1.")
		t("-1. DNEGATE", expected: "1.")
		t("MAX-2INT DNEGATE", expected: "MIN-2INT SWAP 1+ SWAP")
		t("MIN-2INT SWAP 1+ SWAP DNEGATE", expected: "MAX-2INT")
	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING D+ with small integers
//
	func testDPlus()
	{
		t(" 0.  5. D+", expected: " 5.")
		t("-5.  0. D+", expected: "-5.")
		t(" 1.  2. D+", expected: " 3.")
		t(" 1. -2. D+", expected: "-1.")
		t("-1.  2. D+", expected: " 1.")
		t("-1. -2. D+", expected: "-3.")
		t("-1.  1. D+", expected: " 0.")
		
		// TESTING D+ with mid range integers
		
		t(" 0  0  0  5 D+", expected: " 0  5")
		t("-1  5  0  0 D+", expected: "-1  5")
		t(" 0  0  0 -5 D+", expected: " 0 -5")
		t(" 0 -5 -1  0 D+", expected: "-1 -5")
		t(" 0  1  0  2 D+", expected: " 0  3")
		t("-1  1  0 -2 D+", expected: "-1 -1")
		t(" 0 -1  0  2 D+", expected: " 0  1")
		t(" 0 -1 -1 -2 D+", expected: "-1 -3")
		t("-1 -1  0  1 D+", expected: "-1  0")
		t("MIN-INTD 0 2DUP D+", expected: "0 1")
		t("MIN-INTD S>D MIN-INTD 0 D+", expected: "0 0")
		
		// TESTING D+ with large double integers
		
		t("HI-2INT 1. D+", expected: "0 HI-INT 1+")
		t("HI-2INT 2DUP D+", expected: "1SD 1- MAX-INTD")
		t("MAX-2INT MIN-2INT D+", expected: "-1.")
		t("MAX-2INT LO-2INT D+", expected: "HI-2INT")
		t("HI-2INT MIN-2INT D+ 1. D+", expected: "LO-2INT")
		t("LO-2INT 2DUP D+", expected: "MIN-2INT")
	}
//
//	\ ------------------------------------------------------------------------------

	func testDMinus()
	{
		//	TESTING D- with small integers
		
		t(" 0.  5. D-", expected: "-5.")
		t(" 5.  0. D-", expected: " 5.")
		t(" 0. -5. D-", expected: " 5.")
		t(" 1.  2. D-", expected: "-1.")
		t(" 1. -2. D-", expected: " 3.")
		t("-1.  2. D-", expected: "-3.")
		t("-1. -2. D-", expected: " 1.")
		t("-1. -1. D-", expected: " 0.")

	// TESTING D- with mid-range integers

		t(" 0  0  0  5 D-", expected: " 0 -5")
		t("-1  5  0  0 D-", expected: "-1  5")
		t(" 0  0 -1 -5 D-", expected: " 1  4")
		t(" 0 -5  0  0 D-", expected: " 0 -5")
		t("-1  1  0  2 D-", expected: "-1 -1")
		t(" 0  1 -1 -2 D-", expected: " 1  2")
		t(" 0 -1  0  2 D-", expected: " 0 -3")
		t(" 0 -1  0 -2 D-", expected: " 0  1")
		t(" 0  0  0  1 D-", expected: " 0 -1")
		t("MIN-INTD 0 2DUP D-", expected: "0.")
		t("MIN-INTD S>D MAX-INTD 0 D-", expected: "1 1SD")

	// TESTING D- with large integers

		t("MAX-2INT MAX-2INT D-", expected: "0.")
		t("MIN-2INT MIN-2INT D-", expected: "0.")
		t("MAX-2INT HI-2INT  D-", expected: "LO-2INT DNEGATE")
		t("HI-2INT  LO-2INT  D-", expected: "MAX-2INT")
		t("LO-2INT  HI-2INT  D-", expected: "MIN-2INT 1. D+")
		t("MIN-2INT MIN-2INT D-", expected: "0.")
		t("MIN-2INT LO-2INT  D-", expected: "LO-2INT")

	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING D0< D0=

	func testD0LessD0Equals()
	{
		t("0. D0<", expected: "FALSE")
		t("1. D0<", expected: "FALSE")
		t("MIN-INTD 0 D0<", expected: "FALSE")
		t("0 MAX-INTD D0<", expected: "FALSE")
		t("MAX-2INT  D0<", expected: "FALSE")
		t("-1. D0<", expected: "TRUE")
		t("MIN-2INT D0<", expected: "TRUE")

		t("1. D0=", expected: "FALSE")
		t("MIN-INTD 0 D0=", expected: "FALSE")
		t("MAX-2INT  D0=", expected: "FALSE")
		t("-1 MAX-INTD D0=", expected: "FALSE")
		t("0. D0=", expected: "TRUE")
		t("-1. D0=", expected: "FALSE")
		t("0 MIN-INTD D0=", expected: "FALSE")

	}
//
//
//	\ ------------------------------------------------------------------------------
//	TESTING D2* D2/

	func testD2Times0D2Div()
	{
		t("0. D2*", expected: "0. D2*")
		t("MIN-INTD 0 D2*", expected: "0 1")
		t("HI-2INT D2*", expected: "MAX-2INT 1. D-")
		t("LO-2INT D2*", expected: "MIN-2INT")

		t("0. D2/", expected: "0.")
		t("1. D2/", expected: "0.")
		t("0 1 D2/", expected: "MIN-INTD 0")
		t("MAX-2INT D2/", expected: "HI-2INT")
		t("-1. D2/", expected: "-1.")
		t("MIN-2INT D2/", expected: "LO-2INT")
	}
//
//
//	\ ------------------------------------------------------------------------------
//	TESTING D< D=

	func testDLessDEq()
	{
		t(" 0.  1. D<", expected: "TRUE ")
		t(" 0.  0. D<", expected: "FALSE")
		t(" 1.  0. D<", expected: "FALSE")
		t("-1.  1. D<", expected: "TRUE ")
		t("-1.  0. D<", expected: "TRUE ")
		t("-2. -1. D<", expected: "TRUE ")
		t("-1. -2. D<", expected: "FALSE")
		t("0 1   1. D<", expected: "FALSE")  // Suggested by Helmut Eller
		t("1.  0 1  D<", expected: "TRUE ")
		t("0 -1 1 -2 D<", expected: "FALSE")
		t("1 -2 0 -1 D<", expected: "TRUE ")
		t("-1. MAX-2INT D<", expected: "TRUE")
		t("MIN-2INT MAX-2INT D<", expected: "TRUE")
		t("MAX-2INT -1. D<", expected: "FALSE")
		t("MAX-2INT MIN-2INT D<", expected: "FALSE")
		t("MAX-2INT 2DUP -1. D+ D<", expected: "FALSE")
		t("MIN-2INT 2DUP  1. D+ D<", expected: "TRUE ")
		t("MAX-INTD S>D 2DUP 1. D+ D<", expected: "TRUE") // Ensure D< acts on MS cells

		t("-1. -1. D=", expected: "TRUE ")
		t("-1.  0. D=", expected: "FALSE")
		t("-1.  1. D=", expected: "FALSE")
		t(" 0. -1. D=", expected: "FALSE")
		t(" 0.  0. D=", expected: "TRUE ")
		t(" 0.  1. D=", expected: "FALSE")
		t(" 1. -1. D=", expected: "FALSE")
		t(" 1.  0. D=", expected: "FALSE")
		t(" 1.  1. D=", expected: "TRUE ")

		t("0 -1 0 -1 D=", expected: "TRUE ")
		t("0 -1 0  0 D=", expected: "FALSE")
		t("0 -1 0  1 D=", expected: "FALSE")
		t("0  0 0 -1 D=", expected: "FALSE")
		t("0  0 0  0 D=", expected: "TRUE ")
		t("0  0 0  1 D=", expected: "FALSE")
		t("0  1 0 -1 D=", expected: "FALSE")
		t("0  1 0  0 D=", expected: "FALSE")
		t("0  1 0  1 D=", expected: "TRUE ")

		t("MAX-2INT MIN-2INT D=", expected: "FALSE")
		t("MAX-2INT 0. D=", expected: "FALSE")
		t("MAX-2INT MAX-2INT D=", expected: "TRUE")
		t("MAX-2INT HI-2INT  D=", expected: "FALSE")
		t("MAX-2INT MIN-2INT D=", expected: "FALSE")
		t("MIN-2INT MIN-2INT D=", expected: "TRUE")
		t("MIN-2INT LO-2INT  D= ", expected: "FALSE")
		t("MIN-2INT MAX-2INT D=", expected: "FALSE")

	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING 2LITERAL 2VARIABLE
//
	func test2Literal2Variable()
	{
		t(": CD3 [ MAX-2INT ] 2LITERAL ;", expected: "") 
		t("CD3", expected: "MAX-2INT ")
		t("2VARIABLE 2V1", expected: "")
		t("0. 2V1 2!", expected: "")
		t("2V1 2@", expected: "0. ")
		t("-1 -2 2V1 2!", expected: "")
		t("2V1 2@", expected: "-1 -2 ")
		t(": CD4 2VARIABLE ;", expected: "")
		t("CD4 2V2", expected: "")
		t(": CD5 2V2 2! ;", expected: "")
		t("-2 -1 CD5", expected: "")
		t("2V2 2@", expected: "-2 -1 ")
		t("2VARIABLE 2V3 IMMEDIATE 5 6 2V3 2!", expected: "")
		t("2V3 2@", expected: "5 6 ")
		t(": CD7 2V3 [ 2@ ] 2LITERAL ; CD7", expected: "5 6 ")
		t(": CD8 [ 6 7 ] 2V3 [ 2! ] ; 2V3 2@", expected: "6 7 ")
	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING DMAX DMIN

	func testDMaxDMin()
	{
		t(" 1.  2. DMAX", expected: "2.")
		t(" 1.  0. DMAX", expected: "1.")
		t(" 1. -1. DMAX", expected: "1.")
		t(" 1.  1. DMAX", expected: "1.")
		t(" 0.  1. DMAX", expected: "1.")
		t(" 0. -1. DMAX", expected: "0.")
		t("-1.  1. DMAX", expected: "1.")
		t("-1. -2. DMAX", expected: "-1.")

		t("MAX-2INT HI-2INT  DMAX", expected: "MAX-2INT")
		t("MAX-2INT MIN-2INT DMAX", expected: "MAX-2INT")
		t("MIN-2INT MAX-2INT DMAX", expected: "MAX-2INT")
		t("MIN-2INT LO-2INT  DMAX", expected: "LO-2INT ")

		t("MAX-2INT  1. DMAX", expected: "MAX-2INT")
		t("MAX-2INT -1. DMAX", expected: "MAX-2INT")
		t("MIN-2INT  1. DMAX", expected: " 1.")
		t("MIN-2INT -1. DMAX", expected: "-1.")


		t(" 1.  2. DMIN", expected: " 1.")
		t(" 1.  0. DMIN", expected: " 0.")
		t(" 1. -1. DMIN", expected: "-1.")
		t(" 1.  1. DMIN", expected: " 1.")
		t(" 0.  1. DMIN", expected: " 0.")
		t(" 0. -1. DMIN", expected: "-1.")
		t("-1.  1. DMIN", expected: "-1.")
		t("-1. -2. DMIN", expected: "-2.")

		t("MAX-2INT HI-2INT  DMIN", expected: "HI-2INT ")
		t("MAX-2INT MIN-2INT DMIN", expected: "MIN-2INT")
		t("MIN-2INT MAX-2INT DMIN", expected: "MIN-2INT")
		t("MIN-2INT LO-2INT  DMIN", expected: "MIN-2INT")

		t("MAX-2INT  1. DMIN", expected: " 1.")
		t("MAX-2INT -1. DMIN", expected: "-1.")
		t("MIN-2INT  1. DMIN", expected: "MIN-2INT")
		t("MIN-2INT -1. DMIN", expected: "MIN-2INT")
	}
//
//
//	\ ------------------------------------------------------------------------------
//	TESTING D>S DABS

	func testDtoSDabs()
	{
		t(" 1234  0 D>S", expected: " 1234")
		t("-1234 -1 D>S", expected: "-1234")
		t("MAX-INTD  0 D>S", expected: "MAX-INTD")
		t("MIN-INTD -1 D>S", expected: "MIN-INTD")

		t(" 1. DABS", expected: "1.")
		t("-1. DABS", expected: "1.")
		t("MAX-2INT DABS", expected: "MAX-2INT")
		t("MIN-2INT 1. D+ DABS", expected: "MAX-2INT")
	}
//
//
//	\ ------------------------------------------------------------------------------
//	TESTING M+ M*/

	func testMPlusMStarSlash()
	{
		t("HI-2INT   1 M+", expected: "HI-2INT   1. D+")
		t("MAX-2INT -1 M+", expected: "MAX-2INT -1. D+")
		t("MIN-2INT  1 M+", expected: "MIN-2INT  1. D+")
		t("LO-2INT  -1 M+", expected: "LO-2INT  -1. D+")

		predefine(#"""
		\ To correct the result if the division is floored, only used when
		\ necessary i.e. negative quotient and remainder <> 0

		: ?FLOORED [ -3 2 / -2 = ] LITERAL IF 1. D- THEN ;
		"""#)

		t(" 5.  7 11 M*/", expected: " 3.")
		t(" 5. -7 11 M*/", expected: "-3. ?FLOORED")    // FLOORED -4.
		t("-5.  7 11 M*/", expected: "-3. ?FLOORED")    // FLOORED -4.
		t("-5. -7 11 M*/", expected: " 3.")
		t("MAX-2INT  8 16 M*/", expected: "HI-2INT")
		t("MAX-2INT -8 16 M*/", expected: "HI-2INT DNEGATE ?FLOORED")  // FLOORED SUBTRACT 1
		t("MIN-2INT  8 16 M*/", expected: "LO-2INT")
		t("MIN-2INT -8 16 M*/", expected: "LO-2INT DNEGATE")
		t("MAX-2INT MAX-INTD MAX-INTD M*/", expected: "MAX-2INT")
		t("MAX-2INT MAX-INTD 2/ MAX-INTD M*/", expected: "MAX-INTD 1- HI-2INT NIP")
		t("MIN-2INT LO-2INT NIP 1+ DUP 1- NEGATE M*/", expected: "0 MAX-INTD 1-")
		t("MIN-2INT LO-2INT NIP 1- MAX-INTD M*/", expected: "MIN-INTD 3 + HI-2INT NIP 2 +")
		t("MAX-2INT LO-2INT NIP DUP NEGATE M*/", expected: "MAX-2INT DNEGATE")
		t("MIN-2INT MAX-INTD DUP M*/", expected: "MIN-2INT")
	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING D. D.R

	func testDDotDDotR()
	{
		//	Create some large double numbers
		predefine(#"""
			MAX-2INT 71 73 M*/ 2CONSTANT DBL1
			MIN-2INT 73 79 M*/ 2CONSTANT DBL2
		
			: D>ASCII  ( D -- CADDR U )
			   DUP >R <# DABS #S R> SIGN #>    ( -- CADDR1 U )
			   HERE SWAP 2DUP 2>R CHARS DUP ALLOT MOVE 2R>
			;
		
			DBL1 D>ASCII 2CONSTANT "DBL1"
			DBL2 D>ASCII 2CONSTANT "DBL2"
		
			: DOUBLEOUTPUT
			   CR ." You should see lines duplicated:" CR
			   5 SPACES "DBL1" TYPE CR
			   5 SPACES DBL1 D. CR
			   8 SPACES "DBL1" DUP >R TYPE CR
			   5 SPACES DBL1 R> 3 + D.R CR
			   5 SPACES "DBL2" TYPE CR
			   5 SPACES DBL2 D. CR
			   10 SPACES "DBL2" DUP >R TYPE CR
			   5 SPACES DBL2 R> 5 + D.R CR
			;
		"""#)

		t("DOUBLEOUTPUT", expected: "")
	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING 2ROT DU< (Double Number extension words)
//
	func test2RotDuLess()
	{
		t("1. 2. 3. 2ROT", expected: "2. 3. 1.")
		t("MAX-2INT MIN-2INT 1. 2ROT", expected: "MIN-2INT 1. MAX-2INT")

		t(" 1.  1. DU<", expected: "FALSE")
		t(" 1. -1. DU<", expected: "TRUE ")
		t("-1.  1. DU<", expected: "FALSE")
		t("-1. -2. DU<", expected: "FALSE")
		t("0 1   1. DU<", expected: "FALSE")
		t("1.  0 1  DU<", expected: "TRUE ")
		t("0 -1 1 -2 DU<", expected: "FALSE")
		t("1 -2 0 -1 DU<", expected: "TRUE ")

		t("MAX-2INT HI-2INT  DU<", expected: "FALSE")
		t("HI-2INT  MAX-2INT DU<", expected: "TRUE ")
		t("MAX-2INT MIN-2INT DU<", expected: "TRUE")
		t("MIN-2INT MAX-2INT DU<", expected: "FALSE")
		t("MIN-2INT LO-2INT  DU<", expected: "TRUE")
	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING 2VALUE
//
	func test2Value()
	{
		#warning("2value tests omitted until 2value implemented")
//		t("1111 2222 2VALUE 2VAL", expected: "")
//		t("2VAL", expected: "1111 2222")
//		t("3333 4444 TO 2VAL", expected: "")
//		t("2VAL", expected: "3333 4444")
//		t(": TO-2VAL TO 2VAL ; 5555 6666 TO-2VAL", expected: "")
//		t("2VAL", expected: "5555 6666")
	}
//
//	\ ------------------------------------------------------------------------------
//
//	DOUBLE-ERRORS SET-ERROR-COUNT
//
//	CR .( End of Double-Number word tests) CR


}
