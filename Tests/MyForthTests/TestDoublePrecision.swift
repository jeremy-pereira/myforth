//
//  TestDoublePrecision.swift
//  
//
//  Created by Jeremy Pereira on 20/06/2020.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
import Toolbox
@testable import MyForth

final class TestDoublePrecision: XCTestCase
{
	func testDiv2()
	{
		let number = DoublePrecision<Int>(tuple: (high: 0, low: UInt.max - 1))
		do
		{
			let result = try number.divided(by: 2)
			XCTAssert(result.quotient == Int((UInt.max - 1) / 2))
			XCTAssert(result.remainder == 0)
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testGeq0()
	{
		let number = DoublePrecision<Int>(tuple: (high: 0, low: UInt.max - 1))
		XCTAssert(number >= DoublePrecision<Int>(0))
	}

    static var allTests =
	[
        ("testDiv2", testDiv2),
		("testGeq0", testGeq0),
    ]

}
