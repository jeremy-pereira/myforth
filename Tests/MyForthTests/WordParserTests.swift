//
//  WordParserTests.swift
//  
//
//  Created by Jeremy Pereira on 06/06/2020.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
@testable import MyForth
import Toolbox

private let log = Logger.getLogger("MyForthTests.WordParserTests")

final class WordParserTests: XCTestCase
{
    func testSimpleParse() throws
	{
		let parser = InputParser(startState: InputParser.State.normal, absoluteAddress: 0)
		parser.inputSource = try Forth.InputSource("foo  bar\nbaz", sourceId: 0)
		XCTAssert(try parser.refill(), "Failed to refill the buffer")
		if let word = try parser.next()
		{
			XCTAssert(word == "foo")
			XCTAssert(word == "FOO")
		}
		else
		{
			XCTFail("No foo") ; return
		}
		if let word = try parser.next()
		{
			XCTAssert(word == "bar")
		}
		else
		{
			XCTFail("No bar") ; return
		}
		XCTAssert(try parser.refill(), "Failed to refill the buffer")
		if let word = try parser.next()
		{
			XCTAssert(word == "baz")
		}
		else
		{
			XCTFail("No baz") ; return
		}
    }

	func testPredefinedWords() throws
	{
		let testSource = try Forth.InputSource(ThreadedForth.predefinedWords(), sourceId: 0)
		let parser = InputParser(startState: .normal, absoluteAddress: 0)
		parser.inputSource = testSource
		while try parser.refill()
		{
			while let aWord = try parser.next()
			{
				log.debug("Parsed \(aWord)")
				guard aWord.string != "" else
				{
					XCTFail("Got an empty word")
					break
				}
			}
		}
		switch parser.state.state
		{
		case .normal:
			break
		default:
			XCTFail("Parser should be in state .normal, but it is in state \(parser.state.state)")
		}
	}

	func testLineWithCR() throws
	{
		log.pushLevel(.debug)
		defer { log.popLevel() }

		let string = """
		;
		"""
		let parser = InputParser(startState: .normal, absoluteAddress: 0)
		parser.inputSource = try Forth.InputSource(string, sourceId: 0)
		XCTAssert(try parser.refill(), "Failed to refill the parser buffer")
		while let aWord = try parser.next()
		{
			log.debug("Parsed \(aWord)")
			guard aWord.string != "" else
			{
				XCTFail("Got an empty word")
				break
			}
		}
	}


	func testLineBasedComment() throws
	{
		let string = "ignore ignore\nread this"
		let parser = InputParser(startState: .comment("\n"), absoluteAddress: 0)
		parser.inputSource = try Forth.InputSource(string, sourceId: 0)
		var wordCount = 0
		while try parser.refill()
		{
			while let aWord = try parser.next()
			{
				wordCount += 1
				log.debug("Parsed \(aWord)")
				guard aWord.string != "ignore" else
				{
					XCTFail("Got word we should ignore")
					break
				}
			}
		}
		switch parser.state.state
		{
		case .normal:
			break
		default:
			XCTFail("Parser should be in state .normal, but it is in state \(parser.state.state)")
		}
		XCTAssert(wordCount == 3) // ) read this
	}


    static var allTests = [
        ("testSimpleParse", testSimpleParse),
    ]
}
