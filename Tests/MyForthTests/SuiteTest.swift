//
//  SuiteTest.swift
//  
//
//  Created by Jeremy Pereira on 16/06/2020.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
import MyForth
import Foundation
import Toolbox

private let log = Logger.getLogger("MyForthTests.SuiteTest")


/// A class from which all tests derived from the Forth test suite can inherit
///
/// The [Forth test suite](https://github.com/gerryjackson/forth2012-test-suite)
/// provides useful coverage for most ANS compliant functions. This class is
/// designed to make it easy to convert tests into Swift test cases.
///
/// The Forth test suite creates some definitions as it goes along and each set
/// of tests may use previously defined words. The Swift test suite differs in
/// that new Forth machines are created for each set of tests. So some definitions
/// need to be applied for each test that needs them.
///
/// There are two Forth machines: a test machine and an expect machine. They are
/// created anew before each Swift test function and set to `hex` mode.
/// Additionally, a word is defined that clears the stack on each machine and
/// the test machine's trace flag is enabled.
///
/// At the start of each Forth test `t(_, expected:,debug:,...)`the stacks of
/// both machines are cleared down. The script is run on the test machine and
/// the expected script is run on the expect machine. The stacks of the two
/// machines are then compared for equality. The test fails if the stacks are
/// not identical.
public class SuiteTest: XCTestCase
{
	var testMachines: [ForthProtocol & ForthTesting] = []
	private var expectMachines: [ForthProtocol] = []

	/// Set up for running a test
	///
	/// This runs before each test and sets up some common word definitions. If
	/// you override it, you must call the super implementation.
	///
	/// The test machine is set up with trace enabled, which means that you
	/// might see a lot of output if you also enable debug.
	public override func setUp()
	{
		log.pushLevel(.info)
		testMachines = [ThreadedForth(trace: true)]
		expectMachines = [ThreadedForth()]
		predefine("""
		hex
		: clearstack {stackDataEmpty} ;
		: 1s 0 invert ; ( all bits set to 1 )
		: 0s 0 ;  ( all bits set to 0 )
		1S 1 RSHIFT INVERT CONSTANT MSB
		""")
	}

	public override func tearDown()
	{
		log.popLevel()
	}

	/// Get the definition in the test machine for the given word.
	///
	/// - Parameter word: The word whose definition we want
	/// - Returns: A string representation of the words in the definition
	func definitionInTest(machine: ForthProtocol & ForthTesting, for word: String) -> String?
	{
		return machine.dumpDefinition(for: word)
	}

	/// Run a test
	///
	/// - Parameters:
	///   - script: The test script to run
	///   - stdIn: An optional string that represents input to stdin
	///   - expected: The expected content of the stack
	///   - expectedOutput: Optional string containing the expected output of
	///                     the test machine while running the test. Defaults
	///                     to `nil` and, if `nil` the output will be ignored
	///   - debug: set to `true` if you want trace output from the test machine.
	///            Defaults to `false`
	///   - file: The file the test is in - default to `#file`
	///   - line: The line number the test is o, defaults to `#line`
	///   - function: The  function that called this test, defaults to `#function`
	func t(_ script: String,
		   stdIn: String? = nil,
		   expected: String,
		   expectedOutput: String? = nil,
		      debug: Bool = false,
			   file: String = #file, line: Int = #line, function: String = #function)
	{
		log.info("=== Test script: \(script) ===")
		if debug
		{
			Logger.pushLevel(.debug, forName: "MyForth.ThreadedForth")
		}
		defer
		{
			if debug
			{
				Logger.popLevel(forName: "MyForth.ThreadedForth")
			}
		}
		let fileURL = URL(fileURLWithPath: file)
		let location = "\(fileURL.lastPathComponent)(\(line)) \(function)"
		do
		{
			for var (machine, expectMachine) in zip(testMachines, expectMachines)
			{
				var output = ""
				var expectedStackOutput = ""
				try expectMachine.interpret(input: "clearstack", output: &output)
				try expectMachine.interpret(input: expected, output: &output)
				try expectMachine.interpret(input: ".s", output: &expectedStackOutput)
				do
				{
					try machine.interpret(input: "clearstack", output: &output)
					if let stdIn = stdIn
					{
						machine.stdin = AnyIterator(stdIn.utf8.makeIterator())
					}
					try machine.interpret(input: script, sourceId: 1, output: &output)
					var actualStackOutput = ""
					try machine.interpret(input: ".s", output: &actualStackOutput)
					XCTAssert(actualStackOutput == expectedStackOutput, "\(location): \(type(of: machine)) Expected stack '\(expectedStackOutput)', got '\(actualStackOutput)'")
					if let expectedOutput = expectedOutput
					{
						XCTAssert(output == expectedOutput, "\(location): \(type(of: machine)) Expected output '\(expectedOutput)', got '\(output)'")
					}
				}
				catch
				{
					XCTFail("\(location): \(type(of: machine)) Error thrown: \(error)")
				}
			}
		}
		catch
		{
			XCTFail("\(location): Error thrown: \(error)")
		}
	}

	/// Run a script to put the test Forth machine into a predefined state
	///
	/// Use this to put definitions into the test Forth machine. If there are
	/// any errors, a fatalError occurs since they are considered to be
	/// programming errors, not test failures.
	/// - Parameter string: The definitions to run
	func predefine(_ string: String)
	{
		do
		{
			var output = ""
			for var (machine, expectMachine) in zip(testMachines, expectMachines)
			{
				do
				{
					try machine.interpret(input: string, output: &output)
				}
				catch
				{
					XCTFail("\(type(of: machine)) \(error)")
				}
				try expectMachine.interpret(input: string, output: &output)
			}
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	static let utilitiesDefs = #"""
DECIMAL

VARIABLE (\?) 0 (\?) !     ( Flag: Word defined = 0 | word undefined = -1 )

: [?DEF]  ( "name" -- )
   BL WORD FIND SWAP DROP 0= (\?) !
;

\ Buffer for strings in interpretive mode since S" only valid in compilation
\ mode when File-Access word set is defined

64 CONSTANT SBUF-SIZE
CREATE SBUF1 SBUF-SIZE CHARS ALLOT
CREATE SBUF2 SBUF-SIZE CHARS ALLOT

\ ($") saves a counted string at (caddr)
: ($")  ( caddr "ccc" -- caddr' u )
   [CHAR] " PARSE ROT 2DUP C!       ( -- ca2 u2 ca)
   CHAR+ SWAP 2DUP 2>R CHARS MOVE   ( -- )  ( R: -- ca' u2 )
   2R>
;

: $"   ( "ccc" -- caddr u )  SBUF1 ($") ;
: $2"  ( "ccc" -- caddr u )  SBUF2 ($") ;
: $CLEAR  ( caddr -- ) SBUF-SIZE BL FILL ;
: CLEAR-SBUFS  ( -- )  SBUF1 $CLEAR SBUF2 $CLEAR ;

"""#
}

