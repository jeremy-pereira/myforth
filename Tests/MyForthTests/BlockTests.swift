//
//  BlockTests.swift
//  
//
//  Created by Jeremy Pereira on 30/11/2023.
//
//  Copyright (c) Jeremy Pereira 2023
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
import Toolbox

private let log = Logger.getLogger("MyForthTests.BlockTests")

///	To test the ANS Forth Block word set and extension words
///
///	This test case is based o a program written by Steve Palmer in 2015, with
///	contributions from others where indicated. The original code bore the
///	following notice:
///
///	>This program is distributed in the hope that it will be useful,
/// >but WITHOUT ANY WARRANTY; without even the implied warranty of
/// >MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
///
/// The tests are not claimed to be comprehensive or correct
///
final class BlockTests: SuiteTest
{
	override class func setUp() 
	{
		super.setUp()
		let currentDirectory = FileManager.default.currentDirectoryPath
		log.level = .info
		log.info("\n====\nDirectory: \(currentDirectory)\n====")
	}
	override func setUp()
	{
		super.setUp()
		predefine(SuiteTest.utilitiesDefs)
		predefine(#"""
BASE @ 2 BASE ! -1 0 <# #S #> SWAP DROP CONSTANT BITS/CELL BASE !

  \ Equivalents of [IF] [ELSE] [THEN], these must not be nested
  : [?IF]  ( f -- )  (\?) ! ; IMMEDIATE
  : [?ELSE]  ( -- )  (\?) @ 0= (\?) ! ; IMMEDIATE
  : [?THEN]  ( -- )  0 (\?) ! ; IMMEDIATE
  ( \? is a conditional comment )
  : \?  ( "..." -- )  (\?) @ IF EXIT THEN SOURCE >IN ! DROP ; IMMEDIATE

 \ Define these constants from the system documentation provided.
 \ WARNING: The contents of the test blocks will be destroyed by this test.
 \ The blocks tested will be in the range
 \    FIRST-TEST-BLOCK <= u < LIMIT-TEST-BLOCK
 \ The tests need at least 2 test blocks in the range to complete.
 \ jforth caches 16 blocks by default. Make sure we have more than that
 20 CONSTANT FIRST-TEST-BLOCK
 40 CONSTANT LIMIT-TEST-BLOCK \ one beyond the last

 FIRST-TEST-BLOCK LIMIT-TEST-BLOCK U< 0= [?IF]
 \?  .( Error: Test Block range not identified ) CR ABORT
 [?THEN]

 LIMIT-TEST-BLOCK FIRST-TEST-BLOCK - CONSTANT TEST-BLOCK-COUNT
 TEST-BLOCK-COUNT 2 U< [?IF]
 \?  .( Error: At least 2 Test Blocks are required to run the tests ) CR ABORT
 [?THEN]

 \ The block tests make extensive use of random numbers to select blocks to test
 \ and to set the contents of the block.  It also makes use of a Hash code to
 \ ensure the integrity of the blocks against unexpected changes.

 \ == Memory Walk tools ==

 : @++ ( a-addr -- a-addr+4 a-addr@ )
	 DUP CELL+ SWAP @ ;

 : !++ ( x a-addr -- a-addr+4 )
	 TUCK ! CELL+ ;

 : C@++ ( c-addr -- c-addr;char+ c-addr@ )
	 DUP CHAR+ SWAP C@ ;

 : C!++ ( char c-addr -- c-addr+1 )
	 TUCK ! CHAR+ ;

 \ == Random Numbers ==
 \ Based on "Xorshift" PRNG wikipedia page
 \ reporting on results by George Marsaglia
 \ https://en.wikipedia.org/wiki/Xorshift
 \ Note: THIS IS NOT CRYPTOGRAPHIC QUALITY

 : PRNG
	 CREATE ( "name" -- )
		 4 CELLS ALLOT
	 DOES> ( -- prng )
 ;

 : PRNG-ERROR-CODE ( prng -- errcode | 0 )
	 0 4 0 DO          \ prng acc
		 >R @++ R> OR  \ prng acc'
	 LOOP              \ prng xORyORzORw
	 NIP 0= ;          \ xORyORzORw=0

 : PRNG-COPY ( src-prng dst-prng -- )
	 4 CELLS MOVE ;

 : PRNG-SET-SEED ( prng w z y x -- )
	 4 PICK                 \ prng w z y x prng
	 4 0 DO !++ LOOP DROP   \ prng
	 DUP PRNG-ERROR-CODE IF \ prng
		 1 OVER +!          \ prng
	 THEN                   \ prng
	 DROP ;                 \

 BITS/CELL 64 = [?IF]
 \?  : PRNG-RND ( prng -- rnd )
 \?      DUP @
 \?      DUP 21 LSHIFT XOR
 \?      DUP 35 RSHIFT XOR
 \?      DUP  4 LSHIFT XOR
 \?      TUCK SWAP ! ;
 [?THEN]

 BITS/CELL 32 = [?IF]
 \?  : PRNG-RND ( prng -- rnd )
 \?      DUP @                            \ prng x
 \?      DUP 11 LSHIFT XOR                \ prng t=x^(x<<11)
 \?      DUP 8 RSHIFT XOR                 \ prng t'=t^(t>>8)
 \?      OVER DUP CELL+ SWAP 3 CELLS MOVE \ prng t'
 \?      OVER 3 CELLS + @                 \ prng t' w
 \?      DUP 19 RSHIFT XOR                \ prng t' w'=w^(w>>19)
 \?      XOR                              \ prng rnd=w'^t'
 \?      TUCK SWAP 3 CELLS + ! ;          \ rnd
 [?THEN]

 BITS/CELL 16 = [?IF]
 \?  .( === NOT TESTED === )
 \?  \ From http://b2d-f9r.blogspot.co.uk/2010/08/16-bit-xorshift-rng-now-with-more.html
 \?  : PRNG-RND ( prng -- rnd )
 \?      DUP @                        \ prng x
 \?      DUP 5 LSHIFT XOR             \ prng t=x^(x<<5)
 \?      DUP 3 RSHIFT XOR             \ prng t'=t^(t>>3)
 \?      OVER DUP CELL+ @ TUCK SWAP ! \ prng t' y
 \?      DUP 1 RSHIFT XOR             \ prng t' y'=y^(y>>1)
 \?      XOR                          \ prng rnd=y'^t'
 \?      TUCK SWAP CELL+ ! ;          \ rnd
 [?THEN]

 [?DEF] PRNG-RND
 \?  .( You need to add a Psuedo Random Number Generator for your cell size: )
 \?  BITS/CELL U. CR
 \?  ABORT
 [?THEN]

 : PRNG-RANDOM ( lower upper prng -- rnd )
	 >R OVER - R> PRNG-RND UM* NIP + ;
 \ PostCondition: T{ lower upper 2DUP 2>R prng PRNG-RANDOM 2R> WITHIN -> TRUE }T

 PRNG BLOCK-PRNG
 \ Generated by Random.org
 BLOCK-PRNG -1865266521 188896058 -2021545234 -1456609962 PRNG-SET-SEED
 : BLOCK-RND ( -- rnd )                BLOCK-PRNG PRNG-RND ;
 : BLOCK-RANDOM ( lower upper -- rnd ) BLOCK-PRNG PRNG-RANDOM ;

 : RND-TEST-BLOCK ( -- blk )
	 FIRST-TEST-BLOCK LIMIT-TEST-BLOCK BLOCK-RANDOM ;
 \ PostCondition: T{ RND-TEST-BLOCK FIRST-TEST-BLOCK LIMIT-TEST-BLOCK WITHIN -> TRUE }T

 \ Two distinct random test blocks
 : 2RND-TEST-BLOCKS ( -- blk1 blk2 )
	 RND-TEST-BLOCK BEGIN  \ blk1
		 RND-TEST-BLOCK    \ blk1 blk2
		 2DUP =            \ blk1 blk2 blk1==blk2
	 WHILE                 \ blk1 blk1
		 DROP              \ blk1
	 REPEAT ;              \ blk1 blk2
 \ PostCondition: T{ 2RND-TEST-BLOCKS = -> FALSE }T

 \ first random test block in a sequence of length u
 : RND-TEST-BLOCK-SEQ ( u -- blks )
	 FIRST-TEST-BLOCK LIMIT-TEST-BLOCK ROT 1- - BLOCK-RANDOM ;

 \ I'm not sure if this algorithm is correct if " 1 CHARS 1 <> ".
 : ELF-HASH-ACCUMULATE ( hash c-addr u -- hash )
	 >R SWAP R> 0 DO                          \ c-addr h
		 4 LSHIFT                             \ c-addr h<<=4
		 SWAP C@++ ROT +                      \ c-addr' h+=*s
		 DUP [ HEX ] F0000000 [ DECIMAL ] AND \ c-addr' h high=h&0xF0000000
		 DUP IF                               \ c-addr' h high
			 DUP >R 24 RSHIFT XOR R>          \ c-addr' h^=high>>24 high
		 THEN                                 \ c-addr' h high
		 INVERT AND                           \ c-addr' h&=~high
	 LOOP NIP ;

 : ELF-HASH ( c-addr u -- hash )
	 0 ROT ROT ELF-HASH-ACCUMULATE ;

	1024 8 * BITS/CELL / CONSTANT CELLS/BLOCK
  : BLANK-BUFFER ( blk -- blk-addr )
	  0 {setTrace} >r
	  BUFFER DUP 1024 BL FILL
	  r> {setTrace} drop
	;

: 2= ( x1 x2 x3 x4 -- flag )
	ROT = ROT ROT = AND ;

  : WRITE-BLOCK ( blk c-addr u -- )
	  ROT BLANK-BUFFER SWAP CHARS
	  MOVE
	  UPDATE FLUSH ;

  \ blk: u; blk LOAD
  : TL1 ( u blk -- )
	  SWAP 0 <# #S #> WRITE-BLOCK ;

  : WRITE-AT-END-OF-BLOCK ( blk c-addr u -- )
	  ROT BLANK-BUFFER
	  OVER 1024 SWAP - CHARS +
	  SWAP CHARS MOVE UPDATE FLUSH ;

  : PREPARE-RND-BLOCK ( hash blk -- hash' )
	  BUFFER DUP                     \ hash blk-addr blk-addr
	  CELLS/BLOCK 0 DO               \ hash blk-addr blk-addr[i]
		  BLOCK-RND OVER ! CELL+     \ hash blk-addr blk-addr[i+1]
   LOOP DROP                      \ hash blk-addr
   1024 ELF-HASH-ACCUMULATE ;     \ hash'

   : TUF2 ( xt blk1 blk2 -- hash1'' hash2'' hash1' hash2' hash1 hash2 )
	   OVER BUFFER OVER BUFFER = IF             \ test needs 2 distinct buffers
		   2DROP DROP 0 0 0 0 0 0               \ Dummy result
	   ELSE
		   OVER 0 SWAP PREPARE-RND-BLOCK UPDATE \ xt blk1 blk2 hash1
		   OVER 0 SWAP PREPARE-RND-BLOCK UPDATE \ xt blk1 blk2 hash1 hash2
		   2>R                                  \ xt blk1 blk2
		   FLUSH                                \ xt blk1 blk2
		   OVER 0 SWAP PREPARE-RND-BLOCK        \ xt blk1 blk2 hash1'
		   OVER 0 SWAP PREPARE-RND-BLOCK        \ xt blk1 blk2 hash1' hash2'
		   2>R                                  \ xt blk1 blk2
		   ROT EXECUTE                          \ blk1 blk2
		   FLUSH                                \ blk1 blk2
		   SWAP BLOCK 1024 ELF-HASH             \ blk2 hash1''
		   SWAP BLOCK 1024 ELF-HASH             \ hash1'' hash2''
		   2R> 2R> \ hash1'' hash2'' hash1' hash2' hash1 hash2
	   THEN ;
  : TUF2-1 ( blk1 blk2 -- blk1 blk2 )     \ update blk1 only
	  OVER BLOCK DROP UPDATE ;
  : TUF2-2 ( blk1 blk2 -- blk1 blk2 )     \ update blk2 only
	  DUP BUFFER DROP UPDATE ;

  1S CONSTANT 				<TRUE>
  0  CONSTANT 				<FALSE>
  : S=  \ ( ADDR1 C1 ADDR2 C2 -- T/F ) COMPARE TWO STRINGS.
	  >R SWAP R@ = IF         \ MAKE SURE STRINGS HAVE SAME LENGTH
		  R> ?DUP IF         \ IF NON-EMPTY STRINGS
	  0 DO
		  OVER C@ OVER C@ - IF 2DROP <FALSE> UNLOOP EXIT THEN
		  SWAP CHAR+ SWAP CHAR+
			  LOOP
		  THEN
		  2DROP <TRUE>         \ IF WE GET HERE, STRINGS MATCH
	  ELSE
		  R> DROP 2DROP <FALSE>      \ LENGTHS MISMATCH
	  THEN ;

"""#)

	}

//
//	\ ------------------------------------------------------------------------------
//	TESTING BLOCK ( read-only mode )

	func testBlockReadOnly()
	{
		
		// BLOCK signature
		t("RND-TEST-BLOCK BLOCK DUP ALIGNED =", expected: "TRUE")

		// BLOCK accepts all blocks in the test range
		predefine("""
			: BLOCK-ALL ( blk2 blk1 -- )
				DO
					I BLOCK DROP
				LOOP ;
		""")
		t("LIMIT-TEST-BLOCK FIRST-TEST-BLOCK BLOCK-ALL", expected: "")

		// BLOCK twice on same block returns the same value
		t("RND-TEST-BLOCK DUP BLOCK SWAP BLOCK =", expected: "TRUE")

		// BLOCK twice on distinct block numbers may or may not return the same
		// value!
		// Nothing to test
	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING BUFFER ( read-only mode )
//
	func testBufferReadOnly()
	{
		// Although it is not in the spirit of the specification,
		// a compliant definition of BUFFER would be
		// : BUFFER BLOCK ;
		// So we can only repeat the tests for BLOCK ...

		// BUFFER signature
		t("RND-TEST-BLOCK BUFFER DUP ALIGNED =", expected: "TRUE")

		// BUFFER accepts all blocks in the test range
		predefine("""
		: BUFFER-ALL ( blk2 blk1 -- )
			DO
				I BUFFER DROP
			LOOP ;
		""")
		t("LIMIT-TEST-BLOCK FIRST-TEST-BLOCK BUFFER-ALL", expected: "")

		// BUFFER twice on the same block returns the same value
		t("RND-TEST-BLOCK DUP BUFFER SWAP BUFFER =", expected: "TRUE")

		// BUFFER twice on distinct block numbers
		// may or may not return the same value!
		// Nothing to test

		// Combinations with BUFFER
		t("RND-TEST-BLOCK DUP BLOCK SWAP BUFFER =", expected: "TRUE")
		t("RND-TEST-BLOCK DUP BUFFER SWAP BLOCK =", expected: "TRUE")

	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING Read and Write access with UPDATE and FLUSH
//
	func testReadWriteUpdateFlush()
	{
		//	\ Ideally, we'd like to be able to test the persistence across power cycles
		//	\ of the writes, but we can't do that in a simple test.
		//	\ The tests below could be fooled by a large buffers store and a tricky FLUSH
		//	\ but what else are you going to do?
		//
		//	\ Signatures
			t("RND-TEST-BLOCK BLOCK DROP UPDATE", expected: "")
			t("FLUSH", expected: "")
		predefine("""
			: BLANK-BUFFER ( blk -- blk-addr )
				BUFFER DUP 1024 BL FILL ;
		""")
		
		// Test R/W of a Simple Blank Random Block
		t(#"""
		RND-TEST-BLOCK                 \ blk
		   DUP BLANK-BUFFER               \ blk blk-addr1
		   1024 ELF-HASH                  \ blk hash
		   UPDATE FLUSH                   \ blk hash
		   SWAP BLOCK                     \ hash blk-addr2
		   1024 ELF-HASH =
		"""#, expected: "TRUE")

	// Boundary Test: Modify first character
		t(#"""
		RND-TEST-BLOCK                  \ blk
		   DUP BLANK-BUFFER                \ blk blk-addr1
		   CHAR \ OVER C!                  \ blk blk-addr1
		   1024 ELF-HASH                   \ blk hash
		   UPDATE FLUSH                    \ blk hash
		   SWAP BLOCK                      \ hash blk-addr2
		   1024 ELF-HASH =
		"""#, expected: "TRUE")

	// Boundary Test: Modify last character
		t(#"""
		RND-TEST-BLOCK                  \ blk
			DUP BLANK-BUFFER                \ blk blk-addr1
		   CHAR \ OVER 1023 CHARS + C!     \ blk blk-addr1
		   1024 ELF-HASH                   \ blk hash
		   UPDATE FLUSH                    \ blk hash
		   SWAP BLOCK                      \ hash blk-addr2
		   1024 ELF-HASH =
		"""#, expected: "TRUE")
		//
		//	Boundary Test: First and Last (and all other) blocks in the test range
		//	1024 8 * BITS/CELL / CONSTANT CELLS/BLOCK
		//
		predefine(#"""
			: PREPARE-RND-BLOCK ( hash blk -- hash' )
				BUFFER DUP                     \ hash blk-addr blk-addr
				CELLS/BLOCK 0 DO               \ hash blk-addr blk-addr[i]
					BLOCK-RND OVER ! CELL+     \ hash blk-addr blk-addr[i+1]
				LOOP DROP                      \ hash blk-addr
				1024 ELF-HASH-ACCUMULATE ;     \ hash'
		
			: WRITE-RND-BLOCKS-WITH-HASH ( blk2 blk1 -- hash )
				0 ROT ROT DO                   \ hash
					I PREPARE-RND-BLOCK UPDATE \ hash'
				LOOP ;                         \ hash'
		
			: READ-BLOCKS-AND-HASH ( blk2 blk1 -- hash )
				0 ROT ROT DO                         \ hash(i)
					I BLOCK 1024 ELF-HASH-ACCUMULATE \ hash(i+1)
				LOOP ;                               \ hash
		"""#)
		
		t("""
		LIMIT-TEST-BLOCK FIRST-TEST-BLOCK WRITE-RND-BLOCKS-WITH-HASH FLUSH
			   LIMIT-TEST-BLOCK FIRST-TEST-BLOCK READ-BLOCKS-AND-HASH =
		""", expected: "TRUE")
		predefine(#"""
			: TUF1 ( xt blk -- hash )
				DUP BLANK-BUFFER               \ xt blk blk-addr1
				1024 ELF-HASH                  \ xt blk hash
				ROT EXECUTE                    \ blk hash
				SWAP BLOCK                     \ hash blk-addr2
				1024 ELF-HASH = ;              \ TRUE
		
			\ Double UPDATE make no difference
			: TUF1-1 ( -- ) UPDATE UPDATE FLUSH ;
		"""#)
		t("' TUF1-1 RND-TEST-BLOCK TUF1", expected: "TRUE")
		//
		//	Double FLUSH make no difference
		predefine(": TUF1-2 ( -- ) UPDATE FLUSH FLUSH ;")
		t("' TUF1-2 RND-TEST-BLOCK TUF1", expected: "TRUE")
		//
		//	FLUSH only saves UPDATEd buffers
		t(#"""
			RND-TEST-BLOCK                      \ blk
			   0 OVER PREPARE-RND-BLOCK            \ blk hash
			   UPDATE FLUSH                        \ blk hash
			   OVER 0 SWAP PREPARE-RND-BLOCK DROP  \ blk hash
			   FLUSH ( with no preliminary UPDATE) \ blk hash
			   SWAP BLOCK 1024 ELF-HASH =
		"""#, expected: "TRUE")

		//
		//	UPDATE only marks the current block buffer
		//	This test needs at least 2 distinct buffers, though this is not a
		//	requirement of the language specification.  If 2 distinct buffers
		//	are not returned, then the tests quits with a trivial Pass
		predefine(#"""
			: TUF2-0 ( blk1 blk2 -- blk1 blk2 ) ;   \ no updates
		"""#)
		t(#"""
		' TUF2-0 2RND-TEST-BLOCKS TUF2       \ run test procedure
		   2SWAP 2DROP 2=
		"""#, expected: "TRUE")            // compare expected and actual

		t(#"""
		  ' TUF2-1 2RND-TEST-BLOCKS TUF2       \ run test procedure
		   SWAP DROP SWAP DROP 2=
		"""#, expected: "TRUE")

		t(#"""
		  ' TUF2-2 2RND-TEST-BLOCKS TUF2       \ run test procedure
		   DROP ROT DROP SWAP 2=
		"""#, expected: "TRUE")

		predefine(#"""
		: TUF2-3 ( blk1 blk2 -- blk1 blk2 )     \ update blk1 and blk2
			TUF2-1 TUF2-2 ;
		"""#)
		t(#"""
		  ' TUF2-3 2RND-TEST-BLOCKS TUF2       \ run test procedure
		   2DROP 2=
		"""#, expected: "TRUE")
		//
		//	FLUSH and then UPDATE is ambiguous and untestable
	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING SAVE-BUFFERS
//
	func testSaveBuffers()
	{

		predefine(#"""

		: WRITE-RND-BLOCKS-WITH-HASH ( blk2 blk1 -- hash )
			0 ROT ROT DO                   \ hash
				I PREPARE-RND-BLOCK UPDATE \ hash'
			LOOP ;                         \ hash'
		: READ-BLOCKS-AND-HASH ( blk2 blk1 -- hash )
			0 ROT ROT DO                         \ hash(i)
		   		I BLOCK 1024 ELF-HASH-ACCUMULATE \ hash(i+1)
			LOOP ;                               \ hash

		"""#)
		//	\ In principle, all the tests above can be repeated with SAVE-BUFFERS instead of
		//	\ FLUSH.  However, only the full random test is repeated...
		//
		t("""
			LIMIT-TEST-BLOCK FIRST-TEST-BLOCK WRITE-RND-BLOCKS-WITH-HASH SAVE-BUFFERS
			   LIMIT-TEST-BLOCK FIRST-TEST-BLOCK READ-BLOCKS-AND-HASH =
		""", expected: "TRUE")
		//
		//	FLUSH and then SAVE-BUFFERS is harmless but undetectable
		//	SAVE-BUFFERS and then FLUSH is undetectable
		//
		//	Unlike FLUSH, SAVE-BUFFERS then BUFFER/BLOCK
		//	returns the original buffer address
		t("RND-TEST-BLOCK DUP BLANK-BUFFER SAVE-BUFFERS SWAP BUFFER =", expected: "TRUE")
		t("RND-TEST-BLOCK DUP BLANK-BUFFER UPDATE SAVE-BUFFERS SWAP BUFFER =", expected: "TRUE")
		t("RND-TEST-BLOCK DUP BLANK-BUFFER SAVE-BUFFERS SWAP BLOCK  =", expected: "TRUE")
		t("RND-TEST-BLOCK DUP BLANK-BUFFER UPDATE SAVE-BUFFERS SWAP BLOCK  =", expected: "TRUE")

	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING BLK
//
	func testBlk()
	{
		//	Signature
		t("BLK DUP ALIGNED =", expected: "TRUE")

	//	None of the words considered so far effect BLK
		t("BLK @ RND-TEST-BLOCK BUFFER DROP BLK @ =", expected: "TRUE")
		t("BLK @ RND-TEST-BLOCK BLOCK  DROP BLK @ =", expected: "TRUE")
		t("BLK @ UPDATE                     BLK @ =", expected: "TRUE")

		t("BLK @ FLUSH        BLK @ =", expected: "TRUE")
		t("BLK @ SAVE-BUFFERS BLK @ =", expected: "TRUE")

	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING LOAD and EVALUATE
//
	func testLoadAndEvaluate()
	{
		//	\ Signature: n LOAD --> blank screen
		t("RND-TEST-BLOCK DUP BLANK-BUFFER DROP UPDATE FLUSH LOAD", expected: "")

		t("BLK @ RND-TEST-BLOCK DUP BLANK-BUFFER DROP UPDATE FLUSH LOAD BLK @ =", expected: "TRUE")
		t("BLOCK-RND RND-TEST-BLOCK 2DUP TL1 LOAD =", expected: "TRUE")

//		Logger.pushLevel(.debug, forName: "MyForth.Forth+Block")
//		defer { Logger.popLevel(forName: "MyForth.Forth+Block") }
		// Boundary Test: FIRST-TEST-BLOCK
		t("BLOCK-RND FIRST-TEST-BLOCK 2DUP TL1 LOAD =", expected: "TRUE")

		// Boundary Test: LIMIT-TEST-BLOCK-1
		t("BLOCK-RND LIMIT-TEST-BLOCK 1- 2DUP TL1 LOAD =", expected: "TRUE")

		predefine(#"""

		\ Boundary Test: End of Buffer
		: TL2 ( u blk -- )
			SWAP 0 <# #S #> WRITE-AT-END-OF-BLOCK ;
		"""#)
		t("BLOCK-RND RND-TEST-BLOCK 2DUP TL2 LOAD =", expected: "TRUE")

		// LOAD updates BLK
		// u: "BLK @"; u LOAD
		predefine(#"""
		: TL3 ( blk -- )
			S" BLK @" WRITE-BLOCK ;
		"""#)
		t("RND-TEST-BLOCK DUP TL3 DUP LOAD =", expected: "TRUE")

		// EVALUATE resets BLK
		// u: "EVALUATE-BLK@"; u LOAD
		predefine(#"""
		: EVALUATE-BLK@ ( -- BLK@ )
			S" BLK @" EVALUATE ;
		: TL4 ( blk -- )
			S" EVALUATE-BLK@" WRITE-BLOCK ;
		"""#)
		t("RND-TEST-BLOCK DUP TL4 LOAD", expected: "0")

		// EVALUTE can nest with LOAD
		// u: "BLK @"; S" u LOAD" EVALUATE
		predefine(#"""
		: TL5 ( blk -- c-addr u )
			0 <#                       \ blk 0
				 [CHAR] D HOLD
				 [CHAR] A HOLD
				 [CHAR] O HOLD
				 [CHAR] L HOLD
				 BL HOLD
			#S #> ;                    \ c-addr u
		"""#)
		t("RND-TEST-BLOCK DUP TL3 DUP TL5 EVALUATE =", expected: "TRUE")

		// Nested LOADs
		// u2: "BLK @"; u1: "LOAD u2"; u1 LOAD
		predefine(#"""
		: TL6 ( blk1 blk2 -- )
			DUP TL3                    \ blk1 blk2
			TL5 WRITE-BLOCK ;
		"""#)
		t("2RND-TEST-BLOCKS 2DUP TL6 SWAP LOAD =", expected: "TRUE")

		// LOAD changes the currect block that is effected by UPDATE
		// This test needs at least 2 distinct buffers, though this is not a
		// requirement of the language specification.  If 2 distinct buffers
		// are not returned, then the tests quits with a trivial Pass
		predefine(#"""
		: TL7 ( blk1 blk2 -- u1 u2 rnd2 blk2-addr rnd1' rnd1 )
			OVER BUFFER OVER BUFFER = IF        \ test needs 2 distinct buffers
				2DROP 0 0 0 0 0 0               \ Dummy result
			ELSE
				OVER BLOCK-RND DUP ROT TL1 >R   \ blk1 blk2
				DUP S" SOURCE DROP" WRITE-BLOCK \ blk1 blk2
				\ change blk1 to a new rnd, but don't UPDATE
				OVER BLANK-BUFFER               \ blk1 blk2 blk1-addr
				BLOCK-RND DUP >R                \ blk1 blk2 blk1-addr rnd1'
				0 <# #S #>                      \ blk1 blk2 blk1-addr c-addr u
				ROT SWAP CHARS MOVE             \ blk1 blk2
				\ Now LOAD blk2
				DUP LOAD DUP >R                 \ blk1 blk2 blk2-addr
				\ Write a new blk2
				DUP 1024 BL FILL                \ blk1 blk2 blk2-addr
				BLOCK-RND DUP >R                \ blk1 blk2 blk2-addr rnd2
				0 <# #S #>                      \ blk1 blk2 blk2-addr c-addr u
				ROT SWAP CHARS MOVE             \ blk1 blk2
				\ The following UPDATE should refer to the LOADed blk2, not blk1
				UPDATE FLUSH                    \ blk1 blk2
				\ Finally, load both blocks then collect all results
				LOAD SWAP LOAD                  \ u2 u1
				R> R> R> R>                     \ u2 u1 rnd2 blk2-addr rnd1' rnd1
			THEN ;
		"""#)
		t(#"""
			2RND-TEST-BLOCKS TL7                 \ run test procedure
		   	SWAP DROP SWAP DROP                  \ u2 u1 rnd2 rnd1
		   	2=
		"""#, expected: "TRUE")

	//	I would expect LOAD to work on the contents of the buffer cache
	//	and not the block device, but the specification doesn't say.
	//	Similarly, I would not expect LOAD to FLUSH the buffer cache,
	//	but the specification doesn't say so.

	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING LIST and SCR
//
	func testListAndScr()
	{
		//	\ Signatures
		t("SCR DUP ALIGNED =", expected: " TRUE")
		// LIST signature is test implicitly in the following tests...

		predefine("""
			: TLS1 ( blk -- )
				S" Should show a (mostly) blank screen" WRITE-BLOCK ;
		""")
		t("RND-TEST-BLOCK DUP TLS1 DUP LIST SCR @ =", expected: " TRUE")

		// Boundary Test: FIRST-TEST-BLOCK
		predefine("""
			: TLS2 ( blk -- )
				S" List of the First test block" WRITE-BLOCK ;
		""")
		t("FIRST-TEST-BLOCK DUP TLS2 LIST", expected: "")

		// Boundary Test: LIMIT-TEST-BLOCK
		predefine("""
			: TLS3 ( blk -- )
				S" List of the Last test block" WRITE-BLOCK ;
		""")
		t("LIMIT-TEST-BLOCK 1- DUP TLS3 LIST", expected: "")

		// Boundary Test: End of Screen
		predefine("""
			: TLS4 ( blk -- )
				S" End of Screen" WRITE-AT-END-OF-BLOCK ;
		""")
			t("RND-TEST-BLOCK DUP TLS4 LIST", expected: "")

		// BLOCK, BUFFER, UPDATE et al don't change SCR
  		predefine("""
			: TLS5 ( blk -- )
				S" Should show another (mostly) blank screen" WRITE-BLOCK ;
		""")

		// the first test below sets the scenario for the subsequent tests
		// BLK is unchanged by LIST
		t("BLK @ RND-TEST-BLOCK DUP TLS5 LIST                BLK @ =", expected: " TRUE")
		// SCR is unchanged by Earlier words
		t("SCR @ FLUSH                                       SCR @ =", expected: " TRUE")
		t("SCR @ FLUSH DUP 1+ BUFFER DROP                    SCR @ =", expected: " TRUE")
		t("SCR @ FLUSH DUP 1+ BLOCK DROP                     SCR @ =", expected: " TRUE")
		t("SCR @ FLUSH DUP 1+ BLOCK DROP UPDATE              SCR @ =", expected: " TRUE")
		t("SCR @ FLUSH DUP 1+ BLOCK DROP UPDATE SAVE-BUFFERS SCR @ =", expected: " TRUE")
		predefine("""
			: TLS6 ( blk -- )
				S" SCR @" WRITE-BLOCK ;
		""")
		t("SCR @ RND-TEST-BLOCK DUP TLS6 LOAD                SCR @ OVER 2=", expected: " TRUE")

	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING EMPTY-BUFFERS
//
	func testEmptyBuffers()
	{
		t("EMPTY-BUFFERS", expected: "")
		t("BLK @ EMPTY-BUFFERS BLK @ =", expected: "TRUE ")
		t("SCR @ EMPTY-BUFFERS SCR @ =", expected: "TRUE ")

		// Test R/W, but discarded changes with EMPTY-BUFFERS
		t(#"""
		   RND-TEST-BLOCK                    \ blk
		   DUP BLANK-BUFFER                  \ blk blk-addr1
		   1024 ELF-HASH                     \ blk hash
		   UPDATE FLUSH                      \ blk hash
		   OVER BLOCK CHAR \ SWAP C!         \ blk hash
		   UPDATE EMPTY-BUFFERS FLUSH        \ blk hash
		   SWAP BLOCK                        \ hash blk-addr2
		   1024 ELF-HASH =
		"""#, expected: "TRUE ")

		// EMPTY-BUFFERS discards all buffers
		predefine(#"""
		: TUF2-EB ( blk1 blk2 -- blk1 blk2 )
			TUF2-1 TUF2-2 EMPTY-BUFFERS ;  \ c.f. TUF2-3
		"""#)
		t("' TUF2-EB 2RND-TEST-BLOCKS TUF2 2SWAP 2DROP 2=", expected: "TRUE ")

		//	\ FLUSH and then EMPTY-BUFFERS is acceptable but untestable
		//	\ EMPTY-BUFFERS and then UPDATE is ambiguous and untestable
	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING >IN manipulation from a block source

	func testInManipulation()
	{
		predefine("""
			: TIN ( blk -- )
				S" 1 8 >IN +!     2        3" WRITE-BLOCK ;
		""")
		t("RND-TEST-BLOCK DUP TIN LOAD", expected: "1 3")

	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING \, SAVE-INPUT, RESTORE-INPUT and REFILL from a block source
//
	func testSaveRestoreRefill()
	{
		// Try to determine the number of characters per line
		// Assumes an even number of characters per line
		predefine(#"""
			: | ( u -- u-2 ) 2 - ;
			: C/L-CALC ( blk -- c/l )
				DUP BLANK-BUFFER                 \ blk blk-addr
				[CHAR] \ OVER C!                 \ blk blk-addr  blk:"\"
				511 0 DO                         \ blk c-addr[i]
					CHAR+ CHAR+ [CHAR] | OVER C! \ blk c-addr[i+1]
				LOOP DROP                        \ blk   blk:"\ | | | | ... |"
				UPDATE SAVE-BUFFERS FLUSH        \ blk
				1024 SWAP LOAD ;                 \ c/l
			[?DEF] C/L
			[?ELSE]
			\? .( Given Characters per Line: ) C/L U. CR
			[?ELSE]
			\? RND-TEST-BLOCK C/L-CALC CONSTANT C/L
			\? C/L 1024 U< [?IF]
			\? .( Calculated Characters per Line: ) C/L U. CR
			[?THEN]
		
			: WRITE-BLOCK-LINE ( lin-addr[i] c-addr u -- lin-addr[i+1] )
				2>R DUP C/L CHARS + SWAP 2R> ROT SWAP MOVE ;
		
			\ Discards to the end of the line
			: TCSIRIR1 ( blk -- )
				BLANK-BUFFER
				C/L 1024 U< IF
					S" 2222 \ 3333" WRITE-BLOCK-LINE
					S" 4444"        WRITE-BLOCK-LINE
				THEN
				DROP UPDATE SAVE-BUFFERS ;
		"""#)
		t("RND-TEST-BLOCK DUP TCSIRIR1 LOAD", expected: "2222 4444")
		predefine(#"""
			VARIABLE T-CNT 0 T-CNT !
		
			: MARK ( "<char>" -- ) \ Use between <# and #>
				CHAR HOLD ; IMMEDIATE
		
			: ?EXECUTE ( xt f -- )
				IF EXECUTE ELSE DROP THEN ;
		
			\ SAVE-INPUT and RESTORE-INPUT within a single block
			: TCSIRIR2-EXPECTED S" EDCBCBA" ; \ Remember that the string comes out backwards
			: TCSIRIR2 ( blk -- )
				C/L 1024 U< IF
					BLANK-BUFFER
					S" 0 T-CNT !"                   WRITE-BLOCK-LINE
					S" <# MARK A SAVE-INPUT MARK B" WRITE-BLOCK-LINE
			S" 1 T-CNT +! MARK C ' RESTORE-INPUT T-CNT @ 2 < ?EXECUTE MARK D" WRITE-BLOCK-LINE
					S" MARK E 0 0 #>"               WRITE-BLOCK-LINE
					UPDATE SAVE-BUFFERS DROP
				ELSE
					S" 0 TCSIRIR2-EXPECTED"         WRITE-BLOCK
				THEN ;
		"""#)

		t("RND-TEST-BLOCK DUP TCSIRIR2 LOAD TCSIRIR2-EXPECTED S=", expected: "0 TRUE")

		// REFILL across 2 blocks
		predefine(#"""
			: TCSIRIR3 ( blks -- )
				DUP S" 1 2 3 REFILL 4 5 6" WRITE-BLOCK
				1+  S" 10 11 12"           WRITE-BLOCK ;
		"""#)
		t("2 RND-TEST-BLOCK-SEQ DUP TCSIRIR3 LOAD", expected: "1 2 3 -1 10 11 12")

		// SAVE-INPUT and RESTORE-INPUT across 2 blocks
		predefine(#"""
			: TCSIRIR4-EXPECTED S" HGF1ECBF1ECBA" ; \ Remember that the string comes out backwards
			: TCSIRIR4 ( blks -- )
				C/L 1024 U< IF
					DUP BLANK-BUFFER
					S" 0 T-CNT !"                   WRITE-BLOCK-LINE
					S" <# MARK A SAVE-INPUT MARK B" WRITE-BLOCK-LINE
					S" MARK C REFILL MARK D"        WRITE-BLOCK-LINE
					DROP UPDATE 1+ BLANK-BUFFER
					S" MARK E ABS CHAR 0 + HOLD"    WRITE-BLOCK-LINE
			S" 1 T-CNT +! MARK F ' RESTORE-INPUT T-CNT @ 2 < ?EXECUTE MARK G" WRITE-BLOCK-LINE
					S" MARK H 0 0 #>"               WRITE-BLOCK-LINE
					DROP UPDATE SAVE-BUFFERS
				ELSE
					S" 0 TCSIRIR4-EXPECTED"         WRITE-BLOCK
				THEN ;
		"""#)
		t("2 RND-TEST-BLOCK-SEQ DUP TCSIRIR4 LOAD TCSIRIR4-EXPECTED S=", expected: "0 TRUE")

	}
//
//	\ ------------------------------------------------------------------------------
//	TESTING THRU
//
	func testThru()
	{
		predefine("""
			: TT1 ( blks -- )
				DUP S" BLK" WRITE-BLOCK
				1+  S" @"   WRITE-BLOCK ;
		""")
		t("2 RND-TEST-BLOCK-SEQ DUP TT1 DUP DUP 1+ THRU 1- =", expected: "TRUE")

	}
//
//	\ ------------------------------------------------------------------------------
//
//	BLOCK-ERRORS SET-ERROR-COUNT
//
//	CR .( End of Block word tests) CR

}


