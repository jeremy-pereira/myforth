//
//  CoreExtraTests.swift
//  
//
//  Created by Jeremy Pereira on 26/08/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
import Toolbox
@testable import MyForth

private let log = Logger.getLogger("MyForthTests.CoreTests")

/// Test suite tests for extra bita
///
/// These tests are not in core but are used to isolate issues using the same
/// mechanism.
///
final class CoreExtraTests: SuiteTest
{
	func testCharImmediateIssue()
	{
		t(": testFunc [CHAR] 9 1+ [CHAR] 0 DO I 0 SPACES EMIT LOOP CR ; ", expected: "")
	}

	func testRefill()
	{
		t("refill", expected: "0")
		t("refill 1", expected: "0 1")
		t("""
		  refill 1
		  2 3
		  """, expected: "-1 2 3")
	}

	func testQuit()
	{
		t(": foo 1 2 quit 3 4 ; foo 5 6", expected: "1 2")
	}

	func testAbort()
	{
		t(": foo 1 2 abort 3 4 ; foo 5 6", expected: "")
	}


	func testAbortDQ()
	{
		t(": foo 1 2 1 abort\" Hello\" 3 4 ; foo 5 6", expected: "", expectedOutput: "Hello")
		t(": foo 1 2 0 abort\" Hello\" 3 4 ; foo 5 6", expected: "1 2 3 4 5 6", expectedOutput: "")
	}

	func testEnvironment()
	{
		predefine("""
		0S CONSTANT 				<FALSE>
		1S CONSTANT 				<TRUE>

		: getCountedString s" /COUNTED-STRING" environment? ;
		: getAddressUnitSize s" ADDRESS-UNIT-BITS" environment? ;
		: getNonsense s" hgfdhgf" environment? ;
		: getMaxU s" MAX-U" environment? ;
		: getMaxD s" MAX-D" environment? ;
		: getMaxUD s" MAX-UD" environment? ;
		: getFloored s" FLOORED" environment? ;
		""")
		t("getCountedString", expected: "ff <TRUE>")
		t("getAddressUnitSize", expected: "40 <TRUE>")
		t("getNonsense", expected: "<FALSE>")
		t("getMaxU", expected: "-1 <TRUE>")
		t("getMaxD", expected: "-1 7fffffffffffffff <TRUE>")
		t("getMaxUD", expected: "-1 -1 <TRUE>")
		t("getFloored", expected: "<FALSE> <TRUE>")
	}

	func testSourceId()
	{
		predefine("""
		0S CONSTANT 				<FALSE>
		1S CONSTANT 				<TRUE>
		: GE1 S" source-id" ; IMMEDIATE
		""")
		// For these tests source-id is faked as 1
		t("SOURCE-ID 1 = ", expected: "<TRUE>")
		t(#"GE1 evaluate -1 = "#, expected: "<TRUE>")
	}

	func testMinimalBlk()
	{
		predefine("""
		0S CONSTANT 				<FALSE>
		1S CONSTANT 				<TRUE>
		""")
		t("blk @ 0= ", expected: "<TRUE>")
	}

	func testQuestionDoCompilation()
	{
		let definition = """
		: ?do
			{csFrame}
			postpone 2dup
			postpone -
			4 {sToCompiling}
			postpone 0<>branch?
			postpone 2drop
			1								\\ Count of origs to resolve is one
			-1 {sToCompiling}
			{orig}
			postpone branch
			{dest}							\\ Target for jump back
			postpone {s>rs.loop} 			\\ Save loop variables on the return stack
			0							 	\\ orig count for leave
			; immediate compile-only
		"""
		t(definition, expected: "")
		if log.isDebug
		{
			for machine in testMachines
			{
				machine.dumpWordDefinition(word: Forth.Word("?do"), to: &log.debugStream)
			}
		}
	}

	func testCaseCompiles()
	{
		let definition = #"""
: case
	0					( count of case endof origs to resolve )
	; immediate compile-only

: of
	postpone over		( x1 x2 -- x1 x2 x1 )
	postpone =			( x1 x2 x1 -- x1 x2=x1 )
	-1 {sToCompiling}	( x1 x2=x1 -- x1 x2=x1 dest )
	{orig}
	postpone 0=branch?	( x1 x2=x1 dest -- x1 )
	postpone drop
	; immediate compile-only

: endof
	-1 {sToCompiling}
	{orig}
	1 +
	postpone branch
	1 cs-roll			( bring the orig for the of to the top )
	{resolveOrig}
	; immediate compile-only

: endcase
	\ Resolve all the endof origs
	begin
		dup				( c -- c c )
		0>				( c c -- c c>0 )
	while
		{resolveOrig}	( c c>0 -- c )
		1-				( c -- c-1 )
	repeat
	drop
	; immediate compile-only
"""#
//		Logger.pushLevel(.debug, forName: "MyForth.ThreadedForth")
//		defer { Logger.popLevel(forName: "MyForth.ThreadedForth") }
		t(definition, expected: "")
//		log.pushLevel(.debug)
//		defer { log.popLevel() }
		if log.isDebug
		{
			for machine in testMachines
			{
				log.debug("case:")
				machine.dumpWordDefinition(word: Forth.Word("case"), to: &log.debugStream)
			}
		}

	}

	func testEraseCompileIssue()
	{
		let definition = #"""
  : eraseTest ( addr u -- )
	  0 			( addr u -- addr u 0 )
	  ?do
  \		i 		( addr -- addr i )
  \		over +	( addr i -- addr i+addr )
  \		0 swap	( addr i*+addr -- addr 0 i+addr )
  \		c!
	  loop
	  drop		( addr -- )
	  ;
"""#
		t(definition, expected: "")
//		log.pushLevel(.debug)
//		defer { log.popLevel() }
		if log.isDebug
		{
			for machine in testMachines
			{
				log.debug("eraseTest:")
				machine.dumpWordDefinition(word: Forth.Word("eraseTest"), to: &log.debugStream)
			}
		}
	}

	func testParseLeadingDelimiter()
	{
		t("CHAR A PARSE A SWAP DROP", expected: "0")
	}

	func testAllCoreWordsDefined()
	{
		let coreWords = """
		! # #> #s ' * */ */mod + +! postpone +loop , - . postpone ." / /mod
		0= 0< 1+ 1- 2! 2* 2/ 2@ 2drop 2dup 2over 2swap : ; < <# = > >body >in
		>number >r ?dup @ abort abort" abs accept align aligned allot and base
		begin bl c! c, c@ cell+ cells char char+ chars constant count cr create
		decimal depth do does> drop dup else emit environment? evaluate execute
		exit fill find fm/mod here hold if immediate invert j key leave literal
		loop lshift m* max min mod move negate or over postpone quit r> r@ recurse
		repeat rot rshift s" s>d sign sm/rem source space spaces state swap then
		type u. u< um* um/mod unloop until variable while word xor [ ['] [char]
		]
		"""
		let testList = coreWords.split(omittingEmptySubsequences: true)
		{
			$0.isWhitespace
		}
		for word in testList
		{
			for machine in testMachines
			{
				XCTAssert(machine.isDefined(string: word), "\(type(of: machine)): \(word) is undefined")
			}
		}
	}


	func testIncludeFile() throws
	{
		let fileName = "included-helper.fth"
		try "source-id\n".write(toFile: fileName, atomically: false, encoding: .utf8)

		predefine("""
: openFileOrThrow ( caddr u -- fileId )
	r/o open-file	( caddr u -- fileId errno )
	dup 0<>			( fileID errno -- fileid errno errno<>0 )
	if
		decimal
  		\(Forth.ErrorNumber.openFile.rawValue) {throw}
	then
	drop
	;
""")
		t(#"""
		source-id s" \#(fileName)" openFileOrThrow	( -- source-id1 fid )

		include-file source-id
		rot over = 			( x y x -- y x true )
		rot rot    			( y x true -- true y x )
		over =				( true y x -- true y false )
		swap dup 0 =		( true y false -- true false y false )
		swap -1 =			( true false y false -- true false false false )
		"""#, expected: "-1 0 0 0")
	}


	func testIncluded() throws
	{
		let fileName = "included-helper.fth"
		try "source-id\n".write(toFile: fileName, atomically: false, encoding: .utf8)

		t(#"""
		source-id s" \#(fileName)" included source-id
		rot over = 			( x y x -- y x true )
		rot rot    			( y x true -- true y x )
		over =				( true y x -- true y false )
		swap dup 0 =		( true y false -- true false y false )
		swap -1 =			( true false y false -- true false false false )
		"""#, expected: "-1 0 0 0")
	}


	func testRequired() throws
	{
		let fileName = "included-helper.fth"
		try "1+\n".write(toFile: fileName, atomically: false, encoding: .utf8)

		t(#"""
		1 s" \#(fileName)" required
		s" \#(fileName)" required
		10 s" \#(fileName)" included
		"""#, expected: "2 11")
	}

	func testPrelimStart() throws
	{
		let code = #"""
CR CR SOURCE TYPE ( Preliminary test ) CR
SOURCE ( These lines test SOURCE, TYPE, CR and parenthetic comments ) TYPE CR
( The next line of output should be blank to test CR ) SOURCE TYPE CR CR
"""#
		t(code, expected: "")
	}

	func testDASCII()
	{
		predefine(#"""
		\ Constant definitions

		DECIMAL
		0 INVERT        CONSTANT 1SD
		1SD 1 RSHIFT    CONSTANT MAX-INTD   \ 01...1
		MAX-INTD INVERT CONSTANT MIN-INTD   \ 10...0
		MAX-INTD 2/     CONSTANT HI-INT     \ 001...1
		MIN-INTD 2/     CONSTANT LO-INT     \ 110...1
		0S CONSTANT <FALSE>
		1S CONSTANT <TRUE>

		\ ------------------------------------------------------------------------------
		\ Some 2CONSTANTs for the following tests

		1SD MAX-INTD 2CONSTANT MAX-2INT  \ 01...1
		0   MIN-INTD 2CONSTANT MIN-2INT  \ 10...0
		MAX-2INT 2/  2CONSTANT HI-2INT   \ 001...1
		MIN-2INT 2/  2CONSTANT LO-2INT   \ 110...0

		"""#)

		predefine("""
		MAX-2INT 71 73 M*/ 2CONSTANT DBL1
		MIN-2INT 73 79 M*/ 2CONSTANT DBL2
		""")

		t("""
		: D>ASCII  ( D -- CADDR U )
	 DUP >R <# DABS #S R> SIGN #>    ( -- CADDR1 U )
		   HERE SWAP 2DUP 2>R CHARS DUP ALLOT MOVE 2R>
		;
""", expected: "")
		t("""
   DBL1 D>ASCII 2CONSTANT "DBL1"
   DBL2 D>ASCII 2CONSTANT "DBL2"

   : DOUBLEOUTPUT
	  CR ." You should see lines duplicated:" CR
	  5 SPACES "DBL1" TYPE CR
	  5 SPACES DBL1 D. CR
	  8 SPACES "DBL1" DUP >R TYPE CR
	  5 SPACES DBL1 R> 3 + D.R CR
	  5 SPACES "DBL2" TYPE CR
	  5 SPACES DBL2 D. CR
	  10 SPACES "DBL2" DUP >R TYPE CR
	  5 SPACES DBL2 R> 5 + D.R CR
   ;
""", expected: "")
	}

	func testPredefinedCreate()
	{
		t("create foo", expected: "")
	}

	// TODO: This could be fixed at some point...
//	func testColonDefinition()
//	{
//		t("""
//		{parseWord} :2 {createNamedWordSlot} -1 {changeState}
//			{parseWord} {createNamedWordSlot} -1 {changeState} ;
//		""", expected: "", debug: true)
//		t(":2 foo 1 ;", expected: "")
//	}

	func testImmediateControlledThroughDataSpace()
	{
		t(": foo [ 5 ] 4 ;", expected: "5")
		t("foo", expected: "4")
	}
}
