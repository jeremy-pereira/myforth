//
//  InputSourceTests.swift
//  
//
//  Created by Jeremy Pereira on 20/11/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
import MyForth

final class InputSourceTests: XCTestCase
{
	func testReadLines() throws
	{
		let input = """
		The quick brown fox
		jumps over
		the lazy dog
		"""

		let lines = input.split(separator: "\n")

		var source = try Forth.InputSource(input, sourceId: 0, name: "test input 1")
		guard let line1 = try source.readLine()
		else { XCTFail("Failed to read line 1") ; return }
		XCTAssert(line1 == lines[0], "Expected '\(lines[0])', got '\(line1)'")
		XCTAssert(source.lineEndingsRead == 1)
		XCTAssert(source.charsSinceLastLineEnding == 0)
		guard let char = try source.next()
		else { XCTFail("Failed to read first char of line 2") ; return }
		XCTAssert(char == "j")
		XCTAssert(source.lineEndingsRead == 1)
		XCTAssert(source.charsSinceLastLineEnding == 1)
		guard let line2 = try source.readLine()
		else { XCTFail("Failed to read line 2") ; return }
		XCTAssert(line2 == String(lines[1].dropFirst()))
		XCTAssert(source.lineEndingsRead == 2)
		XCTAssert(source.charsSinceLastLineEnding == 0)
		guard let line3 = try source.readLine()
		else { XCTFail("Failed to read line 3") ; return }
		XCTAssert(line3 == lines[2], "Expected '\(lines[2])', got '\(line3)'")
		if let line4 = try source.readLine()
		{
			XCTFail("Managed to read an extra line: '\(line4)'")
		}
	}
}
