//
//  CoreExtensionsTests.swift
//  
//
//  Created by Jeremy Pereira on 09/10/2023.
//
//  Copyright (c) Jeremy Pereira 2023
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
import MyForth
import Toolbox

private let log = Logger.getLogger("MyForthTests.CoreExtensionsTests")

final class CoreExtensionsTests: SuiteTest
{
	public override func setUp()
	{
		super.setUp()
		predefine("decimal")
		predefine("""
		0 INVERT         CONSTANT MAX-UINT
		0 INVERT 1 RSHIFT      CONSTANT MAX-INT
		0 INVERT 1 RSHIFT INVERT   CONSTANT MIN-INT
		0 INVERT 1 RSHIFT      CONSTANT MID-UINT
		0 INVERT 1 RSHIFT INVERT   CONSTANT MID-UINT+1
		""")
		predefine(#"""
		: S=  \ ( ADDR1 C1 ADDR2 C2 -- T/F ) COMPARE TWO STRINGS.
		   >R SWAP R@ = IF         \ MAKE SURE STRINGS HAVE SAME LENGTH
			  R> ?DUP IF         \ IF NON-EMPTY STRINGS
			0 DO
			   OVER C@ OVER C@ - IF 2DROP FALSE UNLOOP EXIT THEN
			   SWAP CHAR+ SWAP CHAR+
				 LOOP
			  THEN
			  2DROP TRUE         \ IF WE GET HERE, STRINGS MATCH
		   ELSE
			  R> DROP 2DROP FALSE      \ LENGTHS MISMATCH
		   THEN ;
		"""#)

	}

	//
	//	TESTING TRUE FALSE
	func testTrueFalse()
	{
		t("TRUE", expected: "0 INVERT")
		t("FALSE", expected: "0")
	}
//
//
//	\ -----------------------------------------------------------------------------
//	TESTING <> U>   (contributed by James Bowman)
//
	func testNotEqualandUnidgendGreater()
	{

		t("0 0 <>", expected: "FALSE")
		t("1 1 <>", expected: "FALSE")
		t("-1 -1 <>", expected: "FALSE")
		t("1 0 <>", expected: "TRUE")
		t("-1 0 <>", expected: "TRUE")
		t("0 1 <>", expected: "TRUE")
		t("0 -1 <>", expected: "TRUE")

		t("0 1 U>", expected: "FALSE")
		t("1 2 U>", expected: "FALSE")
		t("0 MID-UINT U>", expected: "FALSE")
		t("0 MAX-UINT U>", expected: "FALSE")
		t("MID-UINT MAX-UINT U>", expected: "FALSE")
		t("0 0 U>", expected: "FALSE")
		t("1 1 U>", expected: "FALSE")
		t("1 0 U>", expected: "TRUE")
		t("2 1 U>", expected: "TRUE")
		t("MID-UINT 0 U>", expected: "TRUE")
		t("MAX-UINT 0 U>", expected: "TRUE")
		t("MAX-UINT MID-UINT U>", expected: "TRUE")
	}
//
//	\ -----------------------------------------------------------------------------
//	TESTING 0<> 0>   (contributed by James Bowman)
//
	func testNot0AndGreater0()
	{
		t("0 0<>", expected: "FALSE")
		t("1 0<>", expected: "TRUE")
		t("2 0<>", expected: "TRUE")
		t("-1 0<>", expected: "TRUE")
		t("MAX-UINT 0<>", expected: "TRUE")
		t("MIN-INT 0<>", expected: "TRUE")
		t("MAX-INT 0<>", expected: "TRUE")

		t("0 0>", expected: "FALSE")
		t("-1 0>", expected: "FALSE")
		t("MIN-INT 0>", expected: "FALSE")
		t("1 0>", expected: "TRUE")
		t("MAX-INT 0>", expected: "TRUE")
	}
//
//	\ -----------------------------------------------------------------------------
//	TESTING NIP TUCK ROLL PICK   (contributed by James Bowman)
//
	func testNipTuckRollPick()
	{
		t("1 2 NIP", expected: "2")
		t("1 2 3 NIP", expected: "1 3")

		t("1 2 TUCK", expected: "2 1 2")
		t("1 2 3 TUCK", expected: "1 3 2 3")

		predefine(": RO5 100 200 300 400 500 ;")
		t("RO5 3 ROLL", expected: "100 300 400 500 200")
		t("RO5 2 ROLL", expected: "RO5 ROT")
		t("RO5 1 ROLL", expected: "RO5 SWAP")
		t("RO5 0 ROLL", expected: "RO5")

		t("RO5 2 PICK", expected: "100 200 300 400 500 300")
		t("RO5 1 PICK", expected: "RO5 OVER")
		t("RO5 0 PICK", expected: "RO5 DUP")

	}
//
//	\ -----------------------------------------------------------------------------
//	TESTING 2>R 2R@ 2R>   (contributed by James Bowman)

	func test2ToR2Rat2FromR()
	{
		t(": RR0 2>R 100 R> R> ;", expected: "")
		t("300 400 RR0", expected: "100 400 300")
		t("200 300 400 RR0", expected: "200 100 400 300")

		t(": RR1 2>R 100 2R@ R> R> ;", expected: "")
		t("300 400 RR1", expected: "100 300 400 400 300")
		t("200 300 400 RR1", expected: "200 100 300 400 400 300")

		t(": RR2 2>R 100 2R> ;", expected: "")
		t("300 400 RR2", expected: "100 300 400")
		t("200 300 400 RR2", expected: "200 100 300 400")
	}
//
//
//	\ -----------------------------------------------------------------------------
//	TESTING HEX   (contributed by James Bowman)
//
	func testHex()
	{
		t("BASE @ HEX BASE @ DECIMAL BASE @ - SWAP BASE !", expected: "6")
	}
//
//
//	\ -----------------------------------------------------------------------------
//	TESTING WITHIN   (contributed by James Bowman)
//
	func testWithin()
	{
		t("0 0 0 WITHIN", expected: "FALSE")
		t("0 0 MID-UINT WITHIN", expected: "TRUE")
		t("0 0 MID-UINT+1 WITHIN", expected: "TRUE")
		t("0 0 MAX-UINT WITHIN", expected: "TRUE")
		t("0 MID-UINT 0 WITHIN", expected: "FALSE")
		t("0 MID-UINT MID-UINT WITHIN", expected: "FALSE")
		t("0 MID-UINT MID-UINT+1 WITHIN", expected: "FALSE")
		t("0 MID-UINT MAX-UINT WITHIN", expected: "FALSE")
		t("0 MID-UINT+1 0 WITHIN", expected: "FALSE")
		t("0 MID-UINT+1 MID-UINT WITHIN", expected: "TRUE")
		t("0 MID-UINT+1 MID-UINT+1 WITHIN", expected: "FALSE")
		t("0 MID-UINT+1 MAX-UINT WITHIN", expected: "FALSE")
		t("0 MAX-UINT 0 WITHIN", expected: "FALSE")
		t("0 MAX-UINT MID-UINT WITHIN", expected: "TRUE")
		t("0 MAX-UINT MID-UINT+1 WITHIN", expected: "TRUE")
		t("0 MAX-UINT MAX-UINT WITHIN", expected: "FALSE")
		t("MID-UINT 0 0 WITHIN", expected: "FALSE")
		t("MID-UINT 0 MID-UINT WITHIN", expected: "FALSE")
		t("MID-UINT 0 MID-UINT+1 WITHIN", expected: "TRUE")
		t("MID-UINT 0 MAX-UINT WITHIN", expected: "TRUE")
		t("MID-UINT MID-UINT 0 WITHIN", expected: "TRUE")
		t("MID-UINT MID-UINT MID-UINT WITHIN", expected: "FALSE")
		t("MID-UINT MID-UINT MID-UINT+1 WITHIN", expected: "TRUE")
		t("MID-UINT MID-UINT MAX-UINT WITHIN", expected: "TRUE")
		t("MID-UINT MID-UINT+1 0 WITHIN", expected: "FALSE")
		t("MID-UINT MID-UINT+1 MID-UINT WITHIN", expected: "FALSE")
		t("MID-UINT MID-UINT+1 MID-UINT+1 WITHIN", expected: "FALSE")
		t("MID-UINT MID-UINT+1 MAX-UINT WITHIN", expected: "FALSE")
		t("MID-UINT MAX-UINT 0 WITHIN", expected: "FALSE")
		t("MID-UINT MAX-UINT MID-UINT WITHIN", expected: "FALSE")
		t("MID-UINT MAX-UINT MID-UINT+1 WITHIN", expected: "TRUE")
		t("MID-UINT MAX-UINT MAX-UINT WITHIN", expected: "FALSE")
		t("MID-UINT+1 0 0 WITHIN", expected: "FALSE")
		t("MID-UINT+1 0 MID-UINT WITHIN", expected: "FALSE")
		t("MID-UINT+1 0 MID-UINT+1 WITHIN", expected: "FALSE")
		t("MID-UINT+1 0 MAX-UINT WITHIN", expected: "TRUE")
		t("MID-UINT+1 MID-UINT 0 WITHIN", expected: "TRUE")
		t("MID-UINT+1 MID-UINT MID-UINT WITHIN", expected: "FALSE")
		t("MID-UINT+1 MID-UINT MID-UINT+1 WITHIN", expected: "FALSE")
		t("MID-UINT+1 MID-UINT MAX-UINT WITHIN", expected: "TRUE")
		t("MID-UINT+1 MID-UINT+1 0 WITHIN", expected: "TRUE")
		t("MID-UINT+1 MID-UINT+1 MID-UINT WITHIN", expected: "TRUE")
		t("MID-UINT+1 MID-UINT+1 MID-UINT+1 WITHIN", expected: "FALSE")
		t("MID-UINT+1 MID-UINT+1 MAX-UINT WITHIN", expected: "TRUE")
		t("MID-UINT+1 MAX-UINT 0 WITHIN", expected: "FALSE")
		t("MID-UINT+1 MAX-UINT MID-UINT WITHIN", expected: "FALSE")
		t("MID-UINT+1 MAX-UINT MID-UINT+1 WITHIN", expected: "FALSE")
		t("MID-UINT+1 MAX-UINT MAX-UINT WITHIN", expected: "FALSE")
		t("MAX-UINT 0 0 WITHIN", expected: "FALSE")
		t("MAX-UINT 0 MID-UINT WITHIN", expected: "FALSE")
		t("MAX-UINT 0 MID-UINT+1 WITHIN", expected: "FALSE")
		t("MAX-UINT 0 MAX-UINT WITHIN", expected: "FALSE")
		t("MAX-UINT MID-UINT 0 WITHIN", expected: "TRUE")
		t("MAX-UINT MID-UINT MID-UINT WITHIN", expected: "FALSE")
		t("MAX-UINT MID-UINT MID-UINT+1 WITHIN", expected: "FALSE")
		t("MAX-UINT MID-UINT MAX-UINT WITHIN", expected: "FALSE")
		t("MAX-UINT MID-UINT+1 0 WITHIN", expected: "TRUE")
		t("MAX-UINT MID-UINT+1 MID-UINT WITHIN", expected: "TRUE")
		t("MAX-UINT MID-UINT+1 MID-UINT+1 WITHIN", expected: "FALSE")
		t("MAX-UINT MID-UINT+1 MAX-UINT WITHIN", expected: "FALSE")
		t("MAX-UINT MAX-UINT 0 WITHIN", expected: "TRUE")
		t("MAX-UINT MAX-UINT MID-UINT WITHIN", expected: "TRUE")
		t("MAX-UINT MAX-UINT MID-UINT+1 WITHIN", expected: "TRUE")
		t("MAX-UINT MAX-UINT MAX-UINT WITHIN", expected: "FALSE")

		t("MIN-INT MIN-INT MIN-INT WITHIN", expected: "FALSE")
		t("MIN-INT MIN-INT 0 WITHIN", expected: "TRUE")
		t("MIN-INT MIN-INT 1 WITHIN", expected: "TRUE")
		t("MIN-INT MIN-INT MAX-INT WITHIN", expected: "TRUE")
		t("MIN-INT 0 MIN-INT WITHIN", expected: "FALSE")
		t("MIN-INT 0 0 WITHIN", expected: "FALSE")
		t("MIN-INT 0 1 WITHIN", expected: "FALSE")
		t("MIN-INT 0 MAX-INT WITHIN", expected: "FALSE")
		t("MIN-INT 1 MIN-INT WITHIN", expected: "FALSE")
		t("MIN-INT 1 0 WITHIN", expected: "TRUE")
		t("MIN-INT 1 1 WITHIN", expected: "FALSE")
		t("MIN-INT 1 MAX-INT WITHIN", expected: "FALSE")
		t("MIN-INT MAX-INT MIN-INT WITHIN", expected: "FALSE")
		t("MIN-INT MAX-INT 0 WITHIN", expected: "TRUE")
		t("MIN-INT MAX-INT 1 WITHIN", expected: "TRUE")
		t("MIN-INT MAX-INT MAX-INT WITHIN", expected: "FALSE")
		t("0 MIN-INT MIN-INT WITHIN", expected: "FALSE")
		t("0 MIN-INT 0 WITHIN", expected: "FALSE")
		t("0 MIN-INT 1 WITHIN", expected: "TRUE")
		t("0 MIN-INT MAX-INT WITHIN", expected: "TRUE")
		t("0 0 MIN-INT WITHIN", expected: "TRUE")
		t("0 0 0 WITHIN", expected: "FALSE")
		t("0 0 1 WITHIN", expected: "TRUE")
		t("0 0 MAX-INT WITHIN", expected: "TRUE")
		t("0 1 MIN-INT WITHIN", expected: "FALSE")
		t("0 1 0 WITHIN", expected: "FALSE")
		t("0 1 1 WITHIN", expected: "FALSE")
		t("0 1 MAX-INT WITHIN", expected: "FALSE")
		t("0 MAX-INT MIN-INT WITHIN", expected: "FALSE")
		t("0 MAX-INT 0 WITHIN", expected: "FALSE")
		t("0 MAX-INT 1 WITHIN", expected: "TRUE")
		t("0 MAX-INT MAX-INT WITHIN", expected: "FALSE")
		t("1 MIN-INT MIN-INT WITHIN", expected: "FALSE")
		t("1 MIN-INT 0 WITHIN", expected: "FALSE")
		t("1 MIN-INT 1 WITHIN", expected: "FALSE")
		t("1 MIN-INT MAX-INT WITHIN", expected: "TRUE")
		t("1 0 MIN-INT WITHIN", expected: "TRUE")
		t("1 0 0 WITHIN", expected: "FALSE")
		t("1 0 1 WITHIN", expected: "FALSE")
		t("1 0 MAX-INT WITHIN", expected: "TRUE")
		t("1 1 MIN-INT WITHIN", expected: "TRUE")
		t("1 1 0 WITHIN", expected: "TRUE")
		t("1 1 1 WITHIN", expected: "FALSE")
		t("1 1 MAX-INT WITHIN", expected: "TRUE")
		t("1 MAX-INT MIN-INT WITHIN", expected: "FALSE")
		t("1 MAX-INT 0 WITHIN", expected: "FALSE")
		t("1 MAX-INT 1 WITHIN", expected: "FALSE")
		t("1 MAX-INT MAX-INT WITHIN", expected: "FALSE")
		t("MAX-INT MIN-INT MIN-INT WITHIN", expected: "FALSE")
		t("MAX-INT MIN-INT 0 WITHIN", expected: "FALSE")
		t("MAX-INT MIN-INT 1 WITHIN", expected: "FALSE")
		t("MAX-INT MIN-INT MAX-INT WITHIN", expected: "FALSE")
		t("MAX-INT 0 MIN-INT WITHIN", expected: "TRUE")
		t("MAX-INT 0 0 WITHIN", expected: "FALSE")
		t("MAX-INT 0 1 WITHIN", expected: "FALSE")
		t("MAX-INT 0 MAX-INT WITHIN", expected: "FALSE")
		t("MAX-INT 1 MIN-INT WITHIN", expected: "TRUE")
		t("MAX-INT 1 0 WITHIN", expected: "TRUE")
		t("MAX-INT 1 1 WITHIN", expected: "FALSE")
		t("MAX-INT 1 MAX-INT WITHIN", expected: "FALSE")
		t("MAX-INT MAX-INT MIN-INT WITHIN", expected: "TRUE")
		t("MAX-INT MAX-INT 0 WITHIN", expected: "TRUE")
		t("MAX-INT MAX-INT 1 WITHIN", expected: "TRUE")
		t("MAX-INT MAX-INT MAX-INT WITHIN", expected: "FALSE")
	}
//
//	\ -----------------------------------------------------------------------------
//	TESTING UNUSED  (contributed by James Bowman & Peter Knaggs)
//
	func testUnused()
	{
		predefine("VARIABLE UNUSED0")
		t("UNUSED DROP", expected: "")
		t("ALIGN UNUSED UNUSED0 ! 0 , UNUSED CELL+ UNUSED0 @ =", expected: "TRUE")
		t("UNUSED UNUSED0 ! 0 C, UNUSED CHAR+ UNUSED0 @ =", expected: "TRUE")
		// aligned -> unaligned
		t("UNUSED UNUSED0 ! 0 C, UNUSED CHAR+ UNUSED0 @ =", expected: "TRUE")
		// unaligned -> ?

	}
//
//	\ -----------------------------------------------------------------------------
//	TESTING AGAIN   (contributed by James Bowman)
//
	func testAgain()
	{
		t(": AG0 701 BEGIN DUP 7 MOD 0= IF EXIT THEN 1+ AGAIN ;", expected: "")
		t("AG0", expected: "707")
	}
//
//	\ -----------------------------------------------------------------------------
//	TESTING MARKER   (contributed by James Bowman)
//
	func testMarker()
	{
		t(": MA? BL WORD FIND NIP 0<> ;", expected: "")
		t("MARKER MA0", expected: "")
		t(": MA1 111 ;", expected: "")
		t("MARKER MA2", expected: "")
		t(": MA1 222 ;", expected: "")
		t("MA? MA0 MA? MA1 MA? MA2", expected: "TRUE TRUE TRUE")
		t("MA1 MA2 MA1", expected: "222 111")
		t("MA? MA0 MA? MA1 MA? MA2", expected: "TRUE TRUE FALSE")
		t("MA0", expected: "")
		t("MA? MA0 MA? MA1 MA? MA2", expected: "FALSE FALSE FALSE")

	}
//
//	\ -----------------------------------------------------------------------------
//	TESTING ?DO

	func testQuestionDo()
	{
		predefine(": QD ?DO I LOOP ;")
		t("789 789 QD", expected: "")
		t("-9876 -9876 QD", expected: "")
		t("5 0 QD", expected: "0 1 2 3 4")

	 	predefine(": QD1 ?DO I 10 +LOOP ;")

		t("50 1 QD1", expected: "1 11 21 31 41")
		t("50 0 QD1", expected: "0 10 20 30 40")

		predefine(": QD2 ?DO I 3 > IF LEAVE ELSE I THEN LOOP ;")
		t("5 -1 QD2", expected: "-1 0 1 2 3")

		predefine(": QD3 ?DO I 1 +LOOP ;")
		t("4  4 QD3", expected: "")
		t("4  1 QD3", expected: "1 2 3")
		t("2 -1 QD3", expected: "-1 0 1")

		predefine(": QD4 ?DO I -1 +LOOP ;")
		t(" 4 4 QD4", expected: "")
		t(" 1 4 QD4", expected: "4 3 2 1")
		t("-1 2 QD4", expected: "2 1 0 -1")

		predefine(": QD5 ?DO I -10 +LOOP ;")
		t("  1 50 QD5", expected: "50 40 30 20 10")
		t("  0 50 QD5", expected: "50 40 30 20 10 0")
		t("-25 10 QD5", expected: "10 0 -10 -20")

		predefine("""
		VARIABLE ITERS
		VARIABLE INCRMNT

		: QD6 ( limit start increment -- )
			INCRMNT !
			0 ITERS !
			?DO
		   		1 ITERS +!
		   		I
		   		ITERS @  6 = IF LEAVE THEN
		   		INCRMNT @
			+LOOP ITERS @
		;
		""")

		t(" 4  4 -1 QD6", expected: "0")
		t(" 1  4 -1 QD6", expected: "4 3 2 1 4")
		t(" 4  1 -1 QD6", expected: "1 0 -1 -2 -3 -4 6")
		t(" 4  1  0 QD6", expected: "1 1 1 1 1 1 6")
		t(" 0  0  0 QD6", expected: "0")
		t(" 1  4  0 QD6", expected: "4 4 4 4 4 4 6")
		t(" 1  4  1 QD6", expected: "4 5 6 7 8 9 6")
		t(" 4  1  1 QD6", expected: "1 2 3 3")
		t(" 4  4  1 QD6", expected: "0")
		t(" 2 -1 -1 QD6", expected: "-1 -2 -3 -4 -5 -6 6")
		t("-1  2 -1 QD6", expected: "2 1 0 -1 4")
		t(" 2 -1  0 QD6", expected: "-1 -1 -1 -1 -1 -1 6")
		t("-1  2  0 QD6", expected: "2 2 2 2 2 2 6")
		t("-1  2  1 QD6", expected: "2 3 4 5 6 7 6")
		t(" 2 -1  1 QD6", expected: "-1 0 1 3")

	}
//
// \ -----------------------------------------------------------------------------
// TESTING BUFFER:
	func testBufferColon()
	{
		t(" 2 CELLS BUFFER: BUF:TEST", expected: "")
		t(" BUF:TEST DUP ALIGNED =", expected: "TRUE")
		t(" 111 BUF:TEST ! 222 BUF:TEST CELL+ !", expected: "")
		t(" BUF:TEST @ BUF:TEST CELL+ @", expected: "111 222 ")
	}
//
//
// \ -----------------------------------------------------------------------------
// TESTING VALUE TO
//
	func testValueTo()
	{
//		Logger.pushLevel(.debug, forName: "MyForth.ThreadedForth")
//		defer { Logger.popLevel(forName: "MyForth.ThreadedForth") }
		t("111 VALUE VAL1 -999 VALUE VAL2", expected: "")
//		log.level = .debug
		if log.isDebug
		{
			for machine in testMachines
			{
				machine.dumpWordDefinition(word: Forth.Word("val1"), to: &log.debugStream)
			}
		}
//		log.level = .info
		t("VAL1", expected: "111")
		t("VAL2", expected: "-999")
		t("222 TO VAL1", expected: "")
		t("VAL1", expected: "222")
		t(": VD1 VAL1 ;", expected: "")
		t("VD1", expected: "222")
		t(": VD2 TO VAL2 ;", expected: "")
		t("VAL2", expected: "-999")
		t("-333 VD2", expected: "")
		t("VAL2", expected: "-333")
		t("VAL1", expected: "222")
		t("123 VALUE VAL3 IMMEDIATE VAL3", expected: "123")
		t(": VD3 VAL3 LITERAL ; VD3", expected: "123")
	}
//
// \ -----------------------------------------------------------------------------
// TESTING CASE OF ENDOF ENDCASE
//
	func testCaseOfEndOfEndCase()
	{
		predefine("""
		 : CS1 CASE 1 OF 111 ENDOF
					2 OF 222 ENDOF
					3 OF 333 ENDOF
					>R 999 R>
			   ENDCASE
		 ;
		""")

		t("1 CS1", expected: "111")
		t("2 CS1", expected: "222")
		t("3 CS1", expected: "333")
		t("4 CS1", expected: "999")

		 // Nested CASE's

		predefine("""
		 : CS2 >R CASE -1 OF CASE R@ 1 OF 100 ENDOF
									 2 OF 200 ENDOF
									>R -300 R>
							 ENDCASE
						  ENDOF
					   -2 OF CASE R@ 1 OF -99  ENDOF
									 >R -199 R>
							 ENDCASE
						  ENDOF
						  >R 299 R>
				  ENDCASE R> DROP
		 ;
		""")

		t("-1 1 CS2", expected: " 100")
		t("-1 2 CS2", expected: " 200")
		t("-1 3 CS2", expected: "-300")
		t("-2 1 CS2", expected: "-99 ")
		t("-2 2 CS2", expected: "-199")
		t(" 0 2 CS2", expected: " 299")

		 // Boolean short circuiting using CASE
		predefine("""
		 : CS3  ( N1 -- N2 )
			CASE 1- FALSE OF 11 ENDOF
				 1- FALSE OF 22 ENDOF
				 1- FALSE OF 33 ENDOF
				 44 SWAP
			ENDCASE
		 ;
		""")

		t("1 CS3", expected: "11 ")
		t("2 CS3", expected: "22 ")
		t("3 CS3", expected: "33 ")
		t("9 CS3", expected: "44 ")

		 // Empty CASE statements with/without default

		t(": CS4 CASE ENDCASE ; 1 CS4", expected: "")
		t(": CS5 CASE 2 SWAP ENDCASE ; 1 CS5", expected: "2 ")
		t(": CS6 CASE 1 OF ENDOF 2 ENDCASE ; 1 CS6", expected: "")
		t(": CS7 CASE 3 OF ENDOF 2 ENDCASE ; 1 CS7", expected: "1 ")

	}
//
// \ -----------------------------------------------------------------------------
// TESTING :NONAME RECURSE
//
	func testNoNameRecurse()
	{
		predefine("""
		VARIABLE NN1
		VARIABLE NN2
		:NONAME 1234 ; NN1 !
		:NONAME 9876 ; NN2 !
		""")
		t("NN1 @ EXECUTE", expected: "1234")
		t("NN2 @ EXECUTE", expected: "9876")

		t("""
		:NONAME ( n -- 0,1,..n ) DUP IF DUP >R 1- RECURSE R> THEN ;
		   CONSTANT RN1
		""", expected: "")
		t("0 RN1 EXECUTE", expected: "0")
		t("4 RN1 EXECUTE", expected: "0 1 2 3 4")

		predefine(#"""
		:NONAME  ( n -- n1 )    \ Multiple RECURSEs in one definition
		   1- DUP
		   CASE 0 OF EXIT ENDOF
				1 OF 11 SWAP RECURSE ENDOF
				2 OF 22 SWAP RECURSE ENDOF
				3 OF 33 SWAP RECURSE ENDOF
				DROP ABS RECURSE EXIT
		   ENDCASE
		; CONSTANT RN2
		"""#)

		t(" 1 RN2 EXECUTE", expected: "0")
		t(" 2 RN2 EXECUTE", expected: "11 0")
		t(" 4 RN2 EXECUTE", expected: "33 22 11 0")
		t("25 RN2 EXECUTE", expected: "33 22 11 0")

	}
//
// \ -----------------------------------------------------------------------------
// TESTING C"

	func testCDQuote()
	{
		t(#": CQ1 C" 123" ;"#, expected: "")
		t("CQ1 COUNT EVALUATE", expected: "123")
		t(#": CQ2 C" " ;"#, expected: " ")
		t("CQ2 COUNT EVALUATE", expected: "")
		t(#": CQ3 C" 2345"COUNT EVALUATE ; CQ3"#, expected: "2345")
	}
//
//
// \ -----------------------------------------------------------------------------
// TESTING COMPILE,
//
	func testCompileComma()
	{
		predefine(":NONAME DUP + ; CONSTANT DUP+")
		t(": Q DUP+ COMPILE, ;", expected: "")
		t(": AS1 [ Q ] ;", expected: "")
		t("123 AS1", expected: "246")
	}
//
// \ -----------------------------------------------------------------------------
// \ Cannot automatically test SAVE-INPUT and RESTORE-INPUT from a console source
//
// TESTING SAVE-INPUT and RESTORE-INPUT with a string source
//
	func testSaveInputRestoreInput()
	{
		predefine("""
		VARIABLE SI_INC 0 SI_INC !

		: SI1
			SI_INC @ >IN +!
			15 SI_INC !
			;

		: S$ S" SAVE-INPUT SI1 RESTORE-INPUT 12345" ;
		""")
		t("S$ EVALUATE SI_INC @", expected: "0 2345 15")

	}
//
// \ -----------------------------------------------------------------------------
// TESTING .(
//
	func testDotOpenParenthesis()
	{
		predefine("CR CR .( Output from .()")
		t("CR .( You should see -9876: ) -9876 .", expected: "")
		t("CR .( and again: ).( -9876)CR", expected: "")

		predefine("CR CR .( On the next 2 lines you should see First then Second messages:)")
		t(#"""
		: DOTP  CR ." Second message via ." [CHAR] " EMIT    \ Check .( is immediate
			  [ CR ] .( First message via .( ) ; DOTP
		"""#, expected: "")

		predefine("CR CR")
		t(": IMM? BL WORD FIND NIP ; IMM? .(", expected: "1")
	}
//
// \ -----------------------------------------------------------------------------
// TESTING .R and U.R - has to handle different cell sizes
//
	func testDotRAndUDotR()
	{
		// Create some large integers just below/above MAX and Min INTs
		predefine(#"""
		 MAX-INT 73 79 */ CONSTANT LI1
		 MIN-INT 71 73 */ CONSTANT LI2
		
		 LI1 0 <# #S #> NIP CONSTANT LENLI1
		
		 : (.R&U.R)  ( u1 u2 -- )  \ u1 <= string length, u2 is required indentation
			TUCK + >R
			LI1 OVER SPACES  . CR R@    LI1 SWAP  .R CR
			LI2 OVER SPACES  . CR R@ 1+ LI2 SWAP  .R CR
			LI1 OVER SPACES U. CR R@    LI1 SWAP U.R CR
			LI2 SWAP SPACES U. CR R>    LI2 SWAP U.R CR
		 ;
		
		 : .R&U.R  ( -- )
			CR ." You should see lines duplicated:" CR
			." indented by 0 spaces" CR 0      0 (.R&U.R) CR
			." indented by 0 spaces" CR LENLI1 0 (.R&U.R) CR \ Just fits required width
			." indented by 5 spaces" CR LENLI1 5 (.R&U.R) CR
		 ;
		
		 CR CR .( Output from .R and U.R)
"""#)
		t(".R&U.R", expected: "")

	}
//
// \ -----------------------------------------------------------------------------
// TESTING PAD ERASE
// \ Must handle different size characters i.e. 1 CHARS >= 1

	func testPadErase()
	{
		predefine(#"""
		 84 CONSTANT CHARS/PAD      \ Minimum size of PAD in chars
		 CHARS/PAD CHARS CONSTANT AUS/PAD
		 : CHECKPAD  ( caddr u ch -- f )  \ f = TRUE if u chars = ch
			SWAP 0
			?DO
			   OVER I CHARS + C@ OVER <>
			   IF 2DROP UNLOOP FALSE EXIT THEN
			LOOP
			2DROP TRUE
		 ;
		"""#)

		t("PAD DROP", expected: "")
		t("0 INVERT PAD C!", expected: "")
		t("PAD C@ CONSTANT MAXCHAR", expected: "")
		t("PAD CHARS/PAD 2DUP MAXCHAR FILL MAXCHAR CHECKPAD", expected: "TRUE ")
//		Logger.pushLevel(.debug, forName: "MyForth.ThreadedForth")
		t("PAD CHARS/PAD 2DUP CHARS ERASE 0 CHECKPAD", expected: "TRUE ")
//		Logger.popLevel(forName: "MyForth.ThreadedForth")
		t("PAD CHARS/PAD 2DUP MAXCHAR FILL PAD 0 ERASE MAXCHAR CHECKPAD", expected: "TRUE ")
		t("PAD 43 CHARS + 9 CHARS ERASE", expected: "")
		t("PAD 43 MAXCHAR CHECKPAD", expected: "TRUE ")
		t("PAD 43 CHARS + 9 0 CHECKPAD", expected: "TRUE ")
		t("PAD 52 CHARS + CHARS/PAD 52 - MAXCHAR CHECKPAD", expected: "TRUE ")

		// Check that use of WORD and pictured numeric output do not corrupt PAD
		// Minimum size of buffers for these are 33 chars and (2*n)+2 chars respectively
		// where n is number of bits per cell

		predefine("""
		 PAD CHARS/PAD ERASE
		 2 BASE !
		 MAX-UINT MAX-UINT <# #S CHAR 1 DUP HOLD HOLD #> 2DROP
		 DECIMAL
		 BL WORD 12345678123456781234567812345678 DROP
		""")
		t("PAD CHARS/PAD 0 CHECKPAD", expected: "TRUE")

	}
//
// \ -----------------------------------------------------------------------------
// TESTING PARSE

	func testParse()
	{
		
		t("CHAR | PARSE 1234| DUP ROT ROT EVALUATE", expected: "4 1234")
		t("CHAR ^ PARSE  23 45 ^ DUP ROT ROT EVALUATE", expected: "7 23 45")
		predefine(": PA1 [CHAR] $ PARSE DUP >R PAD SWAP CHARS MOVE PAD R> ;")
		t("""
			PA1 3456
				DUP ROT ROT EVALUATE
		""", expected: "4 3456")
		t("CHAR A PARSE A SWAP DROP", expected: "0")
		t("""
			CHAR Z PARSE
				SWAP DROP
		""", expected: "0")
		t(#"CHAR " PARSE 4567 "DUP ROT ROT EVALUATE"#, expected: "5 4567")
	}
//
// \ -----------------------------------------------------------------------------
// TESTING PARSE-NAME  (Forth 2012)
// \ Adapted from the PARSE-NAME RfD tests

	func testParseName()
	{
		predefine("""
		: STR1  S" abcd" ;  : STR2  S" abcde" ;
		""")
		t("PARSE-NAME abcd  STR1  S= ", expected: "TRUE ")        // No leading spaces
		t("PARSE-NAME      abcde STR2 S= ", expected: "TRUE ")    // Leading spaces

		// Test empty parse area, new lines are necessary
		t("PARSE-NAME\n  NIP", expected: "0")
		// Empty parse area with spaces after PARSE-NAME
		t("PARSE-NAME          \n  NIP", expected: "0 ")

		t(#"""
		: PARSE-NAME-TEST ( "name1" "name2" -- n )
			PARSE-NAME PARSE-NAME S= ;
		"""#, expected: "")
		t("PARSE-NAME-TEST abcd abcd  ", expected: "TRUE ")
		t("PARSE-NAME-TEST abcd   abcd ", expected: "TRUE ")  // Leading spaces
		t("PARSE-NAME-TEST abcde abcdf ", expected: "FALSE ")
		t("PARSE-NAME-TEST abcdf abcde ", expected: "FALSE ")
		t("""
		PARSE-NAME-TEST abcde abcde
		""", expected: "TRUE ")         // Parse to end of line
		t("PARSE-NAME-TEST abcde           abcde    \n  ", expected: "TRUE ")         // Leading and trailing spaces
	}
//
// \ -----------------------------------------------------------------------------
// TESTING DEFER DEFER@ DEFER! IS ACTION-OF (Forth 2012)
// \ Adapted from the Forth 200X RfD tests
//
	func testDeferIsActionOf()
	{
		t("DEFER DEFER1", expected: "")
		t(": MY-DEFER DEFER ;", expected: "")
		t(": IS-DEFER1 IS DEFER1 ;", expected: "")
		t(": ACTION-DEFER1 ACTION-OF DEFER1 ;", expected: "")
		t(": DEF! DEFER! ;", expected: "")
		t(": DEF@ DEFER@ ;", expected: "")

		t("' * ' DEFER1 DEFER!", expected: "")
		t("2 3 DEFER1", expected: "6 ")
		t("' DEFER1 DEFER@", expected: "' * ")
		t("' DEFER1 DEF@", expected: "' * ")
		t("ACTION-OF DEFER1", expected: "' * ")
		t("ACTION-DEFER1", expected: "' * ")
		t("' + IS DEFER1", expected: "")
		t("1 2 DEFER1", expected: "3 ")
		t("' DEFER1 DEFER@", expected: "' + ")
		t("' DEFER1 DEF@", expected: "' + ")
		t("ACTION-OF DEFER1", expected: "' + ")
		t("ACTION-DEFER1", expected: "' + ")
		t("' - IS-DEFER1", expected: "")
		t("1 2 DEFER1", expected: "-1 ")
		t("' DEFER1 DEFER@", expected: "' - ")
		t("' DEFER1 DEF@", expected: "' - ")
		t("ACTION-OF DEFER1", expected: "' - ")
		t("ACTION-DEFER1", expected: "' - ")

		t("MY-DEFER DEFER2", expected: "")
		t("' DUP IS DEFER2", expected: "")
		t("1 DEFER2", expected: "1 1 ")
	}
//
// \ -----------------------------------------------------------------------------
// TESTING HOLDS  (Forth 2012)
//
	func testHolds()
	{
		predefine("""
		 : HTEST S" Testing HOLDS" ;
		 : HTEST2 S" works" ;
		 : HTEST3 S" Testing HOLDS works 123" ;
		""")

		 t("0 0 <#  HTEST HOLDS #> HTEST S=", expected: "TRUE")
		 t("""
		 123 0 <# #S BL HOLD HTEST2 HOLDS BL HOLD HTEST HOLDS #>
			HTEST3 S=
		""", expected: "TRUE")
		 t(": HLD HOLDS ;", expected: "")
		 t("0 0 <#  HTEST HLD #> HTEST S=", expected: "TRUE")
	}
//
// \ -----------------------------------------------------------------------------
// TESTING REFILL SOURCE-ID
	func testRefillSourceId()
	{
		// REFILL and SOURCE-ID from the user input device can't be tested from a file,
		// can only be tested from a string via EVALUATE

		t(#": RF1  S" REFILL" EVALUATE ; RF1"#, expected: "FALSE")
		t(#": SID1  S" SOURCE-ID" EVALUATE ; SID1"#, expected: "-1")
	}
//
// \ ------------------------------------------------------------------------------
// TESTING S\"  (Forth 2012 compilation mode)
// \ Extended the Forth 200X RfD tests
// \ Note this tests the Core Ext definition of S\" which has unedfined
// \ interpretation semantics. S\" in interpretation mode is tested in the tests on
// \ the File-Access word set

	func testSBackSlashQuote()
	{
		
		t(#": SSQ1 S\" abc" S" abc" S= ;"#, expected: "")  // No escapes
		t(#"SSQ1"#, expected: "TRUE ")
		t(#": SSQ2 S\" " ; SSQ2 SWAP DROP"#, expected: "0 ")    // Empty string

		Logger.pushLevel(.debug, forName: "MyForth.InputParser")
		// Changed the last hex value because AB translates to two UTF-8 bytes
		t(#": SSQ3 S\" \a\b\e\f\l\m\q\r\t\v\x0F0\x1Fa\xaBx\z\"\\" ;"#, expected: "")
		Logger.popLevel(forName: "MyForth.InputParser")
		t(#"SSQ3 SWAP DROP         "#, expected: " 21 ")    // String length
		t(#"SSQ3 DROP            C@"#, expected: "  7 ")    // \a   BEL  Bell
		t(#"SSQ3 DROP  1 CHARS + C@"#, expected: "  8 ")    // \b   BS   Backspace
		t(#"SSQ3 DROP  2 CHARS + C@"#, expected: " 27 ")    // \e   ESC  Escape
		t(#"SSQ3 DROP  3 CHARS + C@"#, expected: " 12 ")    // \f   FF   Form feed
		t(#"SSQ3 DROP  4 CHARS + C@"#, expected: " 10 ")    // \l   LF   Line feed
		t(#"SSQ3 DROP  5 CHARS + C@"#, expected: " 13 ")    // \m        CR of CR/LF pair
		t(#"SSQ3 DROP  6 CHARS + C@"#, expected: " 10 ")    //           LF of CR/LF pair
		t(#"SSQ3 DROP  7 CHARS + C@"#, expected: " 34 ")    // \q   "    Double Quote
		t(#"SSQ3 DROP  8 CHARS + C@"#, expected: " 13 ")    // \r   CR   Carriage Return
		t(#"SSQ3 DROP  9 CHARS + C@"#, expected: "  9 ")    // \t   TAB  Horizontal Tab
		t(#"SSQ3 DROP 10 CHARS + C@"#, expected: " 11 ")    // \v   VT   Vertical Tab
		t(#"SSQ3 DROP 11 CHARS + C@"#, expected: " 15 ")    // \x0F      Given Char
		t(#"SSQ3 DROP 12 CHARS + C@"#, expected: " 48 ")    // 0    0    Digit follow on
		t(#"SSQ3 DROP 13 CHARS + C@"#, expected: " 31 ")    // \x1F      Given Char
		t(#"SSQ3 DROP 14 CHARS + C@"#, expected: " 97 ")    // a    a    Hex follow on
		t(#"SSQ3 DROP 15 CHARS + C@"#, expected: "194 ")    // \xab -> c2 ac     Insensitive Given Char
		t(#"SSQ3 DROP 17 CHARS + C@"#, expected: "120 ")    // x    x    Non hex follow on
		t(#"SSQ3 DROP 18 CHARS + C@"#, expected: "  0 ")    // \z   NUL  No Character
		t(#"SSQ3 DROP 19 CHARS + C@"#, expected: " 34 ")    // \"   "    Double Quote
		t(#"SSQ3 DROP 20 CHARS + C@"#, expected: " 92 ")    // \\   \    Back Slash

		// The above does not test \n as this is a system dependent value.
		// Check it displays a new line
	   predefine("""
		CR .( The next test should display:)
		CR .( One line...)
		CR .( another line)
		""")
		t(#": SSQ4 S\" \nOne line...\nanotherLine\n" TYPE ; SSQ4"#, expected: "")

		// Test bare escapable characters appear as themselves
		t(#": SSQ5 S\" abeflmnqrtvxz" S" abeflmnqrtvxz" S= ; SSQ5"#, expected: "TRUE ")

		t(#": SSQ6 S\" a\""2DROP 1111 ; SSQ6"#, expected: "1111 ") // Parsing behaviour

		t(#": SSQ7  S\" 111 : SSQ8 S\\\" 222\" EVALUATE ; SSQ8 333" EVALUATE ;"#, expected: "")
		t(#"SSQ7"#, expected: "111 222 333 ")
		t(#": SSQ9  S\" 11 : SSQ10 S\\\" \\x32\\x32\" EVALUATE ; SSQ10 33" EVALUATE ;"#, expected: "")
		t(#"SSQ9"#, expected: "11 22 33 ")

	}

}
