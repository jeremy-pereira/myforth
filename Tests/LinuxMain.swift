import XCTest

import MyForthTests

var tests = [XCTestCaseEntry]()
tests += MyForthTests.allTests
tests += WordParserTests.allTests
tests += CoreTests.allTests
tests += TestDoublePrecision.allTests
tests += StackTests.allTests
XCTMain(tests)
