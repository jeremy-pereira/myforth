//
//  CStackTests.swift
//  
//
//  Created by Jeremy Pereira on 22/07/2021.
//
//  Copyright (c) Jeremy Pereira xxxx
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.


import XCTest
import CForth
@testable import MyForth

class StackTests: XCTestCase
{

	override func setUp()
	{
		super.setUp()
	}

	override func tearDown()
	{
		// Put teardown code here. This method is called after the invocation of each test method in the class.
		super.tearDown()
	}

	func testSingleItem()
	{
		let stack = CForth.stackNamed("test")
		defer { stackFree(stack) }
		stackPush(stack, 1)
		stackPush(stack, 2)
		XCTAssert(stackCount(stack) == 2, "Wrong stack count")
		var ret = stackTop(stack)
		XCTAssert(ret.status == SC_OK && ret.result == 2, "Invalid peek")
		ret = stackPop(stack)
		XCTAssert(ret.status == SC_OK && ret.result == 2, "Invalid peek")
		ret = stackPop(stack)
		XCTAssert(ret.status == SC_OK && ret.result == 1, "Seconf pop failed")
		XCTAssert(stackIsEmpty(stack), "Stack should be empty")
		ret = stackPop(stack)
		XCTAssert(ret.status == SC_STACK_UNDERFLOW, "Managed to pop empty stack")
	}

	func testStackFrame()
	{
		let stack = CForth.stackNamed("test")
		XCTAssert(stackFrameOffsetOfTop(stack) == 0)
		stackPush(stack, 1)
		XCTAssert(stackFrameOffsetOfTop(stack) == 1)
		stackCreateFrame(stack)
		XCTAssert(stackFrameOffsetOfTop(stack) == 0)
		stackPush(stack, 2)
		XCTAssert(stackFrameOffsetOfTop(stack) == 1)
		stackPush(stack, 3)
		do
		{
			XCTAssert(try stack.peek(frameOffset: -1) == 1)
			XCTAssert(try stack.peek(frameOffset: 1) == 2)
			XCTAssert(try stack.peek(frameOffset: 2) == 3)
			_ = stack.pop()
			_ = stack.pop()
			try stack.popFrame()
			XCTAssert(try stack.peek(frameOffset: 1) == 1)
		}
		catch
		{
			XCTFail("\(error)")
		}
	}


}
