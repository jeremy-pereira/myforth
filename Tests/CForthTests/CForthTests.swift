//
//  CForthTests.swift
//  
//
//  Created by Jeremy Pereira on 21/07/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.


import XCTest
import Toolbox
import MyForth

private let log = Logger.getLogger("MyForthTests.MyForthTests")

final class CForthTests: XCTestCase
{

	func testExercise2()
	{
		Logger.pushLevel(.debug, forName: "MyForth.Forth")
		defer { Logger.popLevel(forName: "MyForth.Forth") }
		let inputString = "2 3 4 + + ."
		var output = ""
		let forthMachine = MyForth(trace: true)
		do
		{
			try forthMachine.interpret(input: inputString, output: &output)
			XCTAssert(output == "9")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testStackUnderflow()
	{
		let inputString = "2 3 4 + + + ."
		var output = ""
		let forthMachine = MyForth()
		do
		{
			try forthMachine.interpret(input: inputString, output: &output)
			XCTFail("Should throw here")
		}
		catch MyForth.Error.stackUnderflow
		{
			// This is OK
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testInvalidWord()
	{
		let errorWord = "notdefinedbyme"
		let inputString = "2 3 4 + \(errorWord) ."
		var output = ""
		let forthMachine = MyForth()
		do
		{
			try forthMachine.interpret(input: inputString, output: &output)
			XCTAssert(output == "9")
		}
		catch MyForth.Error.unrecognisedWord(let word)
		{
			XCTAssert(word == errorWord)
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testArithmetic()
	{
		runTest(input: "2 2 + .", expected: "4")
		runTest(input: "2 1 - .", expected: "1")
		runTest(input: "7 3 mod .", expected: "1")
		runTest(input: "7 3 / .", expected: "2")
		runTest(input: "3 4 + 5 * .", expected: "35")
		runTest(input: "2 negate .", expected: "-2")
		runTest(input: "7 3 /mod . .", expected: "21")
		runTest(input: "-7 3 /mod .s", expected: "<2> -1 -2")
	}

	func testMiscellaneous()
	{
		runTest(input: "1 2 .s", expected: "<2> 1 2")
	}

	func testStackManipulation()
	{
		runTest(input: "1 drop .s", expected: "<0>")
		runTest(input: "1 dup .s", expected: "<2> 1 1")
		runTest(input: "1 2 over .s", expected: "<3> 1 2 1")
		runTest(input: "1 2 swap .s", expected: "<2> 2 1")
		runTest(input: "1 2 3 rot .s", expected: "<3> 2 3 1")
		runTest(input: "1 2 3 4 2swap .s", expected: "<4> 3 4 1 2")
		runTest(input: "1 2 2drop .s", expected: "<0>")

	}

	func testDefine()
	{
		let squareString =
		"""
		: squared
			dup * ;
		5 squared .
		7 squared .
		"""
		runTest(input: squareString, expected: "2549")
	}

	func testBranch()
	{
		runTest(input: ": btest 5 3 branch 6 7 + . ; btest", expected: "12")
		runTest(input: ": btest 4 0<>branch? 1 + ; 1 0 btest .", expected: "2")
		runTest(input: ": btest 4 0<>branch? 1 + ; 1 1 btest .", expected: "1")
		runTest(input: ": btest 4 0=branch? 1 + ; 1 0 btest .", expected: "1")
		runTest(input: ": btest 4 0=branch? 1 + ; 1 1 btest .", expected: "2")
	}

	func testConditionals()
	{
		runTest(input: "1 2 < .", expected: "-1")
		runTest(input: "2 1 < .", expected: "0")
		runTest(input: "2 2 < .", expected: "0")
		runTest(input: "1 2 > .", expected: "0")
		runTest(input: "2 1 > .", expected: "-1")
		runTest(input: "2 2 > .", expected: "0")
		runTest(input: "1 2 <= .", expected: "-1")
		runTest(input: "2 1 <= .", expected: "0")
		runTest(input: "2 2 <= .", expected: "-1")
		runTest(input: "1 2 >= .", expected: "0")
		runTest(input: "2 1 >= .", expected: "-1")
		runTest(input: "2 2 >= .", expected: "-1")
		runTest(input: "2 1 = .", expected: "0")
		runTest(input: "2 2 = .", expected: "-1")
		runTest(input: "2 1 <> .", expected: "-1")
		runTest(input: "2 2 <> .", expected: "0")
	}

	func testImmediate()
	{
		let program = """
		1
		: imm 5 . ; immediate
		: foo imm 3 ;
		"""
		runTest(input: program, expected: "5")
	}

	func testComment()
	{
		runTest(input: "3 ( 4 ) .", expected: "3")
		runTest(input: "3 ( 4 xxx) .", expected: "3")
		let lineComment1 = """
		3 \\ 4
		.
		"""
		runTest(input: lineComment1, expected: "3")
		runTest(input: ": foo 3 ( 4 ) ; foo .", expected: "3")
		let lineComment2 = """
		: foo 3 \\ 4
		;
		foo .
		"""
		runTest(input: lineComment2, expected: "3")
	}

	func testIf()
	{
		log.debug("if definition: " + (MyForth().dumpDefinition(for: "if2") ?? "none"))
		runTest(input: ": foo if 3 then 6 ; 1 0 foo .s", expected: "<2> 1 6")
		runTest(input: ": foo if 3 then 6 ; 1 5 foo .s", expected: "<3> 1 3 6")
		runTest(input: ": foo if 3 else 4 then 6 ; 1 0 foo .s", expected: "<3> 1 4 6")
		runTest(input: ": foo if 3 else 4 then 6 ; 1 5 foo .s", expected: "<3> 1 3 6")
	}

	func testIfThen()
	{
		let program2 =
		"""
		: fibo
			dup
			if ( non zero )
				dup 1 =
				if ( is 1 )
				else ( is greater than 1 )
					dup 1 - fibo swap 2 - fibo +
				then
			then ;
		26 fibo .s
		"""
		runTest(input: program2, expected: "<1> 121393")

		if let error = runExpectException(input: "3 if ")
		{
			switch error
			{
			case MyForth.Error.interpretingACompileOnlyWord(let string):
				XCTAssert(string == "if", "Expecting 'if', got '\(string)'")
			default:
				XCTFail("\(error)")
			}
		}
		else
		{
			XCTFail("Expected error")
		}
		if let error = runExpectException(input: "3 else ")
		{
			switch error
			{
			case MyForth.Error.interpretingACompileOnlyWord(let string):
				XCTAssert(string == "else")
			default:
				XCTFail("\(error)")
			}
		}
		else
		{
			XCTFail("Expected error")
		}
		if let error = runExpectException(input: "3 then ")
		{
			switch error
			{
			case MyForth.Error.interpretingACompileOnlyWord(let string):
				XCTAssert(string == "then")
			default:
				XCTFail("\(error)")
			}
		}
		else
		{
			XCTFail("Expected error")
		}
	}

	func testIfThenUnmatched()
	{
		guard let error = runExpectException(input: ": foo if 3  6 ; 1 0 foo .s")
			else { return }
		switch error
		{
		case MyForth.Error.unmatchedOrigin:
			break
		default:
			XCTFail("\(error)")
		}
	}

	func testJumpOutOfBounds()
	{
		guard let error = runExpectException(input: ": foo -200 branch ; 1 0 foo .s")
			else { return }
		switch error
		{
		case MyForth.Error.codeIndexOutOfBounds:
			break
		default:
			XCTFail("\(error)")
		}
	}

	func testBeginUntil()
	{
		runTest(input: ": GI4 BEGIN DUP 1 + DUP 5 > UNTIL ; 3 GI4 .s", expected: "<4> 3 4 5 6")
		runTest(input: ": GI4 BEGIN DUP 1 + DUP 5 > UNTIL ; 5 GI4 .s", expected: "<2> 5 6")
		runTest(input: ": GI4 BEGIN DUP 1 + DUP 5 > UNTIL ; 6 GI4 .s", expected: "<2> 6 7")
	}

	func testBeginWhileRepeat()
	{
		runTest(input: ": GI3 BEGIN DUP 5 < WHILE DUP 1 + REPEAT ; 0 GI3 .s", expected: "<6> 0 1 2 3 4 5")
		runTest(input: ": GI3 BEGIN DUP 5 < WHILE DUP 1 + REPEAT ; 4 GI3 .s", expected: "<2> 4 5")
		runTest(input: ": GI3 BEGIN DUP 5 < WHILE DUP 1 + REPEAT ; 5 GI3 .s", expected: "<1> 5")
		runTest(input: ": GI3 BEGIN DUP 5 < WHILE DUP 1 + REPEAT ; 6 GI3 .s", expected: "<1> 6")
		runTest(input: ": GI5 BEGIN DUP 2 > WHILE DUP 5 < WHILE DUP 1 + REPEAT 123 ELSE 345 THEN ; 1 GI5 .s", expected: "<2> 1 345")
		runTest(input: ": GI5 BEGIN DUP 2 > WHILE DUP 5 < WHILE DUP 1 + REPEAT 123 ELSE 345 THEN ; 2 GI5 .s", expected: "<2> 2 345")
		runTest(input: ": GI5 BEGIN DUP 2 > WHILE DUP 5 < WHILE DUP 1 + REPEAT 123 ELSE 345 THEN ; 3 GI5 .s", expected: "<4> 3 4 5 123")
		runTest(input: ": GI5 BEGIN DUP 2 > WHILE DUP 5 < WHILE DUP 1 + REPEAT 123 ELSE 345 THEN ; 4 GI5 .s", expected: "<3> 4 5 123")
		runTest(input: ": GI5 BEGIN DUP 2 > WHILE DUP 5 < WHILE DUP 1 + REPEAT 123 ELSE 345 THEN ; 5 GI5 .s", expected: "<2> 5 123")
	}

	func testDepth()
	{
		runTest(input: "depth .s", expected: "<1> 0")
		runTest(input: "99 depth .s", expected: "<2> 99 1")
	}

	func testConstant()
	{
		runTest(input: "2 constant two two .s", expected: "<1> 2")
	}

	func testHexDecimal()
	{
		runTest(input: "20 hex .", expected: "14")
		runTest(input: "hex 20 decimal .", expected: "32")
		runTest(input: "hex f decimal .", expected: "15")
	}

	func testFDivMod()
	{
		runTest(input: """
		0 INVERT 1 RSHIFT INVERT   	CONSTANT MIN-INT
		MIN-INT S>D MIN-INT FM/MOD .s
		""", expected: "<2> 0 1")
	}

	private func runTest(input: String, expected: String, trace: Bool = false)
	{
		var output = ""
		var forthMachine: ForthProtocol = CForthWrapper()
		forthMachine.trace = true
		if trace
		{
			Logger.pushLevel(.debug, forName: "MyForth.MyForth")
		}
		defer
		{
			if trace
			{
				Logger.popLevel(forName: "MyForth.MyForth")
			}
		}

		do
		{
			try forthMachine.interpret(input: input, output: &output)
			XCTAssert(output == expected, "Running '\(input)', expected: '\(expected)', got: '\(output)'")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	private func runExpectException(input: String) -> Error?
	{
		var output = ""
		let forthMachine = MyForth()
		do
		{
			try forthMachine.interpret(input: input, output: &output)
			XCTFail("Should have thrown but got: '\(output)'")
			return nil
		}
		catch
		{
			return error
		}
	}


	static var allTests = [
		("testExercise2", testExercise2),
		("testStackUnderflow", testStackUnderflow),
		("testInvalidWord", testInvalidWord),
		("testArithmetic", testArithmetic),
		("testMiscellaneous", testMiscellaneous),
		("testStackManipulation", testStackManipulation),
		("testDefine", testDefine),
		("testBranch", testBranch),
		("testConditionals", testConditionals),
		("testComment", testComment),
		("testIf", testIf),
	]
}

