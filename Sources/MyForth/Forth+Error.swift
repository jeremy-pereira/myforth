//
//  Forth+Error.swift
//  
//
//  Created by Jeremy Pereira on 20/06/2020.
//
//  Copyright (c) Jeremy Pereira 2020-2023
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.


extension Forth
{
	/// Errors that can be thrown by the Forth machine
	public enum Error: Swift.Error
	{
		/// Forth code threw an error that is not mapped to one of the errors
		/// here
		case undefinedError(Int)
		/// Some integer operation occurred that won't fit into an integer
		///
		/// **Error** #1
		case integerOverflow
		/// A division by zero error occurred
		///
		/// **Error** #2
		case divisionByZero
		/// Word hasn't been defined in the dictionary
		case unrecognisedWord(String)
		/// Word definition exists but is not implemented
		case wordNotImplemented(String)
		/// `:` was encountered when compiling
		case alreadyCompiling
		/// `;` was encountered in interpretative mode
		case interpretingACompileOnlyWord(String)
		/// Tried to branch to a location not in the currently executing list
		/// of words.
		case codeIndexOutOfBounds
		/// The interpreter was at the start of the definition and encountered
		/// something other than a word string
		///
		/// This is probably a programming error. `gforth` for example seems to
		/// let you use anything as the word name - even numbers.
		case expectedWordNameAtDefinitionStart(String)
		/// When returning from executing a word, the state machine is invalid
		///
		/// This might happen if, for example a word definition uses `:` but no
		/// matching `;`
		case invalidReturnState(String)
		/// Trying to match an origin, found something else
		case unmatchedOrigin
		/// Trying to match an destination, found something else
		case unmatchedDestination
		/// Trying to put non data type onto the data stack
		case typeMismatch
		/// The control stack was popped when looking for a word name and
		/// didn't get a `.definition` or a `.constant`
		case expectedDefinitionOrConstant
		/// Attempting a shift by an amount that is not in the range `0 ..< Int.bitWidth`
		case shiftOutOfRange
		/// Storing a cell at an unaligned address
		///
		/// Cells must be 8 byte aligned
		case alignment
		/// An address in the data space is either less than zero or greater
		/// than the data space pointer
		case addressOutOfBounds
		/// Tried to execute an exectution token that does not exist
		case unrecognisedExecutionToken(String)
		/// Attempting to convert an invalid character sequence into UTF8
		case invalidUTF8
		/// Attempt to convert an Int to a unicode scalr value
		case invalidUnicode(Int)
		/// Thrown if `pushNext` does not have a parsed literal after it
		case expectedParsedLiteral
		/// If a stack underflows
		case stackUnderflow(String)
		/// Tried to pop a stack frame when the item is not a frame element.
		case notAStackFrame(String)
		/// There is no data field defined for the execution token.
		///
		/// This usuually means the wrd was not defined with `create`
		case noDataFieldForExecutionToken(String)
		/// `does>` called when the last defined word was not defined by
		/// `create`.
		case invalidDoes(lastDefined: String, caller: String)
		/// Tried to create a dictionary slot without a word in the input
		case noWordToCreate
		/// Need to execute a word when we haven't parsed one
		case noWordToExecute
		/// Thrown if we try to restore input following `evaluate` and there isn't any
		case noInputToRestore
		/// Placeholder in primitive switch to indicate not implemented
		case primitiveNotImplemented(Int)
		/// Reached end of file when expected more characters
		case unexpectedEOF
		/// Thrown when we try to parse a word but there aren't any
		case endOfInput
		/// Tried to apply `immediate` when no word has been compiled
		case noWordCompiled
		/// Thrown if some function has not yet been implemented
		case notImplemented(file: String, line: Int, function: String)
		/// Attempt to convert a number using an invalid base
		case invalidBase(Int)
		/// Attempt to write to a read only part of data space
		///
		/// Note that the associated data is the local address within the buffer
		/// that raised the exception
		case writeToReadOnlyCell(Cell)
		/// Unable to convert an alleged hex string to an integer
		case invalidHexString(String)
		/// An escaped string is terminated in the middle of an escape sequence
		case invalidEscapeSequence(String)
		/// Attempted to include a file without knowing its name
		case includingUnnamedFileDescriptor
		/// Block number is less than one or too big
		case invalidBlockNumber(Int)
		/// Try to read or possibly write an unknown block into a block buffer
		case noBlockNumberSpecified
		/// No `buffer` or `block` has been executed
		case noCurrentBlock
		/// Attempt made to negate an unsigned integer
		case cantNegateUnsigned
		/// unrecognised function for current word
		case unrecognisedCurrentWordFunc(Cell)
	}
	
	static func notImplementedError(file: String = #file, line: Int = #line, function: String = #function) -> Error
	{
		return Forth.Error.notImplemented(file: file, line: line, function: function)
	}

}

extension Forth
{
	public enum FileError: Swift.Error
	{
		case invalidAccessMode(Int32)
	}
}
