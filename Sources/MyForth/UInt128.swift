//
//  UInt128.swift
//
//
//  Created by Jeremy Pereira on 22/12/2023.
//
//  Copyright (c) Jeremy Pereira 2023
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Toolbox

struct UInt128
{
	static var bitWidth: Int = UInt.bitWidth * 2

	internal let low: UInt
	internal let high: UInt

	init(low: UInt, high: UInt)
	{
		self.low = low
		self.high = high
	}

	init(_ uint: UInt)
	{
		self.low = uint
		self.high = 0
	}

	func addingReportingOverflow(_ rhs: UInt128) -> (partialValue: UInt128, overflow: Bool)
	{
		let (newLow, halfCarry) = self.low.addingReportingOverflow(rhs.low)
		var (newHigh, fullCarry) = self.high.addingReportingOverflow(rhs.high)
		if halfCarry
		{
			let savedCarry = fullCarry
			(newHigh, fullCarry) = newHigh.addingReportingOverflow(1)
			fullCarry = savedCarry || fullCarry
		}
		return (partialValue: UInt128(low: newLow, high: newHigh), overflow: fullCarry)
	}

	func subtractingReportingOverflow(_ rhs: UInt128) -> (partialValue: UInt128, overflow: Bool)
	{
		let (newLow, halfCarry) = self.low.subtractingReportingOverflow(rhs.low)
		var (newHigh, fullCarry) = self.high.subtractingReportingOverflow(rhs.high)
		if halfCarry
		{
			let savedCarry = fullCarry
			(newHigh, fullCarry) = newHigh.subtractingReportingOverflow(1)
			fullCarry = savedCarry || fullCarry
		}
		return (partialValue: UInt128(low: newLow, high: newHigh), overflow: fullCarry)
	}

	func multipliedFullWidth(by other: UInt128) -> (high: UInt128, low: UInt128)
	{
		// Going to use long multiplication (carrys omitted)
		//
		//            h1    l1
		// x          h2    l2
		// -------------------
		//         h1xl2 l1xl2
		//   h1xh2 l1xh2     0
		// -------------------
		// p4   p3    p2    p1

		// Do all the multiplications first
		let (l1l2Carry, l1l2) = self.low.multipliedFullWidth(by: other.low)
		let (h1l2Carry, h1l2) = self.high.multipliedFullWidth(by: other.low)
		let (l1h2Carry, l1h2) = self.low.multipliedFullWidth(by: other.high)
		let (h1h2Carry, h1h2) = self.high.multipliedFullWidth(by: other.high)

		// Now add up the columns including all the carries
		let p1 = l1l2
		let (p2, p2Carry) = UInt.sumWithCarry([h1l2, l1h2, l1l2Carry])
		let (p3, p3Carry) = UInt.sumWithCarry([p2Carry, h1h2, h1l2Carry, l1h2Carry])
		let p4 = p3Carry + h1h2Carry // Should not be possible for this to overflow
		return (high: UInt128(low: p3, high: p4), low: UInt128(low: p1, high: p2))
	}

	func multipliedReportingOverflow(by rhs: UInt128) -> (partialValue: UInt128, overflow: Bool)
	{
		let (high, low) = self.multipliedFullWidth(by: rhs)
		return (partialValue: low, overflow: high != 0)
	}

	func quotientAndRemainder(dividingBy divisor: UInt128) -> (quotient: UInt128, remainder: UInt128)
	{
		// Get some easy cases out of the way
		guard divisor != 0 else { fatalError("Division by 0") }
		guard divisor != 1 else { return (quotient: self, remainder: 0) }
		guard divisor <= self else { return (quotient: 0, remainder: self) }
		// How many times we need to test the divisor against the shifting dividend?
		// We need to stop when the number of remaining bits in the dividend
		// is less than the bits in the divisor.
		let numberOfTests = self.significantBits - divisor.significantBits + 1
		// We also need to be ready to shift the first significant bit into
		// position to align with the divisor to skip testing any leading
		// zeros.
		let initialShift = divisor.leadingZeroBitCount + 1 - self.leadingZeroBitCount
		var dividendHigh = self >> initialShift
		var dividendLow = self << (UInt128.bitWidth - initialShift)
		var quotient: UInt128 = 0
		for _ in 0 ..< numberOfTests
		{
			quotient <<= 1
			dividendHigh <<= 1
			dividendHigh |= dividendLow.isTopBitSet ? 1 : 0
			dividendLow <<= 1
			if dividendHigh >= divisor
			{
				quotient |= 1
				dividendHigh -= divisor
			}
		}
		return (quotient: quotient, remainder: dividendHigh)
	}

	func dividedReportingOverflow(by rhs: UInt128) -> (partialValue: UInt128, overflow: Bool)
	{
		guard rhs != 0 else { return (partialValue: self, overflow: true) }
		let (q, _) = self.quotientAndRemainder(dividingBy: rhs)
		return (q, false)
	}

	func remainderReportingOverflow(dividingBy rhs: UInt128) -> (partialValue: UInt128, overflow: Bool)
	{
		guard rhs != 0 else { return (partialValue: self, overflow: true) }
		let (_, r) = self.quotientAndRemainder(dividingBy: rhs)
		return (r, false)
	}

	var nonzeroBitCount: Int { low.nonzeroBitCount + high.nonzeroBitCount }

	var leadingZeroBitCount: Int
	{
		high == 0 ? (UInt.bitWidth + low.leadingZeroBitCount) : high.leadingZeroBitCount
	}

	/// The number of significant bits
	///
	/// one more than the position of the most significant 1
	var significantBits: Int { UInt128.bitWidth - leadingZeroBitCount }

	var byteSwapped: UInt128
	{
		UInt128(low: high.byteSwapped, high: low.byteSwapped)
	}

	var words: [UInt] { [low, high] }

	var trailingZeroBitCount: Int
	{
		low == 0 ? (UInt.bitWidth + high.trailingZeroBitCount) : low.trailingZeroBitCount
	}

	typealias IntegerLiteralType = UInt

	var isTopBitSet: Bool { high & 0x8000_0000_0000_0000 != 0 }

	static func <<= <RHS: BinaryInteger> (lhs: inout UInt128, rhs: RHS)
	{
		lhs = lhs << rhs
	}


	static func << <RHS: BinaryInteger> (lhs: UInt128, rhs: RHS) -> UInt128
	{
		guard rhs != 0 else { return lhs }

		if rhs < 0
		{
			return lhs >> (0 - rhs)
		}
		else if rhs > UInt.bitWidth
		{
			let newHigh = lhs.low << (rhs - RHS(UInt.bitWidth))
			return  UInt128(low: 0, high: newHigh)
		}
		else
		{
			let newLow = lhs.low << rhs
			let newHigh = (lhs.high << rhs) | (lhs.low >> (RHS(UInt.bitWidth) - rhs))
			return UInt128(low: newLow, high: newHigh)
		}
	}

	static func >>= <RHS: BinaryInteger> (lhs: inout UInt128, rhs: RHS)
	{
		lhs = lhs >> rhs
	}

	static func >> <RHS: BinaryInteger> (lhs: UInt128, rhs: RHS) -> UInt128
	{
		guard rhs != 0 else { return lhs }

		if rhs < 0
		{
			return lhs << (0 - rhs)
		}
		else if rhs > UInt.bitWidth
		{
			let newLow = lhs.high >> (rhs - RHS(UInt.bitWidth))
			return UInt128(low: newLow, high: 0)
		}
		else
		{
			let newHigh = lhs.high >> rhs
			let newLow = (lhs.low >> rhs) | (lhs.high << (RHS(UInt.bitWidth) - rhs))
			return UInt128(low: newLow, high: newHigh)
		}
	}

	static func == (lhs: UInt128, rhs: UInt128) -> Bool
	{
		lhs.low == rhs.low && lhs.high == rhs.high
	}

	static func != (lhs: UInt128, rhs: UInt128) -> Bool
	{
		lhs.low != rhs.low || lhs.high != rhs.high
	}

	static func < (lhs: UInt128, rhs: UInt128) -> Bool
	{
		lhs.high < rhs.high || (lhs.high == rhs.high && lhs.low < rhs.low)
	}

}

extension UInt128: FixedWidthInteger
{
	init(integerLiteral value: UInt)
	{
		self.init(low: value, high: 0)
	}

	init(_truncatingBits value: UInt)
	{
		self.init(low: value, high: 0)
	}

	typealias Magnitude = UInt128

	static var max: UInt128 { UInt128(low: UInt.max, high: UInt.max) }

	static var min: UInt128 { UInt128(low: UInt.min, high: UInt.min) }

	func dividingFullWidth(_ dividend: (high: UInt128, low: Magnitude)) -> (quotient: UInt128, remainder: UInt128)
	{
		notImplemented()
	}

	static var isSigned: Bool = false

	static func / (lhs: UInt128, rhs: UInt128) -> UInt128
	{
		let (quotient, _) = lhs.quotientAndRemainder(dividingBy: rhs)
		return quotient
	}

	static func /= (lhs: inout UInt128, rhs: UInt128)
	{
		lhs = lhs / rhs
	}

	static func % (lhs: UInt128, rhs: UInt128) -> UInt128
	{
		let (_, remainder) = lhs.quotientAndRemainder(dividingBy: rhs)
		return remainder
	}

	static func %= (lhs: inout UInt128, rhs: UInt128)
	{
		lhs = lhs % rhs
	}

	static func * (lhs: UInt128, rhs: UInt128) -> UInt128
	{
		let (result, overflow) = lhs.multipliedReportingOverflow(by: rhs)
		guard !overflow else { fatalError("Multiplication overflow") }
		return result
	}

	static func *= (lhs: inout UInt128, rhs: UInt128)
	{
		lhs = lhs * rhs
	}


	static func & (lhs: UInt128, rhs: UInt128) -> UInt128
	{
		UInt128(low: lhs.low & rhs.low, high: lhs.high & rhs.high)
	}

	static func &= (lhs: inout UInt128, rhs: UInt128)
	{
		lhs = lhs & rhs
	}

	static func | (lhs: UInt128, rhs: UInt128) -> UInt128
	{
		UInt128(low: lhs.low | rhs.low, high: lhs.high | rhs.high)
	}

	static func |= (lhs: inout UInt128, rhs: UInt128)
	{
		lhs = lhs | rhs
	}

	static func ^ (lhs: UInt128, rhs: UInt128) -> UInt128
	{
		UInt128(low: lhs.low ^ rhs.low, high: lhs.high ^ rhs.high)
	}

	static func ^= (lhs: inout UInt128, rhs: UInt128)
	{
		lhs = lhs ^ rhs
	}

	static func + (lhs: UInt128, rhs: UInt128) -> UInt128
	{
		let (result, overflow) = lhs.addingReportingOverflow(rhs)
		guard !overflow else { fatalError("Addition overflow") }
		return result
	}

	static func - (lhs: UInt128, rhs: UInt128) -> UInt128
	{
		let (result, overflow) = lhs.subtractingReportingOverflow(rhs)
		guard !overflow else { fatalError("Subtraction overflow") }
		return result
	}

}

extension UInt128: UnsignedInteger
{
}


extension UInt
{
	static func sumWithCarry(_ list: [UInt]) -> (sum: UInt, carry: UInt)
	{
		return list.reduce((sum: 0, carry: 0))
		{
			result, number in
			let (runningTotal, carry) = result
			let (newTotal, overflow) = runningTotal.addingReportingOverflow(number)
			return (sum: newTotal, carry: carry + (overflow ? 1 : 0))
		}
	}
}
