//
//  Forth.swift
//
//
//  Created by Jeremy Pereira on 06/06/2020.
//
//  Copyright (c) Jeremy Pereira 2023
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
import Toolbox
import SystemPackage

private let log = Logger.getLogger("MyForth.Forth")

/// A Forth interpreter
///
/// This class implements ``MyForth/ForthProtocol`` to provide a servicable
/// Forth interpreter.
///
///
public class Forth
{
	/// Data that goes on the data stack
	///
	/// This implementation uses Swift's natural `Int` type. If `Int` is only
	/// 32 bit, it might be broken.
	public typealias Cell = Int
	typealias ReturnStackCell = Int


	/// Forth standard error numbers for throwing
	///
	/// The numbers mostly come from the Forth Error Standard
	///
	///
	/// https://forth-standard.org/standard/exception
	///
	/// Exceptions that cannot easily be defined by the standard are in the
	/// system range: `-4096 ... -256`
	///
 	public enum ErrorNumber: Cell
 	{
		case integerOverflow = -256
		case openFile = -69
		case divisionByZero = -10
 	}
	
	/// Size of Forth IO blocks
	///
	/// This is set at 1024 by the Forth standard
	public static let blockSize = 1024
	/// Width in characters of a block screen
	public static let blockScreenWidth = 64

	/// The default name for the block file
	public static let blockFileName = "block.fb"
}

internal extension Forth.ReturnStackCell
{
	init(wordNumber: Int, index: Int)
	{
		self = (wordNumber & 0xffff_ffff) | (index << 32)
	}
	/// Offset within a word definition
	var index: Int { (self >> 32) & 0xffff_ffff }
	/// Index of word definition
	var wordNumber: Int { self & 0xffff_ffff }
	/// Descrition of a return stack entry
	var rsDescription: String
	{
		return "[#\(String(self.wordNumber, radix: 16)),\(String(self.index, radix: 16))]"
	}
}

internal extension Optional
{
	mutating func pop() -> Self
	{
		let ret = self
		self = nil
		return ret
	}
}

extension Forth
{
	////// The type of a Forth word.
	///
	/// This is pretty much a string but with case insensitive comparison
	public struct Word: ExpressibleByStringLiteral
	{
		private(set) var string: String

		init<S: StringProtocol>(_ string: S)
		{
			self.string = String(string)
		}

		public init(stringLiteral: String)
		{
			self.init(stringLiteral)
		}

		mutating func append(_ c: Character)
		{
			string.append(c)
		}
	}
}

extension Forth.Word: Hashable
{
	public func hash(into: inout Hasher)
	{
		into.combine(string.lowercased())
	}

	public static func == (lhs: Forth.Word, rhs: Forth.Word) -> Bool
	{
		lhs.string.lowercased() == rhs.string.lowercased()
	}
}

internal extension Forth
{
	enum ControlWord: CustomStringConvertible, Frameable
	{
		case orig(Int)
		case dest(Int)
		case frame(old: Int)

		var description: String
		{
			switch self
			{
			case .orig(let value):
				return ".orig(\(value))"
			case .dest(let value):
				return ".dest(\(value))"
			case .frame(old: let old):
				return ".frame(old: \(old))"
			}
		}

		static func makeFrom(framePointer: Int) -> Forth.ControlWord
		{
			return .frame(old: framePointer)
		}

		func asFramePointer() throws -> Int
		{
			switch self
			{
			case .frame(old: let old):
				return old
			default:
				throw Error.notAStackFrame("Control stack: \(self)")
			}
		}
	}
}

internal extension Forth.Cell
{
	var asTokenString: String
	{
		let stringValue = String(UInt(bitPattern: self), radix: 16)
		return "0x" + stringValue
	}
	/// The bytes required to store a cell
	static var bytesPerCell: Int { bitWidth / UInt8.bitWidth }
	/// Bytes needed to store a Forth character
	static var bytesPerChar: Int { 1 }

	var bytes: [UInt8]
	{
		var ret: [UInt8] = []
		let mask = ~((~UInt(0)) << UInt8.bitWidth)
		var bits = UInt(bitPattern: self)
		let bytes = self.bitWidth / UInt8.bitWidth

		for _ in 0 ..< bytes
		{
			ret.append(UInt8(bits & mask))
			bits >>= UInt8.bitWidth
		}
		return ret
	}
}

internal extension Forth
{
	private struct NullStream: TextOutputStream
	{
		mutating func write(_ string: String) {}
	}
	/// A t text output streamwrapper that gies access to the origianl
	/// stream
	struct TextOutputStreamWraper: TextOutputStream
	{
		init<STREAM: TextOutputStream>(wrapped: STREAM)
		{
			self.wrapped = wrapped
		}

		private(set) var wrapped: TextOutputStream


		mutating func write(_ string: String)
		{
			wrapped.write(string)
		}

		/// A null output stream
		///
		/// Anything written to it is discarded.
		static var null = TextOutputStreamWraper(wrapped: NullStream())

		mutating func write(item: String, width: Int)
	 	{
			let padding: String = width > item.count ? 
				String(repeating: " ", count: width - item.count) : ""
			write(padding)
			write(item)
			write(" ")
		}
	}
}

extension Forth
{
	struct Environment
	{
		enum Value
		{
			case int(Int)
			case uint(UInt)
			case doubleInt(DoublePrecision<Int>)
			case doubleUInt(DoublePrecision<UInt>)
			case flag(Bool)
		}

		private var entries: [String : Value] =
		[
			   "/COUNTED-STRING" : .int(Int(UInt8.max)),
			             "/HOLD" : .int(Int(Int32.max)),
			              "/PAD" : .int(Int(Int32.max)),
			 "ADDRESS-UNIT-BITS" : .int(64),
			           "FLOORED" : .flag(false),
			          "MAX-CHAR" : .uint(256),
			             "MAX-D" : .doubleInt(DoublePrecision<Int>.max),
			             "MAX-N" : .int(Int.max),
			             "MAX-U" : .uint(UInt.max),
			            "MAX-UD" : .doubleUInt(DoublePrecision<UInt>.max),
			"RETURN-STACK-CELLS" : .int(Int.max),
			       "STACK-CELLS" : .int(Int.max),
		]

		init()
		{
			for key in entries.keys
			{
				if key != key.uppercased()
				{
					log.warn("Environment key '\(key)' is not all caps. It will be inaccessible")
				}
			}
		}

		subscript(key: String) -> Value?
		{
			return entries[key.uppercased()]
		}
	}
	
	/// Default permissions to create new files with
	public static let defaultCreatePermissions: FilePermissions = [.ownerReadWrite, .groupRead, .otherRead]
}
