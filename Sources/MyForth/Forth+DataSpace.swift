//
//  Forth+DataSpace.swift
//
//
//  Created by Jeremy Pereira on 31/07/2021.
//
//  Copyright (c) Jeremy Pereira 2021, 2023-2024
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

/// Anything that is managed by the ``MyForth/Forth/DataSpace`` conforms to
/// this protocol
///
/// Conceptually the buffer can be viewed as a seqience of cells or a sequence
/// of bytes. It can be a fixed size or can be extended as needed. If it's a
/// fixed size, loads and stores outside the range of cells must throw an
/// exception rather than crash the interpreter.
internal protocol DataBuffer
{

	/// Fetch the cell at the specified address.
	///
	/// Implementations are free to decide if the address must be cell aligned.
	/// If it must, and the address is not aligned, throw an exception.
	/// - Parameter address: Address of cell to fetch
	/// - Returns: The cell at the address
	/// - Throws: If the address is out of range or not aligned
	func loadCell(from address: Forth.Cell) throws -> Forth.Cell

	/// Store a cell at a given address
	/// - Parameters:
	///   - cell: The cell to store
	///   - address: The address at which to store it
	/// - Throws: If the address is out of range or the alignment is wrong.
	mutating func store(cell: Forth.Cell, at address: Forth.Cell) throws

	/// Store a character (8-bit) at a given address
	/// - Parameters:
	///   - chracter: The character to store
	///   - address: The address at which to store it
	/// - Throws: If the address is out of range
	mutating func store(character: Forth.Cell, at address: Forth.Cell) throws

	/// Store Forth characters at a given address
	///
	/// - Parameters:
	///   - characters: Chraacters to store
	///   - address: The address at which to store them
	mutating func store<S: Collection>(characters: S, at address: Forth.Cell) throws
	where S.Element == UInt8

	/// Load a Forth character from a cell
	///
	/// Although this will return a ``MyForth/Forth/Cell``, it only reads the
	/// 8 bits at the given address.
	/// 
	/// - Parameter address: Address to load from
	/// - Returns: the character at the address
	/// - Throws: If the address is out of bounds
	func loadCharacter(from address: Forth.Cell) throws -> Forth.Cell

	/// Load byte array
	///
	/// - Parameters:
	///   - address: Address to load from
	///   - count: The number of bytes to load
	/// - Returns: An array of bytes
	/// - Throws: If the addresses are out of range
	func loadBytes(address: Forth.Cell, count: Forth.Cell) throws -> [UInt8]
}

internal extension Forth
{
	static let padSpaceMinSize = 84

	private struct NullSpace: DataBuffer
	{

		func loadCell(from address: Forth.Cell) throws -> Forth.Cell
		{
			throw Error.addressOutOfBounds
		}

		mutating func store(cell: Forth.Cell, at address: Forth.Cell) {}

		mutating func store(character: Forth.Cell, at address: Forth.Cell)  {}

		func loadCharacter(from address: Forth.Cell) throws -> Forth.Cell
		{
			throw Error.addressOutOfBounds
		}

		func loadBytes(address: Forth.Cell, count: Forth.Cell) throws -> [UInt8]
		{
			throw Error.addressOutOfBounds
		}

		func storeCountedTransient(string: String) throws -> Forth.Cell
		{
			throw Forth.Error.addressOutOfBounds
		}

		mutating func store<S: Collection>(characters: S, at address: Forth.Cell) throws
		where S.Element == UInt8
		{}
	}
	private static let nullSpace = NullSpace()

	/// The dataspace managers all internal heap style memory and other system
	/// buffers.
	///
	/// Data is addressed by a `Cell` but the top byte is used to determine
	/// which buffer is used. buffer 0 is always the traditional dataspace.
	struct DataSpace
	{
		private var buffers: [DataBuffer]

		init()
		{
			buffers = [DataSpaceBuffer()]
		}

		subscript(index: Int) -> DataBuffer
		{
			get
			{
				buffers[index]
			}
			set
			{
				if buffers.count <= index
				{
					buffers += [DataBuffer](repeating: Forth.nullSpace, count: index - buffers.count + 1)
				}
				buffers[index] = newValue
			}
		}

		var pointer: Forth.Cell = 0

		func loadCell(from address: Cell) throws -> Cell
		{
			let (space, spaceAddress) = try getSpaceAndAddress(address: address)
			return try buffers[space].loadCell(from: spaceAddress)
		}

		func loadCharacter(from address: Forth.Cell) throws -> Forth.Cell
		{
			let (space, spaceAddress) = try getSpaceAndAddress(address: address)
			return try buffers[space].loadCharacter(from: spaceAddress)
		}

		mutating func store(cell: Forth.Cell, at address: Forth.Cell) throws
		{
			let (space, spaceAddress) = try getSpaceAndAddress(address: address)
			try buffers[space].store(cell: cell, at: spaceAddress)
		}

		mutating func store(character: Forth.Cell, at address: Forth.Cell) throws
		{
			let (space, spaceAddress) = try getSpaceAndAddress(address: address)
			try buffers[space].store(character: character, at: spaceAddress)
		}

		mutating func allot(_ count: Forth.Cell) throws
		{
			if count > 0
			{
				guard Int.max - count > pointer else { throw Error.integerOverflow }
			}
			let newPointer = pointer + count
			guard newPointer >= 0 else { throw Error.integerOverflow }

			pointer = newPointer
		}

		func loadBytes(address: Forth.Cell, count: Forth.Cell) throws -> [UInt8]
		{
			let (space, spaceAddress) = try getSpaceAndAddress(address: address)
			return try buffers[space].loadBytes(address: spaceAddress, count: count)
		}

		private static let localAddressMask = 0xffffffffffffff

		private func getSpaceAndAddress(address: Cell) throws -> (Int, Cell)
		{
			let space = (address >> 56) & 0xff
			let newAddress = address & DataSpace.localAddressMask
			guard space < buffers.count else { throw Error.addressOutOfBounds }
			return (space, newAddress)
		}


		/// Create a global address from a data space and an address
		/// - Parameters:
		///   - space: The space of the global address
		///   - spaceAddress: The address within the space
		/// - Returns: the global address corresponding to the space and address
		static func globalAddress(space: Int, spaceAddress: Cell) -> Cell
		{
			return (space << 56) | (spaceAddress & localAddressMask)
		}
		
		/// Calculate the number of cells needed to store a number of bytes
		/// - Parameter bytes: Bytes needoing to be stored
		/// - Returns: The number of cells needed for the given number of bytes
		static func cellsNeededFor(bytes: Int) -> Int
		{
			(bytes + Forth.Cell.bytesPerCell - 1) / Forth.Cell.bytesPerCell
		}


		/// Get the next cell aligned address
		///
		/// If the address is already aligned we return it, otherwise we return the
		/// next address above this address that is aligned.
		/// Different buffers are allowed to have different alignment requirements
		/// - Parameter address: The address to find the aligned address after
		/// - Returns: The next cell aligned address
		func nextAligned(address: Forth.Cell) -> Forth.Cell
		{
			return Forth.Cell.bytesPerCell * DataSpace.cellsNeededFor(bytes: address)
		}

		/// The next aligned to a ``Forth/Cell`` pointer
		var alignedPointer: Int
		{
		   return nextAligned(address: pointer)
		}

		/// Store a counted string in the transient area
		///
		/// The transient area is anywhere above the current pointer.
		///
		/// - Parameter string: The string to store
		/// - Returns: The address in which the string was stored
		mutating func storeCountedTransient(string: String) throws -> Forth.Cell
		{
		   let utf8 = string.utf8
		   let count = utf8.count
		   if count > UInt8.max
		   {
			   throw Forth.Error.integerOverflow
		   }
		   let bytesToStore = [UInt8(count)] + utf8
		   try store(characters: bytesToStore, at: alignedPointer)
		   return alignedPointer
		}
		/// Store a counted buffer of `UInt8` in the transient area
		///
		/// The transient area is anywhere above the current pointer.
		///
		/// - Parameter characters: A collection of bytes
		/// - Returns: The address in which the string was stored
		mutating func storeCountedTransient<C: Collection>(characters: C) throws -> Forth.Cell
		where C.Element == UInt8
		{
		   let count = characters.count
		   if count > UInt8.max
		   {
			   throw Forth.Error.integerOverflow
		   }
		   let bytesToStore = [UInt8(count)] + characters
		   try store(characters: bytesToStore, at: alignedPointer)
		   return alignedPointer
		}

		mutating func store<S: Collection>(characters: S, at address: Forth.Cell) throws
		where S.Element == UInt8
		{
			let (space, spaceAddress) = try getSpaceAndAddress(address: address)
			try buffers[space].store(characters: characters, at: spaceAddress)
		}

		func loadString(address: Forth.Cell, count: Forth.Cell) throws -> String
		{
		   let utf8 = try loadBytes(address: address, count: count)

		   guard let ret = String(bytes: utf8, encoding: .utf8)
		   else
		   {
			   throw Forth.Error.invalidUTF8
		   }
		   return ret
		}
	}
	/// The data space buffer
	///
	/// Addresses in the data space are byte addresses. So reserving one cell
	/// adds 8 to the address (`Cell` is `Int` and `Int` is 8 bytes)
	/// Characters are eight bits. The bottom 3 bits of an address represent
	/// the byte within a cell the character is in.
	///
	struct DataSpaceBuffer: DataBuffer
	{
		/// The bytes required to store a character
		///
		/// Each character is 1 byte.
		let bytesPerChar = 1
		/// Mask to remove buts not part of the character
		///
		/// `charMask` must be made consistent with `bytesPerChar`
		let charMask = 0xff
		/// The number of bits in a byte
		let bitsPerByte = UInt8.bitWidth

		private var data: [Forth.Cell] = []

		init()
		{
		}

		var dataByteCount: Int { data.count * Cell.bytesPerCell }

		private mutating func ensureSpaceAvailable(for index: Cell)
		{
			guard index >= data.endIndex else { return }
			let spaceNeeded = index - data.endIndex + 1
			data += [Cell].init(repeating: 0, count: spaceNeeded)
		}

		mutating func store(cell: Cell, at address: Cell) throws
		{
			guard address % Cell.bytesPerCell == 0 else { throw Forth.Error.alignment }
			let index = address / Cell.bytesPerCell
			ensureSpaceAvailable(for: index)
			data[index] = cell
		}

		mutating func store(character: Cell, at address: Cell) throws
		{
			let index = address / Cell.bytesPerCell
			ensureSpaceAvailable(for: index)

			let byteNumber = address % Cell.bytesPerCell
			let existingChars = data[index] & ~(charMask << (byteNumber * bitsPerByte))
			data[index] = ((character & charMask) << (byteNumber * bitsPerByte)) | existingChars
		}

		func loadCell(from address: Cell) throws -> Cell
		{
			guard address % Cell.bytesPerCell == 0 else { throw Forth.Error.alignment }
			let cellNumber = address / Cell.bytesPerCell
			guard cellNumber >= 0 && cellNumber < data.count
				else { throw Forth.Error.addressOutOfBounds }
			return data[cellNumber]
		}

		private func loadUInt8(from address: Cell) throws -> UInt8
		{
			return UInt8(try loadCharacter(from: address))
		}


		func loadCharacter(from address: Cell) throws -> Cell
		{
			guard address >= 0
				else { throw Forth.Error.addressOutOfBounds }
			let byteNumber = address % Cell.bytesPerCell
			let cellNumber = address / Cell.bytesPerCell
			guard cellNumber < data.count else { throw Forth.Error.addressOutOfBounds }
			let cell = data[cellNumber]
			return (cell >> (byteNumber * bitsPerByte)) & charMask
		}

		func loadBytes(address: Cell, count: Cell) throws -> [UInt8]
		{
			var utf8: [UInt8] = []
			for cellAddress in  address ..< address + count
			{
				utf8.append(try loadUInt8(from: cellAddress))
			}
			return utf8
		}

		mutating func store<S: Collection>(characters: S, at address: Forth.Cell) throws
		where S.Element == UInt8
		{
			let dataTotalBytesNeeded = address + characters.count
			let dataTotalCellsNeeded = DataSpace.cellsNeededFor(bytes: dataTotalBytesNeeded)
			ensureSpaceAvailable(for: dataTotalCellsNeeded)

			let startCell = address / Cell.bytesPerCell
			let startOffset = address % Cell.bytesPerCell

			// Now we iterate through the array filling up the cells in our data
			var current = startCell
			var currentCell = data[current]
			var bytesUnsaved = false
			for (offset, byte) in characters.enumerated()
			{
				let offsetInCell = (offset + startOffset) % Cell.bytesPerCell
				// If we are doing the first byte of the cell, we need its current contents
				if offsetInCell == 0
				{
					currentCell = data[current]
				}
				let bitShift = offsetInCell * UInt8.bitWidth
				currentCell &= ~(0b1111_1111 << bitShift)
				currentCell |= Int(byte) << bitShift
				bytesUnsaved = true
				// If we've just done the top byte, save the cell
				if offsetInCell == Cell.bytesPerCell - 1
				{
					data[current] = currentCell
					current += 1
					bytesUnsaved = false
				}
			}
			if bytesUnsaved
			{
				data[current] = currentCell
			}
		}
	}
}
