//
//  InputParser.swift
//  
//
//  Created by Jeremy Pereira on 06/06/2020.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Toolbox

private let log = Logger.getLogger("MyForth.InputParser")

/// Parses a stream of characters into words
final class InputParser
{
	typealias Cell = Forth.Cell

	/// The location in parser space of `in`
	static let inAddress = 0
	/// The blk address relative to the start of the input parser
	static let blkAddress = inAddress + Cell.bytesPerCell
	/// The buffer address relative to the start of the input parser
	///
	/// If the source is in data space or a block buffer, this is not used.
	static let bufferStartAddress = blkAddress + Cell.bytesPerCell

	/// The absolute address of the parser
	///
	/// We need to know this to calculate the absolute address of the
	/// buffer when the buffer did not come from DataSpace
	private let absoluteAddress: Cell
	/// The absolute address of the buffer
	///
	/// This is so that the input buffer has a location, if the input buffer
	/// is not in the dataspace i.e. not created by `evaluate`
	var bufferAbsoluteAddress: Cell
	{
		if let sourceAddress = inputSource.dataSpaceAddress
		{
			return sourceAddress
		}
		return absoluteAddress + InputParser.bufferStartAddress
	}

	var blkAddress: Cell { absoluteAddress + InputParser.blkAddress }

	var bufferUInt8Count: Cell { buffer.uint8Count }

	var inputSource = try! Forth.InputSource("", sourceId: 0, name: "init input")

	private var buffer: Buffer

	private(set) var state: StateMachine<State>

	/// A dummy initialiser for a null input parser
	convenience init(absoluteAddress: Forth.Cell)
	{
		self.init(startState: .normal, absoluteAddress: absoluteAddress)
	}

	/// Initialise an input parser with a given state and a given character iterator
	/// - Parameters:
	///   - startState: The state in which to start
	///   - absoluteAddress: The absolute address of the parser to be used when the source
	///                      is not in the data space
	init(startState: State, absoluteAddress: Forth.Cell)
	{
		self.absoluteAddress = absoluteAddress
		self.buffer = ""
		self.state = StateMachine(startState: startState)
	}


	func set(state: State)
	{
		self.state = StateMachine(startState: state)
	}

	func refill() throws -> Bool
	{
		guard let nextLine = try inputSource.readLine() else { return false }

		// TODO: This is a bug if a dataspace input source contains a newline.
		self.buffer = Buffer(characters: nextLine)
		return true
	}


	var sourceId: Forth.Cell { inputSource.sourceId }

	private var stringStack = Stack<([Buffer], Forth.InputSource)>()

	func push(inputSource: Forth.InputSource)
	{
		stringStack.push(([self.buffer] + savedBuffers, self.inputSource))
		self.inputSource = inputSource
	}

	private var requireTracker: [String : Forth.Cell] = [:]

	/// Include a file in the input stream
	///
	/// This performs the function of inclued and required. The input source
	/// replaced by the one passed in as a parameter
	///
	/// - Parameters:
	///   - inputSource: The new input source
	///   - wordListIndex: cont of word lists, used so that require tracking
	///                    can unwind in the event of a marker being executed
	func include(inputSource: Forth.InputSource, wordListIndex: Int)
	{
		push(inputSource: inputSource)
		guard let fileName = inputSource.name else { return  }
		if requireTracker[fileName] == nil
		{
			requireTracker[fileName] = wordListIndex
		}
	}

	func isIncluded(file: String) -> Bool
	{
		requireTracker[file] != nil
	}

	func forgetIncludes(from xt: Forth.Cell)
	{
		requireTracker = requireTracker.filter{ $0.value < xt }
	}

	/// Pop an iterator off the saved stack
	///
	/// Used at the end of an evaluate to get the previous character iterator
	/// back.
	/// - Returns: true if something was popped, false if there was nothing on
	///            the stack.
	func pop() -> Bool
	{
		guard !stringStack.isEmpty else { return false }
		let buffers: [Buffer]
		(buffers, inputSource) = stringStack.pop()!
		buffer = buffers.first!
		if buffers.count > 1
		{
			savedBuffers = Array(buffers[1...])
		}
		else
		{
			savedBuffers = []
		}
		return true
	}

	/// Clear the current iterator and the iterator stack
	func clear()
	{
		while pop() {}
	}

	var savedBuffers: [Buffer] = []

	/// Saves information with respect to our location in the current input source
	///
	/// The array returned will look like this:
	///
	/// - `0          ` : index of saved buffer
	/// - `1 ..< n    ` : location data for the input source
	///
	/// - Returns: An array of cells that describe the state of the input source
	///            or an empty array if the state could not be saved

	func saveLocation() -> [Forth.Cell]
	{
		let bufferIndex = savedBuffers.endIndex

		let inputSourceSave =  inputSource.saveLocation()
		guard !inputSourceSave.isEmpty else { return [] }
		savedBuffers.append(buffer)
		return [bufferIndex] + inputSourceSave
	}

	func restore(location: [Forth.Cell]) -> Bool
	{
		// Must be the buffer index + save info from the input source. Any less
		// and we haven't got enoufgh information to restore
		guard location.count > 1 else { return false }
		let bufferIndex = location.first!
		guard savedBuffers.indices.contains(bufferIndex) else { return false }
		let sourceInfo = location[1...]
		guard inputSource.restore(location: sourceInfo) else { return false }
		self.buffer = savedBuffers[bufferIndex]
		return true
	}

	/// Parses a word delimited by the given delimiter
	/// 
	/// Scans through the input until it comes to the given delimiter. Then
	/// discards all consecutive instances of the delimiter and returns a string
	/// consisting of all characters up to the next occurrence of the delimiter
	/// or the end of the buffer or a `\n`.
	/// 
	/// - Parameter delimiter: Delimiter of the word. If null, we use all white space
	/// - Parameter ignoreLeadingDelimiters: `true` if we want to ignore leading delimiters.
	/// 									 Defaults to `true`
	///
	/// - Returns: A string containing the parsed word
	///
	func parseWord(delimitedBy delimiter: Character?, ignoreLeadingDelimiters: Bool = true) throws -> (Forth.Cell, Forth.Cell)
	{
		let delimiterTest: (Character) -> Bool
		// Create a test for the delimiter. If not supplied, we assume any
		// white space
		if let delimiter = delimiter
		{
			delimiterTest = { $0 == delimiter || $0 == "\n" }
		}
		else
		{
			delimiterTest = { $0.isWhitespace }
		}
		// Skip all the leading delimiters if requested
		if ignoreLeadingDelimiters
		{
			while let aChar = buffer.currentChar, delimiterTest(aChar)
			{
				buffer.getNextChar()
			}
		}
		let startCAddress = try buffer.localAddressOfChar(index: buffer.index)
		while let anotherChar = buffer.currentChar, !delimiterTest(anotherChar)
		{
			buffer.getNextChar()
		}
		let endCAddress = try buffer.localAddressOfChar(index: buffer.index)
		// flush the trailing delimiter
		if let aChar = buffer.currentChar, delimiterTest(aChar)
		{
			buffer.getNextChar()
		}
		return (startCAddress + InputParser.bufferStartAddress, endCAddress - startCAddress)
	}
}

extension InputParser
{
	func next() throws -> Forth.Word?
	{
		// If we are in a comment, we just consume characters until we have
		// found the end of a comment and we emit the Forth end comment word
		// ")"

		let ret: Forth.Word?
		switch state.state
		{
		case .comment(let commentChar):
			ret = readComment(terminator: commentChar)
		case .string(let stringSoFar):
			ret = readString(stringSoFar: stringSoFar, terminator: "\"")
		case .escapeString(let stringSoFar):
			ret = try readEscapeString(stringSoFar: stringSoFar, terminator: "\"")
		case .displayComment(let stringSoFar):
			ret = readString(stringSoFar: stringSoFar, terminator: ")")
		case .normal:
			ret = readNormal()
		}
		return ret
	}

	private func readComment(terminator: Character) -> Forth.Word?
	{
		while let c = buffer.currentChar,
			  c != terminator && (inputSource.blockNumber == 0 || buffer.screenLineIndex < Forth.blockScreenWidth)
		{
			buffer.getNextChar()
		}
		if let currentChar = buffer.currentChar, currentChar == terminator
		{
			state.transition(to: .normal)
			buffer.getNextChar()
			return ")"
		}
		else if terminator == "\n"
		{
			state.transition(to: .normal)
			return ")"
		}
		return nil
	}

	private func readString(stringSoFar: String, terminator: Character) -> Forth.Word?
	{
		var ret = stringSoFar
		while let c = buffer.currentChar
		{
			guard c != terminator else
			{
				state.transition(to: .normal)
				buffer.getNextChar()
				return Forth.Word(ret)
			}
			ret += String(c)
			buffer.getNextChar()
		}
		state.transition(to: .string(ret))
		return nil
	}

	private func readEscapeString(stringSoFar: String, terminator: Character) throws -> Forth.Word?
	{
		enum EscapeMode
		{
			case normal
			case bsFound
			case hex0Needed
			case hex1Needed
		}

		var ret = stringSoFar
		var escapeMode = EscapeMode.normal
		var hexString = ""

		while let c = buffer.currentChar
		{
			switch escapeMode
			{
			case .normal:
				guard c != terminator else
				{
					state.transition(to: .normal)
					buffer.getNextChar()
					log.debug("escapedString: '\(ret)', length \(ret.count)")
					return Forth.Word(ret)
				}
				switch c
				{
				case "\\":
					escapeMode = .bsFound
				default:
					ret.append(c)
				}
			case .bsFound:
				escapeMode = .normal	// Most escape codes go back to normal
				switch c
				{
				case "\"", "\\":
					ret.append(c)
				case "a" :
					ret.append("\u{7}")		// bell
				case "b" :
					ret.append("\u{8}")		// backspace
				case "e" :
					ret.append("\u{1b}")	// escape
				case "f" :
					ret.append("\u{c}")		// form feed
				case "l" :
					ret.append("\u{a}")		// line feed
				case "m" :
					ret.append("\r")
					ret.append("\n")
				case "n":
					ret.append("\n")
				case "q" :
					ret.append("\"")
				case "r":
					ret.append("\r")
				case "t" :
					ret.append("\t")
				case "v" :
					ret.append("\u{b}")	// vertical tab
				case "x":
					hexString = ""
					escapeMode = .hex0Needed
				case "z" :
					ret.append("\0")
				default:
					ret.append(c)
				}
			case .hex0Needed:
				hexString.append(c)
				escapeMode = .hex1Needed
			case .hex1Needed:
				hexString.append(c)
				guard let intValue = UInt32(hexString, radix: 16) else { throw Forth.Error.invalidHexString(hexString) }
				guard let unicode = Unicode.Scalar(intValue) else { throw Forth.Error.invalidUnicode(Int(intValue)) }
				let char = Character(unicode)
				ret.append(char)
				escapeMode = .normal
			}
			buffer.getNextChar()
		}
		guard escapeMode == .normal else { throw Forth.Error.invalidEscapeSequence(ret) }
		state.transition(to: .escapeString(ret))
		log.debug("(non terminated) escapedString: '\(ret)', length \(ret.count)")
		return nil
	}

	private func readNormal() -> Forth.Word?
	{
		var ret: Forth.Word = ""

		// Consume any white space
		while let possibleWhiteSpace = buffer.currentChar, possibleWhiteSpace.isWhitespace
		{
			buffer.getNextChar()
		}
		guard buffer.currentChar != nil else { return nil }
		// Now consume the characters in the word
		while let aChar = buffer.currentChar, !aChar.isWhitespace
		{
			ret.append(aChar)
			buffer.getNextChar()
		}
		// Consume the trailing delimiter
		if buffer.currentChar != nil
		{
			buffer.getNextChar()
		}
		return ret
	}
}

extension InputParser
{
	enum State: StateType
	{
		case normal
		case comment(Character)
		case string(String)
		case escapeString(String)
		case displayComment(String)

		func canTransition(to: InputParser.State) -> TransitionShould<InputParser.State>
		{
			switch (self, to)
			{
			case (.normal, .comment), (.normal, .string), (.normal, .displayComment),
					 (.comment, .normal), 
				(.string, .normal), (.string, .string),
				(.escapeString, .normal), (.escapeString, .escapeString),
				(.displayComment, .normal):
					return .carryOn
			default:
				fatalError("Cannot transition from \(self) to \(to)")
			}
		}
	}
}

extension InputParser: DataBuffer
{
	func loadCell(from address: Forth.Cell) throws -> Forth.Cell
	{
		switch address
		{
		case InputParser.inAddress:
			return buffer.index
		case InputParser.blkAddress:
			let source = inputSource
			return source.blockNumber
		default:
			throw Forth.Error.notImplemented(file: #file, line: #line, function: #function)
		}
	}

	func store(cell: Forth.Cell, at address: Forth.Cell) throws
	{
		if address == InputParser.inAddress
		{
			try buffer.set(index: cell)
		}
		else
		{
			throw Forth.Error.writeToReadOnlyCell(address)
		}
	}

	func store(character: Forth.Cell, at address: Forth.Cell) throws
	{
		throw Forth.Error.writeToReadOnlyCell(address)
	}

	func loadCharacter(from address: Forth.Cell) throws -> Forth.Cell
	{
		switch address
		{
		case InputParser.inAddress:
			return buffer.index
		case InputParser.blkAddress:
			notImplemented()
		default:
			let charIndex = try buffer.indexOfChar(localAddress: address - InputParser.bufferStartAddress)
			// TODO: Need to fix this. There may be more than one unicode scalar
			let character = try buffer.char(at: charIndex)
			assert (character.unicodeScalars.count == 1)
			return Forth.Cell(character.unicodeScalars.first!.value)
		}
	}
	
	/// Load bytes from the buffer
	///
	/// Note that, as currently implemented, if the boundaries of the requested
	/// region do match UTF-8 character boundaries i.e. the address is not the
	/// start of a unicode character and the address + the count does not match
	/// the start of another character, there is an alignment error.
	/// - Parameters:
	///   - address: Local address to load from
	///   - count: Number of bytes to load
	/// - Returns: An array of the bytes
	/// - Throws: if the requested region overlaps either end of the buffer or
	///           there is a character alignment issue
	func loadBytes(address: Forth.Cell, count: Forth.Cell) throws -> [UInt8]
	{
		guard address >= InputParser.bufferStartAddress
		else
		{
			// TODO: Make a better error
			throw Forth.Error.alignment
		}

		var charIndex = try buffer.indexOfChar(localAddress: address - InputParser.bufferStartAddress)
		var bytes: [UInt8] = []
		while bytes.count < count
		{
			let char = try buffer.char(at: charIndex)
			bytes += char.utf8
			charIndex += 1
		}
		guard bytes.count == count else { throw Forth.Error.alignment }
		return bytes
	}

	func store<S: Collection>(characters: S, at address: Forth.Cell) throws
	   where S.Element == UInt8
	{
		throw Forth.Error.writeToReadOnlyCell(address)
	}

}

extension InputParser
{

	/// Models an input buffer for the use of `source` and `>in`
	///
	/// Semantically, the input buffer needs to appear to the Forth machine as a
	/// part of the dataspace, when it's really a Swift `String`. The buffer
	/// maintains the input string and the address in the dataspace from whence
	/// it came.
	///
	/// Note that, in Forth machines, the input buffer is line oriented, so we
	/// reset the start of the input buffer to the current position each time
	/// we come across a neew line.
	/// - TODO: The buffer address needs to change when we receive a `\n`
	struct Buffer
	{
		private let characters: [Character]
		fileprivate private(set) var index: Array.Index
		fileprivate private(set) var screenLineIndex: Int
		private var inStart: Array.Index

		func char(at index: Int) throws -> Character
		{
			guard characters.indices.contains(index) else { throw Forth.Error.addressOutOfBounds }
			return characters[index]
		}

		fileprivate init<S: Sequence>(characters: S) where S.Element == Character
		{
			self.characters = Array(characters)
			self.index = self.characters.startIndex
			self.screenLineIndex = 0
			self.inStart = self.index
			constructAddressMappings()
		}

		fileprivate var currentChar: Character? { index == characters.endIndex ? nil : characters[index] }

		fileprivate mutating func getNextChar()
		{
			guard let oldChar = currentChar else { return }
			index = characters.index(after: index)
			if screenLineIndex >= Forth.blockScreenWidth
			{
				screenLineIndex = 0
			}
			else
			{
				screenLineIndex += oldChar.utf8.count
			}
			if oldChar == "\n"
			{
				inStart = index
			}
		}

		fileprivate mutating func set(index: Forth.Cell) throws
		{
			guard characters.indices.contains(index) || index == characters.endIndex
			else { throw Forth.Error.addressOutOfBounds }
			self.index = index
		}

		fileprivate var count: Int { characters.count }

		fileprivate func currentLineDebug() -> String?
		{
			guard index != characters.endIndex else  { return nil }
			let beginningOfLine = characters[inStart ..< index]
			let bufferSuffix = characters[index...]
			let endOfLine: ArraySlice<Character>
			if let indexOfThisLineEnd = bufferSuffix.firstIndex(where: {  $0.isNewline })
			{
				endOfLine = bufferSuffix[..<indexOfThisLineEnd]
			}
			else
			{
				endOfLine = bufferSuffix
			}
			let ret = String(beginningOfLine).appending("↥").appending(String(endOfLine))
			return ret
		}

		// MARK: Address/Index conversions

		// We keep lookup tables for converting both ways, but we construct them
		// on demand because most of the time we don't need them.
		//
		// The lookup tables contain addresses for all valid characters AND
		// endIndex

		private var characterAddresses: [Forth.Cell] = []
		private var addressIndices: [Forth.Cell : Int] = [:]
		private var addressMappingsCreated = false

		private mutating func constructAddressMappings()
		{
			guard !addressMappingsCreated else { return }
			var totalBytes = 0
			for (index, character) in characters.enumerated()
			{
				characterAddresses.append(totalBytes)
				addressIndices[totalBytes] = index

				totalBytes += character.utf8.count
			}
			// Add an entry for endIndex
			characterAddresses.append(totalBytes)
			addressIndices[totalBytes] = characters.endIndex
			addressMappingsCreated = true
		}

		func localAddressOfChar(index: Int) throws -> Forth.Cell
		{
			guard characterAddresses.indices.contains(index) else { throw Forth.Error.addressOutOfBounds }
			return characterAddresses[index]
		}

		func indexOfChar(localAddress: Forth.Cell) throws -> Int
		{
			guard (0 ... characterAddresses[characters.endIndex]).contains(localAddress)
			else { throw Forth.Error.addressOutOfBounds }
			guard let index = addressIndices[localAddress] else { throw Forth.Error.alignment }
			return index
		}

		var uint8Count: Int { characterAddresses.last! }
	}
}

extension InputParser
{
	func bufferDebug() -> String?
	{
		buffer.currentLineDebug()
	}
}

extension InputParser.Buffer: ExpressibleByStringLiteral
{
	init(stringLiteral value: String)
	{
		self.init(characters: value)
	}
}
