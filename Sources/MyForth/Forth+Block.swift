//
//  Forth+Block.swift
//
//
//  Created by Jeremy Pereira on 01/12/2023.
//
//  Copyright (c) Jeremy Pereira 2023
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Toolbox
import SystemPackage

private let log = Logger.getLogger("MyForth.Forth+Block")

extension Forth
{
	/// Maintains the block buffers
	///
	/// Each buffer is 1024 bytes in size and maps to a block in the blocks file.
	/// Buffers may be reused for different blocks.
	///
	/// For fun, we will use the LRU algorithm for block reuse
	///
	/// Blocks are simply stored sequentially in the blocks file. The standard
	/// and the gforth docs strongly suggest that the content should be ASCII
	/// or at least printable characters. We will use UTF-8 and all blocks and
	/// buffers will be initialised to 1024 `bl` characters
	final class BlockSpace
	{
		private var accessClock: Int = 1
		private var buffers: [Buffer]
		private var currentIndex: Int?
		/// Initialise the buffer space
		/// - Parameter bufferCount: Number of buffers to allocate
		init(bufferCount: Int)
		{
			precondition(bufferCount > 0)
			// The below only works if `Buffer` is a value type
			buffers = Array(repeating: Buffer(), count: bufferCount)
		}

		func cache(block: Cell, fromFile: Bool) throws -> Cell
		{
			let allocatedBufferIndex: Int
			// Is there a buffer already allocated?
			if let existingBufferIndex = buffers.firstIndex(where: { $0.blockNumber == block })
			{
				allocatedBufferIndex = existingBufferIndex
				log.debug("Block: Cache hit \(block) at index \(allocatedBufferIndex)")
			}
			else
			{
				allocatedBufferIndex = try findOrCreateEmptyBuffer()
				buffers[allocatedBufferIndex].blockNumber = block
				log.debug("Block: Cache miss allocating \(block) to index \(allocatedBufferIndex)")
				if fromFile
				{
					try read(into: allocatedBufferIndex)
				}
			}
			currentIndex = allocatedBufferIndex
			return allocatedBufferIndex * Forth.blockSize
		}

		func update() throws
		{
			guard let currentIndex else { throw Forth.Error.noCurrentBlock }
			guard buffers[currentIndex].blockNumber != nil
			else { throw Forth.Error.noBlockNumberSpecified }
			buffers[currentIndex].isDirty = true
		}

		func saveBuffers() throws
		{
			let fd = try makeSureFileIsOpen()
			for i in buffers.indices
			{
				log.debug("Block save: buffer[\(i)] isDirty? \(buffers[i].isDirty), number: \(buffers[i].blockNumber?.description ?? "nil"), lastAccess: \(buffers[i].lastAccessTime)")
				if buffers[i].isDirty && buffers[i].blockNumber != nil
				{
					log.debug("Block save: buffer[\(i)] saved")
					try buffers[i].write(to: fd)
				}
			}
		}

		func emptyBuffers()
		{
			for i in buffers.indices
			{
				buffers[i].blockNumber = nil
				buffers[i].isDirty = false
			}
			currentIndex = nil
		}

		var fileName: String?
		private(set) var fileDescriptor: FileDescriptor?

		private func findOrCreateEmptyBuffer() throws -> Int
		{
			var oldestAccess: Int = 0
			var bestIndexSoFar: Int = -1

			for (index, buffer) in buffers.enumerated()
			{
				// First if we have an unused buffer, that's best, we can stop looking
				guard buffer.blockNumber != nil
				else
				{
					bestIndexSoFar = index
					break
				}
				if accessClock &- buffer.lastAccessTime >= oldestAccess
				{
					oldestAccess = buffer.lastAccessTime
					bestIndexSoFar = index
				}
			}
			if buffers[bestIndexSoFar].isDirty && buffers[bestIndexSoFar].blockNumber != nil
			{
				try write(from: bestIndexSoFar)
			}
			buffers[bestIndexSoFar].blockNumber = nil
			buffers[bestIndexSoFar].isDirty = false
			buffers[bestIndexSoFar].lastAccessTime = accessClock
			buffers[bestIndexSoFar].initialiseBytes()
			return bestIndexSoFar
		}

		private func read(into bufferIndex: Int) throws
		{
			let fd = try makeSureFileIsOpen()
			try buffers[bufferIndex].read(from: fd)
		}

		private func write(from buffer: Int) throws
		{
			let fd = try makeSureFileIsOpen()
			try buffers[buffer].write(to: fd)
		}

		private func makeSureFileIsOpen() throws -> FileDescriptor
		{
			if fileDescriptor == nil
			{
				let path = FilePath(fileName ?? Forth.blockFileName)
				fileDescriptor = try FileDescriptor.open(path, .readWrite,
														 options: [.create],
														 permissions: Forth.defaultCreatePermissions)
			}
			return fileDescriptor!
		}
	}
}

fileprivate extension Forth.BlockSpace
{
	struct Buffer
	{
		/// The number of the block in this buffer
		///
		/// If `nil` it means the buffer is unused
		var blockNumber: Int?
		/// true if the buffer has been written to
		var isDirty: Bool = false
		/// Last time the buffer was accessed
		///
		/// Not a real time, just a counter that increments each time
		/// an access is made into the block cache
		var lastAccessTime: Int = 0
		/// The content of the block
		var bytes = ContiguousArray<UInt8>(repeating: 0, count: Forth.blockSize)

		fileprivate mutating func initialiseBytes(from: Int = 0)
		{
			for i in from ..< bytes.endIndex
			{
				bytes[i] = 0
			}
		}

		fileprivate mutating func read(from fd: FileDescriptor) throws
		{
			let filePos = try fileOffset()
			let bytesRead = try bytes.withUnsafeMutableBytes
			{
				try fd.read(fromAbsoluteOffset: filePos, into: $0, retryOnInterrupt: false)
			}
			if bytesRead < Forth.blockSize
			{
				initialiseBytes(from: bytesRead)
			}
		}

		fileprivate mutating func write(to fd: FileDescriptor) throws
		{
			let filePos = try fileOffset()
			try fd.writeAll(toAbsoluteOffset: filePos, bytes)
			isDirty = false
		}

		private func fileOffset() throws -> Int64
		{
			guard let blockNumber else { throw Forth.Error.noBlockNumberSpecified }
			guard blockNumber >= 1 && blockNumber < Int.max / Forth.blockSize
			else { throw Forth.Error.invalidBlockNumber(blockNumber) }
			return Int64((blockNumber - 1) * Forth.blockSize)
		}


		mutating func store(cell: Forth.Cell, at address: Forth.Cell, clock: Int) throws
	 	{
			guard bytes.indices.contains(address) && bytes.indices.contains(address + Forth.Cell.bytesPerCell - 1)
			else { throw Forth.Error.addressOutOfBounds }
			let bytes = cell.bytes
			for i in bytes.indices
			{
				self.bytes[address + i] = bytes[i]
			}
			self.lastAccessTime = clock
		}

		mutating func loadByte(address: Forth.Cell, clock: Int) throws -> UInt8
	 	{
			guard bytes.indices.contains(address)
			else { throw Forth.Error.addressOutOfBounds }
			self.lastAccessTime = clock
			return bytes[address]
	 	}

		mutating func store(byte: UInt8, at address: Forth.Cell, clock: Int) throws
		{
			guard bytes.indices.contains(address) else { throw Forth.Error.addressOutOfBounds }
			bytes[address] = byte
			lastAccessTime = clock
		}
	}
}

extension Forth.BlockSpace: DataBuffer
{
	func loadCell(from address: Forth.Cell) throws -> Forth.Cell
	{
		notImplemented()
	}
	func store(cell: Forth.Cell, at address: Forth.Cell) throws
	{
		let (bufferIndex, byteIndex) = try calculateBufferAndIndex(address: address)
		accessClock &+= 1
		try buffers[bufferIndex].store(cell: cell, at: byteIndex, clock: accessClock)
	}

	func store(character: Forth.Cell, at address: Forth.Cell) throws
	{
		let (bufferIndex, byteIndex) = try calculateBufferAndIndex(address: address)
		accessClock &+= 1
		let byte = character.bytes.first!
		return try buffers[bufferIndex].store(byte: byte, at: byteIndex, clock: accessClock)
	}

	func store<S>(characters: S, at address: Forth.Cell) throws where S : Collection, S.Element == UInt8
	{
		let (bufferIndex, startIndex) = try calculateBufferAndIndex(address: address)
		var index = startIndex
		accessClock &+= 1
		for byte in characters
		{
			try buffers[bufferIndex].store(byte: byte, at: index, clock: accessClock)
			index += 1
		}
	}

	func loadCharacter(from address: Forth.Cell) throws -> Forth.Cell
	{
		let (bufferIndex, byteIndex) = try calculateBufferAndIndex(address: address)
		accessClock &+= 1
		return try Forth.Cell(buffers[bufferIndex].loadByte(address: byteIndex, clock: accessClock))
	}

	func loadBytes(address: Forth.Cell, count: Forth.Cell) throws -> [UInt8]
	{
		let (bufferIndex, startIndex) = try calculateBufferAndIndex(address: address)
		var ret: [UInt8] = []
		accessClock &+= 1
		for i in 0 ..< count
		{
			try ret.append(buffers[bufferIndex].loadByte(address: startIndex + i, clock: accessClock))
		}
		return ret
	}

	private func calculateBufferAndIndex(address: Forth.Cell) throws -> (Forth.Cell, Forth.Cell)
	{
		let (bufferIndex, byteIndex) = address.quotientAndRemainder(dividingBy: Forth.blockSize)
		guard buffers.indices.contains(bufferIndex) else { throw Forth.Error.addressOutOfBounds }
		return (bufferIndex, byteIndex)
	}
}
