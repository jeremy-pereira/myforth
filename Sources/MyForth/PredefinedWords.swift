//
//  PredefinedWords.swift
//  
//
//  Created by Jeremy Pereira on 17/06/2020.
//
//  Copyright (c) Jeremy Pereira 2020-2023
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.


extension ThreadedForth
{
	internal static func predefinedWords() -> String
	{
		return #"""
: \ -1 {beginComment} ; immediate
: ( 0 {beginComment} ; immediate
: ) ; immediate
\ Comments come first so I can do comments !

: .( {displayCommentMode} {parseWord} \#(ThreadedForth.CWFunction.toOutput.rawValue) {cwFunc} ; immediate

: ' {parseWord} \#(ThreadedForth.CWFunction.toSXT.rawValue) {cwFunc} ;
: ['] {parseWord} \#(ThreadedForth.CWFunction.toCompilingXT.rawValue) {cwFunc} ; immediate
: char {parseWord} \#(ThreadedForth.CWFunction.toSChar.rawValue) {cwFunc} ;
: [char] {parseWord} \#(ThreadedForth.CWFunction.toCompilingChar.rawValue) {cwFunc} ; immediate
: postpone {parseWord} \#(ThreadedForth.CWFunction.postpone.rawValue) {cwFunc} ; immediate

: [ 0 state ! ; immediate compile-only
: ] -1 state ! ;

: {compilingListIndexToS} {compilingListAddress} @ ;
: {setTrace} {traceAddress} @ swap {traceAddress} ! ;

: :noname {createUnnamedWordSlot} ] ;

: decimal #10 base ! ;
: hex $10 base ! ;
: align here dup aligned swap - allot ;
: evaluate 0 {stringInputPush} 0 execute {inputPop} ;

: create
	{parseWord} {createNamedWordSlot}
	align here dup {sToCompiling}
	['] {noOp} dup dup compile, compile, compile,
	{createDictionaryEntry}
	{markCreated}
	;
: marker
	{parseWord} {createNamedWordSlot}
	{compilingListIndexToS} {sToCompiling}
	['] {truncateDictionaryAtXT} compile,
	{endDefinition}
	;

: quit {stackreturnEmpty} 0 >r {inputClear} ;
: abort {stackDataEmpty} quit ;

: rshift 0 {rshift} ;
: 2* 1 lshift ;
: 2/ 1 -1 {rshift} ;
: */mod >r m* r> sm/rem ;
: / /mod swap drop ;
: mod /mod drop ;
: negate 0 swap - ;
: <> = invert ;
: <= > invert ;
: >= < invert ;
: 0< 0 < ;
: 0> 0 > ;
: 0= 0 = ;
: 0<> 0= invert ;
: 1+ 1 + ;
: 1- 1 - ;
: */ */mod swap drop ;
: +! dup @ rot + swap ! ;

: . 0 .r ;
: u. 0 u.r ;

: 2drop drop drop ;
: 2dup over over ;
: 2over  >r >r 2dup r> r> 2swap ;
: max 2dup > 2 0<>branch? swap drop ;
: min 2dup < 2 0<>branch? swap drop ;
: constant {parseWord} {createNamedWordSlot} {sToCompiling} {createDictionaryEntry} ;
: variable align here 1 cells allot constant ;
: abs dup 0< 2 0=branch? negate ;
: literal {sToCompiling} ; immediate compile-only

: char+ 1 chars + ;
: cell+ 1 cells + ;
: 2! swap over ! cell+ ! ;
: 2@ dup cell+ @ swap @ ;
: , here 1 cells allot ! ;
: c, here 1 allot c! ;

\ core extension words needed later

: 2>r 			( x1 x2 -- ) ( R: -- x1 x2)
	r> 			( x1 x2 -- x1 x2 ret ) ( R: ret --)
	swap		( x1 x2 ret -- x1 ret x2 )
	rot			( x1 ret x2 -- ret x2 x1 )
	>r >r >r    ( ret x2 x1 -- ) ( R: -- x1 x2 ret )
	;
: 2R@ 			( -- x1 x2 ) ( R: x1 x2 -- x1 x2 )
	r>			( -- ret ) ( R: x1 x2 ret -- x1 x2 )
	r> r>		( ret -- ret x2 x1 ) ( R: x1 x2 -- )
	2dup		( ret x2 x1 -- ret x2 x1 x2 x1 )
	>r >r		( ret x2 x1 x2 x1 -- ret x2 x1 ) ( R: -- x1 x2 )
	rot >r		( ret x2 x1 -- x2 x1 ) ( R: x1 x2 -- x1 x2 ret )
	swap		( x2 x1 -- x1 x2 )
	;
: 2r>			( -- x1 x2 ) ( R: x1 x2 -- )
	r>			( -- ret ) ( R: x1 x2 ret -- x1 x2 )
	r> r>		( ret -- ret x2 x1 ) ( R: x1 x2 -- )
	swap		( ret x2 x1 -- ret x1 x2 )
	rot			( ret x1 x2  -- x1 x2 ret )
	>r			( x1 x2 ret -- x1 x2 ) ( R: -- ret )
	;

\ Control structures

: if
	$80000000 {sToCompiling} {orig} postpone 0=branch? ; immediate compile-only
: else
	$80000000 {sToCompiling} {orig}  postpone branch 1 cs-roll {resolveOrig} ; immediate compile-only
: then {resolveOrig} ; immediate compile-only

: begin {dest} ; immediate compile-only
: while
	postpone if 1 cs-roll ; immediate compile-only
: repeat
	{resolveDest} postpone branch {resolveOrig} ; immediate compile-only
: until
	{resolveDest} postpone 0=branch? ; immediate compile-only

\ cannot use 2r> and 2>r in i and j because they both manipulate the return
\ stack below their own frames and cannot therefore be used "inline" i.e. with
\ postpone
: {s>rs.loop} swap >r >r ;
: {rs>s.loop} r> r> swap ;

: unloop
	postpone r>
	postpone r>
 	postpone drop
	postpone drop ; immediate compile-only

\ Loop test code only used in +loop
\ e d+i i d -- b
\ e is the end of the loop
\ i is the value of i - the current iteration
\ d  is the delta
\ b is true if we need to go round the loop again
\ d >= 0 => b == i < e && e <= i+d
\ d < 0 => b == i >= e && e > i+d == i+d < e && e <= i
\
\ If d is negative, we can swap i and i+d and use the same test
\ In order to avoid wrapping issues, we subtract e from i and i+d so we have
\ d >= 0 => b == i < 0 && i+d >= 0
\ d < 0 => b == i >= 0 && 0 > i+d
\
: {+loopTest}
	0< 				( e d+i i d -- e d+i i d<0 )
	if
		swap 			( e d+i i -- e i d+i )
	then
	rot dup				( e d+i i -- d+i i e e )
	rot swap			( d+i i e e -- d+i e i e )
	-					( d+i e i e -- d+i e i-e )
	rot rot				( d+i e i-e -- i-e d+i e )
	-					( i-e d+i e -- i-e d+i-e )
	0< invert			( i-e d+i-e -- i-e d+i-e>=0 )
	swap				( i-e d+i-e>=0 -- d+i-e>=0 i-e )
	0<					( d+i-e>=0 i-e -- d+i-e>=0 i-e<0 )
	and					( d+i-e>=0 i-e<0 -- d+i-e>=0&&i-e<0 )
	invert
	;

: do 
	{csFrame}
   	{dest}							\ Target for jump back
   	postpone {s>rs.loop} 			\ Save loop variables on the return stack
   	0							 	\ orig count for leave
   	; immediate compile-only

: ?do
	{csFrame}
	postpone 2dup
	postpone -
	5 {sToCompiling}
	postpone 0<>branch?
	postpone 2drop
	-1 {sToCompiling}
	{orig}
	postpone branch
	{dest}							\ Target for jump back
	postpone {s>rs.loop} 			\ Save loop variables on the return stack
	1							 	\ orig count for leave
	; immediate compile-only

: +loop
	postpone {rs>s.loop}	( d -- d e i )
	postpone dup
	postpone >r				( d e i -- d e i R: i )
	postpone rot			( d e i -- e i d )
	postpone dup			( e i d -- e i d d )
	postpone >r				( e i d d -- e i d R: i d )
	postpone +				( e i d -- e d+i )
	postpone 2dup			( e d+i -- e d+1 e d+i )
	postpone r>	
	postpone r>				( e d+1 e d+i R: i d -- e d+1 e d+i d i )
	postpone swap			( e d+1 e d+i d i -- e d+1 e d+i i d )
 	postpone {+loopTest}	( e d+i e d+i i -- e d+i true|false )
	{resolveDest}
	postpone 0<>branch?		( e i+step true|false -- e i+step )
	postpone 2drop
	\ Resolve all the leave origs
	begin
		dup				( lc lc )
		0>				( lc lc>0 )
	while
		{resolveOrig}	( lc )
		1-				( lc-1 )
	repeat
	{csPopFrame}
	drop 				( remove the leave count )
	; immediate compile-only

: loop 1 {sToCompiling} postpone +loop ; immediate compile-only

: leave
	\ First drop the loop information from the return stack
	postpone {rs>s.loop}
	postpone 2drop
	-1 {sToCompiling}
	{orig}				( orig for the end of the loop )
	postpone branch
	1+					( Add one to the leave orig count )
	{csFrameSize} over - ( Calculate how much is above the leave origs )
	dup					( lc lc->i)
	begin
		1 -				( lc i-1 )
		dup				( lc i-1 i-1 )
		0< invert		( count down number of things above leave origs )
	while				( lc i-1 )
		over			( lc i-1 lc )
		cs-roll			( push the new item down below everything that should be on top )
	repeat
	2drop				( remove loop count and cs-roll offset )
	; immediate compile-only

: i
	postpone {rs>s.loop}	( -- e i )
	postpone over
	postpone over			( e i -- e i e i )
	postpone {s>rs.loop}	( e i e i -- e i )
	postpone swap
	postpone drop			( e i -- i )
	; immediate compile-only

: j
	postpone {rs>s.loop}	( -- e i )
  	postpone {rs>s.loop}	( e i e' i' )
	postpone over
	postpone over			( e i e' i' e' i' )
  	postpone {s>rs.loop} 	( e i e' i' )
	postpone 2swap
	postpone {s>rs.loop}	( e' i' )
	postpone swap
	postpone drop			( i' )
	; immediate compile-only


\ ------------------------------------------------------------------------------
\ Core words that need control structures

: s>d
	dup 0< if -1 else 0 then ;
: ?dup dup if dup then ;

\ ------------------------------------------------------------------------------
\ Extension words

: true -1 ;
: false 0 ;
: nip swap drop ;
: tuck swap over ;
: roll ( x0 i*x u.i -- i*x x0 )
	dup if swap >r 1- recurse r> swap exit then  drop ;
: pick
	dup 0= if drop dup exit then  swap >r 1- recurse r> swap ;
\ ------------------------------------------------------------------------------
\ string extensions

: /string ( c-addr1 u1 n -- c-addr2 u2 )
	dup >r - swap r> chars + swap ;
: count 1+ dup 1- c@ ;

\ ------------------------------------------------------------------------------
\ Printing stuff out

: sign 0< if 45 hold then ;
: bl $20 ;
: space bl emit ;
: cr #10 emit ;

: # 							( ud1 -- ud1/base )
		base @ 0				( get base on the stack as a double precision )
		du/mod                  ( ud1 -- udr udq )
		2swap					( udr udq -- udq udr )
		drop					( udq udr -- udq dr )
		dup 9 > if				( if it is greater than 9, we need a letter )
			55 +				( add 'A' - 10 )
		else
			48 +				( add '0')
		then
		hold
		;

: #s							( ud1 -- 0 0 )
		begin
			#					( ud1 -- ud1/base )
			2dup				( ud2 -- ud2 ud2.l ud2.h )
			0=					( ud2 ud2.l ud2.h -- ud2 ud2.l ud2.h==0 )
			swap 0=				( ud2 ud2.l ud2.h==0 -- ud2 ud2.h==0 ud2.l==0 )
			and                 ( ud2 ud2.h==0 ud2.l==0 -- !(ud2.h==0&&ud2.l==0) )
		until
		;

: #> drop drop {completePicture} ;

: ..<						( n l u -- n>=l&&n<u )

\ Checks if a number is within a given range, equivalent to ..< in Swift

		rot						( n l u -- l u n )
		dup						( l u n -- l u n n )
		rot						( l u n n -- l n n u )
		<						( l n n u -- l n n<u )
		rot rot					( l n n<u -- n<u l n )
		<=						( n<u l n -- n<u l<=n )
		and						( n<u l<=n -- n<u&&l<=n )
		;

: {singleDigit}					( ud1 c-addr -- ud2 t||f )

\ performs a conversion to a digit on the char at c-addr. If successful,
\ multuiplies ud1 by base and adds it, then puts true on the stack. If not successful
\ puts false on the stack leaving ud1 unchanged

		c@						( ud c-addr -- ud c )
		\ must be '0' ... '9', 'a' ... 'z', 'A' ... 'Z'
		\ for each range, we convert to a numerical value
		dup 48 58 ..<			( ud c -- ud c c-in-range-0-9? )
		if
			48 -				( ud c -- ud c-'0' )
		else
			dup 65 91 ..<  		( ud c -- ud c c-in-range-A-Z? )
			if
				55 - 			( ud c -- ud c-'A'+10 )
			else
				dup 97 123 ..<  ( ud c -- ud c c-in-range-a-z? )
				if
					87 - 0		( ud c -- ud c-'a'+10 0 )
				else
					drop 0		( ud c -- ud 0 )
					exit
				then
			then
		then
		\ now we have the number as a digit. Check it is less than base
		dup base @	<			( ud n -- ud n n<base )
		if
			0					( ud n -- ud nd )
			2swap				( ud nd -- nd ud )
			base @ 0 du*		( nd ud -- nd ud*base )
			d+					( nd ud*base -- ud*base+nd )
			-1					( ud*base+nd -- ud*base+nd true )
		else
			drop 0				( ud n -- ud false )
		then
		;

: >number						( ud1 c-addr1 u1 – ud2 c-addr2 u2 )

\ Flag to say last digit converted ok
		-1						( ud c-addr u -- ud c-addr u true )
		begin					( ud c-addr u true||false )
			over				( ud c-addr u true||false --  ud c-addr u true||false u )
			0=	invert			( ud c-addr u true||false u -- ud c-addr u true||false u!=0 )
			and					( ud c-addr u true||false u!=0  -- ud c-addr u [true||false]&&u!=0 )
		while
			>r						( ud c-addr u -- ud c-addr, R: u  )
			dup >r					( ud c-addr, R: u -- ud c-addr, R: u c-addr )
			{singleDigit}			( ud c-addr, R: u c-addr  -- ud2 t||f, R: u c-addr )
			if
				r> 1 +				( ud2, R: u c-addr -- ud2 c-addr+1, R: u )
				r> 1 - 				( ud2 c-addr+1, R: u -- ud2 c-addr+1 u-1 )
				-1					( ud2 c-addr+1 u-1 -- ud2 c-addr+1 u-1 true )
			else
				r> r> 0				( ud2, R: u c-addr -- ud2 c-addr u false )
			then
		repeat
		;

: fill 							( c-addr u char -- )
		>r						( c-addr u char -- c-addr u, R: char )
		begin
			dup 0<>
		while
			over				( c-addr u, R: char -- c-addr u c-addr, R: char )
			r@ swap				( c-addr u c-addr, R: char -- c-addr u char c-addr, R: char )
			c!					( c-addr u, R: char )
			1 - swap 1 + swap	( c-addr u, R: char -- c-addr+1 u-1, R: char )
		repeat
		r> drop drop drop		( c-addr u, R: char -- )
		;

: move							( addr1 addr2 u -- )
\ addr1 is the address of the source buffer, addr2 is the address of the
\ destination buffer, u is the length of the buffer in bytes.
\
\ move copies U characters starting from addr1 to addr2. We need to be
\ intelligent enough that, if the buffers overlap, we do not corrupt the
\ destination. We can achieve this by starting from the end of the buffer if
\ the source address is less than the destination address

	 	0 {setTrace} >r			\ Turn trace off

		dup 0=					( make sure we need to copy more than 0 bytes )
		if
			drop drop drop
	 		r> {setTrace} drop		\ Restore old trace
			exit
		then

		>r						( addr1 addr2 u -- addr1 addr2, R: u )
		2dup u<					( addr1 addr2, R: u --  addr1 addr2 addr1-addr2, R: u )
		if						( addr1 < addr2 means copy from the end to the beginning )
			\ Copy in a downwards direction
			r@					( addr1 addr2, R: u -- addr1 addr2 u, R: u )
			+ swap				( addr1 addr2 u, R: u -- addr2+u addr1, R: u )
			r@ +				( addr2+u addr1, R: u -- addr2+u addr1+u, R: u )
			r>					( addr2+u addr1+u, R: u - addr2+u addr1+u u )
			begin
				dup 0<>			( addr2+u addr1+u u --  addr2+u addr1+u u u!=0 )
			while				( addr2+u addr1+u u u!=0 -- addr2+u addr1+u u )
				1 - >r			( addr2+u addr1+u u -- addr2+u addr1+u, R: u-1 )
				1 - dup c@		( addr2+u addr1+u, R: u-1 -- addr2+u addr1+u-1 c, R: u-1 )
				>r swap	1 -		( addr2+u addr1+u-1 c, R: u-1 -- addr1+u-1 addr2+u-1, R: u-1 c )
				r> over			( addr1+u-1 addr2+u-1, R: u-1 c -- addr1+u-1 addr2+u-1 c addr2+u-1, R: u-1 )
				c!				(  addr1+u-1 addr2+u-1 c addr2+u-1, R: u-1 -- addr1+u-1 addr2+u-1, R: u-1 )
				swap r>			( addr1+u-1 addr2+u-1, R: u-1 -- addr2+u-1 addr1+u-1 u-1 )
			repeat
		else
			\ Copy in an upwards direction
			r>					( addr1 addr2, R: u -- addr1 addr2 u )
			begin
				dup 0<>			( addr1 addr2 u -- addr1 addr2 u u<>0 )
			while				( addr1 addr2 u u<>0 -- addr1 addr2 u )
				1 - >r			( addr1 addr2 u -- addr1 addr2, R: u-1 )
				swap dup c@		( addr1 addr2, R: u-1 -- addr2 addr1 c, R: u-1 )
				>r				( addr2 addr1 c, R: u-1 -- addr2 addr1, R: u-1 c )
				1 + swap		( addr2 addr1, R: u-1 c -- addr1+1 addr2, R: u-1 c )
				r>	over		( addr1+1 addr2, R: u-1 c -- addr1+1 addr2 c addr2, R: u-1 )
				c!				( addr1+1 addr2 c addr2, R: u-1 -- addr1+1 addr2, R: u-1 )
				1 +				( addr1+1 addr2, R: u-1 -- addr1+1 addr2+1, R: u-1 )
				r>				( addr1+1 addr2+1, R: u-1 -- addr1+1 addr2+1 u-1 )
			repeat
		then
		drop drop drop			( addr1+u addr2+u 0 -- )
   		r> {setTrace} drop		\ Restore old trace
		;

: type ( c-addr u -- )
		begin
			dup 0<>
		while
			1 - swap 			( c-addr u -- u-1 c-addr )
			dup c@				( u-1 c-addr -- u-1 c-addr c )
			emit				( u-1 c-addr c -- u-1 c-addr )
			1 + swap			( u-1 c-addr -- c-addr+1 u-1 )
		repeat
		drop drop
		;
: s"  0  {stringMode} {parseWord} \#(ThreadedForth.CWFunction.toString.rawValue) {cwFunc} state @ if swap {sToCompiling} {sToCompiling} then ; immediate
: s\" -1 {stringMode} {parseWord} \#(ThreadedForth.CWFunction.toString.rawValue) {cwFunc} state @ if swap {sToCompiling} {sToCompiling} then ; immediate
: ."  postpone s" postpone type ; immediate

: spaces ( n -- )
		begin
			dup 0<>
		while
			1 - 				( n -- n-1 )
			bl emit				( n-1 -- n-1 )
		repeat
		drop
		;

\
\ abort" has to get a string from input and display it if an abort is necessary
\ To get the string from input, it needs to run immediate which means lots of
\ postpones or a helper definition. We choose the latter
\
: {conditionalAbort} 		( c-addr u bool -- )
	if
		type				( c-addr u -- )
		abort
	else
		drop drop			( c-addr u -- )
	then
	;

: abort"
	0 {stringMode} {parseWord}
	\#(ThreadedForth.CWFunction.toString.rawValue) {cwFunc} 	( get string )
	swap {sToCompiling} {sToCompiling}			( and compile its location )
	postpone rot								( the bool needs to be on the top )
	postpone {conditionalAbort}					( do the abort if needed )
	; immediate compile-only


\ A buffer set aside for the key word
variable {keybuffer}

\ Note that this definition is incorrect. Needs to get a key direct from the
\ keyboard
: key
 	{keybuffer} 1 accept
	0= if
		-39 {throw}
	then
 	{keybuffer} c@ ;

\ Some core extension words

: within ( test low high -- flag ) OVER - >R - R> U< ;
: unused 4294967296 here -  ;
: again {resolveDest} postpone branch ; immediate compile-only
: buffer: create allot ;

\ Found on Stack Overflow. Thanks to Albert van der Horst
\ https://stackoverflow.com/a/28421090/169346
\ This would obviously bel broken in a multi-threaded environment
variable {to-message}   \ 0: from ,  1: to .
0 {to-message} !		\ initialise the variable
: to 1 {to-message} ! ;
: value create , does> {to-message} @ if ! else @ then
  0 {to-message} ! ;

: case
	0					( count of case endof origs to resolve )
	; immediate compile-only

: of
	postpone over		( x1 x2 -- x1 x2 x1 )
	postpone =			( x1 x2 x1 -- x1 x2=x1 )
	-1 {sToCompiling}	( x1 x2=x1 -- x1 x2=x1 dest )
	{orig}
	postpone 0=branch?	( x1 x2=x1 dest -- x1 )
	postpone drop
	; immediate compile-only

: endof
	-1 {sToCompiling}
	{orig}
	1 +
	postpone branch
	1 cs-roll			( bring the orig for the of to the top )
	{resolveOrig}
	; immediate compile-only

: endcase
	postpone drop		\ discard the case selector
	\ Resolve all the endof origs
	begin
		dup				( c -- c c )
		0>				( c c -- c c>0 )
	while
		{resolveOrig}	( c c>0 -- c )
		1-				( c -- c-1 )
	repeat
	drop
	; immediate compile-only

: c"
\ Allocate space for the length (init to zero)
	here 0 c,									( -- c-addr )
\ read the next word in string mode
 	0 {stringMode} {parseWord}
	\#(ThreadedForth.CWFunction.toString.rawValue) {cwFunc}  ( c-addr  -- c-addr c-addr+1 length )
\ convert to a counted string
	swap 1 -									( c-addr c-addr+1 length --  c-addr length c-addr )
	c!											( c-addr length c-addr -- c-addr )
\ Add the instruction to put the address of the counted string on the stack
	{sToCompiling}
	; immediate

\ Erase u cells from addr on
 : erase ( addr u -- )
 	0 			( addr u -- addr u 0 )
	?do
		i 		( addr -- addr i )
		over +	( addr i -- addr i+addr )
		0 swap	( addr i*+addr -- addr 0 i+addr )
		c!
	loop
	drop		( addr -- )
	;

: parse 0 {universal-parse} ;
: word -1 {universal-parse} {sStringToCountedTransient} ;
: parse-name 0 -1 {universal-parse} ;

: defer ( "name" -- )
   	create ['] abort ,
	does> ( ... -- ... )
   		@ execute ;
: defer@ ( xt1 -- xt2 ) >body @ ;
: defer! ( xt2 xt1 -- ) >body ! ;
: action-of
   state @ if
	 postpone ['] postpone defer@
   else
	 ' defer@
   then ; immediate
: is
   state @ if
	 postpone ['] postpone defer!
   else
	 ' defer!
   then ; immediate

: holds ( addr u -- )
   begin dup while 1- 2dup + c@ hold repeat 2drop ;

\ File access words

: bin ;	\ bin is a noop on macOS
: flush-file drop 0 ; \ This is a noop since we use Unix descriptors for our files

: {openFileOrThrow} ( caddr u -- fileId )
	open-file		( caddr u -- fileId errno )
	dup 0<>			( fileID errno -- fileid errno errno<>0 )
	if
		decimal
  		\#(Forth.ErrorNumber.openFile.rawValue) {throw}
	then
	drop
	;
: {closeFileOrThrow} ( fileid -- )
	close-file		( fileId -- errno )
	dup 0<>			( errno -- errno errno<>0 )
	if
		decimal
		\#(Forth.ErrorNumber.openFile.rawValue) {throw}
	then
	drop
	;

: include-file
	dup >r				\ preserve a copy of the file id
	{includeFile} 0 execute
	{inputPop}
	r> {closeFileOrThrow}
	;
: included
	r/o {openFileOrThrow}
	include-file
	;
: required
	r/o {openFileOrThrow}
	dup {isIncluded}
	if 
		drop
	else
		include-file
	then
	;
: include parse-name included ;
: require parse-name required ;

\ Block words

: block -1 {cacheBlock} ;
: buffer 0 {cacheBlock} ;
: flush save-buffers empty-buffers ;
: load
	dup ( blockNumber -- blockNumber blockNumber )
	block	( blockNumber blockNumber -- blockNumber address )
	1024 rot	(  blockNumber address --  address 1024 blockNumber )
	{stringInputPush} 0 execute {inputPop}
	;
\ Write block u to stdout
\ Write as 16 x 64 byte lines
: list ( u -- )
	dup scr !
	block {listBlock}
	;

: refill
	blk @ dup 0 <>				( -- bn bn<>0 )
	if
 		dup						( bn -- bn bn )
		1 + block 1024 rot		( bn bn -- b-addr 1024 bn )
		{stringInputReplace}
	else
		drop
	then
	{refill}
	;
: thru 1+ swap do i load loop ;

\ ------------------------------------------------------------------------------
\ Double precision

: 2literal swap {sToCompiling} {sToCompiling} ; immediate compile-only
: 2constant {parseWord} {createNamedWordSlot} swap {sToCompiling} {sToCompiling}  {createDictionaryEntry} ;
: 2variable align here 2 cells allot constant ;
: d0< 0 0 d< ;
: d0= 0 0 d= ;
: d2* 1 d<< ;
: d2/ 1 d>> ;
: dmax ( d1l d1h d2l d2h -- d3l d3h ) \ d3 is the max of d1 and d2
	2over 2over ( d1l d1h d2l d2h -- d1l d1h d2l d2h d1l d1h d2l d2h )
	d<			( d1l d1h d2l d2h d1l d1h d2l d2h -- d1l d1h d2l d2h d1<d2 )
	if
		2swap
	then
	2drop
	;
: dmin ( d1l d1h d2l d2h -- d3l d3h ) \ d3 is the max of d1 and d2
	2over 2over 		( d1l d1h d2l d2h -- d1l d1h d2l d2h d1l d1h d2l d2h )
	d< 			( d1l d1h d2l d2h d1l d1h d2l d2h -- d1l d1h d2l d2h d1<d2 )
	if
	else
		2swap
	then
	2drop
	;
: d>s drop ;
: dabs 2dup d0< if dnegate then ;
: m+ s>d d+ ;
: d. 0 d.r ;
: 2rot
	2>r
	2swap
	2r>
	2swap
	;

: um*
	0 rot 0 		\ double precision both operands
	du*
	;
: um/mod
	0				\ double precision divisor
	du/mod
	0<> if			\ check for overflow
		-256 {throw}
	then
	swap drop
	;

: 2value .( 2value not implemented ) ;

\ Exception stuff
: catch .( catch not implemented ) ;
: throw .( throw not implemented ) ;
"""#
	}
}
