//
//  Forth+Stack.swift
//  
//
//  Created by Jeremy Pereira on 12/07/2020.
//
//  Copyright (c) Jeremy Pereira 2023
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

/// Any stack that supports stack frames must have elements that can be c
/// converted to or from a frame pointer - which is an `Int`
///
/// This is so that we can store frame pointers on the stack
protocol Frameable
{
	/// Make an `Element` from a frame pointer
	/// - Parameter framePointer: The frame pointer to create from
	/// - Returns: An element that can be pushed onto the stack
	static func makeFrom(framePointer: Int) -> Self

	/// Get an element as a frame pointer i.e. an `Int`
	///
	/// - Returns: an `Int` representing a frame pointer
	/// - Throws: if the element cannot be converted to a frame pointer.
	func asFramePointer() throws -> Int
}

extension Forth
{
	struct Stack<Element>
	{
		let name: String

		private var framePointer = -1
		private var stackPointer = 0

		/// All the elements on the stack
		private var _elements: [Element] = []

		var elements: ArraySlice<Element>
		{
			return _elements[0 ..< stackPointer]
		}

		init(name: String)
		{
			self.name = name
		}

		///    Push an element onto the top of the stack
		///
		/// - parameter newElement: The element to push
		mutating func push(_ newElement: Element)
		{
			if stackPointer == _elements.count
			{
				_elements.append(newElement)
			}
			else
			{
				_elements[stackPointer] = newElement
			}
			stackPointer += 1
		}

		///    Pops the top element off the stack
		///
		///    - returns: The top element or nil if the stack is empty.
		mutating func pop() -> Element?
		{
			guard stackPointer > 0 else { return nil }
			stackPointer -= 1
			return _elements[stackPointer]
		}

		/// Pop the top element off the stack.
		///
		/// If the stack is empty an error is thrown
		///
		/// - Returns: The top element of the stack
		/// - Throws: If the stack is empty
		mutating func throwingPop() throws -> Element
		{
			guard let ret = self.pop() else { throw Error.stackUnderflow (name) }
			return ret
		}

		mutating func throwingTop() throws -> Element
		{
			guard let ret = self.top else { throw Error.stackUnderflow (name) }
			return ret
		}

		///  The top element of the stack. Will be nil if the stack is empty
		var top: Element?
		{
			return stackPointer > 0 ? _elements[stackPointer - 1] : nil
		}

		/// Number of items in the stack
		var count: Int
		{
			return stackPointer
		}

		/// True if the stack is empty
		var isEmpty: Bool
		{
			return stackPointer == 0
		}

		mutating func empty()
		{
			stackPointer = 0
			framePointer = -1
		}
	}
}

extension Forth.Stack where Element: Frameable
{

	/// The offset from the current frame base of the top element on the stack
	var frameOffsetOfTop: Int
	{
		return stackPointer - framePointer - 1
	}
	/// Create a new stack frame
	///
	/// The old frame base is pushed onto the stack and a new one is created
	/// with offset zero pointing at the old one on the stack.
	mutating func createFrame()
	{
		let newFramePointer = stackPointer
		self.push(Element.makeFrom(framePointer: framePointer))
		framePointer = newFramePointer
	}

	/// Peek at the element with the given offset from the frame base.
	/// - Parameter frameOffset: The offset of the element to peek
	/// - Returns: The element at the given offset from the frame base.
	/// - Throws: if the offset is to an element below the bottom or above the
	///           top of the stack
	func peek(frameOffset: Int) throws -> Element
	{
		let elementIndex = framePointer + frameOffset
		guard (0 ..< stackPointer).contains(elementIndex)
			else { throw Forth.Error.addressOutOfBounds }
		return elements[elementIndex]
	}

	mutating func popFrame() throws
	{
		guard framePointer > -1
			else { throw Forth.Error.stackUnderflow("Already at bottom frame") }
		stackPointer -= frameOffsetOfTop
		framePointer = try throwingPop().asFramePointer()
	}
}

extension Int: Frameable
{
	func asFramePointer() -> Int
	{
		return self
	}

	static func makeFrom(framePointer: Int) -> Int
	{
		return framePointer
	}
}
