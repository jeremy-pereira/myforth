//
//  DoublePrecision.swift
//  
//
//  Created by Jeremy Pereira on 19/06/2020.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Toolbox


/// Double precision number conversion protocol
///
/// Double precision numbers need to be convertible to and from ``MyForth/Forth/Cell``
/// Both the type itself and the magnitude need to be convertible
protocol CellConvertible: FixedWidthInteger
{
	/// Create an instance from a ``MyForth/Forth/Cell``
	/// - Parameter cell: The cell vaue to convert
	/// - Returns: An instance of the type created from the `Cell`'s **bit pattern**
	static func from(cell: Forth.Cell) -> Self

	/// Create an instance of the type's `Magnitude` from the `Cell`'s bit pattern
	/// - Parameter cell: The cell value to convert
	/// - Returns: An instance of the type's `Magnitude`
	static func magnitudeFrom(cell: Forth.Cell) -> Self.Magnitude
	static func asCell(_ int: Self) -> Forth.Cell
	static func magnitudeAsCell(_ magnitude: Self.Magnitude) -> Forth.Cell
	static func magnitude(bitPattern: Self) -> Self.Magnitude
	static func fromMagnitude(bitPattern: Self.Magnitude) -> Self

	/// Get the absolute value of `self`
	/// - Returns: The absolute value of `self`
	/// - Throws: if `self` is negative but its negation cannot be represented
	///           as an instance of `Self` e.g. `Int.min`
	func absolute() throws -> Self
	
}

extension CellConvertible
{
	var asCell: Forth.Cell { Self.asCell(self) }
}
/// A double precision version of an existing integer type
///
/// Can be used to create a double precsion version of any fixed width integer`
struct DoublePrecision<INT: CellConvertible> where INT.Magnitude: CellConvertible
{
	static var bitWidth: Int { INT.bitWidth + INT.Magnitude.bitWidth }
	
	typealias Cell = Forth.Cell

	fileprivate(set) var low: INT.Magnitude
	fileprivate(set) var high: INT

	init(low: Cell, high: Cell)
	{
		self.low = INT.magnitudeFrom(cell: low)
		self.high = INT.from(cell: high)
	}

	init(tuple: (high: INT, low: INT.Magnitude))
	{
		self.low = tuple.low
		self.high = tuple.high
	}

	init(_ singlePrecision: INT)
	{
		if INT.isSigned
		{
			low = INT.magnitude(bitPattern: singlePrecision)
			high = singlePrecision < 0 ? -1 : 0
		}
		else
		{
			high = 0
			low = singlePrecision.magnitude
		}
	}

	/// Divide this double precision by a number of its single precesion type
	///
	/// This must work using symmetric division for signed types. i.e. quotient
	/// rounds towards zero, remainder has the same sign as the dividend.
	/// - Parameter divisor: Nmber to divide by
	/// - Returns: the quotient and the remainder
	/// - Throws: If the quotient of the result won't fit into the single
	///           precision type or the divisor is zero
	func divided(by divisor: INT) throws -> (quotient: INT, remainder: INT)
	{
		guard divisor != 0 else { throw Forth.Error.divisionByZero }
		let extremeMultiplier = (divisor >= 0 && !self.isNegative) || (divisor < 0 && self.isNegative) ? INT.max : INT.min
		let extremeDividendTuple = divisor.multipliedFullWidth(by: extremeMultiplier)
		var extremeDividend = DoublePrecision(tuple: extremeDividendTuple)
		// We can go more extreme because of rounding with a remainder.
		// Calcualte the amount we can go further from 0. Note that we can't just
		// do abs(divisor) - 1 because, for Int.min, that overflows
		let extraExtreme = try DoublePrecision(divisor < 0 ? (divisor + 1).absolute() : (divisor - 1).absolute())
		try extremeDividend.add((extremeDividend >= DoublePrecision<INT>.init(0)) ? extraExtreme : extraExtreme.negated())
		assert((extremeDividend.isNegative && self.isNegative) || (!extremeDividend.isNegative && !self.isNegative))
		guard (self.isNegative && extremeDividend <= self) || (!self.isNegative && extremeDividend >= self)
		else
		{
			throw Forth.Error.integerOverflow
		}

		// Now we have checked the overflow, we can do the division

		return divisor.dividingFullWidth((high: self.high, low: self.low))
	}

	var cells: (low: Cell, high: Cell)
	{
		return (low: INT.magnitudeAsCell(low), high: INT.asCell(high))
	}

	var isNegative: Bool { INT.isSigned && high < 0 }

	func negated() throws -> Self
	{
		if !(INT.isSigned)
		{
			throw Forth.Error.cantNegateUnsigned
		}
		return twosComplement()
	}

	func inverted() -> Self
	{
		Self.init(tuple: (high: ~self.high, low: ~self.low))
	}

	func twosComplement() -> Self
	{
		let lowInverted = ~low
		let highInverted = ~high
		let (newLow, carry) = lowInverted.addingReportingOverflow(1)
		let newHigh = highInverted &+ (carry ? 1 : 0)
		return Self.init(tuple: (high: newHigh, low: newLow))
	}

	func twosComplementWithOverflow() -> (Self, Bool)
	{
		let lowInverted = ~low
		let highInverted = ~high
		let (newLow, carry) = lowInverted.addingReportingOverflow(1)
		let (newHigh, overflow) = highInverted.addingReportingOverflow(carry ? 1 : 0)
		return( Self.init(tuple: (high: newHigh, low: newLow)), overflow)
	}

	/// Add another double precision to this one
	///
	/// Overflow is ignored
	/// - Parameter other: Another double precision number
	mutating func add(_ other: Self)
	{
		let unsignedSelfHigh = INT.magnitude(bitPattern: self.high)
		let unsignedOtherHigh = INT.magnitude(bitPattern: other.high)
		let (newLow, halfCarry) = low.addingReportingOverflow(other.low)
		let newHigh = unsignedSelfHigh &+ unsignedOtherHigh &+ (halfCarry ? 1 : 0)
		self.high = INT.fromMagnitude(bitPattern: newHigh)
		self.low = newLow
	}

	mutating func subtract(_ other: Self)
	{
		self.add(other.twosComplement())
	}

	func addingReportingOverflow(_ other: Self) -> (Self, Bool)
	{
		var ret = self
		ret.add(other)
		let overflow: Bool
		if INT.isSigned
		{
			overflow = other >= Self.init(0) ? ret < self : ret >= self
		}
		else
		{
			overflow = ret < self
		}
		return (ret, overflow)
	}

	func subtractingReportingOverflow(_ other: Self) -> (Self, Bool)
	{
		var ret = self
		ret.subtract(other)
		let overflow: Bool
		if INT.isSigned
		{
			overflow = other >= Self.init(0) ? ret > self : ret <= self
		}
		else
		{
			overflow = ret > self
		}
		return (ret, overflow)
	}

	func multipliedSinglePrecisionFullWidth(_ single: INT) -> (high: Self, low: DoublePrecision<INT.Magnitude>)
	{
		// Get rid of sme easy edge cases
		guard !(self.is0 || single == 0) 
		else { return (high: Self.zero, low: DoublePrecision<INT.Magnitude>.zero) }

		let positiveSelf = self.isNegative ? self.twosComplement() : self
		let positiveSingle = INT.magnitude(bitPattern: single < 0 ? (~single &+ 1) : single) // Uses 2's complement to avoid abort

		let (lowCarry, lowResult) = Self.singleMultiplyWithCarry(positiveSelf.low, 
																 positiveSingle,
																 carry: 0)
		let (highCarry, highResult) = Self.singleMultiplyWithCarry(INT.magnitude(bitPattern: positiveSelf.high),
																   positiveSingle,
																   carry: lowCarry)
		let high = Self.init(INT.fromMagnitude(bitPattern: highCarry))
		let low = DoublePrecision<INT.Magnitude>(tuple: (high: highResult, low: lowResult))
		if self.isNegative == (single < 0)
		{
			return (high: high, low: low)
		}
		else
		{
			// Negate by doing twos complement across low and high
			let (invertedLow, carry) = low.twosComplementWithOverflow()
			let invertedHigh: Self
			invertedHigh = carry ? high.twosComplement() : high.inverted()
			return (high: invertedHigh, low: invertedLow)
		}
	}

	fileprivate static func singleMultiplyWithCarry(_ a: INT.Magnitude, _ b: INT.Magnitude, carry: INT.Magnitude) -> (high: INT.Magnitude, low: INT.Magnitude)
	{
		let (timesHigh, timesLow) = a.multipliedFullWidth(by: b)
		let (addLow, overflow) = timesLow.addingReportingOverflow(carry)

		let (addHigh, finalCarry) = timesHigh.addingReportingOverflow(overflow ? 1 : 0)
		guard !finalCarry else { fatalError("Carry out of full width multiplication should not happen") }
		return (high: addHigh, low: addLow)
	}

	static var max: Self
	{
		Self.init(tuple: (high: INT.max, low: INT.Magnitude.max))
	}

	var leadingZeroBitCount: Int
	{
		high == 0 ? (INT.bitWidth + low.leadingZeroBitCount) : high.leadingZeroBitCount
	}

	/// The number of significant bits
	///
	/// one more than the position of the most significant 1
	var significantBits: Int { INT.bitWidth - leadingZeroBitCount }

	var isTopBitSet: Bool
	{
		high & (1 << (INT.bitWidth - 1)) != 0
	}

	var is0: Bool { high == 0 && low == 0 }
	var is1: Bool { high == 0 && low == 1 }
	static var zero: Self { Self.init(0) }

	var magnitude: DoublePrecision<INT.Magnitude>
	{
		DoublePrecision<INT.Magnitude>(tuple: (high: high.magnitude, low: low))
	}

	func asSinglePrecision() throws -> INT
	{
		let intLow = INT.fromMagnitude(bitPattern: low)
		// High can only be sign extension or self won't fit a single precision
		guard high == 0 || (INT.isSigned && high == -1)
		else { throw Forth.Error.integerOverflow }
		// If INT is signed, need to check that intLow is the right sign
		guard !INT.isSigned || (high == -1 && intLow < 0 ) || (high == 0 && intLow >= 0)
		else { throw Forth.Error.integerOverflow }
		return intLow
	}

	static func <<= <RHS: BinaryInteger> (lhs: inout Self, rhs: RHS)
	{
		lhs = lhs << rhs
	}

	static func << <RHS: BinaryInteger> (lhs: Self, rhs: RHS) -> Self
	{
		guard rhs != 0 else { return lhs }

		if rhs < 0
		{
			return lhs >> (0 - rhs)
		}
		else if rhs > INT.bitWidth
		{
			let newHigh = INT.magnitudeAsCell(lhs.low << (rhs - RHS(INT.bitWidth)))
			return  Self(low: 0, high: newHigh)
		}
		else
		{
			let newLow = INT.magnitudeAsCell(lhs.low << rhs)

			let newHighBitsFromLow = INT.magnitudeAsCell(lhs.low >> (RHS(UInt.bitWidth) - rhs))
			let newHighBitsFromHigh = INT.asCell(lhs.high << rhs)
			let newHigh = newHighBitsFromLow | newHighBitsFromHigh

			return Self(low: newLow, high: newHigh)
		}
	}

	static func >>= <RHS: BinaryInteger> (lhs: inout Self, rhs: RHS)
	{
		lhs = lhs >> rhs
	}

	static func >> <RHS: BinaryInteger> (lhs: Self, rhs: RHS) -> Self
	{
		guard rhs != 0 else { return lhs }

		let isNegative = INT.isSigned && lhs.high < 0
		if rhs < 0
		{
			return lhs << (0 - rhs)
		}
		else if rhs > INT.bitWidth
		{
			let newLow = INT.asCell(lhs.high >> (rhs - RHS(INT.bitWidth)))
			let newHigh = isNegative ? -1 : 0
			return Self(low: newLow, high: newHigh)
		}
		else
		{
			let newHigh = INT.asCell(lhs.high >> rhs)
			let newLowBitsFromLow = INT.magnitudeAsCell(lhs.low >> rhs)
			let newLowBitsFromHigh = INT.asCell(lhs.high << (RHS(UInt.bitWidth) - rhs))
			let newLow = newLowBitsFromLow | newLowBitsFromHigh
			return Self(low: newLow, high: newHigh)
		}
	}

	static func |= (lhs: inout Self, rhs: Self)
	{
		lhs.high |= rhs.high
		lhs.low |= rhs.low
	}

	static func | (lhs: Self, rhs: Self) -> Self
	{
		let newHigh = lhs.high | rhs.high
		let newLow = lhs.low | rhs.low
		return Self(tuple: (high: newHigh, low: newLow))
	}

}

extension DoublePrecision: Comparable
{
	static func < (lhs: DoublePrecision<INT>, rhs: DoublePrecision<INT>) -> Bool 
	{
		lhs.high < rhs.high || (lhs.high == rhs.high && lhs.low < rhs.low)
	}
	
	static func == (lhs: DoublePrecision<INT>, rhs: DoublePrecision<INT>) -> Bool
	{
		lhs.high == rhs.high && lhs.low == rhs.low
	}
}

extension DoublePrecision: ExpressibleByIntegerLiteral where INT: _ExpressibleByBuiltinIntegerLiteral
{
	init(integerLiteral value: INT)
	{
		self.init(value)
	}

}

extension DoublePrecision
where INT == INT.Magnitude
{
	func quotientAndRemainder(dividingBy divisor: Self) -> (quotient: Self, remainder: Self)
	{
		// Get some easy cases out of the way
		guard divisor != Self.init(0) else { fatalError("Division by 0") }
		guard divisor != Self.init(1) else { return (quotient: self, remainder: Self.zero) }
		guard divisor <= self else { return (quotient: Self.zero, remainder: self) }
		// How many times we need to test the divisor against the shifting dividend?
		// We need to stop when the number of remaining bits in the dividend
		// is less than the bits in the divisor.
		let numberOfTests = self.significantBits - divisor.significantBits + 1
		// We also need to be ready to shift the first significant bit into
		// position to align with the divisor to skip testing any leading
		// zeros.
		let initialShift = divisor.leadingZeroBitCount + 1 - self.leadingZeroBitCount
		var dividendHigh = self >> initialShift
		var dividendLow = self << (UInt128.bitWidth - initialShift)
		var quotient = Self.zero
		for _ in 0 ..< numberOfTests
		{
			quotient <<= 1
			dividendHigh <<= 1
			dividendHigh |= dividendLow.isTopBitSet ? Self.init(1) : Self.zero
			dividendLow <<= 1
			if dividendHigh >= divisor
			{
				quotient |= Self.init(1)
				dividendHigh.subtract(divisor)
			}
		}
		return (quotient: quotient, remainder: dividendHigh)
	}

	func multipliedFullWidth(by other: Self) -> (high: Self, low: Self)
	{
		// Going to use long multiplication (carrys omitted)
		//
		//            h1    l1
		// x          h2    l2
		// -------------------
		//         h1xl2 l1xl2
		//   h1xh2 l1xh2     0
		// -------------------
		// p4   p3    p2    p1

		let (l1l2Carry, p1) = Self.singleMultiplyWithCarry(self.low, other.low, carry: 0)
		let (h1l2Carry, h1l2) = Self.singleMultiplyWithCarry(self.high, other.low, carry: l1l2Carry)
		let (l1h2Carry, l1h2) = Self.singleMultiplyWithCarry(self.low, other.high, carry: 0)
		let (h1h2Carry, h1h2) = Self.singleMultiplyWithCarry(self.high, other.high, carry: l1h2Carry)
		let (p2, p2Carry) = l1h2.addingReportingOverflow(h1l2)
		let (p3p, p3Carryp) = h1h2.addingReportingOverflow(h1l2Carry)
		let (p3, p3Carry) = p3p.addingReportingOverflow(p2Carry ? 1 : 0)
		assert(!(p3Carry && p3Carryp))
		let p4 = h1h2Carry + (p3Carry ? 1 : 0)
		return (high: Self(tuple: (high: p4, low: p3)),
				 low: Self(tuple: (high: p2, low: p1)))
	}

	func multipliedReportingOverflow(by rhs: Self) -> (partialValue: Self, overflow: Bool)
	{
		let (high, low) = self.multipliedFullWidth(by: rhs)
		return (partialValue: low, overflow: high != Self.zero)
	}
}

extension DoublePrecision: StringParsableNumber
{
	init?<S: StringProtocol>(_ seq: S, radix: Int)
	{
		var isNegative = false

		var startIndex = seq.startIndex
		if seq[startIndex] == "-"
		{
			guard INT.isSigned else { return nil }
			isNegative = true
			seq.formIndex(after: &startIndex)
		}
		guard let positiveResult = UInt128(seq[startIndex...], radix: radix)
		else { return nil }

		guard !positiveResult.isTopBitSet || !INT.isSigned else { return nil } // UInt is too big for an int
		let cellLow = UInt.asCell(positiveResult.low)
		let cellHigh = UInt.asCell(positiveResult.high)
		let positiveInt = Self(low: cellLow, high: cellHigh)
		if isNegative
		{
			let negativeInt = try! positiveInt.negated()
			low = negativeInt.low
			high = negativeInt.high
		}
		else
		{
			low = positiveInt.low
			high = positiveInt.high
		}
	}

	var asInterpretationResult: ThreadedForth.InterpretationResult
	{
		let cells = self.cells
		return .doubleLiteral(cells.low, cells.high)
	}

	typealias OverflowFallback = StringParsableNumberError
}

extension Int: CellConvertible
{
	static func magnitudeFrom(cell: Forth.Cell) -> UInt { UInt(bitPattern: cell) }
	static func from(cell: Forth.Cell) -> Int { cell }

	static func asCell(_ int: Self) -> Forth.Cell { int }
	static func magnitudeAsCell(_ magnitude: Self.Magnitude) -> Forth.Cell
	{
		Forth.Cell(bitPattern: magnitude)
	}

	static func magnitude(bitPattern: Int) -> UInt 
	{
		UInt(bitPattern: bitPattern)
	}
	static func fromMagnitude(bitPattern: UInt) -> Int 
	{
		Int(bitPattern: bitPattern)
	}

	func absolute() throws -> Int 
	{
		guard self != Int.min else { throw Forth.Error.integerOverflow }
		return abs(self)
	}
	
}

extension UInt: CellConvertible
{
	static func magnitude(bitPattern: UInt) -> UInt { bitPattern }
	static func fromMagnitude(bitPattern: UInt) -> UInt { bitPattern }

	static func from(cell: Forth.Cell) -> UInt 
	{
		UInt(bitPattern: cell)
	}

	static func asCell(_ int: Self) -> Forth.Cell 
	{
		Forth.Cell(bitPattern: int)
	}

	static func magnitudeFrom(cell: Forth.Cell) -> UInt 
	{
		UInt(bitPattern: cell)
	}

	static func magnitudeAsCell(_ magnitude: UInt) -> Forth.Cell 
	{
		Forth.Cell(bitPattern: magnitude)
	}
	
	func absolute() throws -> UInt 
	{
		self
	}
}

extension UInt128: StringParsableNumber
{
	typealias OverflowFallback = StringParsableNumberError

	var asInterpretationResult: ThreadedForth.InterpretationResult 
	{
		.doubleLiteral(UInt.magnitudeAsCell(low), UInt.asCell(high))
	}
	
	init?<S: StringProtocol>(_ seq: S, radix: Int)
	{
		guard !seq.isEmpty else { return nil }
		let multiplier = UInt128(low: UInt(bitPattern: radix), high: 0)
		var result: UInt128 = 0
		for char in seq
		{
			if let digit = UInt(String(char), radix: radix)
			{
				let (resultx10, overflow) = result.multipliedReportingOverflow(by: multiplier)
				guard !overflow else { return nil }
				let (resultx10plusDigit, overflow2) = resultx10.addingReportingOverflow(UInt128(digit))
				guard !overflow2 else { return nil }
				result = resultx10plusDigit
			}
			else
			{
				return nil
			}
		}
		self.high = result.high
		self.low = result.low
	}
}

extension CellConvertible
{
	func dividing(high: Self, low: [Self.Magnitude]) -> (high: Self, low: [Self.Magnitude], remainder: Self)
	{
		// We can only divide positive numbers by positive numbers, so negate
		// anything negative
		let divisor = self.magnitude
		var dividend: [Self.Magnitude]
		if high < 0
		{
			dividend = []
			var carry: Bool = true
			(dividend, carry) = low.twosComplement()
			dividend.append((~high &+ (carry ? 1 : 0)).magnitude)
		}
		else
		{
			dividend = low + [high.magnitude]
		}
		// Now do the division
		var quotient: [Self.Magnitude] = []
		var carry: Self.Magnitude = 0
		for part in dividend.reversed()
		{
			let (qpart, rpart) = divisor.dividingFullWidth((high: carry, low: part))
			// We want to avoid leading zero pasrts
			if !quotient.isEmpty || qpart != 0
			{
				quotient.append(qpart)
			}
			carry = rpart
		}
		// If the answer is zero, we need at least one zero part to the quotient
		// and for multi part quotients, the parts need to be put into little
		// endian order.
		if quotient.isEmpty
		{
			quotient = [0]
		}
		else if quotient.count > 1
		{
			quotient = quotient.reversed()
		}
		// Check if we need to reverse the sign of anything
		let newLow: [Self.Magnitude]
		let newHigh: Self

		if (self < 0) != (high < 0)
		{
			let (negative, _) = quotient.twosComplement()
			newLow = Array(negative[0 ..< (negative.endIndex - 1)])
			newHigh = Self.fromMagnitude(bitPattern: negative.last!)
		}
		else
		{
			newLow = Array(quotient[0 ..< (quotient.endIndex - 1)])
			newHigh = Self.fromMagnitude(bitPattern: quotient.last!)
		}
		let newRemainder = Self.fromMagnitude(bitPattern: high < 0 ? (~carry &+ 1) : carry)
		return (high: newHigh, low: Array(newLow), remainder: newRemainder)
	}
}

extension Sequence where Element: UnsignedInteger, Element: FixedWidthInteger
{
	func twosComplement() -> ([Element], Bool)
	{
		var carry: Bool = true
		var result: [Self.Element] = []
		for number in self
		{
			let (negative, newCarry) = (~number).addingReportingOverflow(carry ? 1 : 0)
			result.append(negative)
			carry = newCarry
		}
		return (result, carry)
	}
}

extension String
{
	init<INT>(_ value: DoublePrecision<INT>, radix: Int)
	{
		guard (2 ... 36).contains(radix) else { fatalError("Radix must be between 2 and 36") }
		let divisor = DoublePrecision.init(INT.Magnitude(radix))
		var quotient = value.magnitude
		var digits: [String] = []
		while !quotient.is0
		{
			let (q, r) = quotient.quotientAndRemainder(dividingBy: divisor)
			try! digits.append(String(r.asSinglePrecision(), radix: radix))
			quotient = q
		}
		if digits.isEmpty
		{
			digits = ["0"]
		}
		if value.isNegative
		{
			digits.append("-")
		}
		self.init(digits.reversed().joined())
	}
}
