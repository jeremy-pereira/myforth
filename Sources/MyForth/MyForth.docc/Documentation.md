# ``MyForth``

A Forth Compiler that aspires to be Forth 2012 compliant

## Overview

This is a Forth compiler built entirely in Swift. 

## Topics

### <!--@START_MENU_TOKEN@-->Group<!--@END_MENU_TOKEN@-->

- <!--@START_MENU_TOKEN@-->``Symbol``<!--@END_MENU_TOKEN@-->
