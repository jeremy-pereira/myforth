//
//  Forth+InputSource.swift
//  
//
//  Created by Jeremy Pereira on 20/11/2021.
//
//  Copyright (c) Jeremy Pereira 2021, 2023
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import SystemPackage
import Toolbox

private let log = Logger.getLogger("MyForth.Forth+InputSource")

/// Describes an iterator that has the property it can be relocated.
///
/// Works lile a sequence iterator except it can potentially be relocated
public protocol LocatableIterator
{
	/// Return the next character in the iteration
	/// - Returns: The next character or `nil` if we reached the end
	mutating func next() throws -> Character?
	/// Can this iterator be relocated or not
	var isRelocatable: Bool { get }

	/// Save the state of the locator
	///
	/// Returns an array of `Int` which  is used to denote a save state that
	/// can be restored. The content of the array is opaque to the Forth
	/// interpreter, so can be anything you like. For example, the `String`
	/// implementation uses a single value array as an index into an array of s
	/// aved `String.Index` to avoid the problems associated with finding an
	/// offset of a particular character in the string (finding an index at a
	/// certain offset requires scanning the string.
	///
	/// You can return an empty array if you do not support relocation
	/// - Returns: An `[Int]` describing the save point.
	mutating func saveLocation() -> [Forth.Cell]
	
	/// Resstore a previously saved location
	/// - Parameter location: The location to restore encoded as an array of
	///   ``MyForth/Forth/Cell``
	/// - Returns: true if the restore was successful
	mutating func restore(location: [Forth.Cell]) -> Bool
	
	/// Location of the start of the source if it coame from the data space
	///
	/// Sources for `evaluate` are in the data space and the `source` word
	/// expects the address to be that of a `UInt8` in the data space.
	/// This should return a data space address if it came from the database or
	/// `nil` otherwise. There is a default implementation that returns `nil`
	var dataSpaceAddress: Forth.Cell? { get }
	
	/// The block number from which input is taken
	///
	/// For the majority of input sources, this will be 0. For block input
	/// sources, it is the number of the block.
	var blockNumber: Forth.Cell { get }
}

extension LocatableIterator
{
	public var dataSpaceAddress: Forth.Cell? { nil }
	public var blockNumber: Forth.Cell { 0 }
}

/// Can manufactir a ``LocatableIterator``
///
/// Types that implement this protocol can manufacture a ``LocatableIterator``
/// for use with an ``MyForth/Forth/InputSource``
public protocol LocatableIterable
{
	associatedtype InputSourceIterator: LocatableIterator
	
	/// Make an iterator for use with an ``MyForth/Forth/InputSource``
	/// - Returns: An input source iterator
	func makeInputSourceIterator() -> InputSourceIterator
}

public extension Forth
{

	/// A type that describes a source of input for the Forth interpreter
	///
	/// This is essentially a sequence of characters with some extra information
	/// that can be used for diagnostic purposes.
	struct InputSource
	{

		/// Create a new input source
		/// - Parameters:
		///   - sequence: The iterator that gives us a source of characters
		///   - sourceId: The source id that Forth uses to identify this inout
		///               source. -1 = evaluate, 0 = stdin, anything else is a file
		///   - name: Namer of the inputr source for debug purposes
		/// - Throws: if getting the first character throws an error
		public init<S: LocatableIterable>(_ sequence: S, sourceId: Cell, name: String? = nil) throws
		{
			self.sourceId = sourceId
			var iterator = sequence.makeInputSourceIterator()
			self._next = { try iterator.next() }
			self._saveLocation = { iterator.saveLocation() }
			self._restoreLocation = { iterator.restore(location: $0) }
			self.dataSpaceAddress = iterator.dataSpaceAddress
			self.blockNumber = iterator.blockNumber
			self.name = name
			// Preload the first character
			nextChar = try iterator.next()
		}

		private var _next: () throws -> Character?
		private var nextChar: Character?
		private var _saveLocation: () -> [Cell]
		private var _restoreLocation: ([Cell]) -> Bool

		let sourceId: Cell

		/// Tet if we are at the end of the input
		public var atEnd: Bool { nextChar == nil }
		
		/// Address in dataspace of input source
		///
		/// This will be `nil` for most input sources but will be a
		/// data space address for `evaluate` input sources
		public let dataSpaceAddress: Cell?

		/// The block number of the source
		///
		/// This will be zero if the source is not a block buffer
		public let blockNumber: Cell

		private mutating func getNextChar() throws
		{
			guard !atEnd else { return  }
			nextChar = try _next()
		}

		/// A function that mirrors `IteratorProtocol.next()`
		///
		/// This function also updates the stats about the input source i.e.
		/// `lineNumber` and `columnNumber`
		/// - Todo: Allow for non Unix line endings.
		/// - Returns: The next character unless we have reached the end of the
		///            sequence.
		/// - Throws: if getting the character raises an error
		public mutating func next() throws -> Character?
		{
			guard !atEnd else { return nil }
			guard let ret = nextChar else { return nil }
			if ret == "\n"
			{
				lineEndingsRead += 1
				charsSinceLastLineEnding = 0
			}
			else
			{
				charsSinceLastLineEnding += 1
			}
			try getNextChar()
			return ret
		}

		/// Read a line of text from the input source
		///
		/// - Todo: if the last line has no line ending, ``lineEndingsRead``
		///         will be effectively off by one at the end of the file.
		/// - Returns: The next line of text
		/// - Throws: if reading the line causes an error
		public mutating func readLine() throws -> String?
		{
			guard !atEnd else { return nil }
			var ret = ""
			while let aChar = try next(), aChar != "\n"
			{
				ret.append(aChar)
			}
			return ret
		}
		
		/// Saves information with respect to our location in this input source
		///
		/// The array returned will look like this:
		///
		/// - `0          ` : line endings read
		/// - `1          ` : chars since last line ending
		/// - `2          ` : n = nextChar count of unicode code points
		/// - `3 ..< (n+3)` : `nextChar` (-1 => `nil`)
		/// - `n+3...     ` : iterator location
		///
		/// - Returns: An array of cells that describe the state of the input source
		///            or an empty array if the state could not be saved
		mutating func saveLocation() -> [Cell]
		{
			let nextCharEncoding: [Cell]
			if let nextChar
			{
				nextCharEncoding = nextChar.unicodeScalars.map{ Int($0.value) }
			}
			else
			{
				nextCharEncoding = [-1]
			}
			let iteratorLocation = _saveLocation()
			guard !iteratorLocation.isEmpty else { return [] }
			return [lineEndingsRead, charsSinceLastLineEnding, nextCharEncoding.count]
					+ nextCharEncoding
					+ iteratorLocation
		}

		private struct Err: Swift.Error {}

		/// Restore this input source's location
		/// - Parameter location: Location to be restored encoded as am
		///                       array of cells
		/// - Returns: the associated buffer with the save or `nil` if we could
		///            not restore
		mutating func restore<C: Collection>(location: C) -> Bool
		where C.Element == Forth.Cell
		{
			guard location.count >= 5 else { return false }
			var index = location.startIndex
			// Get logical line positional information
			let lineEndings = location[index]
			index = location.index(after: index)
			let charsInLine = location[index]
			index = location.index(after: index)
			// Get the restored value of nextChar
			let unicodeScalarCount = location[index]
			index = location.index(after: index)
			let newNextChar: Character?
			let charEndIndex = location.index(index, offsetBy: unicodeScalarCount)
			let nextCharInts = location[index ..< charEndIndex]
			if nextCharInts.first! == -1
			{
				newNextChar = nil
			}
			else
			{
				// Convert the `Int`s to a character. We swallow any errors and
				// just return false if anything goes wrong
				do
				{
					let unicodeScalars = try nextCharInts.map
					{
						guard let scalar = Unicode.Scalar($0) else { throw Err() }
						return scalar
					}
					let view = String.UnicodeScalarView(unicodeScalars)
					let string = String(view)
					newNextChar = string.first
				}
				catch
				{
					return false
				}
			}
			index = charEndIndex

			// Now we have all the bits, attempt to relocate the iterator
			guard _restoreLocation(Array(location[index...])) else { return false }
			// Everything worked, restore our state
			self.lineEndingsRead = lineEndings
			self.charsSinceLastLineEnding = charsInLine
			self.nextChar = newNextChar
			return true
		}

		/// The number of line endings read from the source
		///
		public private(set) var lineEndingsRead: Int = 0

		/// The number of characters read since the last line ending
		///
		/// By "character" we mean a Swift `Character`.
		public private(set) var charsSinceLastLineEnding: Int = 0

		/// The name of the input source
		///
		/// If the source is something that has a name e.g. a file, this will
		/// be it.
		public private(set) var name: String?
	}
}

extension String: LocatableIterable
{
	public struct InputSourceIterator: LocatableIterator
	{
		fileprivate init(_ string: String)
		{
			self.string = string
			self.index = string.startIndex
		}
		private let string: String
		private var index: String.Index
		private var savedIndexes: [String.Index] = []

		public mutating func next() -> Character?
		{
			guard index != string.endIndex else { return nil }
			defer { index = string.index(after: index) }
			return string[index]
		}

		public mutating func saveLocation() -> [Forth.Cell] 
		{
			let ret = savedIndexes.count
			savedIndexes.append(index)
			return [ret]
		}

		public mutating func restore(location: [Forth.Cell]) -> Bool 
		{
			guard let locationIndex = location.first, savedIndexes.indices.contains(locationIndex)
			else { return false }
			index = savedIndexes[locationIndex]
			return true
		}

		public var isRelocatable: Bool { true }
	}

	public func makeInputSourceIterator() -> InputSourceIterator
	{
		return InputSourceIterator(self)
	}
}

extension Array: LocatableIterable where Element == Character
{
	public struct InputSourceIterator: LocatableIterator
	{
		fileprivate init(_ array: [Character])
		{
			self.array = array
			self.index = 0
		}
		private let array: [Character]
		private var index: Int

		public mutating func next() -> Character?
		{
			guard index != array.endIndex else { return nil }
			defer { index += 1 }
			return array[index]
		}

		public mutating func saveLocation() -> [Forth.Cell]
		{
			return [index]
		}

		public mutating func restore(location: [Forth.Cell]) -> Bool
		{
			guard let locationIndex = location.first, array.indices.contains(locationIndex)
			else { return false }
			index = locationIndex
			return true
		}

		public var isRelocatable: Bool { true }

	}

	public func makeInputSourceIterator() -> InputSourceIterator
	{
		return InputSourceIterator(self)
	}
}

extension ThreadedForth
{
	struct DataSpaceSlice: LocatableIterable
	{
		let machine: ThreadedForth
		let address: Cell
		let byteCount: Cell
		let blockNumber: Cell

		func makeInputSourceIterator() -> DataSpaceIterator
		{
			DataSpaceIterator(machine: machine, address: address, byteCount: byteCount, blockNumber: blockNumber)
		}
	}

	func dataSpaceSlice(address: Cell, byteCount: Cell, blockNumber: Cell) -> DataSpaceSlice
	{
		DataSpaceSlice(machine: self, address: address, byteCount: byteCount, blockNumber: blockNumber)
	}

	public struct DataSpaceIterator: LocatableIterator
	{
		private unowned let machine: ThreadedForth
		private let address: Cell
		private let byteCount: Cell
		private var index: Cell

		public init(machine: ThreadedForth, address: Forth.Cell, byteCount: Forth.Cell, blockNumber: Forth.Cell)
		{
			self.machine = machine
			self.address = address
			self.byteCount = byteCount
			self.blockNumber = blockNumber
			index = 0
		}

		public mutating func next() throws -> Character?
		{
			try Character.makeFromBytes
			{
				() -> UInt8? in
				guard (0 ... byteCount).contains(index) else { throw Forth.Error.addressOutOfBounds }
				guard index != byteCount else { return nil }
				defer { index += 1 }
				return try machine.byte(at: address &+ index)
			}
		}
		
		public let isRelocatable = true

		public mutating func saveLocation() -> [Forth.Cell] 
		{
			[index]
		}
		
		public mutating func restore(location: [Forth.Cell]) -> Bool 
		{
			guard let index = location.first else { return false }
			self.index = index
			return true
		}

		public var dataSpaceAddress: Forth.Cell? { address }
		public let blockNumber: Forth.Cell
	}
}

extension FileDescriptor
{

	/// Read some bytes and return them in a contiguous array
	/// - Parameter maxCount: Maximum number of bytes to read
	/// - Returns: A contiguous array containing the bytes read. Will be empty if
	///            end of file was reached
	/// - Throws: If there is an IO error while reading the bytes
	func readBytes(maxCount: Int) throws -> ContiguousArray<UInt8>
	{
		let buffer = try ContiguousArray<UInt8>(unsafeUninitializedCapacity: maxCount)
		{
			buffer, initializedCount in
			let rawBuffer = UnsafeMutableRawBufferPointer(buffer)
			try initializedCount = self.read(into: rawBuffer)
		}
		return buffer
	}
}

extension FileDescriptor: LocatableIterable
{
	public struct FileIterator: LocatableIterator
	{
		private let bufferSize = 4096

		private var fileDescriptor: FileDescriptor
		private var buffer: ContiguousArray<UInt8> = []
		private var bufferPosition: ContiguousArray.Index
		private var oldFilePosition: Int64

		fileprivate init(fileDescriptor: FileDescriptor) throws
		{
			self.bufferPosition = buffer.startIndex
			self.fileDescriptor = fileDescriptor
			self.oldFilePosition = try fileDescriptor.seek(offset: 0, from: .current)
		}


		/// Get some bytes from the file into a buffer
		///
		/// - Todo: Need to propagate swallowed errors
		/// - Returns: `true` if more bytes were found, `false`if end of file or error
		private mutating func getMoreBytes() throws -> Bool
		{
			guard bufferPosition == buffer.endIndex else { return true }
			oldFilePosition = try fileDescriptor.seek(offset: 0, from: .current)
			buffer = try fileDescriptor.readBytes(maxCount: bufferSize)
			bufferPosition = buffer.startIndex
			return !buffer.isEmpty
		}

		private mutating func getAByte() throws -> UInt8?
		{
			guard try getMoreBytes() else { return nil }
			let ret = buffer[bufferPosition]
			buffer.formIndex(after: &bufferPosition)
			return ret
		}

		public mutating func next() throws -> Character?
		{
			try Character.makeFromBytes(byteGetter: { try self.getAByte() })
		}
		
		public let isRelocatable = true

		public mutating func saveLocation() -> [Forth.Cell] 
		{
			return [Forth.Cell(oldFilePosition) + bufferPosition]
		}
		
		public mutating func restore(location: [Forth.Cell]) -> Bool 
		{
			do
			{
				try fileDescriptor.seek(offset: Int64(location.first!), from: .start)
				buffer = []
				bufferPosition = buffer.startIndex
				oldFilePosition = 0
				return true
			}
			catch
			{
				log.error("\(error) raised while repositioning a file")
				return false
			}
		}
	}

	public typealias InputSourceIterator = FileIterator
	public func makeInputSourceIterator()  -> FileIterator
	{
		// TODO: We have to add throws to the protocol for some stuff
		return try! FileIterator(fileDescriptor: self)
	}

}

extension Character
{
	static func makeFromBytes(byteGetter: () throws -> UInt8?) throws -> Character?
	{
		var utf8Array: [UInt8] = []

		guard let firstByte = try byteGetter() else { return nil }
		utf8Array.append(firstByte)
		let leadingOneCount = (~firstByte).leadingZeroBitCount
		switch leadingOneCount
		{
		case 0:
			break
		case 1:
			throw Forth.Error.invalidUTF8
		case 2, 3, 4:
			while let aByte = try byteGetter(), utf8Array.count < leadingOneCount
			{
				utf8Array.append(aByte)
			}
		default:
			throw Forth.Error.invalidUTF8
		}
		guard let ret = String(bytes: utf8Array, encoding: .utf8)
		else { throw Forth.Error.invalidUTF8 }
		return ret.first!
	}
}
