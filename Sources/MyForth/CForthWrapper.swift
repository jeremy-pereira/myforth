//
//  CForthWrapper.swift
//  
//
//  Created by Jeremy Pereira on 22/07/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import CForth
import Toolbox

private let log = Logger.getLogger("MyForth.CForthWrapper")

/// A Forth interpreter
public class CForthWrapper: ForthProtocol
{
	/// Data that goes on the data stack
	typealias Cell = Int

	/// The dictionary maps a string representing a word to its execution token
	private var words: [MyForth.Word : MyForth.CompiledWord]
	/// A list of word definitions indexed by execution token.
	private(set) var wordLists: [WordList]

	/// The data stack
	///
	/// This is the main stack on which calculations are done
	private var dataStack = CForth.stackNamed("data")
	/// The return stack
	///
	/// The stack that holds return addresses. Also can be used as a temporary
	/// stack for data.
	private var returnStack = CForth.stackNamed("return")
	/// The control stack
	///
	/// Used for holding information while compilig control structures
	private var controlStack = MyForth.Stack<MyForth.ControlWord>(name: "control flow")
	/// Space for holding non stack based data
	private var dataSpace: MyForth.DataSpace
	/// The top of the data stack is held separately as an optimisation
	fileprivate var dataStackTop: Cell?
	/// The top of the return stack is held separately as an optimisation
	fileprivate var returnStackTop: MyForth.ReturnStackCell?

	/// True if the return stack is logically empty
	///
	/// There is stuff ion the return stackk idf the top is not nil or the
	/// stack itself is not empty.
	private var returnStackIsEmpty: Bool { returnStackTop == nil && returnStack.isEmpty }

	/// Are we in intepretation state or compile state.
	private var isCompiling = false
	/// Set to true when we want to exit to the parser without otherwise
	/// changing the state
	private var shouldExitToParser = false

	/// The last word read from the input
	private var currentWord: MyForth.Word?
	/// Word we are compiling, if we are compiling a word
	private var compilingWordName: MyForth.Word?
	/// Index of the word we are currently compiling in `wordLists`
	private var compilingListIndex = -1
	/// The currently compiling word list
	private var compilingList: WordList
	{
		get { wordLists[compilingListIndex] }
		set { wordLists[compilingListIndex] = newValue }
	}
	/// The number of words this forth machine has executed
	public private(set) var wordsExecuted = 0
	/// Trace execution of words
	///
	/// If this is set, each word that is executed and the data stack state is
	/// logged at debug level.
	public var trace = false

	/// Cell containing the current numeric base
	let baseAddress: Int
	let stateAddress: Int

	private var base: Cell
	{
		try! dataSpace.loadCell(from: baseAddress)
	}

	private var inputParser = InputParser()
	private var output = MyForth.TextOutputStreamWraper.null
	private var outputPicture = ""

	public init(trace: Bool = false)
	{
		self.trace = trace
		do
		{
			dataSpace = MyForth.DataSpace()
			stateAddress = dataSpace.pointer
			dataSpace.allot(MyForth.Cell.bytesPerCell)	// Allocate space for state
			baseAddress = dataSpace.pointer
			dataSpace.allot(MyForth.Cell.bytesPerCell)	// Allocate space for base
			try dataSpace.store(cell: 10, at: baseAddress)	// Start in base 10
			try dataSpace.store(cell: 0, at: stateAddress)	// Start in base interpreting mode
			let mfWordLists: [MyForth.WordList]
			(mfWordLists, words) = MyForth.populatePrimitives()
			wordLists = mfWordLists.map{ WordList(from: $0) }
			// Everything must be initialised at this point since we are about
			// to compile the core words
			var outputString = ""
			try self.interpret(input: predefinedWords, output: &outputString)
			guard case InputParser.State.normal = inputParser.state.state
			else
			{
				fatalError("Predefines left parser in an invalid state: \(inputParser.state.state)")
			}
			log.debug("Output from core definitions: '\(outputString)'")
		}
		catch
		{
			fatalError("Failed to load core, reason: \(error)")
		}
	}

	deinit
	{
		CForth.stackFree(dataStack)
		CForth.stackFree(returnStack)
	}

	private var isDebug = false
	/// Dump the dictionary after each input sequence is interpreted
	///
	/// This only effective if the log is at `debug` level. Set `debug` level as
	/// follows:
	///
	/// ```
	/// Logger.push(level: .debug, forName "MyForth.MyForth")
	/// myForthMachine.dumpDictionary = true
	/// // The Forth you want to execute
	/// myForthMachine.dumpDictionary = false
	/// Logger.popLevel(forName "MyForth.MyForth")
	/// ```
	public var dumpDictionary = false

	/// Interpret a stream of charcters
	///
	/// This is the main entry point into the Forth interpreter. State is
	/// mintained between calls so that multiple separate streams can be fed in
	/// one after the other and appear as one contiguous input stream.
	/// - Parameters:
	///   - input: The stream of characters to interpret
	///   - output: Any output that is generated
	/// - Throws: If an error occurs during interpretation
	public func interpret<IN: Sequence, OUT: TextOutputStream>(input: IN,
																		output: inout OUT) throws
		where IN.Element == Character
	{
		isDebug = log.isDebug

		self.output = MyForth.TextOutputStreamWraper(wrapped: output)
		defer { output = self.output.wrapped as! OUT }

		inputParser.append(buffer: InputParser.Buffer(characters: input,
													  address: MyForth.DataSpace.globalAddress(space: parserSpaceIndex,
																							   spaceAddress: InputParser.bufferAddress)))
		do
		{
			while inputParser.hasCharacters
			{
				while let word = inputParser.next()
				{
					currentWord = word

					if shouldExitToParser
					{
						shouldExitToParser = false
						try resume()
					}
					else
					{
						assert(wordNumber == 0, "Interpreting new word with non zero word number")
						let words = try lookup(word: word)
						if words[0].isImmediate || !isCompiling
						{
							if !isCompiling && words[0].isCompileOnly
							{
								throw MyForth.Error.interpretingACompileOnlyWord("\(description(of: words[0]))")
							}

							wordLists[0] = WordList(name: "interpeter", words, cellsNeeded: 1)
							wordIndex = 0
							try resume()
						}
						else
						{
							compile(words: words)
						}
					}
				}
				if !shouldExitToParser && !returnStackIsEmpty
				{
					// We should only get here when we've finished evaluating a
					// string. We need to carry on with what we were doing
					// before.
					wordLists[0].removeAll()	// Make sure we don't accidentally execute the evaluated word again
					try doExit()				// Return to where we were in `evaluate`
					try resume()				// Go!
				}
			}
		}
		catch
		{
			if log.isDebug
			{
				log.debug("Error: \(error)")
			}
			// If we have thrown an error, unwind the stacks and set the state
			// to "interpreting"
			log.debug("Dumping return stack:")
			flushReturnStackTop()
			while let item = returnStack.pop()
			{
				log.debug("Return stack: " + item.rsDescription)
			}
			inputParser.clear()
			isCompiling = false
			wordNumber = 0
			throw error
		}
		if isDebug && dumpDictionary
		{
			log.debug("Dictionary is: \(words)")
		}
	}

	/// Look up a word in the dictionary and convert it to a sequence of
	/// execution tokens and data.
	///
	/// The word is first looked up to see if it exists in the dictionary and
	/// if it does, we return the execution token in a list of one. If it is
	/// not, we attempt to convert the word to a number using the current
	/// `base` and return a list consisting of a `.pushNext` and the number.
	/// If that doesn't work, we assume the word is undefined.
	/// - Parameter word: Th word as a case insensistive string
	/// - Throws: if the word is not recognised and is not a number
	/// - Returns: a list of execution tokens and data.
	private func lookup(word: MyForth.Word) throws -> [MyForth.CompiledWord]
	{
		let ret: [MyForth.CompiledWord]
		if let compiledWord = words[word]
		{
			ret = [compiledWord]
		}
		else if let number = Int(word.string, radix: base)
		{
			ret = [MyForth.CompiledWord.pushNext, MyForth.CompiledWord(rawValue: number)]
		}
		else
		{
			log.debug("Dictionary: \(words)")
			throw MyForth.Error.unrecognisedWord(word.string)
		}
		return ret
	}
	/// The index in `wordLists` of the currently executing word definition
	var wordNumber: Int = 0
	/// The index within a word definition of the last executed word or primitive
	var wordIndex: Int = 0

	/// Resumes execution of the current state of the Forth machine.
	///
	/// Execution will continue until we need to go back to the interpreter for
	/// another word from the input stream. This can occur for several reasons
	///
	/// - A word from the input stream has been fully executed and we need a new
	///   one from the input stream
	/// - A defining word needs a name which comes from the input stream
	/// - We are evaluating a string as a forth program, the string having been
	///   loaded into the input parser.
	/// - Parameters:
	///   - wordStream: The list of words to interpret
	///   - immediate: true if this was called by amn immediate word while
	///                compiling.
	///   - output: where any output is written to
	/// - Throws: If anything goes wrong
	private func resume() throws
	{
		while (wordNumber != 0 || wordIndex < wordLists[wordNumber].endIndex) && !shouldExitToParser
		{
			let wordDefinition = wordLists[wordNumber][wordIndex]
			wordIndex += 1

			let token2: MyForth.CompiledWord?
			if cellsNeeded(wordDefinition) == 2 && wordIndex != wordLists[wordNumber].endIndex
			{
				token2 = wordLists[wordNumber][wordIndex]
				wordIndex += 1
			}
			else
			{
				token2 = nil
			}
			if trace && isDebug
			{
				let secondPart: String
				if let t2 = token2
				{
					secondPart = " \(t2.rawValue.asTokenString)"
				}
				else
				{
					secondPart = ""
				}
				log.debug("[\(wordNumber), \(wordIndex - (token2 != nil ? 2 : 1))] word: "
							+ wordLists[wordDefinition.index].name + secondPart
							+ " state: \(isCompiling ? "compiling" : "interpreting")")
			}
			if wordDefinition.isPrimitive
			{
				try execute(primitive: wordDefinition.primitive, token2: token2)
			}
			else
			{
				try doCall(newWordNumber: wordDefinition.index)
			}
			// IF we have run off the end, we need to unwind the return stack
			try doExits()

			if trace && isDebug
			{
				log.debug("\tdata stack   : \(stringFromDataStack())")
				if !controlStack.isEmpty
				{
					log.debug("\tcontrol stack: \(stringFrom(stack: controlStack))")
				}
				if returnStackTop != nil || !returnStack.isEmpty
				{
					log.debug("\treturn stack : \(stringFromReturnStack())")
				}
			}
		}
	}

	private func compile(words: [MyForth.CompiledWord])
	{
		compilingList.append(words)
	}

	/// Call a new word that isn't a primitive
	/// - Parameter newWordNumber: The new word to call
	/// - Throws: If the new word is not in the word list.
	private func doCall(newWordNumber: Int) throws
	{
		guard newWordNumber < wordLists.count
		else
		{
			throw MyForth.Error.unrecognisedExecutionToken(newWordNumber.asTokenString)
		}
		if isDebug && trace
		{
			log.debug("calling " + (dumpDefinition(for: wordLists[newWordNumber].name) ?? "???"))
		}
		flushReturnStackTop()
		returnStackTop = MyForth.ReturnStackCell(wordNumber: wordNumber, index: wordIndex)
		wordNumber = newWordNumber
		wordIndex = wordLists[newWordNumber].startIndex
		wordsExecuted += 1
	}
	/// Unwinds the return stack returning from words until we get to one that
	/// hasn't finished executing
	///
	/// We will stop when we reach word number 0 because that means we are back
	/// at the interpreter level
	private func doExits() throws
	{
		while  wordIndex == wordLists[wordNumber].endIndex && wordNumber != 0
		{
			try doExit()
		}
	}

	private func doExit() throws
	{
		let target = try returnStackTop.pop() ?? returnStack.throwingPop()
		wordNumber = target.wordNumber
		wordIndex = target.index
	}

	// MARK: Primitive execution
	private func execute(primitive: MyForth.Primitive, token2: MyForth.CompiledWord?) throws
	{
		var ipAdjustment = 0
		switch primitive
		{
		case .fatal:
			fatalError("Fatal executed")
		case .drop:
			if dataStackTop != nil
			{
				dataStackTop = nil
			}
			else
			{
				_ = try dataStack.throwingPop()
			}
		// MARK: Input
		case .accept:
			// TODO: accept needs implementing
			throw MyForth.Error.primitiveNotImplemented(primitive.rawValue)
		// MARK: Output
		case .print:
			let value = try dataStackTop.pop() ?? dataStack.throwingPop()
			let string = String(value, radix: base)
			output.write(string)
		case .printUnsigned:
			let value = try dataStackTop.pop() ?? dataStack.throwingPop()
			let unsignedValue = UInt(bitPattern: value)
			let string = String(unsignedValue, radix: base)
			output.write(string)
			output.write(" ")
		case .emit:
			let value = try dataStackTop.pop() ?? dataStack.throwingPop()
			guard let char = Unicode.Scalar(value)
			else { throw MyForth.Error.invalidUnicode(value) }
			output.write(String(char))
		case .printStack:
			output.write(stringFromDataStack())
		case .initPicture:
			outputPicture = ""
		case .hold:
			let value = try dataStackTop.pop() ?? dataStack.throwingPop()
			guard let unicode = UnicodeScalar(value) else { throw MyForth.Error.invalidUnicode(value) }
			outputPicture.insert(Character(unicode), at: outputPicture.startIndex)
		case .completePicture:
			let utf8Chars = outputPicture.utf8
			let startAddress = dataSpace.pointer
			dataSpace.allot(utf8Chars.count)
			try dataSpace.store(characters: utf8Chars, at: startAddress)
			flushDataStackTop()
			dataStack.push(startAddress)
			dataStackTop = utf8Chars.count
		case .plus:
			let op1 = try dataStackTop.pop() ?? dataStack.throwingPop()
			dataStackTop = try op1 &+ dataStack.throwingPop()
		case .minus:
			let operand = try dataStackTop.pop() ?? dataStack.throwingPop()
			dataStackTop = try dataStack.throwingPop() &- operand
		case .times:
			let operand = try dataStackTop ?? dataStack.throwingPop()
			dataStackTop = try dataStack.throwingPop() &* operand
		case .timesFullWidth:
			let op1 = try dataStackTop ?? dataStack.throwingPop()
			let op2 = try dataStack.throwingPop()
			let dpResult = op2.multipliedFullWidth(by: op1)
			dataStack.push(Int(bitPattern: dpResult.low))
			dataStackTop = dpResult.high
		case .divMod:
			let denominator = try dataStackTop.pop() ?? dataStack.throwingPop()
			guard denominator != 0 else { throw MyForth.Error.divisionByZero }
			let numerator = try dataStack.throwingPop()
			let (q, r) = numerator.quotientAndRemainder(dividingBy: denominator)
			dataStack.push(r)
			dataStackTop = q
		case .udDivMod:
			let divisorHigh = try dataStackTop.pop() ?? dataStack.throwingPop()
			let divisorLow = try dataStack.throwingPop()
			let dividendHigh = try dataStack.throwingPop()
			let dividendLow = try dataStack.throwingPop()

			let divisor = DoubleUInt(low: UInt(bitPattern: divisorLow), high: UInt(bitPattern: divisorHigh))
			let dividend = DoubleUInt(low: UInt(bitPattern: dividendLow), high: UInt(bitPattern: dividendHigh))
			guard divisor != 0 else { throw MyForth.Error.divisionByZero }
			let (q, r) = dividend.quotientAndRemainder(dividingBy: divisor)
			dataStack.push(Cell(bitPattern: r.low))
			dataStack.push(Cell(bitPattern: r.high))
			dataStack.push(Cell(bitPattern: q.low))
			dataStackTop = Cell(bitPattern: q.high)
		case .udTimes:
			let m1High = try dataStackTop.pop() ?? dataStack.throwingPop()
			let m1Low = try dataStack.throwingPop()
			let m2High = try dataStack.throwingPop()
			let m2Low = try dataStack.throwingPop()

			let m1 = DoubleUInt(low: UInt(bitPattern: m1Low), high: UInt(bitPattern: m1High))
			let m2 = DoubleUInt(low: UInt(bitPattern: m2Low), high: UInt(bitPattern: m2High))
			let (result, overflow) = m2.multipliedReportingOverflow(by: m1)
			if overflow
			{
				throw MyForth.Error.integerOverflow
			}
			dataStack.push(Cell(bitPattern: result.low))
			dataStackTop = Cell(bitPattern: result.high)
		case .udPlus:
			let m1High = try dataStackTop.pop() ?? dataStack.throwingPop()
			let m1Low = try dataStack.throwingPop()
			let m2High = try dataStack.throwingPop()
			let m2Low = try dataStack.throwingPop()

			let m1 = DoubleUInt(low: UInt(bitPattern: m1Low), high: UInt(bitPattern: m1High))
			let m2 = DoubleUInt(low: UInt(bitPattern: m2Low), high: UInt(bitPattern: m2High))
			let (result, overflow) = m2.addingReportingOverflow(m1)
			if overflow
			{
				throw MyForth.Error.integerOverflow
			}
			dataStack.push(Cell(bitPattern: result.low))
			dataStackTop = Cell(bitPattern: result.high)
		case .dup:
			if let top = dataStackTop
			{
				dataStack.push(top)
			}
			else
			{
				guard let top = dataStack.top else { throw ToolboxError.stackUnderflow }
				dataStackTop = top
			}
		case .over:
			let s0 = try dataStackTop.pop() ?? dataStack.throwingPop()
			guard let s1 = dataStack.top else { throw ToolboxError.stackUnderflow }
			dataStack.push(s0)
			dataStackTop = s1
		case .swap:
			let s0 = try dataStackTop.pop() ?? dataStack.throwingPop()
			let s1 = try dataStack.throwingPop()
			dataStack.push(s0)
			dataStackTop = s1
		case .rot:
			let s0 = try dataStackTop.pop() ?? dataStack.throwingPop()
			let s1 = try dataStack.throwingPop()
			let s2 = try dataStack.throwingPop()
			dataStack.push(s1)
			dataStack.push(s0)
			dataStackTop = s2
		case .swap2:
			let s0 = try dataStackTop.pop() ?? dataStack.throwingPop()
			let s1 = try dataStack.throwingPop()
			let s2 = try dataStack.throwingPop()
			let s3 = try dataStack.throwingPop()
			dataStack.push(s1)
			dataStack.push(s0)
			dataStack.push(s3)
			dataStackTop = s2
		case .parseWord:
			shouldExitToParser = true
		case .word:
			let delimiter = try dataStackTop.pop() ?? dataStack.throwingPop()
			// TODO: Invalid UTF8 is not really correct here
			guard let delimUnicode = Unicode.Scalar(delimiter) else { throw MyForth.Error.invalidUTF8 }
			let delimiterChar = Character(delimUnicode)
			let parsedString = inputParser.parseWord(delimitedBy: delimiterChar)
			let cAddress = try dataSpace.storeCountedTransient(string: parsedString)
			flushDataStackTop()
			dataStackTop = cAddress
		case .createWordSlot:
			guard let word = currentWord else { throw MyForth.Error.noWordToCreate }
			compilingListIndex = wordLists.count
			wordLists.append(WordList(name: word.string))
			compilingWordName = word
		case .createDictionaryEntry:
			guard let word = compilingWordName else { throw MyForth.Error.noWordToCreate }
			words[word] = MyForth.CompiledWord(rawValue: compilingListIndex)
		case .endDefinition:
			guard controlStack.isEmpty
			else { throw MyForth.Error.unmatchedOrigin }
			isCompiling = false
			try dataSpace.store(cell: 0, at: stateAddress)
		case .char:
			guard let word = currentWord else { throw MyForth.Error.noWordToCreate }
			let firstChar = word.string.utf8.first ?? 0
			flushDataStackTop()
			dataStackTop = Cell(firstChar)
		case .charImmediate:
			guard let word = currentWord else { throw MyForth.Error.noWordToCreate }
			let firstChar = word.string.utf8.first ?? 0
			compilingList.append(MyForth.CompiledWord.pushNext)
			compilingList.append(MyForth.CompiledWord(uint8: firstChar))
		case .string:
			guard let word = currentWord else { throw MyForth.Error.noWordToCreate }
			let startAddress = dataSpace.pointer
			let utf8 = word.string.utf8
			dataSpace.allot(word.string.utf8.count)
			var charIndex = utf8.startIndex
			for address in startAddress ..< dataSpace.pointer
			{
				try dataSpace.store(character: Int(utf8[charIndex]), at: address)
				charIndex = utf8.index(after: charIndex)
			}
			compilingList.append(.pushNext)
			compilingList.append(MyForth.CompiledWord(rawValue: startAddress))
			compilingList.append(.pushNext)
			compilingList.append(MyForth.CompiledWord(rawValue: utf8.count))
		case .quote:
			guard let word = currentWord else { throw MyForth.Error.noWordToCreate }
			guard let token = words[word]?.rawValue
			else
			{
				throw MyForth.Error.unrecognisedWord(word.string)
			}
			flushDataStackTop()
			dataStackTop = token
		case .quoteImmediate:
			guard let word = currentWord else { throw MyForth.Error.noWordToCreate }
			guard let token = words[word]?.rawValue
			else
			{
				throw MyForth.Error.unrecognisedWord(word.string)
			}
			compilingList.append(.pushNext)
			compilingList.append(MyForth.CompiledWord(rawValue: token))
		case .postpone:
			guard let word = currentWord else { throw MyForth.Error.noWordToCreate }
			guard let token = words[word]
				else { throw MyForth.Error.unrecognisedWord(word.string) }
			if token.isImmediate
			{
				if token.isPrimitive
				{
					compilingList.append(token)
				}
				else
				{
					compilingList += wordLists[token.index]
				}
			}
			else
			{
				compilingList.append(.pushNext)
				compilingList.append(token)
				compilingList.append(.toCompile)
			}
		case .branch:
			ipAdjustment = try (dataStackTop.pop() ?? dataStack.throwingPop()) - 1
		case .branchIfNot0:
			let offset = try (dataStackTop.pop() ?? dataStack.throwingPop()) - 1
			if try dataStack.throwingPop() != 0
			{
				ipAdjustment = offset
			}
		case .branchIf0:
			let offset = try (dataStackTop.pop() ?? dataStack.throwingPop()) - 1
			if try dataStack.throwingPop() == 0
			{
				ipAdjustment = offset
			}
		case .lessThan0:
			let st = try dataStackTop.pop() ?? dataStack.throwingPop()
			dataStackTop = st < 0 ? -1 : 0
		case .greaterThan0:
			let st = try dataStackTop.pop() ?? dataStack.throwingPop()
			dataStackTop = st > 0 ? -1 : 0
		case .less:
			let st = try dataStackTop.pop() ?? dataStack.throwingPop()
			dataStackTop = try st > dataStack.throwingPop() ? -1 : 0
		case .greater:
			let st = try dataStackTop.pop() ?? dataStack.throwingPop()
			dataStackTop = try st < dataStack.throwingPop() ? -1 : 0
		case .unsignedLess:
			let st = try UInt(bitPattern: dataStackTop.pop() ?? dataStack.throwingPop())
			dataStackTop = try st > UInt(bitPattern: dataStack.throwingPop()) ? -1 : 0
		case .unsignedGreater:
			let st = try UInt(bitPattern: dataStackTop.pop() ?? dataStack.throwingPop())
			dataStackTop = try st < UInt(bitPattern: dataStack.throwingPop()) ? -1 : 0
		case .equals0:
			let st = try dataStackTop.pop() ?? dataStack.throwingPop()
			dataStackTop = st == 0 ? -1 : 0
		case .invert:
			dataStackTop = try ~(dataStackTop.pop() ?? dataStack.throwingPop())
		case .literal:
			let value = try dataStackTop.pop() ?? dataStack.throwingPop()
			compilingList.append(.pushNext)
			compilingList.append(MyForth.CompiledWord(rawValue: value))
		case .pushNext:
			guard let token2 = token2
			else
			{
				throw MyForth.Error.codeIndexOutOfBounds
			}
			flushDataStackTop()
			dataStackTop = token2.rawValue

		case .immediate:
			guard let word = words[MyForth.Word(compilingList.name)]
				else { throw MyForth.Error.unrecognisedWord(compilingList.name) }
			words[MyForth.Word(compilingList.name)] = word.union(.immediateFlag)
			if isDebug
			{
				log.debug("word \(compilingList.name) is immediate")
			}
		case .compileOnly:
			guard let word = words[MyForth.Word(compilingList.name)]
				else { throw MyForth.Error.unrecognisedWord(compilingList.name) }
			words[MyForth.Word(compilingList.name)] = word.union(.compileOnlyFlag)
			if isDebug
			{
				log.debug("word \(compilingList.name) is compile-only")
			}
		case .beginComment:
			inputParser.set(state: .comment(")"))
		case .beginLineComment:
			inputParser.set(state: .comment("\n"))
		case .endComment:
			break
		case .depth:
			flushDataStackTop()
			dataStackTop = dataStack.count
		case .toR:
			let st = try dataStackTop.pop() ?? dataStack.throwingPop()
			flushReturnStackTop()
			returnStackTop = st
		case .fromR:
			flushDataStackTop()
			dataStackTop = try returnStackTop.pop() ?? returnStack.throwingPop()
		case .copyFromR:
			flushDataStackTop()
			let data = try returnStackTop.pop() ?? returnStack.throwingPop()
			dataStackTop = data
			returnStackTop = data
		case .loopToR:
			let index = try dataStackTop.pop() ?? dataStack.throwingPop()
			let end = try dataStack.throwingPop()
			flushReturnStackTop()
			returnStack.push(end)
			returnStackTop = index
		case .loopFromR:
			let index = try returnStackTop.pop() ?? returnStack.throwingPop()
			let end = try returnStack.throwingPop()
			flushDataStackTop()
			dataStack.push(end)
			dataStackTop = index
		case .and:
			let s0 = try dataStackTop.pop() ?? dataStack.throwingPop()
			dataStackTop = try s0 & dataStack.throwingPop()
		case .or:
			let s0 = try dataStackTop.pop() ?? dataStack.throwingPop()
			dataStackTop = try s0 | dataStack.throwingPop()
		case .xor:
			let s0 = try dataStackTop.pop() ?? dataStack.throwingPop()
			dataStackTop = try s0 ^ dataStack.throwingPop()
		case .rshift:
			let u = try dataStackTop.pop() ?? dataStack.throwingPop()
			let x1 = try UInt(bitPattern: dataStack.throwingPop())
			guard (0 ... Int.bitWidth).contains(u) else { throw MyForth.Error.shiftOutOfRange }
			let x2 = x1 >> u
			dataStackTop = Int(x2)
		case .lshift:
			let u = try dataStackTop.pop() ?? dataStack.throwingPop()
			let x1 = try dataStack.throwingPop()
			guard (0 ... Int.bitWidth).contains(u) else { throw MyForth.Error.shiftOutOfRange }
			dataStackTop = x1 << u
		case .div2:
			let x1 = try dataStackTop.pop() ?? dataStack.throwingPop()
			dataStackTop = x1 >> 1
		case .fmDivMod:
			try flooredMultipleDivide()
		case .smDivRem:
			try symmetricMultipleDivide(DoubleInt.self)
		case .here:
			flushDataStackTop()
			dataStackTop = dataSpace.pointer
		case .allot:
			let count = try dataStackTop.pop() ?? dataStack.throwingPop()
			dataSpace.allot(count)
		case .stackToDataSpace:
			let address = try dataStackTop.pop() ?? dataStack.throwingPop()
			let value = try dataStack.throwingPop()
			try dataSpace.store(cell: value, at: address)
		case .stackToDataSpaceChar:
			let address = try dataStackTop.pop() ?? dataStack.throwingPop()
			let value = try dataStack.throwingPop()
			try dataSpace.store(character: value, at: address)
		case .dataSpaceToStack:
			let address = try dataStackTop.pop() ?? dataStack.throwingPop()
			dataStackTop = try dataSpace.loadCell(from: address)
		case .dataSpaceToStackChar:
			let address = try dataStackTop.pop() ?? dataStack.throwingPop()
			dataStackTop = try dataSpace.loadCharacter(from: address)
		case .aligned:
			let address = try dataStackTop.pop() ?? dataStack.throwingPop()
			dataStackTop = dataSpace.nextAligned(address: address)
		case .cells:
			let count = try dataStackTop.pop() ?? dataStack.throwingPop()
			let bytes = MyForth.Cell.bytesPerCell
			dataStackTop = bytes * count
		case .chars:
			let count = try dataStackTop.pop() ?? dataStack.throwingPop()
			let bytes = MyForth.Cell.bytesPerChar
			dataStackTop = bytes * count
		case .getBody:
			let executionToken = MyForth.CompiledWord(rawValue: try dataStackTop.pop() ?? dataStack.throwingPop())
			guard executionToken.index < wordLists.count else { throw MyForth.Error.codeIndexOutOfBounds }
			guard let dataField = wordLists[executionToken.index].dataField
				else { throw MyForth.Error.noDataFieldForExecutionToken(wordLists[executionToken.index].name ) }
			dataStackTop = dataField
		case .enterInterpretingState:
			isCompiling = false
			try dataSpace.store(cell: 0, at: stateAddress)
		case .enterCompilingState:
			isCompiling = true
			try dataSpace.store(cell: -1, at: stateAddress)
		case .stringMode:
			inputParser.set(state: .string(""))
		case .execute:
			let tokenToExecute = MyForth.CompiledWord(rawValue: try dataStackTop.pop() ?? dataStack.throwingPop())
			guard tokenToExecute.index < wordLists.count
			else
			{
				throw MyForth.Error.unrecognisedExecutionToken(tokenToExecute.rawValue.asTokenString)
			}
			if tokenToExecute.isPrimitive
			{
				try execute(primitive: tokenToExecute.primitive, token2: nil)
			}
			else
			{
				try doCall(newWordNumber: tokenToExecute.index)
			}
		case .startEvaluate:
			let stringCount = try dataStackTop.pop() ?? dataStack.throwingPop()
			let stringAddress = try dataStack.throwingPop()
			let string = try dataSpace.loadString(address: stringAddress, count: stringCount)
			let buffer = InputParser.Buffer(characters: string, address: stringAddress)
			inputParser.push(buffer: buffer)
			// Save the location from which evaluate was called to return to
			// later.
			flushReturnStackTop()
			returnStackTop = MyForth.ReturnStackCell(wordNumber: wordNumber, index: wordIndex)
			// Make it so we exit to the parser to get the next word to
			// interpret
			wordNumber = 0
			wordIndex = wordLists[0].count
		case .popInput:
			guard inputParser.pop()
			else { throw MyForth.Error.noInputToRestore }
		case .find:
			let cAddr = try dataStackTop.pop() ?? dataStack.throwingPop()
			let count = try dataSpace.loadCharacter(from: cAddr)
			let wordString = try dataSpace.loadString(address: cAddr + 1, count: count)
			if let compiledWord = words[MyForth.Word(wordString)]
			{
				dataStack.push(compiledWord.rawValue)
				dataStackTop = compiledWord.isImmediate ? 1 : -1
			}
			else
			{
				dataStack.push(cAddr)
				dataStackTop = 0
			}
		case .toCompile:
			let token = try dataStackTop.pop() ?? dataStack.throwingPop()
			let word = MyForth.CompiledWord(rawValue: token)
			compilingList.append(word)
		case .recurse:
			compilingList.append(MyForth.CompiledWord(rawValue: compilingListIndex))
		case .orig:
			guard compilingList.endIndex > 0
				else { throw MyForth.Error.codeIndexOutOfBounds }
			controlStack.push(.orig(compilingList.endIndex - 1))
		case .resolveOrig:
			try resolveOrig()
		case .dest:
			controlStack.push(.dest(compilingList.wordCount))
		case .resolveDest:
			try resolveDest()
		case .csRoll:
			let itemNumber = try dataStackTop.pop() ?? dataStack.throwingPop()
			try csRoll(item: itemNumber)
		case .csFrame:
			controlStack.createFrame()
		case .csPopFrame:
			try controlStack.popFrame()
		case .csFrameSize:
			flushDataStackTop()
			let frameSize = controlStack.frameOffsetOfTop
			dataStackTop = frameSize
		case .exit:
			try doExit()
		case .throw:
			var mutableSelf = self
			try mutableSelf.throwError()
		case .goto:
			guard let token2 = token2 else { throw MyForth.Error.codeIndexOutOfBounds }
			let index = token2.index
			let offset = token2.gotoOffset
			guard index < wordLists.count else { throw MyForth.Error.codeIndexOutOfBounds }
			wordNumber = index
			wordIndex = offset
		case .does:
			guard compilingList.dataField != nil
				else { throw MyForth.Error.invalidDoes(lastDefined: compilingList.name, caller: wordLists[wordNumber].name) }
			let target = MyForth.CompiledWord(offset: wordIndex, index: wordNumber)
			let gotoIndex = compilingList.count - 2
			compilingList.replace(index: gotoIndex, with: MyForth.CompiledWord.goto)
			compilingList.replace(index: gotoIndex + 1, with: target)
			try doExit()
		case .putBody:
			let newAddress = try dataStackTop.pop() ?? dataStack.throwingPop()
			let xt = try MyForth.CompiledWord(rawValue: dataStack.throwingPop())
			guard xt.index < wordLists.count
				else { throw MyForth.Error.codeIndexOutOfBounds }
			wordLists[xt.index].dataField = newAddress
		case .source:
			flushDataStackTop()
			dataStack.push(inputParser.address)
			dataStackTop = inputParser.pointer - inputParser.address
		case .toIn:
			flushDataStackTop()
			dataStackTop = MyForth.DataSpace.globalAddress(space: parserSpaceIndex,
												           spaceAddress: InputParser.inAddress)
		case .executionXTToStack:
			flushDataStackTop()
			dataStackTop = wordNumber
		case .definingXTToStack:
			flushDataStackTop()
			dataStackTop = compilingListIndex
		case .nop:
			break
		}
		if ipAdjustment != 0
		{
			let newIndex = wordIndex + ipAdjustment
			guard (wordLists[wordNumber].startIndex ... wordLists[wordNumber].endIndex).contains(newIndex)
			else
			{
				throw MyForth.Error.codeIndexOutOfBounds
			}
			wordIndex = newIndex
		}
		wordsExecuted += 1
	}

	private func flushDataStackTop()
	{
		if let st = dataStackTop.pop()
		{
			dataStack.push(st)
		}
	}

	private func flushReturnStackTop()
	{
		if let st = returnStackTop.pop()
		{
			returnStack.push(st)
		}
	}

	/// Perform a floored division on a double precision int
	///
	/// A floored division makes the sign of the remainder the same as the
	/// divisor. That means the quotient rounds towards -infinity, not zero as
	/// is normal for must computer division.
	/// - Throws: if the divisor is zero or if the quotient won't fit into an
	///           `Int`.
	private func flooredMultipleDivide() throws
	{
		let divisor = try dataStackTop.pop() ?? dataStack.throwingPop()
		let dividendHigh = try dataStack.throwingPop()
		let dividendLow = try dataStack.throwingPop()
		let dividend = DoubleInt(high: dividendHigh, low: dividendLow)
		var qr = try dividend.divided(by: divisor)
		// Now adjust the remainder to make it the same sign as the divisor
		// and the quotient to round towards -infinity. Obviously, if the
		// remainder is zero, we can leave it.
		assert(divisor != 0)
		if qr.remainder != 0 && qr.remainder.signum() != divisor.signum()
		{
			qr.remainder += divisor
			qr.quotient -= 1
		}
		dataStackTop = qr.quotient
		dataStack.push(qr.remainder)
	}
	private func symmetricMultipleDivide<N: DoublePrecision>(_ type: N.Type) throws
	{
		let divisor = try dataStackTop.pop() ?? dataStack.throwingPop()
		let dividendHigh = try dataStack.throwingPop()
		let dividendLow = try dataStack.throwingPop()
		let dividend = type.init(high: dividendHigh, low: dividendLow)
		let qr = try dividend.divided(by: type.singlePrecision(cell: divisor))
		dataStackTop = type.cell(singlePrecision: qr.quotient)
		dataStack.push(type.cell(singlePrecision: qr.remainder))
	}

	private func resolveDest() throws
	{
		guard case MyForth.ControlWord.dest(let location)? = controlStack.pop()
			else { throw MyForth.Error.unmatchedDestination }
		compilingList.append(.pushNext)
		let offset = location - (compilingList.wordCount + 1)
		compilingList.append(MyForth.CompiledWord(rawValue: offset))
	}

	private func resolveOrig() throws
	{
		guard let controlWord = controlStack.pop(),
			  case MyForth.ControlWord.orig(let offset) = controlWord
			else { throw MyForth.Error.unmatchedOrigin }
		let currentLoc = compilingList.wordCount
		let branchOffset = currentLoc - offset - 1
		compilingList[offset] = MyForth.CompiledWord(rawValue: branchOffset)
	}

	private func csRoll(item: Int) throws
	{
		var tempStack = MyForth.Stack<MyForth.ControlWord>(name: "temp")

		// Remove all the items above the one we want
		for _ in 0 ..< item
		{
			try tempStack.push(controlStack.throwingPop())
		}
		let newTop = try controlStack.throwingPop()
		// Put all the other items back
		while !tempStack.isEmpty
		{
			controlStack.push(tempStack.pop()!)
		}
		controlStack.push(newTop)
	}

	public func dataStackPop() throws -> Int
	{
		return try dataStackTop.pop() ?? dataStack.throwingPop()
	}

	private func stringFromDataStack() -> String
	{
		stringFrom(cStack: dataStack, top: dataStackTop, stringifyElement: { String($0, radix: base) })
	}

	private func stringFromReturnStack() -> String
	{
		stringFrom(cStack: returnStack, top: returnStackTop, stringifyElement: { $0.rsDescription })
	}


	private func stringFrom<E: CustomStringConvertible>(stack: Stack<E>,
														top: E? = nil,
														stringifyElement: (E) -> String = { $0.description }) -> String
	{
		let countString = "<\(stack.count + (top == nil ? 0 : 1))>"
		var ret = stack.elements.reduce(countString)
		{
			$0 + " " + stringifyElement($1)
		}
		if let st = top
		{
			ret += " " + stringifyElement(st)
		}
		return ret
	}

	/// Dump the definition for a word if it exists
	/// - Parameter wordString: The word whose definition should be found
	/// - Returns: The definition in string form or `nil` if it doesn't exist
	public func dumpDefinition(for wordString: String) -> String?
	{
		let word = MyForth.Word(wordString)
		if let definition = words[word]
		{
			if definition.contains(.primitiveFlag)
			{
				return wordLists[definition.index].name
			}
			else
			{
				let wordList = wordLists[definition.index]
				var ret = wordList.name + ":"
				var i = wordList.makeIterator()
				while let word = i.next()
				{
					ret += " "
					switch word
					{
					case .pushNext:
						if let value = i.next()
						{
							ret += "\(value.rawValue)"
						}
						else
						{
							ret += "<terminated>"
							return ret
						}
					default:
						ret += description(of: word)
					}
				}
				return ret

			}
		}
		else
		{
			return nil
		}
	}

	// MARK: Stuff from outside of main definition

	func description(of compiledWord: MyForth.CompiledWord) -> String
	{
		wordLists[compiledWord.index].name
	}


	func cellsNeeded(_ word: MyForth.CompiledWord) -> Int
	{
		return wordLists[word.index].cellsNeeded
	}

	func stringFrom(cStack stack: CStack,
					top: Int? = nil,
					stringifyElement: (Int) -> String = { $0.description }) -> String
	{
		let countString = "<\(stack.count + (top == nil ? 0 : 1))>"
		var ret = stack.elements.reduce(countString)
		{
			$0 + " " + stringifyElement($1)
		}
		if let st = top
		{
			ret += " " + stringifyElement(st)
		}
		return ret
	}

}

// MARK: A Swift wrapper for the C stack

typealias CStack = OpaquePointer

extension CStack
{
	var isEmpty: Bool { CForth.stackIsEmpty(self) }
	var count: Int { CForth.stackCount(self) }

	var elements: [Int]
	{
		var ret: [Int] = []
		for i in 0 ..< count
		{
			let status = CForth.stackElement(self, i)
			guard status.status == SC_OK else { fatalError("Bounds error on stack") }
			ret.append(status.result)
		}
		return ret
	}

	var top: Int?
	{
		let ret = CForth.stackTop(self)
		guard ret.status == SC_OK else { return nil }
		return ret.result
	}

	func peek(frameOffset: Int) throws -> Int
	{
		let ret = CForth.stackPeek(self, frameOffset)
		guard ret.status == SC_OK else { throw Error.stack(ret.status) }
		return ret.result
	}

	func push(_ anInt: Int)
	{
		CForth.stackPush(self, anInt)
	}

	func pop() -> Int?
	{
		let status = stackPop(self)
		guard status.status == SC_OK else { return nil }
		return status.result
	}

	func throwingPop() throws -> Int
	{
		let status = stackPop(self)
		guard status.status == SC_OK else { throw CStack.Error.stack(status.status) }
		return status.result
	}

	func popFrame() throws
	{
		let status = CForth.stackPopFrame(self)
		guard status == SC_OK else { throw Error.stack(status) }
	}

	enum Error: Swift.Error
	{
		case stack(StatusCode)
	}
}

extension CForthWrapper
{
	/// A list of words for a word definition
	///
	/// Initially it's just an array of `CompiledWord`
	struct WordList
	{
		private(set) var name: String

		private var words: [MyForth.CompiledWord] = []
		/// Data field for `create`d words.
		var dataField: Cell?

		init(from mfWordList: MyForth.WordList)
		{
			self.name = mfWordList.name
			self.words = Array(mfWordList)
			self.dataField = mfWordList.dataField
			self.cellsNeeded = mfWordList.cellsNeeded
		}

		init(name: String, dataField: Cell? = nil, _ words: [MyForth.CompiledWord] = [], cellsNeeded: Int = 1)
		{
			self.name = name
			self.dataField = dataField
			self.words = words
			self.cellsNeeded = cellsNeeded
		}

		/// Append a single word to the compiling list
		/// - Parameter word: The word to append
		mutating func append(_ word: MyForth.CompiledWord)
		{
			words.append(word)
		}

		/// Append an array of words to the compiling list
		/// - Parameter words: The words to append
		mutating func append(_ words: [MyForth.CompiledWord])
		{
			self.words += words
		}

		mutating func replace(index: Int, with newWord: MyForth.CompiledWord)
		{
			words[index] = newWord
		}

		/// Empty all the words in the word definition
		mutating func removeAll()
		{
			self.words.removeAll()
		}

		func delete() { notImplemented() }

		/// How many cells are needed for this wordlist
		///
		/// This is only relevant for primitives. Some of which are two cells
		/// long e.g. `.pushNext`, `.jsr`
		private(set) var cellsNeeded: Int

		var wordCount: Int { words.count }

		static func += (lhs: inout WordList, rhs: WordList)
		{
			lhs.words += rhs.words
		}
	}

}


extension CForthWrapper.WordList: RandomAccessCollection
{
	var startIndex: Int { words.startIndex }
	var endIndex: Int { words.endIndex }

	subscript(i: Int) -> MyForth.CompiledWord
	{
		get { words[i] }
		set { words[i] = newValue }
	}

	func index(after i: Int) -> Int
	{
		words.index(after: i)
	}

	func index(before i: Int) -> Int
	{
		words.index(before: i)
	}

	func index(_ i: Int, offsetBy distance: Int, limitedBy limit: Int) -> Int?
	{
		return words.index(i, offsetBy: distance, limitedBy: limit)
	}

	func index(_ i: Int, offsetBy distance: Int) -> Int
	{
		return words.index(i, offsetBy: distance)
	}
}

// Was in predefinedWords.swift.
//
// Moved for now
extension CForthWrapper
{
/// All the core words that aren't primitives
///
/// These are executed in the init method of `MyForth`. The system base is
/// assumed to be 10.
	internal var predefinedWords: String
	{
		let branchPlaceHolder = "\(MyForth.CompiledWord.pushNext.rawValue) compile, \(Int.min) compile,"
		return MyForth.predefinedWordString(branchPlaceHolder: branchPlaceHolder, baseAddress: baseAddress, stateAddress: stateAddress)
	}
}
