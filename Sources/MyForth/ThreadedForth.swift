//
//  ThreadedForth.swift
//  
//
//  Created by Jeremy Pereira on 18/05/2022.
//
//  Copyright (c) Jeremy Pereira 2022
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Toolbox
import SystemPackage
import Foundation

private let log = Logger.getLogger("MyForth.ThreadedForth")


/// An interpreter for Forth
///
/// This interpreter uses "threaded code" as its execution model.
///
/// There are a number of primitives that, when executed make the Forth machine
/// do something. New words are created by stringing primitives and other
/// words together in an array and then calling each one after the other in
/// sequence, unless a branch is taken.
public final class ThreadedForth: ForthProtocol
{
	public var stdin: AnyIterator<UInt8> = AnyIterator(Array<UInt8>().makeIterator())

	public func dumpDefinition(for word: String) -> String?
	{
		guard let metaData = dictionary[Word(word)]
		else { return nil }
		return "\(word): xt:\(metaData.executionToken), \(metaData.isImmediate ? "immediate" : ""), \(metaData.isCompileOnly ? "compile-only" : "")"
	}

	// MARK: Typaliases to Forth
	public typealias Error = Forth.Error
	typealias ReturnStackCell = Forth.ReturnStackCell
	typealias Stack = Forth.Stack
	typealias DataSpace = Forth.DataSpace
	typealias Cell = Forth.Cell
	typealias Word = Forth.Word
	typealias ControlWord = Forth.ControlWord
	typealias InputSource = Forth.InputSource
	typealias Environment = Forth.Environment

	private var isDebug = false
	/// Turn on for trace mode
	public var trace: Bool
	/// Number of words exectuted
	public private(set) var wordsExecuted: Int = 0

	fileprivate static let dummyThreadCode: ThreadCode = { machine in }
	fileprivate static let handCompiledXT = 2

	/// Where the input parser is in the overall address map
	public let parserSpaceIndex = 1
	/// Where the pad buffer is in the overall address map
	public let padSpaceIndex = 2
	/// Where the block buffers are in the overall address map
	public let blockBufferSpaceIndex = 3
	/// Number of block buffers
	public let blockBuffers = 16
	/// Where we put "transient" data
	public let transientSpaceIndex = 4
	/// A space that provides Forth programs direct access to some `ThreadedForth`
	/// variables
	public let machineSpaceIndex = 5

	fileprivate var blockRefills  = 0

	public init(trace: Bool = false)
	{
		self.trace = trace
		dataSpace = DataSpace()
		inputParser = InputParser(absoluteAddress: DataSpace.globalAddress(space: self.parserSpaceIndex, spaceAddress: 0))
		dataSpace[parserSpaceIndex] = inputParser
		dataSpace[padSpaceIndex] = Forth.DataSpaceBuffer()
		blockCache = Forth.BlockSpace(bufferCount: blockBuffers)
		dataSpace[blockBufferSpaceIndex] = blockCache
		baseAddress = dataSpace.pointer
		try! dataSpace.allot(Cell.bytesPerCell)
		scrAddress = dataSpace.pointer
		try! dataSpace.allot(Cell.bytesPerCell)

		stateAddress = DataSpaceBuffer.SpecialCell.state.absoluteAddress(space: machineSpaceIndex)
		compilingListAddress = DataSpaceBuffer.SpecialCell.compilingList.absoluteAddress(space: machineSpaceIndex)
		traceAddress = DataSpaceBuffer.SpecialCell.trace.absoluteAddress(space: machineSpaceIndex)
		let machineSpace = DataSpaceBuffer(machine: self)
		dataSpace[machineSpaceIndex] = machineSpace
		try! dataSpace.store(cell: 10, at: baseAddress)	// Start in base 10
		try! dataSpace.store(cell: 0, at: stateAddress)	// Start in base interpreting mode

		wordLists.append(CompilingWordList(threadCodes: readLoop, dummyXT: 0))
		wordLists.append(CompilingWordList(threadCodes: interpreter, dummyXT: 1))
		// We want the "hand compiled" dummy execution token to be 2 so we
		// don't need access to the dictionary to find it outside of the context
		// of a Forth machine
		addToDictionary(primitive: "{handCompiled}", threadCode: ThreadedForth.dummyThreadCode)
		assert(dictionary[Forth.Word("{handCompiled}")]!.executionToken == ThreadedForth.handCompiledXT)

		addPrimitivesToDictionary()

		// Everything must be initialised at this point since we are about
		// to compile the core words
		var outputString = ""
		do
		{
			var predefinedSource = try InputSource(ThreadedForth.predefinedWords(), sourceId: 0, name: "predefined words")
			try self.interpret(input: &predefinedSource, output: &outputString)
		}
		catch
		{
			fatalError("Failed to load core, reason: \(error)")
		}
		guard dataStackTop == nil && dataStack.isEmpty
		else { fatalError("Loading core left the data stack not clean") }
		log.info("ThreadedForth: \(wordLists.count) words defined")

		guard case InputParser.State.normal = inputParser.state.state
		else
		{
			fatalError("Predefines left parser in an invalid state: \(inputParser.state.state)")
		}
		guard !isCompiling
		else
		{
			fatalError("Predefined words left us in compile mode, probably forgot a semicolon")
		}
		if log.isDebug
		{
			dumpWordLists(to: &log.debugStream)
		}
	}

	public func dumpWordLists<Stream: TextOutputStream>(to output: inout Stream)
	{
		for xt in 0 ..< wordLists.count
		{
			let metaDesc: String
			if let word = dictionary[xt]
			{
				metaDesc = "\(word.string) \(dictionary[word]!)"
			}
			else
			{
				metaDesc = "not in the dictionary"
			}
			output.write("\(xt) \(metaDesc)\n")
		}
	}

	// MARK: Output

	fileprivate var outputPicture = ""

	// MARK: Input parsing and execution
	typealias ThreadCode = (ThreadedForth) throws -> ()

	/// The index in `wordLists` of the currently executing word definition
	var wordNumber: Int = 0
	/// The index within a word definition of the last executed word or primitive
	var wordIndex: Int = 0

	fileprivate var inputParser: InputParser
	fileprivate var output = Forth.TextOutputStreamWraper.null
	fileprivate var isCompiling = false

	fileprivate var wordLists: [CompilingWordList] = []
	fileprivate var currentWord: Word?
	let stateAddress: Cell

	let baseAddress: Cell

	let traceAddress: Cell

	fileprivate var base: Cell
	{
		get throws
		{ 
			let ret = try dataSpace.loadCell(from: baseAddress)
			guard (2 ... 36).contains(ret) else { throw Forth.Error.invalidBase(ret) }
			return ret
		}
	}

	let scrAddress: Cell


	public func interpret<OUT: TextOutputStream>(input: inout Forth.InputSource,
												 output: inout OUT) throws
	{
		isDebug = log.isDebug

		self.output = Forth.TextOutputStreamWraper(wrapped: output)
		if !inputParser.inputSource.atEnd
		{
			log.warn("About to overwrite non exhausted input source '\(inputParser.inputSource.name ?? "unknown")'")
		}
		self.inputParser.inputSource = input
		defer
		{
			output = self.output.wrapped as! OUT
			input = self.inputParser.inputSource
		}
		try interpret()
	}

	private func interpret() throws
	{
		do
		{
			wordNumber = 0
			wordIndex = 0
			try resume()
		}
		catch
		{
			if log.isDebug
			{
				log.debug("Error: \(error)")
			}
			// If we have thrown an error, unwind the stacks and set the state
			// to "interpreting"
			log.debug("Dumping return stack:")
			flushReturnStackTop()
			while let item = returnStack.pop()
			{
				log.debug("Return stack: " + item.rsDescription)
			}
			inputParser.clear()
			isCompiling = false
			wordNumber = 1
			throw error
		}
	}

	private func resume() throws
	{
		var shouldContinue = !returnStackIsEmpty || (wordNumber == 0 && wordIndex < wordLists[0].endIndex)
		var tracePrefix = "Unknown"
		while shouldContinue
		{
			if trace && isDebug && wordNumber > 1
			{
				tracePrefix = describeRS(cell: ReturnStackCell(wordNumber: wordNumber, index: wordIndex))
				if wordLists[wordNumber].count > wordIndex
				{
					tracePrefix += " is " + describeWord(atXt: wordNumber, index: wordIndex)
				}
			}
			let code = wordLists[wordNumber][wordIndex]
			wordIndex += 1
			try code(self)
			wordsExecuted &+= 1
			try shouldContinue = doExits()
			if trace && isDebug && wordNumber > 1
			{
				logStacks(prefix: "TRACE " + tracePrefix, level: .debug)
			}
		}
	}

	fileprivate func logStacks(prefix: String, level: Logger.Level)
	{
		log.log(prefix, atLevel: level)
		log.log("\tdata stack   : \(stringFrom(stack: dataStack, top: dataStackTop))", atLevel: level)
		if !controlStack.isEmpty
		{
			log.log("\tcontrol stack: \(stringFrom(stack: controlStack))", atLevel: level)
		}
		if returnStackTop != nil || !returnStack.isEmpty
		{
			log.log("\treturn stack : \(stringFromReturnStack())", atLevel: level)
		}
		if let parsing = inputParser.bufferDebug()
		{
			log.log("\tParsing: \(parsing)", atLevel: level)
		}
	}

	private func doExits() throws -> Bool
	{
		// First condition is needed in case we have deleted the current word
		while wordNumber >= wordLists.count || (wordIndex == wordLists[wordNumber].endIndex && !returnStackIsEmpty)
		{
			try doExit()
		}
		return wordIndex < wordLists[wordNumber].endIndex
	}

	fileprivate func doExit() throws
	{
		let target = try returnStackTop.pop() ?? returnStack.throwingPop()
		wordNumber = target.wordNumber
		wordIndex = target.index
	}

	enum InterpretationResult
	{
		case literal(Int)
		case doubleLiteral(Int, Int)
		case interpret(WordMetaData)
	}

	fileprivate func interpretOrCompile(word: Word) throws
	{
		let lookupResult = try lookup(word: word)
		flushDataStackTop()
		switch lookupResult
		{
		case .literal(let int):
			if isCompiling
			{
				compile(triple: tripleForPush(value: int))
				if self.isDebug && self.trace
				{
					let compilingWord = compilingWordName ?? "unknown"
						log.debug("Compiling #{push}(\(int)) \(word) into #\(compilingListIndex) \(compilingWord)")
				}
				dataStackTop = 0
			}
			else
			{
				dataStack.push(int)
				dataStackTop = 0
			}
		case .doubleLiteral(let low, let high):
			if isCompiling
			{
				compile(triple: tripleForPush(value: low))
				compile(triple: tripleForPush(value: high))
				dataStackTop = 0
			}
			else
			{
				dataStack.push(low)
				dataStack.push(high)
				dataStackTop = 0
			}

		case .interpret(let metaData):
			if metaData.isImmediate || !isCompiling
			{
				guard isCompiling || !metaData.isCompileOnly
				else
				{
					let word = dictionary[metaData.executionToken]?.string ?? "#\(metaData.executionToken)"
					throw Error.interpretingACompileOnlyWord(word)
				}
				dataStack.push(metaData.executionToken)
				dataStackTop = -1
			}
			else
			{
				compile(triple: metaData.triple)
				if self.isDebug && self.trace
				{
					let xt = metaData.executionToken
					let word = dictionary.reverse[xt]?.string ?? "unknown"
					let compilingWord = compilingWordName ?? "unknown"
					log.debug("Compiling #\(xt) \(word) into #\(compilingListIndex) \(compilingWord)")
				}
				dataStackTop = 0
			}
		}
	}

	private final func lookup(word: Word) throws -> InterpretationResult
	{
		if let compiledWord = dictionary[word]
		{
			return .interpret(compiledWord)
		}
		let string = word.string
		guard !string.isEmpty else { throw Error.unrecognisedWord("") }
		var charIndex = string.startIndex
		let firstChar = string[charIndex]
		if firstChar == "'"
		{
			guard string.count == 3 && string.last! == "'"
			else { throw Error.unrecognisedWord(string) }
			string.formIndex(after: &charIndex)
			let scalar = string[charIndex].unicodeScalars.first!
			return .literal(Int(scalar.value))
		}
		let wordBase: Int
		switch firstChar
		{
		case "#" :
			wordBase = 10
			charIndex = string.index(after: charIndex)
		case "$" :
			wordBase = 16
			charIndex = string.index(after: charIndex)
		case "%" :
			wordBase = 2
			charIndex = string.index(after: charIndex)
		default:
			wordBase = try base
		}
		let lastIndex = string.index(before: string.endIndex)
		let lastChar = string[lastIndex]
		// We will swallow unrecognised word errors thrown by number parsing
		// and throw our own error using the whole word string
		do
		{
			if lastChar == "."
			{
					return try DoublePrecision<Int>.parseNumber(string: string[charIndex ..< lastIndex], radix: wordBase)
			}
			else
			{
				return try Int.parseNumber(string: string[charIndex...], radix: wordBase)
			}
		}
		catch Forth.Error.unrecognisedWord(_)
		{
			throw Forth.Error.unrecognisedWord(string)
		}
	}

	// MARK: Compiling

	let compilingListAddress: Cell

	fileprivate var compilingListIndex: Cell = -1

	fileprivate var compilingWordName: Word?

	private func compile(triple: CodeTriple)
	{
		wordLists[compilingListIndex].append(triple)
	}

	fileprivate var dictionary = WordDictionary()

	// MARK: Data stack

	fileprivate var dataStackTop: Cell?
	fileprivate var dataStack = Stack<Cell>(name: "data")

	fileprivate func flushDataStackTop()
	{
		if let st = dataStackTop.pop()
		{
			dataStack.push(st)
		}
	}

	public func dataStackPop() throws -> Int
	{
		return try dataStackTop.pop() ?? dataStack.throwingPop()
	}

	// MARK: Return stack

	/// The top of the return stack is held separately as an optimisation
	fileprivate var returnStackTop: ReturnStackCell?
	/// The return stack
	///
	/// The stack that holds return addresses. Also can be used as a temporary
	/// stack for data.
	fileprivate var returnStack = Stack<ReturnStackCell>(name: "return")

	fileprivate func flushReturnStackTop()
	{
		if let st = returnStackTop.pop()
		{
			returnStack.push(st)
		}
	}

	private var returnStackIsEmpty: Bool
	{
		returnStackTop == nil && returnStack.isEmpty
	}

	private func stringFromReturnStack() -> String
	{
		stringFrom(stack: returnStack,
				   top: returnStackTop,
				   stringifyElement: { describeRS(cell: $0) })
	}

	fileprivate func describeRS(cell: ReturnStackCell) -> String
	{
		let number = cell.wordNumber
		let index = cell.index
		let firstPart: String
		if let word = dictionary[number]
		{
			firstPart = word.string
		}
		else
		{
			firstPart = "#" + number.description
		}
		return "[\(firstPart), \(index)]"
	}

	fileprivate func describeWord(atXt xt: Cell, index: Int) -> String
	{
		let xtAndParam = wordLists[xt].executionTokens[index]
		let executingString: String
		if let word = dictionary[xtAndParam.xt]
		{
			executingString = word.string
		}
		else
		{
			executingString = "#\(xtAndParam.xt)"
		}
		let paramString = xtAndParam.param != nil ? "(\(xtAndParam.param!))" : ""
		return "\(executingString)\(paramString)"
	}

	// MARK: Control Stack

	fileprivate var controlStack = Stack<ControlWord>(name: "control flow")

	// MARK: Data Space

	fileprivate var dataSpace: DataSpace

	func byte(at address: Cell) throws -> UInt8
	{
		return try UInt8(dataSpace.loadCharacter(from: address))
	}

	// MARK: Environment

	/// The environment variables
	fileprivate var environment = Environment()

	// MARK: Primitive helpers

	private static func stringFrom<E: CustomStringConvertible>(stack: Stack<E>,
														top: E? = nil,
														stringifyElement: (E) -> String = { $0.description }) -> String
	{
		let countString = "<\(stack.count + (top == nil ? 0 : 1))>"
		var ret = stack.elements.reduce(countString)
		{
			$0 + " " + stringifyElement($1)
		}
		if let st = top
		{
			ret += " " + stringifyElement(st)
		}
		return ret
	}

	fileprivate func doCall(_ newWordNumber: Int) throws
	{
		guard newWordNumber < wordLists.count
		else
		{
			throw Error.unrecognisedExecutionToken(newWordNumber.asTokenString)
		}
		flushReturnStackTop()
		returnStackTop = ReturnStackCell(wordNumber: wordNumber, index: wordIndex)
		wordNumber = newWordNumber
		wordIndex = 0
	}

	// MARK: Primitive bootstrapping

	private func threadCode(_ string: String) -> ThreadCode
	{
		if let metaData = dictionary[Word(string)]
		{
			return metaData.threadCode
		}
		else
		{
			return { machine in throw Error.unrecognisedWord(string) }
		}
	}

	private func xt(_ string: String) -> ThreadCode
	{
		if let metaData = dictionary[Word(string)]
		{
			return push(metaData.executionToken)
		}
		else
		{
			return  { machine in throw Error.unrecognisedWord(string) }
		}
	}

	fileprivate func symmetricMultipleDivide<N: CellConvertible>(_ type: N.Type) throws
	where N.Magnitude: CellConvertible
	{
		let divisor = try dataStackTop.pop() ?? dataStack.throwingPop()
		let dividend: DoublePrecision<N> = try popDouble()
		let qr = try dividend.divided(by: N.from(cell: divisor))
		dataStackTop = qr.quotient.asCell
		dataStack.push(qr.remainder.asCell)
	}

	// swiftlint:disable comma

	private func addPrimitivesToDictionary()
	{
		addToDictionary(primitive: "!", threadCode: sToDS)
		addToDictionary(primitive: "*", threadCode: times)
		addToDictionary(primitive: "+", threadCode: plus)
		addToDictionary(primitive: "-", threadCode: minus)
		addToDictionary(primitive: ".s", threadCode: printStack)
		addToDictionary(primitive: "/mod", threadCode: divMod)
		addToDictionary(primitive: ";", threadCode: endDefinition, isImmediate: true, isCompileOnly: true)
		addToDictionary(primitive: "=", threadCode: equals)
		addToDictionary(primitive: "<", threadCode: less)
		addToDictionary(primitive: "<#", threadCode: initPicture)
		addToDictionary(primitive: ">", threadCode: greater)
		addToDictionary(primitive: ">body", threadCode: body)
		addToDictionary(primitive: ">in", threadCode: push(ThreadedForth.DataSpace.globalAddress(space: parserSpaceIndex,
																								 spaceAddress: InputParser.inAddress)))
		addToDictionary(primitive: ">r", threadCode: sToRS)
		addToDictionary(primitive: "0<>branch?", threadCode: ne0Branch)
		addToDictionary(primitive: "0=branch?", threadCode: eq0Branch)
		addToDictionary(primitive: "2swap", threadCode: swap2)
		addToDictionary(primitive: "@", threadCode: dsToS)
		addToDictionary(primitive: "accept", threadCode: accept)
		addToDictionary(primitive: "aligned", threadCode: aligned)
		addToDictionary(primitive: "allot", threadCode: allot)
		addToDictionary(primitive: "and", threadCode: and)
		addToDictionary(primitive: "base", threadCode: push(baseAddress))
		addToDictionary(primitive: "blk", threadCode: push(inputParser.blkAddress))
		addToDictionary(primitive: "branch", threadCode: branch)
		addToDictionary(primitive: "c!", threadCode: sToDSChar)
		addToDictionary(primitive: "c@", threadCode: dsToSChar)
		addToDictionary(primitive: "chars", threadCode: chars)
		addToDictionary(primitive: "cells", threadCode: cells)
		addToDictionary(primitive: "compile,", threadCode: xtToCompiling)
		addToDictionary(primitive: "compile-only", threadCode: compileOnly)
		addToDictionary(primitive: "cs-roll", threadCode: csRoll)
		addToDictionary(primitive: "depth"  , threadCode: depth)
		addToDictionary(primitive: "does>"  , threadCode: does)
		addToDictionary(primitive: "drop"   , threadCode: drop)
		addToDictionary(primitive: "dup"    , threadCode: dup)
		addToDictionary(primitive: "emit", threadCode: emit)
		addToDictionary(primitive: "environment?", threadCode: environmentQuery)
		addToDictionary(primitive: "execute", threadCode: execute)
		addToDictionary(primitive: "exit", threadCode: exit)
		addToDictionary(primitive: "find", threadCode: find)
		addToDictionary(primitive: "fm/mod", threadCode: fmDivMod)
		addToDictionary(primitive: "here", threadCode: here)
		addToDictionary(primitive: "hold", threadCode: hold)
		addToDictionary(primitive: "immediate", threadCode: immediate)
		addToDictionary(primitive: "invert", threadCode: invert)
		addToDictionary(primitive: "lshift", threadCode: lshift)
		addToDictionary(primitive: "m*", threadCode: timesFullWidth)
		addToDictionary(primitive: "or", threadCode: or)
		addToDictionary(primitive: "over", threadCode: over)
		addToDictionary(primitive: "r>", threadCode: rsToS)
		addToDictionary(primitive: "r@", threadCode: rsCopyToS)
		addToDictionary(primitive: "recurse", threadCode: recurse, isImmediate: true, isCompileOnly: true)
		addToDictionary(primitive: "state", threadCode: push(stateAddress))
		addToDictionary(primitive: "scr"  , threadCode: push(scrAddress))
		addToDictionary(primitive: "rot", threadCode: rot)
		addToDictionary(primitive: "swap", threadCode: swap)
		addToDictionary(primitive: "sm/rem", threadCode: smDivRem)
		addToDictionary(primitive: "source", threadCode: source)
		// TODO: Define source-id to use a cell so we can save it on a stack
		addToDictionary(primitive: "source-id", threadCode: sourceId)
		addToDictionary(primitive: "u<" , threadCode: unsignedLess)
		addToDictionary(primitive: "u>" , threadCode: unsignedGreater)
		addToDictionary(primitive: "xor", threadCode: xor)

		// dummy is used in cases where we have a thread code, but no xt
		// e.g. call and push, the real threadcode is generated on the fly
		// and thread codes in hand compiled primitives

		addToDictionary(primitive: "{beginComment}"         , threadCode: beginComment)
		addToDictionary(primitive: "{call}"                 , threadCode: ThreadedForth.dummyThreadCode)
		addToDictionary(primitive: "{completePicture}"      , threadCode: completePicture)
		addToDictionary(primitive: "{compilingListAddress}" , threadCode: push(compilingListAddress))
		addToDictionary(primitive: "{createDictionaryEntry}", threadCode: createDictionaryEntry)
		addToDictionary(primitive: "{createNamedWordSlot}"  , threadCode: createNamedWordSlot)
		addToDictionary(primitive: "{createUnnamedWordSlot}", threadCode: createUnnamedWordSlot)
		addToDictionary(primitive: "{csFrame}"        , threadCode: csFrame)
		addToDictionary(primitive: "{csFrameSize}"    , threadCode: csFrameSize)
		addToDictionary(primitive: "{csPopFrame}"     , threadCode: csPopFrame)
		addToDictionary(primitive: "{cwFunc}"            , threadCode: cwFunc)
		addToDictionary(primitive: "{dest}"              , threadCode: dest)
		addToDictionary(primitive: "{displayCommentMode}", threadCode: displayCommentMode)
		addToDictionary(primitive: "{endDefinition}"  , threadCode: endDefinition)
		addToDictionary(primitive: "{inputClear}"     , threadCode: inputClear)
		addToDictionary(primitive: "{inputPop}"       , threadCode: inputPop)
		addToDictionary(primitive: "{orig}"           , threadCode: orig)

		addToDictionary(primitive: "{markCreated}"    , threadCode: markCreated)
		addToDictionary(primitive: "{noOp}"           , threadCode: noOp)
		addToDictionary(primitive: "{parseWord}"      , threadCode: parseWord)
		addToDictionary(primitive: "{push}"           , threadCode: ThreadedForth.dummyThreadCode)
		addToDictionary(primitive: "{resolveDest}"    , threadCode: resolveDest)
		addToDictionary(primitive: "{resolveOrig}"    , threadCode: resolveOrig)
		addToDictionary(primitive: "{rshift}"         , threadCode: rshift)
		addToDictionary(primitive: "{stackDataEmpty}"  , threadCode: dsEmpty)
		addToDictionary(primitive: "{stackReturnEmpty}", threadCode: rsEmpty)
		addToDictionary(primitive: "{sToCompiling}"    , threadCode: sToCompiling)
		addToDictionary(primitive: "{stringInputPush}" , threadCode: stringInputPush)
		addToDictionary(primitive: "{stringInputReplace}", threadCode: stringInputReplace)
		addToDictionary(primitive: "{stringMode}"     , threadCode: stringMode)
		addToDictionary(primitive: "{throw}"          , threadCode: `throw`)
		addToDictionary(primitive: "{traceAddress}"   , threadCode: push(traceAddress))
		// Core extensions truncateDictionaryAtXT
		addToDictionary(primitive: ".r"               , threadCode: printRight)
		addToDictionary(primitive: "pad"              , threadCode: push(DataSpace.globalAddress(space: padSpaceIndex, spaceAddress: 0)))
		addToDictionary(primitive: "u.r"              , threadCode: printUnsignedRight)
		addToDictionary(primitive: "{truncateDictionaryAtXT}", threadCode: truncateDictionaryAtXT)
		addToDictionary(primitive: "{universal-parse}", threadCode: universalParse)
		addToDictionary(primitive: "{sStringToCountedTransient}", threadCode: sStringToCountedTransient)
		// File functions
		addToDictionary(primitive: "close-file"     , threadCode: closeFile)
		addToDictionary(primitive: "create-file"    , threadCode: createFile)
		addToDictionary(primitive: "delete-file"    , threadCode: deleteFile)
		addToDictionary(primitive: "file-position"  , threadCode: filePosition)
		addToDictionary(primitive: "file-size"      , threadCode: fileSize)
		addToDictionary(primitive: "file-status"    , threadCode: fileStatus)
		addToDictionary(primitive: "included"       , threadCode: includeFile)
		addToDictionary(primitive: "open-file"      , threadCode: openFile)
		addToDictionary(primitive: "r/o"            , threadCode: push(Cell(FileDescriptor.AccessMode.readOnly.rawValue)))
		addToDictionary(primitive: "r/w"            , threadCode: push(Cell(FileDescriptor.AccessMode.readWrite.rawValue)))
		addToDictionary(primitive: "read-file"      , threadCode: readFile)
		addToDictionary(primitive: "read-line"      , threadCode: readLine)
		addToDictionary(primitive: "rename-file"    , threadCode: renameFile)
		addToDictionary(primitive: "reposition-file", threadCode: repositionFile)
		addToDictionary(primitive: "resize-file"    , threadCode: resizeFile)
		addToDictionary(primitive: "restore-input"  , threadCode: restoreInput)
		addToDictionary(primitive: "save-input"     , threadCode: saveInput)
		addToDictionary(primitive: "w/o"            , threadCode: push(Cell(FileDescriptor.AccessMode.writeOnly.rawValue)))
		addToDictionary(primitive: "write-file"     , threadCode: writeFile)
		addToDictionary(primitive: "write-line"     , threadCode: writeLine)
		addToDictionary(primitive: "{includeFile}"  , threadCode: includeFile)
		addToDictionary(primitive: "{isIncluded}"   , threadCode: isIncluded)
		// Block functions
		addToDictionary(primitive: "empty-buffers"  , threadCode: emptyBuffers)
		addToDictionary(primitive: "save-buffers"   , threadCode: saveBuffers)
		addToDictionary(primitive: "update"   		, threadCode: update)
		addToDictionary(primitive: "{cacheBlock}"   , threadCode: cacheBlock)
		addToDictionary(primitive: "{listBlock}"    , threadCode: listBlock)
		addToDictionary(primitive: "{refill}"       , threadCode: refill)
		// Double precision functions
		addToDictionary(primitive: "d+"      , threadCode: udPlus)
		addToDictionary(primitive: "d-"      , threadCode: udMinus)
		addToDictionary(primitive: "d.r"     , threadCode: printDoubleRight)
		addToDictionary(primitive: "d<"      , threadCode: dLess)
		addToDictionary(primitive: "d<<"     , threadCode: dlshift)
		addToDictionary(primitive: "d="      , threadCode: dEquals)
		addToDictionary(primitive: "d>>"     , threadCode: drshift)
		addToDictionary(primitive: "dnegate" , threadCode: dnegate)
		addToDictionary(primitive: "du<"     , threadCode: duLess)
		addToDictionary(primitive: "du*"     , threadCode: duTimes)
		addToDictionary(primitive: "du/mod"  , threadCode: duDivMod)
		addToDictionary(primitive: "m*/"     , threadCode: dTimesDiv)

		log.info("ThreadedForth: \(wordLists.count) primitives defined")

		addToDictionary(word: ":",
						code: parseWord, createNamedWordSlot, push(-1), push(stateAddress), sToDS)

		log.info("ThreadedForth: \(wordLists.count) primitives and hand coded words defined")
	}

	// swiftlint:enable comma


	private func addToDictionary(primitive: String, threadCode: @escaping ThreadCode, isImmediate: Bool = false, isCompileOnly: Bool = false)
	{
		let xt = wordLists.count
		let word = Word(primitive)
		let metaData = WordMetaData(xt, threadCode: threadCode, isImmediate: isImmediate, isCompileOnly: isCompileOnly, previous: dictionary[word])
		dictionary[word] = metaData
		var compiledWords = CompilingWordList()
		compiledWords.append((threadCode, xt, nil))
		wordLists.append(compiledWords)
	}

	private func addToDictionary(word: String, code: ThreadCode..., isImmediate: Bool = false, isCompileOnly: Bool = false)
	{
		let actualWord = Word(word)
		let xt = wordLists.count
		let threadCode = call(xt)
		let metaData = WordMetaData(xt, threadCode: threadCode, isImmediate: isImmediate, isCompileOnly: isCompileOnly, previous: dictionary[actualWord])
		dictionary[actualWord] = metaData
		var compilingList = CompilingWordList()
		for element in code
		{
			compilingList.append(primitiveTriple(threadCode: element))
		}
		wordLists.append(compilingList)
	}

	// MARK: Tracking File Descriptor Names
	private var descriptorNames: [CInt : String] = [:]

	fileprivate func nameFor(descriptor: FileDescriptor) -> String?
	{
		return descriptorNames[descriptor.rawValue]
	}

	fileprivate func add(name: String, forDescriptor fd: FileDescriptor)
	{
		descriptorNames[fd.rawValue] = name
	}

	fileprivate func removeNameFor(descriptor: FileDescriptor)
	{
		descriptorNames[descriptor.rawValue] = nil
	}

	// MARK: Block handling

	private(set) fileprivate var blockCache: Forth.BlockSpace

	/// Cache a block
	///
	/// Caches the block with the given number in the block cache. Optionally
	/// will pull the block from a file if not already cached.
	/// - Parameters:
	///   - block: The block number to cache
	///   - fromFile: If `true` will read the block from the block file if it is
	///               not already cached
	/// - Returns: The address in the dataspace of the start of the block's buffer.
	fileprivate func cache(block: Cell, fromFile: Bool) throws -> Cell
	{
		let localAddress = try blockCache.cache(block: block, fromFile: fromFile)
		return DataSpace.globalAddress(space: blockBufferSpaceIndex, spaceAddress: localAddress)
	}
}

// MARK: ThreadedForth as a dataspace buffer

fileprivate extension ThreadedForth
{
	struct DataSpaceBuffer: DataBuffer
	{
		enum SpecialCell: Cell
		{
			case state
			case compilingList
			case trace

			fileprivate static func from(address: Cell) throws -> SpecialCell
			{
				guard address % Cell.bytesPerCell == 0 else { throw Error.alignment }
				guard let ret = SpecialCell(rawValue: address / Cell.bytesPerCell)
				else { throw Error.addressOutOfBounds }
				return ret
			}

			fileprivate func absoluteAddress(space: Int) -> Cell
			{
				DataSpace.globalAddress(space: space,
										spaceAddress: rawValue * Cell.bytesPerCell)
			}
		}

		func loadCell(from address: Forth.Cell) throws -> Forth.Cell
		{
			let variable = try SpecialCell.from(address: address)
			switch variable
			{
			case .state:
				return machine.isCompiling ? -1 : 0
			case .compilingList:
				return machine.compilingListIndex
			case .trace:
				return machine.trace ? -1 : 0
			}
		}
		
		mutating func store(cell: Forth.Cell, at address: Forth.Cell) throws 
		{
			let variable = try SpecialCell.from(address: address)
			switch variable
			{
			case .state:
				machine.isCompiling = cell != 0
			case .compilingList:
				machine.compilingListIndex = cell
			case .trace:
				machine.trace = cell != 0
			}
		}
		
		mutating func store(character: Forth.Cell, at address: Forth.Cell) throws 
		{
			throw Forth.notImplementedError()
		}
		
		mutating func store<S>(characters: S, at address: Forth.Cell) throws where S : Collection, S.Element == UInt8 
		{
			throw Forth.notImplementedError()
		}
		
		func loadCharacter(from address: Forth.Cell) throws -> Forth.Cell 
		{
			throw Forth.notImplementedError()
		}
		
		func loadBytes(address: Forth.Cell, count: Forth.Cell) throws -> [UInt8] 
		{
			throw Forth.notImplementedError()
		}
		
		private unowned let machine: ThreadedForth

		init(machine: ThreadedForth)
		{
			self.machine = machine
		}
	}
}

private typealias ThreadCode = ThreadedForth.ThreadCode

// MARK: Hand compiled words

private let readLoop =
[
	push(2), branch, call(1), refill, push(-3), ne0Branch
]

private let interpreter =
[
	push(5), branch,
	interpretWord,
	push(2), eq0Branch, execute,
	wordForInterpreter, push(1), plus, dup, push(1), minus, dsToSChar,
	dup,
	push(-13), ne0Branch,
	drop, drop
]

// MARK: word handling stuff

extension ThreadedForth
{
	final class WordMetaData
	{
		//let name: String
		let executionToken: Int
		var isImmediate: Bool
		var isCompileOnly: Bool
		private(set) var isCreated: Bool = false
		private(set) var dataField: Cell = -1
		let threadCode: ThreadCode
		let previousDefinition: WordMetaData?

		init(_ executionToken: Int, threadCode: @escaping ThreadCode, isImmediate: Bool = false, isCompileOnly: Bool = false, previous: WordMetaData? = nil)
		{
			self.executionToken = executionToken
			self.isImmediate = isImmediate
			self.isCompileOnly = isCompileOnly
			self.threadCode = threadCode
			self.previousDefinition = previous
		}

		func markCreated(dataField: Cell)
		{
			isCreated = true
			self.dataField = dataField
		}

		var triple: CodeTriple { (threadCode, executionToken, nil) }
	}

	fileprivate struct WordDictionary
	{
		var forward: [Word : WordMetaData] = [:]
		var reverse: [ Cell : Word] = [:]

		subscript(word: Word) -> WordMetaData?
		{
			get { forward[word] }
			set
			{
				if let newValue
				{
					forward[word] = newValue
					reverse[newValue.executionToken] = word
				}
				else if let xt = forward[word]?.executionToken
				{
					forward[word] = nil
					reverse[xt] = nil
				}
			}
		}
		subscript(xt: Cell) -> Word? { reverse[xt] }
	}

	typealias CodeTriple = (threadCode: ThreadCode, xt: Cell, parameter: Cell?)

	fileprivate struct CompilingWordList: Collection
	{
		var compiledWords: [ThreadCode] = []
		var executionTokens: [(xt: Cell, param: Cell?)] = []

		var count: Int { compiledWords.count }
		var startIndex: Int { compiledWords.startIndex }
		var endIndex: Int { compiledWords.endIndex }
		subscript(position: Int) -> ThreadCode
		{
			compiledWords[position]
		}

		func index(after i: Int) -> Int { i + 1	}

		mutating func append(_ code: CodeTriple)
		{
			compiledWords.append(code.threadCode)
			executionTokens.append((xt: code.xt, param: code.parameter))
		}

		init() {}

		init<S: Sequence>(threadCodes: S, dummyXT: Cell) where S.Element == ThreadCode
		{
			for threadCode in threadCodes
			{
				self.append((threadCode, dummyXT, nil))
			}
		}

		mutating func replace(index: Int, with code: @escaping ThreadCode) throws
		{
			guard compiledWords.indices.contains(index)
			else { throw Error.addressOutOfBounds }
			compiledWords[index] = code
		}

		static func+= (lhs: inout CompilingWordList, rhs: CompilingWordList)
		{
			lhs.compiledWords += rhs.compiledWords
			lhs.executionTokens += rhs.executionTokens
		}
	}

	fileprivate var pushXT: Cell { dictionary["{push}"]!.executionToken }
	fileprivate var callXT: Cell { dictionary["{call}"]!.executionToken }
	fileprivate var primitiveXT: Cell { dictionary["{handCompiled}"]!.executionToken }
	fileprivate var xtToCompilingXT: Cell { dictionary["compile,"]!.executionToken }
	fileprivate func tripleForPush(value: Cell) -> CodeTriple
	{
		(push(value), pushXT, value)
	}
	fileprivate func tripleForCall(xt: Cell) -> CodeTriple
	{
		(call(xt), callXT, xt)
	}

	fileprivate func primitiveTriple(threadCode: @escaping ThreadCode) -> CodeTriple
	{
		(threadCode, primitiveXT, nil)
	}

	func debugCompilingWordList(xt: Cell) -> String
	{
		let compiledList = wordLists[xt]
		let strings = compiledList.executionTokens.map
		{
			let (xt, cell) = $0
			let wordPart = dictionary[xt]?.string ?? "#\(xt)"
			guard let cell else { return wordPart }
			return wordPart + "(\(cell))"
		}
		return strings.joined(separator: " ")
	}
}

extension ThreadedForth.WordMetaData: CustomStringConvertible
{
	var description: String
	{
		"xt=\(executionToken) \(isImmediate ? "immediate" : "") \(isCompileOnly ? "compile-only" : "") \(isCreated ? "created" : "") dataField=\(dataField)"
	}
}

// MARK: -
// MARK: primitives

// Thre following conventions are used to name objects to be manipulated:
//
// S:  data stack (often omitted)
// RS: Return stack
// CS: Control stack
// DS: Dataspace
// CW: Current word
// in: in address
//
// A suffix of "Char" is used for character operations

private func accept(machine: ThreadedForth) throws
{
	let maxCount = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let address = try machine.dataStack.throwingPop()
	// TODO: We could extract this into a protocol extension
	var utf8BytesNeeded = 0
	var count = 0
	// Loop while we have not exceeded the count, we have characters and
	// we haven't got a return
	while count < maxCount, let aUInt8 = machine.stdin.next(), aUInt8 != 10
	{
		switch (~aUInt8).leadingZeroBitCount
		{
		case 1:	// UTF-8 extension char, make sure we expect it
			utf8BytesNeeded -= 1
			guard utf8BytesNeeded >= 0 else { throw ThreadedForth.Error.invalidUTF8 }
		case 0: // ASCII, must not be expecting an extension char
			guard utf8BytesNeeded == 0 else { throw ThreadedForth.Error.invalidUTF8 }
		case 2:	// 1 extension char
			guard utf8BytesNeeded == 0 else { throw ThreadedForth.Error.invalidUTF8 }
			utf8BytesNeeded = 1
		case 3:	// 2 extension chars
			guard utf8BytesNeeded == 0 else { throw ThreadedForth.Error.invalidUTF8 }
			utf8BytesNeeded = 2
		case 4:	// 3 extension char
			guard utf8BytesNeeded == 0 else { throw ThreadedForth.Error.invalidUTF8 }
			utf8BytesNeeded = 3
		default:
			throw ThreadedForth.Error.invalidUTF8
		}
		try machine.dataSpace.store(character: ThreadedForth.Cell(aUInt8), at: address + count)
		count += 1
	}
	// Make sure we didn't stop in the middle of some UTF-8
	guard utf8BytesNeeded == 0 else { throw ThreadedForth.Error.invalidUTF8 }
	machine.dataStackTop = count

}

private func aligned(machine: ThreadedForth) throws
{
	let address = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	machine.dataStackTop = machine.dataSpace.nextAligned(address: address)
}

private func allot(machine: ThreadedForth) throws
{
	let count = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	try machine.dataSpace.allot(count)
}

private func and(machine: ThreadedForth) throws
{
	let s0 = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	machine.dataStackTop = try s0 & machine.dataStack.throwingPop()
}

private func beginComment(machine: ThreadedForth) throws
{
	let isLineComment = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let commentEnd: Character = isLineComment != 0 ? "\n" : ")"
	machine.inputParser.set(state: .comment(commentEnd))
}

private func body(machine: ThreadedForth) throws
{
	let xt = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	guard let word = machine.dictionary[xt]
	else { throw ThreadedForth.Error.unrecognisedExecutionToken("#\(xt)") }
	guard let metaData = machine.dictionary[word]
	else { throw ThreadedForth.Error.unrecognisedWord(word.string) }
	guard metaData.isCreated else { throw ThreadedForth.Error.noDataFieldForExecutionToken(word.string) }
	machine.dataStackTop = metaData.dataField
}

private func branch(machine: ThreadedForth) throws
{
	let ipAdjustment = try (machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()) &- 1
	let newIndex = machine.wordIndex + ipAdjustment
	guard (machine.wordLists[machine.wordNumber].startIndex ... machine.wordLists[machine.wordNumber].endIndex).contains(newIndex)
	else
	{
		throw ThreadedForth.Error.codeIndexOutOfBounds
	}
	machine.wordIndex = newIndex
}

private func call(_ newWordNumber: Int) -> ThreadedForth.ThreadCode
{
	return { machine in try machine.doCall(newWordNumber) }
}

private func cells(machine: ThreadedForth) throws
{
	let count = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let bytes = ThreadedForth.Cell.bytesPerCell
	machine.dataStackTop = bytes * count
}

private func chars(machine: ThreadedForth) throws
{
	let count = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let bytes = ThreadedForth.Cell.bytesPerChar
	machine.dataStackTop = bytes * count
}

private func compileOnly(machine: ThreadedForth) throws
{
	guard let word = machine.compilingWordName
	else { throw ThreadedForth.Error.noWordCompiled }
	guard let metaData = machine.dictionary[word]
	else { throw ThreadedForth.Error.unrecognisedWord(word.string) }
	metaData.isCompileOnly = true
}

private func completePicture(machine: ThreadedForth) throws
{
	let utf8Chars = machine.outputPicture.utf8
	let startAddress = machine.dataSpace.pointer
	try machine.dataSpace.allot(utf8Chars.count)
	try machine.dataSpace.store(characters: utf8Chars, at: startAddress)
	machine.flushDataStackTop()
	machine.dataStack.push(startAddress)
	machine.dataStackTop = utf8Chars.count
}

private func createDictionaryEntry(machine: ThreadedForth) throws
{
	guard let word = machine.compilingWordName else { throw ThreadedForth.Error.noWordToCreate }
	machine.dictionary[word] = ThreadedForth.WordMetaData(machine.compilingListIndex,
														  threadCode: call(machine.compilingListIndex),
														  previous: machine.dictionary[word])
}

private func createNamedWordSlot(machine: ThreadedForth) throws
{
	guard let word = machine.currentWord else { throw ThreadedForth.Error.noWordToCreate }
	machine.compilingListIndex = machine.wordLists.count
	machine.wordLists.append(ThreadedForth.CompilingWordList())
	machine.compilingWordName = word
}

private func createUnnamedWordSlot(machine: ThreadedForth)
{
	machine.compilingListIndex = machine.wordLists.count
	machine.wordLists.append(ThreadedForth.CompilingWordList())
	machine.compilingWordName = nil
}

private func csFrame(machine: ThreadedForth)
{
	machine.controlStack.createFrame()
}

private func csFrameSize(machine: ThreadedForth)
{
	machine.flushDataStackTop()
	let frameSize = machine.controlStack.frameOffsetOfTop
	machine.dataStackTop = frameSize
}

private func csPopFrame(machine: ThreadedForth) throws
{
	try machine.controlStack.popFrame()
}

private func csRoll(machine: ThreadedForth) throws
{
	let itemNumber = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()

	var tempStack = ThreadedForth.Stack<ThreadedForth.ControlWord>(name: "temp")

	// Remove all the items on the control stack above the one we want
	for _ in 0 ..< itemNumber
	{
		try tempStack.push(machine.controlStack.throwingPop())
	}
	let newTop = try machine.controlStack.throwingPop()
	// Put all the other items back
	while !tempStack.isEmpty
	{
		machine.controlStack.push(tempStack.pop()!)
	}
	machine.controlStack.push(newTop)
}

extension ThreadedForth
{

	/// Functions we can perform wioth the current word
	enum CWFunction: Cell
	{
		/// postpone the current word
		case postpone
		/// Treat as a character and put a push in the compiling word for it
		case toCompilingChar
		/// Put in the dataspace and put its address and length on the stck
		case toString
		/// Print to `stdout`
		case toOutput
		/// Treat as a character and put on the stack
		case toSChar
		/// Look it up in the dictionary and put its execution token on the stack
		case toSXT
		/// Look it up in the dictionary and put its xt in the compiling word
		case toCompilingXT
	}
}

private func cwFunc(machine: ThreadedForth) throws
{
	let rawFunction = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	guard let function = ThreadedForth.CWFunction(rawValue: rawFunction)
	else { throw ThreadedForth.Error.unrecognisedCurrentWordFunc(rawFunction) }
	guard let word = machine.currentWord else { throw ThreadedForth.Error.noWordToCreate }

	switch function
	{
	case .postpone, .toSXT, .toCompilingXT:
		guard let metaData = machine.dictionary[word]
		else { throw ThreadedForth.Error.unrecognisedWord(word.string) }
		switch function
		{
		case .postpone:
			let cli = machine.compilingListIndex
			if metaData.isImmediate
			{
				machine.wordLists[cli].append((metaData.threadCode, metaData.executionToken, nil))
			}
			else
			{
				machine.wordLists[cli].append(machine.tripleForPush(value: metaData.executionToken))
				machine.wordLists[cli].append((xtToCompiling, machine.xtToCompilingXT, nil))
			}
		case .toSXT:
			machine.flushDataStackTop()
			machine.dataStackTop = metaData.executionToken
		case .toCompilingXT:
			machine.wordLists[machine.compilingListIndex].append(machine.tripleForPush(value: metaData.executionToken))
		default:
			fatalError("current word func \(function) does not need metadata")
		}
	case .toCompilingChar:
		let firstChar = word.string.utf8.first ?? 0
		machine.wordLists[machine.compilingListIndex].append(machine.tripleForPush(value: Int(firstChar)))
	case .toString:
		let startAddress = machine.dataSpace.pointer
		let utf8 = word.string.utf8
		try machine.dataSpace.allot(word.string.utf8.count)
		var charIndex = utf8.startIndex
		for address in startAddress ..< machine.dataSpace.pointer
		{
			try machine.dataSpace.store(character: Int(utf8[charIndex]), at: address)
			charIndex = utf8.index(after: charIndex)
		}
		machine.flushDataStackTop()
		machine.dataStack.push(startAddress)
		machine.dataStackTop = utf8.count
	case .toOutput:
		print(word.string, to: &machine.output)
	case .toSChar:
		let firstChar = word.string.utf8.first ?? 0
		machine.flushDataStackTop()
		machine.dataStackTop = ThreadedForth.Cell(firstChar)
	}
}

private func divMod(machine: ThreadedForth) throws
{
	let denominator = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	guard denominator != 0 else { throw ThreadedForth.Error.divisionByZero }
	let numerator = try machine.dataStack.throwingPop()
	let (q, r) = numerator.quotientAndRemainder(dividingBy: denominator)
	machine.dataStack.push(r)
	machine.dataStackTop = q
}

private func depth(machine: ThreadedForth) throws
{
	machine.flushDataStackTop()
	machine.dataStackTop = machine.dataStack.count
}

private func dest(machine: ThreadedForth) throws
{
	machine.controlStack.push(.dest(machine.wordLists[machine.compilingListIndex].count))
}

private func does(machine: ThreadedForth) throws
{
	let cli = machine.compilingListIndex
	guard let compilingWord = machine.dictionary[cli]
	else
	{
		throw ThreadedForth.Error.invalidDoes(lastDefined: "none",
											  caller: machine.dictionary[machine.wordNumber]?.string ?? "#\(machine.wordNumber)")
	}

	guard let metaData = machine.dictionary[compilingWord], metaData.isCreated
	else
	{
		throw ThreadedForth.Error.invalidDoes(lastDefined: compilingWord.string,
											  caller: machine.dictionary[machine.wordNumber]?.string ?? "#\(machine.wordNumber)")
	}
	let target = ThreadedForth.ReturnStackCell(wordNumber: machine.wordNumber, index: machine.wordIndex)
	let gotoIndex = machine.wordLists[cli].count - 3
	try machine.wordLists[cli].replace(index: gotoIndex, with: push(target))
	try machine.wordLists[cli].replace(index: gotoIndex + 1, with: sToRS(machine:))
	try machine.wordLists[cli].replace(index: gotoIndex + 2, with: exit)
	try exit(machine: machine)
}

private func drop(machine: ThreadedForth) throws
{
	if machine.dataStackTop != nil
	{
		machine.dataStackTop = nil
	}
	else
	{
		_ = try machine.dataStack.throwingPop()
	}
}

private func dsToS(machine: ThreadedForth) throws
{
	let address = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	machine.dataStackTop = try machine.dataSpace.loadCell(from: address)
}

private func dsToSChar(machine: ThreadedForth) throws
{
	let address = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	machine.dataStackTop = try machine.dataSpace.loadCharacter(from: address)
}

private func dup(machine: ThreadedForth) throws
{
	if let top = machine.dataStackTop
	{
		machine.dataStack.push(top)
	}
	else
	{
		guard let top = machine.dataStack.top else { throw ToolboxError.stackUnderflow }
		machine.dataStackTop = top
	}
}

private func emit(machine: ThreadedForth) throws
{
	let value = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	guard let char = Unicode.Scalar(value)
	else { throw ThreadedForth.Error.invalidUnicode(value) }
	machine.output.write(String(char))

}

private func dsEmpty(machine: ThreadedForth)
{
	machine.dataStackTop = nil
	machine.dataStack.empty()
}

private func endDefinition(machine: ThreadedForth) throws
{
	guard machine.controlStack.isEmpty else { throw ThreadedForth.Error.unmatchedOrigin }
	if let word = machine.compilingWordName
	{
		machine.dictionary[word] = ThreadedForth.WordMetaData(machine.compilingListIndex,
															  threadCode: call(machine.compilingListIndex),
															  previous: machine.dictionary[word])
	}
	else
	{
		machine.flushDataStackTop()
		machine.dataStackTop = machine.compilingListIndex
	}
	// TODO: Possibly use [ in future
	machine.isCompiling = false
}

private func environmentQuery(machine: ThreadedForth) throws
{
	let length = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let charAddress = try machine.dataStack.throwingPop()
	let key = try machine.dataSpace.loadString(address: charAddress, count: length)
	if let value = machine.environment[key]
	{
		switch value
		{
		case .int(let int):
			machine.dataStack.push(int)
		case .uint(let uint):
			machine.dataStack.push(ThreadedForth.Cell(bitPattern: uint))
		case .doubleInt(let doubleInt):
			machine.dataStack.push(ThreadedForth.Cell(bitPattern: doubleInt.low))
			machine.dataStack.push(doubleInt.high)
		case .doubleUInt(let doubleUInt):
			machine.dataStack.push(ThreadedForth.Cell(bitPattern: doubleUInt.low))
			machine.dataStack.push(ThreadedForth.Cell(bitPattern: doubleUInt.high))
		case .flag(let flag):
			machine.dataStack.push(flag ? -1 : 0)
		}
		machine.dataStackTop = -1
	}
	else
	{
		machine.dataStackTop = 0
	}
}

private func eq0Branch(machine: ThreadedForth) throws
{
	let offset = try (machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()) &- 1
	if try machine.dataStack.throwingPop() == 0
	{
		let newIndex = machine.wordIndex + offset
		guard (machine.wordLists[machine.wordNumber].startIndex ... machine.wordLists[machine.wordNumber].endIndex).contains(newIndex)
		else
		{
			throw ThreadedForth.Error.codeIndexOutOfBounds
		}
		machine.wordIndex = newIndex
	}
}

private func equals(machine: ThreadedForth) throws
{
	let st = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	machine.dataStackTop = try st == machine.dataStack.throwingPop() ? -1 : 0
}

private func execute(machine: ThreadedForth) throws
{
	let tokenToExecute = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	try machine.doCall(tokenToExecute)
}

private func exit(machine: ThreadedForth) throws
{
	try machine.doExit()
}

private func find(machine: ThreadedForth) throws
{
	let cAddr = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let count = try machine.dataSpace.loadCharacter(from: cAddr)
	let wordString = try machine.dataSpace.loadString(address: cAddr + 1, count: count)
	if let metaData = machine.dictionary[ThreadedForth.Word(wordString)]
	{
		machine.dataStack.push(metaData.executionToken)
		machine.dataStackTop = metaData.isImmediate ? 1 : -1
	}
	else
	{
		machine.dataStack.push(cAddr)
		machine.dataStackTop = 0
	}
}

private func fmDivMod(machine: ThreadedForth) throws
{
	let divisor = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let dividend: DoublePrecision<Int> = try machine.popDouble()
	var qr = try dividend.divided(by: divisor)
	// Now adjust the remainder to make it the same sign as the divisor
	// and the quotient to round towards -infinity. Obviously, if the
	// remainder is zero, we can leave it.
	assert(divisor != 0)
	if qr.remainder != 0 && qr.remainder.signum() != divisor.signum()
	{
		qr.remainder += divisor
		qr.quotient -= 1
	}
	machine.dataStackTop = qr.quotient
	machine.dataStack.push(qr.remainder)
}

private func greater(machine: ThreadedForth) throws
{
	let st = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	machine.dataStackTop = try st < machine.dataStack.throwingPop() ? -1 : 0
}

private func here(machine: ThreadedForth)
{
	machine.flushDataStackTop()
	machine.dataStackTop = machine.dataSpace.pointer
}

private func hold(machine: ThreadedForth) throws
{
	let value = try machine.dataStackTop.pop() ??  machine.dataStack.throwingPop()
	guard let unicode = UnicodeScalar(value) else { throw ThreadedForth.Error.invalidUnicode(value) }
	machine.outputPicture.insert(Character(unicode), at:  machine.outputPicture.startIndex)
}

private func immediate(machine: ThreadedForth) throws
{
	guard let word = machine.compilingWordName
	else { throw ThreadedForth.Error.noWordCompiled }
	guard let metaData = machine.dictionary[word]
	else { throw ThreadedForth.Error.unrecognisedWord(word.string) }
	metaData.isImmediate = true
}

private func initPicture(machine: ThreadedForth)
{
	machine.outputPicture = ""
}

private func inputClear(machine: ThreadedForth) throws
{
	machine.inputParser.clear()
}

private func stringInputPush(machine: ThreadedForth) throws
{
	let blockNumber = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let stringCount = try machine.dataStack.throwingPop()
	let stringAddress = try machine.dataStack.throwingPop()
	let iterable = machine.dataSpaceSlice(address: stringAddress, byteCount: stringCount, blockNumber: blockNumber)
	let newSource = try Forth.InputSource(iterable, sourceId: -1, name: "data space")
	machine.inputParser.push(inputSource: newSource)
}

private func stringInputReplace(machine: ThreadedForth) throws
{
	let blockNumber = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let stringCount = try machine.dataStack.throwingPop()
	let stringAddress = try machine.dataStack.throwingPop()
	let iterable = machine.dataSpaceSlice(address: stringAddress, byteCount: stringCount, blockNumber: blockNumber)
	let newSource = try Forth.InputSource(iterable, sourceId: -1, name: "data space")
	machine.inputParser.inputSource = newSource
}


private func inputPop(machine: ThreadedForth) throws
{
	guard machine.inputParser.pop() else { throw ThreadedForth.Error.noInputToRestore }
}

private func interpretWord(machine: ThreadedForth) throws
{
	let count = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let cAddress = try machine.dataStack.throwingPop()
	let wordString = try machine.dataSpace.loadString(address: cAddress, count: count)
	let word = ThreadedForth.Word(wordString)
	machine.currentWord = word
	try machine.interpretOrCompile(word: word)
}

private func invert(machine: ThreadedForth) throws
{
	machine.dataStackTop = try ~(machine.dataStackTop ?? machine.dataStack.throwingPop())
}

private func less(machine: ThreadedForth) throws
{
	let st = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	machine.dataStackTop = try st > machine.dataStack.throwingPop() ? -1 : 0
}

private func lshift(machine: ThreadedForth) throws
{
	let u = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let x1 = try machine.dataStack.throwingPop()
	guard (0 ... Int.bitWidth).contains(u) else { throw ThreadedForth.Error.shiftOutOfRange }
	machine.dataStackTop = x1 << u
}

private func markCreated(machine: ThreadedForth) throws
{
	let address = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()

	guard let word = machine.compilingWordName, let metaData = machine.dictionary[word]
	else { throw ThreadedForth.Error.noWordToCreate }
	metaData.markCreated(dataField: address)
}

private func minus(machine: ThreadedForth) throws
{
	let operand = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	machine.dataStackTop = try machine.dataStack.throwingPop() &- operand
}

private func ne0Branch(machine: ThreadedForth) throws
{
	let offset = try (machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()) &- 1
	if try machine.dataStack.throwingPop() != 0
	{
		let newIndex = machine.wordIndex + offset
		guard (machine.wordLists[machine.wordNumber].startIndex ... machine.wordLists[machine.wordNumber].endIndex).contains(newIndex)
		else
		{
			throw ThreadedForth.Error.codeIndexOutOfBounds
		}
		machine.wordIndex = newIndex
	}
}

private func noOp(machine: ThreadedForth) {}

private func or(machine: ThreadedForth) throws
{
	let s0 = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	machine.dataStackTop = try s0 | machine.dataStack.throwingPop()
}

private func over(machine: ThreadedForth) throws
{
	let s0 = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	guard let s1 = machine.dataStack.top else { throw ToolboxError.stackUnderflow }
	machine.dataStack.push(s0)
	machine.dataStackTop = s1
}

private func orig(machine: ThreadedForth) throws
{
	let compilingListEnd = machine.wordLists[machine.compilingListIndex].endIndex
	guard  compilingListEnd > 0 else { throw ThreadedForth.Error.codeIndexOutOfBounds }
	machine.controlStack.push(.orig(compilingListEnd - 1))
}

private func parseWord(machine: ThreadedForth) throws
{
	guard let word = try machine.inputParser.next()
	else { throw ThreadedForth.Error.endOfInput }
	machine.currentWord = word
}

private func plus(machine: ThreadedForth) throws
{
	let op1 = try machine.dataStackTop ?? machine.dataStack.throwingPop()
	do
	{
		machine.dataStackTop = try op1 &+ machine.dataStack.throwingPop()
	}
	catch
	{
		machine.dataStackTop = nil
		throw error
	}
}

private func printRight(machine: ThreadedForth) throws
{
	let width = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let value = try machine.dataStack.throwingPop()
	let valueString = try String(value, radix: machine.base)
	machine.output.write(item: valueString, width: width)
}

private func printUnsignedRight(machine: ThreadedForth) throws
{
	let width = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let value = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let unsignedValue = UInt(bitPattern: value)
	let valueString = try String(unsignedValue, radix: machine.base)
	machine.output.write(item: valueString, width: width)
}

private func printStack(machine: ThreadedForth) throws
{
	let base = try machine.base
	let stackString = machine.stringFrom(stack: machine.dataStack,
										     top: machine.dataStackTop,
										     stringifyElement: { String($0, radix: base) })
	machine.output.write(stackString)
}

private func push(_ number: ThreadedForth.Cell) -> ThreadedForth.ThreadCode
{
	return {
		machine in
		machine.flushDataStackTop()
		machine.dataStackTop = number
	}
}

private func recurse(machine: ThreadedForth) throws
{
	machine.wordLists[machine.compilingListIndex].append(machine.tripleForCall(xt: machine.compilingListIndex))
}

private func refill(machine: ThreadedForth) throws
{
	machine.flushDataStackTop()
	machine.dataStackTop = try machine.inputParser.refill() ? -1 : 0
}

private func resolveOrig(machine: ThreadedForth) throws
{
	guard let controlWord = machine.controlStack.pop(),
		  case ThreadedForth.ControlWord.orig(let offset) = controlWord
	else { throw ThreadedForth.Error.unmatchedOrigin }
	let currentLoc = machine.wordLists[machine.compilingListIndex].endIndex
	let branchOffset = currentLoc - offset - 1
	try machine.wordLists[machine.compilingListIndex].replace(index: offset, with: push(branchOffset))
}

private func resolveDest(machine: ThreadedForth) throws
{
	guard case ThreadedForth.ControlWord.dest(let location)? = machine.controlStack.pop()
	else { throw ThreadedForth.Error.unmatchedDestination }
	let offset = location - (machine.wordLists[machine.compilingListIndex].count + 1)
	machine.wordLists[machine.compilingListIndex].append(machine.tripleForPush(value: offset))
}

private func rot(machine: ThreadedForth) throws
{
	let s0 = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let s1 = try machine.dataStack.throwingPop()
	let s2 = try machine.dataStack.throwingPop()
	machine.dataStack.push(s1)
	machine.dataStack.push(s0)
	machine.dataStackTop = s2
}

private func rsCopyToS(machine: ThreadedForth) throws
{
	machine.flushDataStackTop()
	machine.dataStackTop = try machine.returnStackTop ?? machine.returnStack.throwingTop()
}

private func rsEmpty(machine: ThreadedForth)
{
	machine.returnStackTop = nil
	machine.returnStack.empty()
}

private func rshift(machine: ThreadedForth) throws
{
	let isArithmetic = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop() != 0
	let u = try machine.dataStack.throwingPop()
	guard (0 ... Int.bitWidth).contains(u) else { throw ThreadedForth.Error.shiftOutOfRange }
	if isArithmetic
	{
		let x1 = try machine.dataStack.throwingPop()
		machine.dataStackTop = x1 >> u
	}
	else
	{
		let x1 = try UInt(bitPattern: machine.dataStack.throwingPop())
		machine.dataStackTop = Int(x1 >> u)
	}
}

private func rsToS(machine: ThreadedForth) throws
{
	machine.flushDataStackTop()
	machine.dataStackTop = try machine.returnStackTop.pop() ?? machine.returnStack.throwingPop()
}

private func smDivRem(machine: ThreadedForth) throws
{
	try machine.symmetricMultipleDivide(Int.self)
}

private func source(machine: ThreadedForth)
{
	machine.flushDataStackTop()
	machine.dataStack.push(machine.inputParser.bufferAbsoluteAddress)
	machine.dataStackTop = machine.inputParser.bufferUInt8Count
}

private func sourceId(machine: ThreadedForth)
{
	machine.flushDataStackTop()
	machine.dataStackTop = machine.inputParser.sourceId
}

private func sToCompiling(machine: ThreadedForth) throws
{
	let op = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	machine.wordLists[machine.compilingListIndex].append(machine.tripleForPush(value: op))
}

private func sStringToCountedTransient(machine: ThreadedForth) throws
{
	let count = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let cAddress = try machine.dataStack.throwingPop()
	let byteArray = try machine.dataSpace.loadBytes(address: cAddress, count: count)
	machine.dataStackTop = try machine.dataSpace.storeCountedTransient(characters: byteArray)
}

private func sToDS(machine: ThreadedForth) throws
{
	let address = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let value = try machine.dataStack.throwingPop()
	try machine.dataSpace.store(cell: value, at: address)
}

private func sToDSChar(machine: ThreadedForth) throws
{
	let address = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let value = try machine.dataStack.throwingPop()
	try machine.dataSpace.store(character: value, at: address)
}

private func sToRS(machine: ThreadedForth) throws
{
	let st = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	machine.flushReturnStackTop()
	machine.returnStackTop = st
}

private func stringMode(machine: ThreadedForth) throws
{
	let useEscapes = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	machine.inputParser.set(state: useEscapes == 0 ? .string("") : .escapeString(""))
}

private func displayCommentMode(machine: ThreadedForth)
{
	machine.inputParser.set(state: .displayComment(""))
}

private func swap(machine: ThreadedForth) throws
{
	let s0 = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let s1 = try machine.dataStack.throwingPop()
	machine.dataStack.push(s0)
	machine.dataStackTop = s1
}

private func swap2(machine: ThreadedForth) throws
{
	let s0 = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let s1 = try machine.dataStack.throwingPop()
	let s2 = try machine.dataStack.throwingPop()
	let s3 = try machine.dataStack.throwingPop()
	machine.dataStack.push(s1)
	machine.dataStack.push(s0)
	machine.dataStack.push(s3)
	machine.dataStackTop = s2
}

private func timesFullWidth(machine: ThreadedForth) throws
{
	let op1 = try machine.dataStackTop ?? machine.dataStack.throwingPop()
	let op2 = try machine.dataStack.throwingPop()
	let dpResult = op2.multipliedFullWidth(by: op1)
	machine.dataStack.push(Int(bitPattern: dpResult.low))
	machine.dataStackTop = dpResult.high
}

private func `throw`(machine: ThreadedForth) throws
{
	var mutableMachine = machine
	try mutableMachine.throwError()
}

private func times(machine: ThreadedForth) throws
{
	let operand = try machine.dataStackTop ?? machine.dataStack.throwingPop()
	machine.dataStackTop = try machine.dataStack.throwingPop() &* operand
}

private func unsignedGreater(machine: ThreadedForth) throws
{
	let st = try UInt(bitPattern: machine.dataStackTop.pop() ?? machine.dataStack.throwingPop())
	machine.dataStackTop = try st < UInt(bitPattern: machine.dataStack.throwingPop()) ? -1 : 0
}

private func unsignedLess(machine: ThreadedForth) throws
{
	let st = try UInt(bitPattern: machine.dataStackTop.pop() ?? machine.dataStack.throwingPop())
	machine.dataStackTop = try st > UInt(bitPattern: machine.dataStack.throwingPop()) ? -1 : 0
}

// TODO: Maybe implement in terms of word
private func wordForInterpreter(machine: ThreadedForth) throws
{
	let parsedString = try machine.inputParser.next()
	let cAddress = try machine.dataSpace.storeCountedTransient(string: parsedString?.string ?? "")
	machine.flushDataStackTop()
	machine.dataStackTop = cAddress
}

private func xor(machine: ThreadedForth) throws
{
	let s0 = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	machine.dataStackTop = try s0 ^ machine.dataStack.throwingPop()
}

private func xtToCompiling(machine: ThreadedForth) throws
{
	let xt = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	guard xt < machine.wordLists.count else { throw ThreadedForth.Error.unrecognisedExecutionToken(xt.asTokenString) }
	machine.wordLists[machine.compilingListIndex] += machine.wordLists[xt]
}

// MARK: -
// MARK: Core etension primitives

private func truncateDictionaryAtXT(machine: ThreadedForth) throws
{
	let firstXTToGo = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	var newDictionary = ThreadedForth.WordDictionary()
	for (key, metaData) in machine.dictionary.forward
	{
		var newMetaData: ThreadedForth.WordMetaData? = metaData
		while let nonNilMetaData = newMetaData, nonNilMetaData.executionToken >= firstXTToGo
		{
			newMetaData = nonNilMetaData.previousDefinition
		}
		newDictionary[key] = newMetaData
	}
	machine.dictionary = newDictionary
	machine.wordLists.removeLast(machine.wordLists.count - firstXTToGo)
}

private func saveInput(machine: ThreadedForth)
{
	let sourceState = machine.inputParser.saveLocation()
	machine.flushDataStackTop()
	for cell in sourceState
	{
		machine.dataStack.push(cell)
	}
	machine.dataStackTop = sourceState.count
}

private func restoreInput(machine: ThreadedForth) throws
{
	let restoreCount = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	var saveState: [Forth.Cell] = []
	for _ in 0 ..< restoreCount
	{
		try saveState.append(machine.dataStack.throwingPop())
	}
	machine.dataStackTop = machine.inputParser.restore(location: saveState.reversed()) ? 0 : -1
}

private func universalParse(machine: ThreadedForth) throws
{
	let ignore = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let delimiter = try machine.dataStack.throwingPop()
	let localCAddress: Forth.Cell
	let count: Forth.Cell
	if delimiter == 0
	{
		(localCAddress, count) = try machine.inputParser.parseWord(delimitedBy: nil, ignoreLeadingDelimiters: ignore != 0)
	}
	else
	{
		// TODO: Invalid UTF8 is not really correct here
		guard let delimUnicode = Unicode.Scalar(delimiter) else { throw ThreadedForth.Error.invalidUTF8 }
		let delimiterChar = Character(delimUnicode)
		(localCAddress, count) = try machine.inputParser.parseWord(delimitedBy: delimiterChar, ignoreLeadingDelimiters: false)
	}
	let cAddress = Forth.DataSpace.globalAddress(space: machine.parserSpaceIndex,
												   spaceAddress: localCAddress)
	log.debug("Parsed address: 0x\(String(cAddress, radix: 16)), count: \(count)")
	machine.flushDataStackTop()
	machine.dataStack.push(cAddress)
	machine.dataStackTop = count
}

// MARK: -
// MARK: File handling primitives

extension ThreadedForth
{
	fileprivate func handleCocoaError(error: NSError)
	{
		log.debug("\(error)")
		// Hopefully we have a Cocoa error with a chained Posix error, if not
		// wee make up an error number
		flushDataStackTop()
		guard error.domain == "NSCocoaErrorDomain"
		else
		{
			log.error("Unexpected error domain \(error.domain)")
			dataStackTop = Int.max
			return
		}
		guard let underlyingError = error.userInfo["NSUnderlyingError"] as? NSError
		else
		{
			log.error("No underlying error in \(error)")
			dataStackTop = Int.max - 1
			return
		}
		guard underlyingError.domain == "NSPOSIXErrorDomain"
		else
		{
			log.error("Unexpected underlying error domain \(underlyingError.domain)")
			dataStackTop = Int.max - 2
			return
		}
		dataStackTop = underlyingError.code
	}
}

private func fileDescriptor(from cell: ThreadedForth.Cell) throws -> FileDescriptor
{
	let fileId = try CInt(checkingBounds: cell)
	let fd = FileDescriptor(rawValue: fileId)
	return fd
}

private func closeFile(machine: ThreadedForth) throws
{
	let fd = try fileDescriptor(from: machine.dataStackTop.pop() ?? machine.dataStack.throwingPop())
	do
	{
		machine.removeNameFor(descriptor: fd)
		try fd.close()
		machine.dataStackTop = 0
	}
	catch let error as Errno
	{
		machine.dataStackTop = Forth.Cell(error.rawValue)
	}
}

private func createFile(machine: ThreadedForth) throws
{
	try openOrCreateFile(machine: machine, options: [.create, .truncate], permissions: Forth.defaultCreatePermissions)
}

private func deleteFile(machine: ThreadedForth) throws
{
	let stringCount = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let stringAddress = try machine.dataStack.throwingPop()
	let fileName = try machine.dataSpace.loadString(address: stringAddress, count: stringCount)
	do
	{
		try FileManager.default.removeItem(atPath: fileName)
		machine.dataStackTop = 0
	}
	catch let error as NSError
	{
		machine.handleCocoaError(error: error)
	}
}

private func filePosition(machine: ThreadedForth) throws
{
	let fd = try fileDescriptor(from: machine.dataStackTop.pop() ?? machine.dataStack.throwingPop())
	do
	{
		let position = try fd.seek(offset: 0, from: .current)

		// File position is double precision. For us the high word is always
		// 0 because our cell is 64 bits.
		machine.dataStack.push(Forth.Cell(position))
		machine.dataStack.push(0)
		machine.dataStackTop = 0
	}
	catch let error as Errno
	{
		machine.dataStack.push(-1)
		machine.dataStack.push(-1)
		machine.dataStackTop = Forth.Cell(error.rawValue)
	}

}

private func fileSize(machine: ThreadedForth) throws
{
	let fd = try fileDescriptor(from: machine.dataStackTop.pop() ?? machine.dataStack.throwingPop())
	do
	{
		let position = try fd.seek(offset: 0, from: .current)
		let size = try fd.seek(offset: 0, from: .end)
		try fd.seek(offset: position, from: .start)

		// File size is double precision. For us the high word is always
		// 0 because our cell is 64 bits.
		machine.dataStack.push(Forth.Cell(size))
		machine.dataStack.push(0)
		machine.dataStackTop = 0
	}
	catch let error as Errno
	{
		machine.dataStack.push(-1)
		machine.dataStack.push(-1)
		machine.dataStackTop = Forth.Cell(error.rawValue)
	}
}

private func fileStatus(machine: ThreadedForth) throws
{
	let stringCount = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let stringAddress = try machine.dataStack.throwingPop()
	let fileName = try machine.dataSpace.loadString(address: stringAddress, count: stringCount)
	do
	{
		let attributes = try FileManager.default.attributesOfItem(atPath: fileName) as NSDictionary
		let accessMode = attributes.filePosixPermissions()
		machine.dataStack.push(accessMode)
		machine.dataStackTop = 0
	}
	catch let error as NSError
	{
		machine.dataStackTop = 0	// File status is meaningless
		machine.handleCocoaError(error: error)
	}
}


/// Perform an include
///
/// In Forth terms
///
/// caddr count require? -- wasIncluded?
///
/// - caddr is the address of the file name
/// - count is the count of utf8 code units in the string
/// - require? is true if this is a require, false if an include
/// - wasIncluded? is true if included or if a require and not included before
///
/// if wasIncluded? then an inputPop will be needed to restore the old input
/// source
///
/// - Parameter machine: The machine to operate on
/// - Throws: if anything goes wrong with the include
private func includeFile(machine: ThreadedForth) throws
{
	let fd = try fileDescriptor(from: machine.dataStackTop.pop() ?? machine.dataStack.throwingPop())
	guard let fileName = machine.nameFor(descriptor: fd)
	else { throw Forth.Error.includingUnnamedFileDescriptor }
	let source = try Forth.InputSource(fd, sourceId: Forth.Cell(fd.rawValue), name: fileName)
	machine.inputParser.include(inputSource: source, wordListIndex: machine.wordLists.count)
}

private func isIncluded(machine: ThreadedForth) throws
{
	let fd = try fileDescriptor(from: machine.dataStackTop.pop() ?? machine.dataStack.throwingPop())
	guard let fileName = machine.nameFor(descriptor: fd)
	else { throw Forth.Error.includingUnnamedFileDescriptor }
	machine.dataStackTop = machine.inputParser.isIncluded(file: fileName) ? -1 : 0
}


private func openFile(machine: ThreadedForth) throws
{
	try openOrCreateFile(machine: machine, options: [], permissions: nil)
}

private func openOrCreateFile(machine: ThreadedForth,
							  options: FileDescriptor.OpenOptions,
							  permissions: FilePermissions?) throws
{
	let accessMode = try FileDescriptor.AccessMode(rawValue: CInt(machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()))
	let stringCount = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let stringAddress = try machine.dataStack.throwingPop()
	let fileName = try machine.dataSpace.loadString(address: stringAddress, count: stringCount)
	let path = FilePath(fileName)
	do
	{
		let fd = try FileDescriptor.open(path,
										 accessMode,
										 options: options,
										 permissions: permissions,
										 retryOnInterrupt: false)
		machine.dataStack.push(Forth.Cell(fd.rawValue))
		machine.dataStackTop = 0
		machine.add(name: fileName, forDescriptor: fd)
	}
	catch let error as Errno
	{
		machine.dataStack.push(-1)
		machine.dataStackTop = Forth.Cell(error.rawValue)
	}
}

private func readFile(machine: ThreadedForth) throws
{
	let fd = try fileDescriptor(from: machine.dataStackTop.pop() ?? machine.dataStack.throwingPop())
	let maxBytes = try machine.dataStack.throwingPop()
	let dataAddress = try machine.dataStack.throwingPop()
	do
	{
		let buffer = try fd.readBytes(maxCount: maxBytes)
		guard buffer.count > 0
		else
		{
			// Nothing read, we reached end of file
			machine.dataStack.push(0)		// 0 character read
			machine.dataStackTop = 0		// 0 error code at eof
			return
		}
		try machine.dataSpace.store(characters: buffer, at: dataAddress)
		machine.dataStack.push(buffer.count)
		machine.dataStackTop = 0
	}
	catch let error as Errno
	{
		machine.dataStack.push(0)	// It's all or nothing for the read count
		machine.dataStackTop = Forth.Cell(error.rawValue)
	}
}

// TODO: Maybe implement in terms of read-file
private func readLine(machine: ThreadedForth) throws
{
	let fd = try fileDescriptor(from: machine.dataStackTop.pop() ?? machine.dataStack.throwingPop())
	let maxNonTerminators = try machine.dataStack.throwingPop()
	let dataAddress = try machine.dataStack.throwingPop()

	do
	{
		// We read up to the number of characters we want + 1 because, a CR LF
		// needs to be read in its entirety or not at all. If the last character
		// in the buffer is a CR we need to know if the next one is going to be
		// an LF. We will revisit this at some point in the future for unicode
		// support.
		let buffer = try ContiguousArray<UInt8>(unsafeUninitializedCapacity: maxNonTerminators + 1)
		{
			buffer, initializedCount in
			let rawBuffer = UnsafeMutableRawBufferPointer(buffer)
			try initializedCount = fd.read(into: rawBuffer)
		}
		// Check for end of file reached
		guard buffer.count > 0
		else
		{
			// Nothing read, we reached enfd of file
			machine.dataStack.push(0)		// 0 character read
			machine.dataStack.push(0)		// flag is false
			machine.dataStackTop = 0		// 0 error code at eof
			return
		}

		// TODO: Detect Unicode new lines
		if let newLineIndex = buffer.firstIndex(where: { $0 == 13 || $0 == 10 }), 
			(newLineIndex - buffer.startIndex) < maxNonTerminators
		{
			// we need to adjust the file pointer to just past the line ending
			// first figure out if we have a CR/LF or a CR LF
			let newLineLen: Int
			if buffer[newLineIndex] == 0x0d
				&& buffer.index(after: newLineIndex) != buffer.endIndex
				&& buffer[newLineIndex + 1] == 0x0a
			{
				newLineLen = 2
			}
			else
			{
				newLineLen = 1
			}
			let bytesShouldHaveRead = newLineIndex + newLineLen
			let adjustment = -Int64(buffer.count - bytesShouldHaveRead)
			try fd.seek(offset: adjustment, from: .current)

			// Only do the store if there are characters to store
			if newLineIndex != buffer.startIndex
			{
				try machine.dataSpace.store(characters: buffer[buffer.startIndex ..< newLineIndex], at: dataAddress)
			}
			machine.dataStack.push(newLineIndex - buffer.startIndex)
		}
		else
		{
			// No line separator

			// If we read more than we need to, there will be a filePointer adjustment
			if buffer.count > maxNonTerminators
			{
				try fd.seek(offset: -Int64(buffer.count - maxNonTerminators), from: .current)
			}
			try machine.dataSpace.store(characters: buffer, at: dataAddress)
			machine.dataStack.push(min(buffer.count, maxNonTerminators)) // Buffer size
		}
		machine.dataStack.push(-1)				  // True for flag
		machine.dataStackTop = 0				  // No error
	}
	catch let error as Errno
	{
		machine.dataStack.push(0)		// 0 character read in an error
		machine.dataStack.push(0)		// flag is false
		machine.dataStackTop = Forth.Cell(error.rawValue)
	}
}

private func renameFile(machine: ThreadedForth) throws
{
	let toStringCount = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let toStringAddress = try machine.dataStack.throwingPop()
	let toFileName = try machine.dataSpace.loadString(address: toStringAddress, count: toStringCount)
	let fromStringCount = try machine.dataStack.throwingPop()
	let fromStringAddress = try machine.dataStack.throwingPop()
	let fromFileName = try machine.dataSpace.loadString(address: fromStringAddress, count: fromStringCount)
	do
	{
		try FileManager.default.moveItem(atPath: fromFileName, toPath: toFileName)
		machine.dataStackTop = 0
	}
	catch let error as NSError
	{
		machine.handleCocoaError(error: error)
	}
}

private func repositionFile(machine: ThreadedForth) throws
{
	let fd = try fileDescriptor(from: machine.dataStackTop.pop() ?? machine.dataStack.throwingPop())
	// Double precision top word is always 0
	_ = try machine.dataStack.throwingPop()
	let filePos = try Int64(machine.dataStack.throwingPop())
	do
	{
		try fd.seek(offset: filePos, from: .start)
		machine.dataStackTop = 0
	}
	catch let error as Errno
	{
		machine.dataStackTop = Forth.Cell(error.rawValue)
	}
}

private func resizeFile(machine: ThreadedForth) throws
{
	let fd = try fileDescriptor(from: machine.dataStackTop.pop() ?? machine.dataStack.throwingPop())
	// Double precision top word is always 0
	_ = try machine.dataStack.throwingPop()
	let newSize = try Int64(machine.dataStack.throwingPop())
	do
	{
		try fd.resize(to: newSize, retryOnInterrupt: false)
		machine.dataStackTop = 0
	}
	catch let error as Errno
	{
		machine.dataStackTop = Forth.Cell(error.rawValue)
	}
}

private func writeFile(machine: ThreadedForth) throws
{
	let fd = try fileDescriptor(from: machine.dataStackTop.pop() ?? machine.dataStack.throwingPop())
	let count = try machine.dataStack.throwingPop()
	let address = try machine.dataStack.throwingPop()
	let bytesToWrite = try machine.dataSpace.loadBytes(address: address, count: count)
	do
	{
		try fd.writeAll(bytesToWrite)
		machine.dataStackTop = 0
	}
	catch let error as Errno
	{
		machine.dataStackTop = Forth.Cell(error.rawValue)
	}
}

private func writeLine(machine: ThreadedForth) throws
{
	// TODO: Can be implemented in terms of write-file
	let fd = try fileDescriptor(from: machine.dataStackTop.pop() ?? machine.dataStack.throwingPop())
	let count = try machine.dataStack.throwingPop()
	let address = try machine.dataStack.throwingPop()
	// TODO: Don't hard code the line terminator
	let bytesToWrite = try machine.dataSpace.loadBytes(address: address, count: count) + [0x0a]
	do
	{
		try fd.writeAll(bytesToWrite)
		machine.dataStackTop = 0
	}
	catch let error as Errno
	{
		machine.dataStackTop = Forth.Cell(error.rawValue)
	}
}

// MARK: Block primitives

private func cacheBlock(machine: ThreadedForth) throws
{
	let fromDisk = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let blockNumber = try machine.dataStack.throwingPop()
	machine.dataStackTop = try machine.cache(block: blockNumber, fromFile: fromDisk != 0)
}

private func emptyBuffers(machine: ThreadedForth)
{
	machine.blockCache.emptyBuffers()
}

private func listBlock(machine: ThreadedForth) throws
{
	let blockAddress = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let displayString = try machine.dataSpace.loadString(address: blockAddress, count: 1024)
	var lineCount = 0
	var line = ""
	for char in displayString
	{
		line.append(char)
		lineCount += 1
		if lineCount == Forth.blockScreenWidth
		{
			line.append("\n")
			machine.output.write(line)
			line = ""
			lineCount = 0
		}
	}
}

private func saveBuffers(machine: ThreadedForth) throws
{
	try machine.blockCache.saveBuffers()
}

private func update(machine: ThreadedForth) throws
{
	try machine.blockCache.update()
}

// MARK: some diagnostic primitives

private func breakPoint(machine: ThreadedForth)
{
	machine.logStacks(prefix: "==== BREAK ====\n\(machine.describeRS(cell: ThreadedForth.ReturnStackCell(wordNumber: machine.wordNumber, index: machine.wordIndex)))", level: .debug)
	log.debug("====  END  ====")
}

// MARK: Double precision primitives

fileprivate extension ThreadedForth
{
	func popDouble<N: FixedWidthInteger>() throws -> DoublePrecision<N>
	{
		let high = try dataStackTop.pop() ?? dataStack.throwingPop()
		let low = try dataStack.throwingPop()
		return DoublePrecision<N>(low: low, high: high)
	}

	func pushDouble<N: CellConvertible>(_ int: DoublePrecision<N>, needsFush: Bool = true)
	{
		if needsFush
		{
			flushDataStackTop()
		}
		let cells = int.cells

		dataStack.push(cells.low)
		dataStackTop = cells.high
	}
}

private func dEquals(machine: ThreadedForth) throws
{
	let st: DoublePrecision<Int> = try machine.popDouble()
	machine.dataStackTop = try st == machine.popDouble() ? -1 : 0
}

private func dLess(machine: ThreadedForth) throws
{
	let st: DoublePrecision<Int> = try machine.popDouble()
	machine.dataStackTop = try st > machine.popDouble() ? -1 : 0
}

private func dlshift(machine: ThreadedForth) throws
{
	let u = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let x1: DoublePrecision<UInt> = try machine.popDouble()
	guard (0 ... DoublePrecision<UInt>.bitWidth).contains(u) else { throw ThreadedForth.Error.shiftOutOfRange }
	machine.pushDouble(x1 << u)
}

private func dnegate(machine: ThreadedForth) throws
{
	let int: DoublePrecision<Int> = try machine.popDouble()
	try machine.pushDouble(int.negated(), needsFush: false)
}

private func drshift(machine: ThreadedForth) throws
{
	let u = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let x1: DoublePrecision<Int> = try machine.popDouble()
	guard (0 ... DoublePrecision<Int>.bitWidth).contains(u) else { throw ThreadedForth.Error.shiftOutOfRange }
	machine.pushDouble(x1 >> u)
}

private func dTimesDiv(machine: ThreadedForth) throws
{
	let divisor = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let multiplier = try machine.dataStack.throwingPop()
	let doubleValue: DoublePrecision<Int> = try machine.popDouble()
	let multiplyResult = doubleValue.multipliedSinglePrecisionFullWidth(multiplier)
	let multiplyHigh = try multiplyResult.high.asSinglePrecision()
	let multiplyLow = [multiplyResult.low.low, multiplyResult.low.high]
	let divisionResult = divisor.dividing(high: multiplyHigh, low: multiplyLow)
	guard divisionResult.low.count < 2 else { throw Forth.Error.integerOverflow }
	if !divisionResult.low.isEmpty
	{
		machine.dataStack.push(UInt.asCell(divisionResult.low.first!))
		machine.dataStackTop = divisionResult.high
	}
	else
	{
		machine.dataStack.push(divisionResult.high)
		machine.dataStackTop = divisionResult.high < 0 ? -1 : 0
	}
}

private func duDivMod(machine: ThreadedForth) throws
{
	let divisor: DoublePrecision<UInt> = try machine.popDouble()
	let dividend: DoublePrecision<UInt> = try machine.popDouble()
	guard divisor != DoublePrecision(0) else { throw ThreadedForth.Error.divisionByZero }
	let (q, r) = dividend.quotientAndRemainder(dividingBy: divisor)
	machine.pushDouble(r, needsFush: false)
	machine.pushDouble(q, needsFush: true)
}

private func duLess(machine: ThreadedForth) throws
{
	let st: DoublePrecision<UInt> = try machine.popDouble()
	machine.dataStackTop = try st > machine.popDouble() ? -1 : 0
}

private func duTimes(machine: ThreadedForth) throws
{
	let m1: DoublePrecision<UInt> = try machine.popDouble()
	let m2: DoublePrecision<UInt> = try machine.popDouble()

	let (result, overflow) = m2.multipliedReportingOverflow(by: m1)
	if overflow
	{
		throw ThreadedForth.Error.integerOverflow
	}
	machine.pushDouble(result, needsFush: false)
}

private func printDoubleRight(machine: ThreadedForth) throws
{
	let width = try machine.dataStackTop.pop() ?? machine.dataStack.throwingPop()
	let value: DoublePrecision<Int> = try machine.popDouble()
	let valueString = try String(value, radix: machine.base)
	machine.output.write(item: valueString, width: width)
}

private func udMinus(machine: ThreadedForth) throws
{
	let m1: DoublePrecision<UInt> = try machine.popDouble()
	let m2: DoublePrecision<UInt> = try machine.popDouble()
	let (result, _) = m2.subtractingReportingOverflow(m1)
	machine.pushDouble(result, needsFush: false)
}

private func udPlus(machine: ThreadedForth) throws
{
	let m1: DoublePrecision<UInt> = try machine.popDouble()
	let m2: DoublePrecision<UInt> = try machine.popDouble()
	let (result, _) = m2.addingReportingOverflow(m1)
	machine.pushDouble(result, needsFush: false)
}

// MARK: -
// MARK: Testing helpers
extension ThreadedForth: ForthTesting
{
	public func dumpWordDefinition<Stream>(word: Forth.Word, to output: inout Stream) where Stream : TextOutputStream 
	{
		if let metaData = dictionary[word]
		{
			print(debugCompilingWordList(xt: metaData.executionToken), to: &output)
		}
		else
		{
			print("Word \(word) not foundf in dictionary", to: &output)
		}
	}
	
	public func isDefined(word: Forth.Word) -> Bool
	{
		dictionary[word] != nil
	}


}

fileprivate extension CInt
{
	init(checkingBounds cell: Forth.Cell) throws
	{
		guard cell >= Forth.Cell(CInt.min) && cell <= Forth.Cell(CInt.max)
		else { throw Forth.Error.integerOverflow }
		self.init(truncatingIfNeeded: cell)
	}
}


/// A number that can be parsed as a string
///
/// This protocol is used to help us parse strings into numbers
protocol StringParsableNumber
{
	/// Fallback type
	///
	/// To be used as a fallback if the string cannot be parsed as the current
	/// type e.g. if it would overflow a signed type but not its unsigned
	/// equivalent.
	associatedtype OverflowFallback: StringParsableNumber

	init?<S: StringProtocol>(_ seq: S, radix: Int)

	var asInterpretationResult: ThreadedForth.InterpretationResult { get }

	static func parseNumber<S: StringProtocol>(string: S, radix: Int) throws -> ThreadedForth.InterpretationResult
}

struct StringParsableNumberError: StringParsableNumber
{
	typealias OverflowFallback = Self

	init?<S: StringProtocol>(_ seq: S, radix: Int)
	{
		nil
	}
	var asInterpretationResult: ThreadedForth.InterpretationResult
	{
		fatalError("Cannot get the interpretation result for an error")
	}

	static func parseNumber<S: StringProtocol>(string: S, radix: Int) throws -> ThreadedForth.InterpretationResult
	{
		throw Forth.Error.unrecognisedWord(String(string))
	}
}

extension Int: StringParsableNumber
{
	typealias OverflowFallback = UInt
	var asInterpretationResult: ThreadedForth.InterpretationResult { .literal(self) }
}

extension UInt: StringParsableNumber
{
	typealias OverflowFallback = StringParsableNumberError
	var asInterpretationResult: ThreadedForth.InterpretationResult { .literal(Int(bitPattern: self)) }
}

extension StringParsableNumber
{
	static func parseNumber<S: StringProtocol>(string: S, radix: Int) throws -> ThreadedForth.InterpretationResult
	{
		guard let number = Self.init(string, radix: radix)
		else
		{
			return try OverflowFallback.parseNumber(string: string, radix: radix)
		}
		return number.asInterpretationResult
	}

}
