//
//  ForthProtocol.swift
//  
//
//  Created by Jeremy Pereira on 21/07/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import SystemPackage
/// Protocol defining the API for an embedded Forth interpreter
public protocol ForthProtocol
{
	/// Trace execution of words
	///
	/// If this is set, each word that is executed and the data stack state is
	/// logged at debug level.
	var trace: Bool { get set }
	/// The number of words this forth machine has executed
	var wordsExecuted: Int { get }

	/// Standard input
	var stdin: AnyIterator<UInt8> { get set }


	/// Interpret a stream of charcters
	///
	/// This is the main entry point into the Forth interpreter. State is
	/// mintained between calls so that multiple separate streams can be fed in
	/// one after the other and appear as one contiguous input stream.
	/// - Parameters:
	///   - input: The stream of characters to interpret as an input source
	///   - sourceId: A source id that will identify trhe
	///   - output: Any output that is generated
	/// - Throws: If an error occurs during interpretation
	mutating func interpret<OUT: TextOutputStream>(input: inout Forth.InputSource,
												   output: inout OUT) throws


	/// Pops the top item off the datastack
	///
	/// - Returns: the top item of the stack
	/// - Throws: if the stack is empty
	mutating func dataStackPop() throws -> Int

	/// Get the definition of a given word as a string
	/// - Parameter word: Word for which we need the definition
	/// - Returns: The word definition or as much of it as we can get
	func dumpDefinition(for word: String) -> String?

	/// The index in the dataspace of the parser
	var parserSpaceIndex: Forth.Cell { get }
	/// The index in the dataspace of the pad buffer
	var padSpaceIndex: Forth.Cell { get }

}

public extension ForthProtocol
{

	/// Interpret a sequence of characters as Forth
	///
	/// A convenience function for interpreting strings and other sequences of
	/// characters without explicitly creating an ``MyForth/MyForth/InputSource``.
	///
	/// This function wraps ``ForthProtocol/interpret(input:output:)``
	/// - Parameters:
	///   - input: the input sequence
	///   - sourceId: A source id used internally by Forth to identify the source
	///   - inputName: a name to give the input for diagnostic purposes e.g. its
	///                file name
	///   - output: A text stream on which all output will be written
	/// - Throws: If an error occurs during interpretation
	mutating func interpret<IN: LocatableIterable, OUT: TextOutputStream>(input:  IN,
																		  sourceId: Forth.Cell = 0,
																 inputName: String = "String",
																 output: inout OUT) throws
	{
		var source = try Forth.InputSource(input, sourceId: sourceId, name: inputName)
		try interpret(input: &source, output: &output)
	}

}

internal extension ForthProtocol
{
	func stringFrom<E: CustomStringConvertible>(stack: Forth.Stack<E>,
												top: E? = nil,
												stringifyElement: (E) -> String = { $0.description }) -> String
	{
		let countString = "<\(stack.count + (top == nil ? 0 : 1))>"
		var ret = stack.elements.reduce(countString)
		{
			$0 + " " + stringifyElement($1)
		}
		if let st = top
		{
			ret += " " + stringifyElement(st)
		}
		return ret
	}


	/// Throws the error described by the top of the data stack
	///
	/// Some errors might require additional items on the data stack. If there
	/// are not enough items on the stack to fully determine the error,
	/// `stackUnderflow` will be thrown. The numbers, where possible are taken
	/// from the Forth standard
	///
	/// https://forth-standard.org/standard/exception
	///
	/// Exceptions that cannot easily be defined by the standard are in the
	/// system range: `-4096 ... -256`
	///
	/// - Throws: An error based on the top of the stack
	mutating func throwError() throws
	{
		let errorNumber = try dataStackPop()
		switch errorNumber
		{
		case Forth.ErrorNumber.integerOverflow.rawValue:
			throw Forth.Error.integerOverflow
		case Forth.ErrorNumber.openFile.rawValue:
			let errno = try CInt(dataStackPop())
			throw Errno(rawValue: errno)
		case Forth.ErrorNumber.divisionByZero.rawValue:
			throw Forth.Error.divisionByZero
		default:
			throw Forth.Error.undefinedError(errorNumber)
		}
	}
}

public protocol ForthTesting
{

	/// Test if a word is defined in the implementation's dictionary
	/// - Parameter word: The word for which to check
	func isDefined(word: Forth.Word) -> Bool

	/// Dump the word list definitions to an output stream
	/// - Parameter output: The stream to dump to
	func dumpWordLists<Stream: TextOutputStream>(to output: inout Stream)

	func dumpWordDefinition<Stream: TextOutputStream>(word: Forth.Word, to output: inout Stream)
}

extension ForthTesting
{

	/// Test if a string is in the word dicvtionary
	/// - Parameter word: the string to test
	/// - Returns: true if the string represents a word that is defined
	func isDefined<S: StringProtocol>(string: S) -> Bool
	{
		let theWord = Forth.Word(String(string))
		return isDefined(word: theWord)
	}
}


