//
//  main.swift
//  
//
//  Created by Jeremy Pereira on 08/06/2020.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import ArgumentParser
import Toolbox
import MyForth
import Foundation
import LineNoise

let log = Logger.getLogger("jforth.main")

log.level = .info

/// This is the command that will run a jforth program
///
/// If the switch `--repeatTest n` is used, where `n` is a number, the program
/// will run an internal test script designed to run a performance test.
///
/// If `--debug` is set, a large amount of debugf information will be printed
/// as it goes.
///
/// If run in normal mode, you get an interpreter that runs lines from `stdin`.
/// Note that, there seems to be no means to interrupt the process at the moment.
struct JForth: ParsableCommand
{
	@Argument(help: "A file containing a Forth program")
	var fileName: String?

	/// Print debug info
	@Flag(help: "jforth Outputs debug info as it runs")
	var debug: Bool = false

	@Option(
        help: "If set, will perform a test run. The parameter is the how many times to repeat the test run")
    var repeatTest: Int?

	private func timedRun(_ block: () throws -> ()) rethrows -> Double
	{
		let start = DispatchTime.now()
		try block()
		let end = DispatchTime.now()
		let nanoseconds = end.uptimeNanoseconds - start.uptimeNanoseconds
		return Double(nanoseconds) / 1e9
	}

	func runCoreTests(repeats: Int) throws
	{
		if debug
		{
			Logger.set(level: .debug, forName: "MyForth.Forth")
		}
		do
		{
			let url = URL(fileURLWithPath: "coretests.fr")
			// TODO: Use a proper file input source based on the file descriptor
			var fileData = try Forth.InputSource(try String(contentsOf: url, encoding: .utf8), sourceId: 1, name: url.path)

			var forth: ForthProtocol = ThreadedForth()
			var output: [String] = []
			let results = try (0 ..< repeats).map
			{
				(testNumber: Int) -> SIMD2<Double> in
				let startWordsExecuted = forth.wordsExecuted
				output = []
				let time = try timedRun
				{
					try forth.interpret(input: &fileData, output: &output)
				}
				let wordsExecuted = forth.wordsExecuted - startWordsExecuted
				let mips = (Double(wordsExecuted) / time) / 1_000_000
				log.info("Time of run #\(testNumber): \(time)s, words executed: \(wordsExecuted), mips: \(mips)")
				log.info("\(output)")
				return SIMD2(x: time, y: Double(wordsExecuted))
			}
			let totalResults = results.reduce(SIMD2<Double>.zero, +)
			let n = Double(repeats)
			let meanTime = totalResults.x / n
			log.info("Mean time = \(meanTime)")
			log.info("Mean mips = \(totalResults.y / totalResults.x  / 1_000_000)")
			let variance: Double = results.reduce(0)
			{
				let delta = $1.x - meanTime
				return $0 + delta * delta
			} / n
			let sd = sqrt(variance)
			let sdAsString = String(format: "%e", sd)
			log.info("Time standard deviation = \(sdAsString)")
		}
		catch
		{
			log.error("\(error)")
		}
	}

	func runFibo(repeats: Int) throws
	{
		if debug
		{
			Logger.set(level: .debug, forName: "MyForth.Forth")
		}
		do
		{
			let program2 =
			"""
			: fibo
				dup
				if ( non zero )
					dup 1 =
					if ( is 1 )
					else ( is greater than 1 )
						dup 1 - recurse swap
						2 - recurse
						+
					then
				then ;
			"""
			var forth: ForthProtocol = ThreadedForth()
			var output: [String] = []
			try forth.interpret(input: program2, sourceId: 0, inputName: "program2", output: &output)
			let results = try (0 ..< repeats).map
			{
				(testNumber: Int) -> SIMD2<Double> in
				let startWordsExecuted = forth.wordsExecuted
				output = []
				let time = try timedRun
				{
					try forth.interpret(input: "30 fibo .", sourceId: 0, inputName: "invocation", output: &output)
				}
				let wordsExecuted = forth.wordsExecuted - startWordsExecuted
				let mips = (Double(wordsExecuted) / time) / 1_000_000
				log.info("Time of run #\(testNumber): \(time)s, words executed: \(wordsExecuted), mips: \(mips)")
				log.info("\(output)")
				return SIMD2(x: time, y: Double(wordsExecuted))
			}
			let totalResults = results.reduce(SIMD2<Double>.zero, +)
			let n = Double(repeats)
			let meanTime = totalResults.x / n
			log.info("Mean time = \(meanTime)")
			log.info("Mean mips = \(totalResults.y / totalResults.x  / 1_000_000)")
			let variance: Double = results.reduce(0)
			{
				let delta = $1.x - meanTime
				return $0 + delta * delta
			} / n
			let sd = sqrt(variance)
			let sdAsString = String(format: "%e", sd)
			log.info("Time standard deviation = \(sdAsString)")
		}
		catch
		{
			log.error("\(error)")
		}
	}

	mutating func run() throws
	{
		var out = FileHandle.standardOutput
		var err = FileHandle.standardError
		do
		{
			if let repeats = repeatTest
			{
				try runFibo(repeats: repeats)
			}
			else if let fileName
			{
				try interpret(fileName: fileName, out: &out)
			}
			else
			{
				try repl(out: &out)
			}
		}
		catch
		{
			print("\n\(error)", to: &err)
		}
	}

	func interpret(fileName: String, out: inout FileHandle) throws
	{
		var forth: ForthProtocol = ThreadedForth()
		let inputString = try String(contentsOfFile: fileName)
		try forth.interpret(input: inputString, sourceId: 0, inputName: fileName, output: &out)
	}

	mutating func repl(out: inout FileHandle) throws
	{
		var forth: ForthProtocol = ThreadedForth()
		let editor = LineNoise()
		var carryOn = true

		while carryOn
		{
			do
			{
				let line = try editor.getLine(prompt: "")
				if line.count > 0
				{
					editor.addHistory(line)
				}
				print(" ", terminator: "", to: &out)
				try forth.interpret(input: line, sourceId: 0, output: &out)
				print("ok", to: &out)
			}
			catch LineNoise.Error.EOF
			{
				print("\nBye", to: &out)
				carryOn = false
			}
		}

	}
}

JForth.main()

extension Array: @retroactive TextOutputStream where Element == String
{
	public mutating func write(_ string: String)
	{
		self.append(string)
	}
}
