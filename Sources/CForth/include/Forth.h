//
//  Forth.h
//  
//
//  Created by Jeremy Pereira on 22/07/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#ifndef Forth_h
#define Forth_h

#include <stdint.h>
#include <stdbool.h>

// MARK: Common stuff
/// Alias for Swift Int type
typedef long Int;

/// Errors that can be raised by operations
enum StatusCode
{
	SC_OK = 0,
	SC_STACK_UNDERFLOW,
	SC_STACK_OUT_OF_BOUNDS,
	SC_STACK_NO_FRAME
};

/// Result from operations that return an Int but may error.
///
/// result is only valid if status == SC_OK
struct IntResult
{
	Int result;
	enum StatusCode status;
};

// MARK: Integer stack

/// A standard stack
///
/// Elements on a stack are Int64_t values which are compatible with Swift `Int`
///
/// The stack has a frame pointer to enable us to define stack frames. This
/// always points at one element below the bottom of the stack or the previous
/// frame pointer if we have created a new frame. Items on the stack in the
/// current frame always have a positive > 0 offset in the frame.
struct Stack;

/// Create  new stack
/// @param name A tring name for diagnostic purposes
/// @return A stack
extern struct Stack * _Nonnull stackNamed(const char* _Nonnull name);

/// Free  previously allocated stack
///
/// There's no check that the pointer is a valid stack.
/// @param aStack The stack to free
extern void stackFree(struct Stack* _Nonnull aStack);

/// Push an item onto the stack
/// @param aStack The stack on which to push the item
/// @param element The element to push
extern void stackPush(struct Stack* _Nonnull aStack, Int element);

/// Count of the items on the stack
/// @param aStack The stack to count
/// @return the count of items on the stack
extern Int stackCount(struct Stack* _Nonnull aStack);

/// Pop an item off the stack
///  If the stack is empty, returns an SC_UNDERFLOW status
/// @param aStack The stack to pop
/// @return a StackResult containing SC_OK and the popped item or a status of SC_STACK_UNDERFLOW
extern struct IntResult stackPop(struct Stack* _Nonnull aStack);
/// Get the  top  item on the stack without popping it
///  If the stack is empty, returns an SC_UNDERFLOW status
/// @param aStack The stack to get the top of
/// @return a StackResult containing SC_OK and the top item or a status of SC_STACK_UNDERFLOW
extern struct IntResult stackTop(struct Stack* _Nonnull aStack);

///  Is the stack empty?
/// @param aStack  Stack to test
/// @return true if there is nothing on the stack, false otherwise
extern bool stackIsEmpty(struct Stack* _Nonnull aStack);

/// Get the frame offset of the stack top
/// @param aStack The stack to use
/// @return the offset from the frame pointer of the top item on the stack
extern Int stackFrameOffsetOfTop(struct Stack* _Nonnull aStack);

/// Create a new stack frame
/// @param aStack The stack in which to create the frame
extern void stackCreateFrame(struct Stack* _Nonnull aStack);

/// Peeek an element relative to the current frame pointer
/// @param aStack The stack on which to peek the element
/// @param frameOffset The offset from the frame pointer of the element
/// @return a StackResult with a status of SC_OK and the value of the element or
/// SC_STACK_OUT_Of_BOUNDS. If the status is not SC_OK, the result is undefined
extern struct IntResult stackPeek(struct Stack* _Nonnull aStack, Int frameOffset);
/// Pop the current stack frame
/// @param aStack The stack on which to peek the element
/// @return SC_OK if successful or SC_STACK_NO_FRAME if the frame is at the
/// bottom of the stack.
extern enum StatusCode stackPopFrame(struct Stack* _Nonnull aStack);

/// Find the element at a given index
/// @param aStack Stack of element to find
/// @param elementOffset offset of element from the bottom of the stack
/// @return a StackResult with a status of SC_OK and the value of the element or
/// SC_STACK_OUT_Of_BOUNDS. If the status is not SC_OK, the result is undefined
extern struct IntResult stackElement(struct Stack* _Nonnull aStack, Int elementOffset);

// MARK: WordList

/// Contains a list of words for a word definition
struct WordList;

/// Create  new WordList
/// @param name A string name for diagnostic purposes
/// @return A WordList
extern struct WordList * _Nonnull wordListNamed(const char* _Nonnull name);

/// Free  previously allocated WordList
///
/// There's no check that the pointer is a valid WordList.
/// @param aWordList The WordList to free
extern void wordListFree(struct Stack* _Nonnull aWordList);

#endif /* Forth_h */
