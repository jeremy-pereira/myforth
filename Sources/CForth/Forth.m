//
//  Forth.m
//  
//
//  Created by Jeremy Pereira on 22/07/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#include "Forth.h"
#include <stdlib.h>
#include <sys/types.h>

#define STACK_CHUNK_SIZE (65536/sizeof(Int))

struct Stack
{
	size_t size;
	size_t count;
	ssize_t framePointer;
	Int* elements;
};

struct Stack * _Nonnull
stackNamed(const char* _Nonnull name)
{
	// TODO: Store the name somewhere
	struct Stack* ret = calloc(1, sizeof *ret);
	ret->elements = calloc(STACK_CHUNK_SIZE, sizeof *(ret->elements));
	ret->count = 0;
	ret->size = STACK_CHUNK_SIZE;
	ret->framePointer = -1;
	return ret;
}


void stackFree(struct Stack* _Nonnull aStack)
{
	free(aStack->elements);
	free(aStack);
}

void stackPush(struct Stack* _Nonnull aStack, Int element)
{
	if (aStack->count == aStack->size)
	{
		// TODO: Check for overflow
		aStack->size += STACK_CHUNK_SIZE;
		aStack->elements = realloc(aStack->elements, aStack->size * sizeof *(aStack->elements));
	}
	aStack->elements[aStack->count++] = element;
}

Int stackCount(struct Stack* _Nonnull aStack)
{
	// TODO: Make sure no overflow
	return (Int) aStack->count;
}

struct IntResult stackPop(struct Stack* _Nonnull aStack)
{
	struct IntResult ret = { 0, SC_OK };
	if (aStack->count == 0)
	{
		ret.status = SC_STACK_UNDERFLOW;
	}
	else
	{
		ret.result = aStack->elements[--aStack->count];
	}
	return ret;
}

struct IntResult stackTop(struct Stack* _Nonnull aStack)
{
	struct IntResult ret = { 0, SC_OK };
	if (aStack->count == 0)
	{
		ret.status = SC_STACK_UNDERFLOW;
	}
	else
	{
		ret.result = aStack->elements[aStack->count - 1];
	}
	return ret;
}

bool stackIsEmpty(struct Stack* _Nonnull aStack)
{
	 return aStack->count == 0;
}

Int stackFrameOffsetOfTop(struct Stack* _Nonnull aStack)
{
	return aStack->count - aStack->framePointer - 1;
}

void stackCreateFrame(struct Stack* _Nonnull aStack)
{
	ssize_t newFramePointer = aStack->count;
	stackPush(aStack, aStack->framePointer);
	aStack->framePointer = newFramePointer;
}

struct IntResult stackPeek(struct Stack* _Nonnull aStack, Int frameOffset)
{
	struct IntResult ret = { 0, SC_OK };
	ssize_t elementIndex = aStack->framePointer + frameOffset;
	if (elementIndex < 0 || elementIndex >= aStack->count)
	{
		ret.status = SC_STACK_OUT_OF_BOUNDS;
	}
	else
	{
		ret.result = aStack->elements[elementIndex];
	}
	return ret;
}

enum StatusCode stackPopFrame(struct Stack* _Nonnull aStack)
{
	enum StatusCode ret = SC_OK;

	if (aStack->framePointer == -1)
	{
		ret = SC_STACK_NO_FRAME;
	}
	else
	{
		aStack->count -= stackFrameOffsetOfTop(aStack);
		struct IntResult popStatus = stackPop(aStack);
		if (popStatus.status != SC_OK)
		{
			ret = popStatus.status;
		}
		else
		{
			aStack->framePointer = popStatus.result;
		}
	}
	return ret;
}

struct IntResult
stackElement(struct Stack* _Nonnull aStack, Int elementOffset)
{
	struct IntResult ret = { 0, SC_OK };

	if (elementOffset < 0 || elementOffset >= aStack->count)
	{
		ret.status = SC_STACK_OUT_OF_BOUNDS;
	}
	else
	{
		ret.result = aStack->elements[elementOffset];
	}
	return ret;
}
